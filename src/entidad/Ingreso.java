package entidad;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the ingreso database table.
 * 
 */
@Entity
public class Ingreso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id	
	private String code;

	private Boolean activo;

	private Timestamp fecha;

	private String tipoingreso;

	private BigDecimal total;

	//bi-directional many-to-one association to Compra
	@OneToMany(mappedBy="ingreso")
	private List<Compra> compras;

	//bi-directional many-to-one association to Detalleingreso
	@OneToMany(mappedBy="ingreso")
	private List<Detalleingreso> detalleingresos;

	//bi-directional many-to-one association to Almacen
	@ManyToOne(fetch=FetchType.EAGER)
	private Almacen almacen;

	//bi-directional many-to-one association to Usuario
	@ManyToOne(fetch=FetchType.EAGER)
	private Usuario usuario;

	//bi-directional many-to-one association to Traspaso
	@OneToMany(mappedBy="ingreso")
	private List<Traspaso> traspasos;

    public Ingreso() {
    }

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Timestamp getFecha() {
		return this.fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public String getTipoingreso() {
		return this.tipoingreso;
	}

	public void setTipoingreso(String tipoingreso) {
		this.tipoingreso = tipoingreso;
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public List<Compra> getCompras() {
		return this.compras;
	}

	public void setCompras(List<Compra> compras) {
		this.compras = compras;
	}
	
	public List<Detalleingreso> getDetalleingresos() {
		return this.detalleingresos;
	}

	public void setDetalleingresos(List<Detalleingreso> detalleingresos) {
		this.detalleingresos = detalleingresos;
	}
	
	public Almacen getAlmacen() {
		return this.almacen;
	}

	public void setAlmacen(Almacen almacen) {
		this.almacen = almacen;
	}
	
	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public List<Traspaso> getTraspasos() {
		return this.traspasos;
	}

	public void setTraspasos(List<Traspaso> traspasos) {
		this.traspasos = traspasos;
	}
	
}