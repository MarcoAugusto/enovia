package entidad;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the traspaso database table.
 * 
 */
@Entity
public class Traspaso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id	
	private String code;

	private Boolean activo;

	private String comentario;

	private Timestamp fecha;

	private String motivo;

	//bi-directional many-to-one association to Recepcion
	@OneToMany(mappedBy="traspaso")
	private List<Recepcion> recepcions;

	//bi-directional many-to-one association to Egreso
	@ManyToOne(fetch=FetchType.EAGER)
	private Egreso egreso;

	//bi-directional many-to-one association to Ingreso
	@ManyToOne(fetch=FetchType.EAGER)
	private Ingreso ingreso;

	//bi-directional many-to-one association to Usuario
	@ManyToOne(fetch=FetchType.EAGER)
	private Usuario usuario;

    public Traspaso() {
    }

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getComentario() {
		return this.comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public Timestamp getFecha() {
		return this.fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public String getMotivo() {
		return this.motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public List<Recepcion> getRecepcions() {
		return this.recepcions;
	}

	public void setRecepcions(List<Recepcion> recepcions) {
		this.recepcions = recepcions;
	}
	
	public Egreso getEgreso() {
		return this.egreso;
	}

	public void setEgreso(Egreso egreso) {
		this.egreso = egreso;
	}
	
	public Ingreso getIngreso() {
		return this.ingreso;
	}

	public void setIngreso(Ingreso ingreso) {
		this.ingreso = ingreso;
	}
	
	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}