package entidad;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the datosboda database table.
 * 
 */
@Entity
public class Datosboda implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long code;

	private Boolean activo;

	private String direccion;

	private Timestamp fecha;

	private Timestamp fechadevolucion;

	private Timestamp fechavestir;

	private BigDecimal garantiajustan;

	private Boolean justan;

	private BigDecimal multa;

	private Integer nrojustan;

	private String salon;

	//bi-directional many-to-one association to Agenda
	@OneToMany(mappedBy="datosboda")
	private List<Agenda> agendas;

	//bi-directional many-to-one association to Venta
	@ManyToOne(fetch=FetchType.LAZY)
	private Venta venta;

    public Datosboda() {
    }

	public Long getCode() {
		return this.code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Timestamp getFecha() {
		return this.fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public Timestamp getFechadevolucion() {
		return this.fechadevolucion;
	}

	public void setFechadevolucion(Timestamp fechadevolucion) {
		this.fechadevolucion = fechadevolucion;
	}

	public Timestamp getFechavestir() {
		return this.fechavestir;
	}

	public void setFechavestir(Timestamp fechavestir) {
		this.fechavestir = fechavestir;
	}

	public BigDecimal getGarantiajustan() {
		return this.garantiajustan;
	}

	public void setGarantiajustan(BigDecimal garantiajustan) {
		this.garantiajustan = garantiajustan;
	}

	public Boolean getJustan() {
		return this.justan;
	}

	public void setJustan(Boolean justan) {
		this.justan = justan;
	}

	public BigDecimal getMulta() {
		return this.multa;
	}

	public void setMulta(BigDecimal multa) {
		this.multa = multa;
	}

	public Integer getNrojustan() {
		return this.nrojustan;
	}

	public void setNrojustan(Integer nrojustan) {
		this.nrojustan = nrojustan;
	}

	public String getSalon() {
		return this.salon;
	}

	public void setSalon(String salon) {
		this.salon = salon;
	}

	public List<Agenda> getAgendas() {
		return this.agendas;
	}

	public void setAgendas(List<Agenda> agendas) {
		this.agendas = agendas;
	}
	
	public Venta getVenta() {
		return this.venta;
	}

	public void setVenta(Venta venta) {
		this.venta = venta;
	}
	
}