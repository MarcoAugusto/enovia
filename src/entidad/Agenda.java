package entidad;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the agenda database table.
 * 
 */
@Entity
public class Agenda implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String code;

	private Boolean activo;

	private Boolean cita;

	private String detalle;

	private Timestamp fecha;

	//bi-directional many-to-one association to Cliente
	@ManyToOne(fetch=FetchType.LAZY)
	private Cliente cliente;

	//bi-directional many-to-one association to Controlmedida
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="controlmedidas_code")
	private Controlmedida controlmedida;

	//bi-directional many-to-one association to Datosboda
	@ManyToOne(fetch=FetchType.LAZY)
	private Datosboda datosboda;

	//bi-directional many-to-one association to Usuario
	@ManyToOne(fetch=FetchType.LAZY)
	private Usuario usuario;

    public Agenda() {
    }

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Boolean getCita() {
		return this.cita;
	}

	public void setCita(Boolean cita) {
		this.cita = cita;
	}

	public String getDetalle() {
		return this.detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public Timestamp getFecha() {
		return this.fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public Cliente getCliente() {
		return this.cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Controlmedida getControlmedida() {
		return this.controlmedida;
	}

	public void setControlmedida(Controlmedida controlmedida) {
		this.controlmedida = controlmedida;
	}
	
	public Datosboda getDatosboda() {
		return this.datosboda;
	}

	public void setDatosboda(Datosboda datosboda) {
		this.datosboda = datosboda;
	}
	
	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}