package entidad;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the compra database table.
 * 
 */
@Entity
public class Compra implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String code;

	private Boolean activo;

	private String comentario;

	private Timestamp fecha;

	private BigDecimal total;

	//bi-directional many-to-one association to Ingreso
	@ManyToOne(fetch=FetchType.EAGER)
	private Ingreso ingreso;

	//bi-directional many-to-one association to Pedido
	@ManyToOne(fetch=FetchType.EAGER)
	private Pedido pedido;

	//bi-directional many-to-one association to Proveedor
	@ManyToOne(fetch=FetchType.EAGER)
	private Proveedor proveedor;

	//bi-directional many-to-one association to Usuario
	@ManyToOne(fetch=FetchType.EAGER)
	private Usuario usuario;

	//bi-directional many-to-one association to Pagocompra
	@OneToMany(mappedBy="compra")
	private List<Pagocompra> pagocompras;

	//bi-directional many-to-one association to Transito
	@OneToMany(mappedBy="compra")
	private List<Transito> transitos;

    public Compra() {
    }

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getComentario() {
		return this.comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public Timestamp getFecha() {
		return this.fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Ingreso getIngreso() {
		return this.ingreso;
	}

	public void setIngreso(Ingreso ingreso) {
		this.ingreso = ingreso;
	}
	
	public Pedido getPedido() {
		return this.pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}
	
	public Proveedor getProveedor() {
		return this.proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}
	
	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public List<Pagocompra> getPagocompras() {
		return this.pagocompras;
	}

	public void setPagocompras(List<Pagocompra> pagocompras) {
		this.pagocompras = pagocompras;
	}
	
	public List<Transito> getTransitos() {
		return this.transitos;
	}

	public void setTransitos(List<Transito> transitos) {
		this.transitos = transitos;
	}
	
}