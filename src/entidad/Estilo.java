package entidad;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the estilo database table.
 * 
 */
@Entity
public class Estilo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer code;

	private Boolean activo;

	private String nombre;

	//bi-directional many-to-one association to Baseproducto
	@OneToMany(mappedBy="estilo")
	private List<Baseproducto> baseproductos;

    public Estilo() {
    }

	public Integer getCode() {
		return this.code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Baseproducto> getBaseproductos() {
		return this.baseproductos;
	}

	public void setBaseproductos(List<Baseproducto> baseproductos) {
		this.baseproductos = baseproductos;
	}
	
}