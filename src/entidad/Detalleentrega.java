package entidad;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the detalleentrega database table.
 * 
 */
@Entity
public class Detalleentrega implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long code;

	private Boolean activo;

	private String comentario;

	private String entregadoa;

	private Timestamp fecha;

	//bi-directional many-to-one association to Detalleegreso
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="detegreso_code")
	private Detalleegreso detalleegreso;

	//bi-directional many-to-one association to Usuario
	@ManyToOne(fetch=FetchType.LAZY)
	private Usuario usuario;

    public Detalleentrega() {
    }

	public Long getCode() {
		return this.code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getComentario() {
		return this.comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public String getEntregadoa() {
		return this.entregadoa;
	}

	public void setEntregadoa(String entregadoa) {
		this.entregadoa = entregadoa;
	}

	public Timestamp getFecha() {
		return this.fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public Detalleegreso getDetalleegreso() {
		return this.detalleegreso;
	}

	public void setDetalleegreso(Detalleegreso detalleegreso) {
		this.detalleegreso = detalleegreso;
	}
	
	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}