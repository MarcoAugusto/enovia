package entidad;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the detallerecepcion database table.
 * 
 */
@Entity
public class Detallerecepcion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long code;

	//bi-directional many-to-one association to Detalleingreso
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="detingreso_code")
	private Detalleingreso detalleingreso;

	//bi-directional many-to-one association to Detalletransito
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="dettransito_code")
	private Detalletransito detalletransito;

	//bi-directional many-to-one association to Recepcion
	@ManyToOne(fetch=FetchType.EAGER)
	private Recepcion recepcion;

    public Detallerecepcion() {
    }

	public Long getCode() {
		return this.code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public Detalleingreso getDetalleingreso() {
		return this.detalleingreso;
	}

	public void setDetalleingreso(Detalleingreso detalleingreso) {
		this.detalleingreso = detalleingreso;
	}
	
	public Detalletransito getDetalletransito() {
		return this.detalletransito;
	}

	public void setDetalletransito(Detalletransito detalletransito) {
		this.detalletransito = detalletransito;
	}
	
	public Recepcion getRecepcion() {
		return this.recepcion;
	}

	public void setRecepcion(Recepcion recepcion) {
		this.recepcion = recepcion;
	}
	
}