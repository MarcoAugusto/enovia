package entidad;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the detalletransito database table.
 * 
 */
@Entity
public class Detalletransito implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long code;

	//bi-directional many-to-one association to Detallerecepcion
	@OneToMany(mappedBy="detalletransito")
	private List<Detallerecepcion> detallerecepcions;

	//bi-directional many-to-one association to Detalleingreso
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="detingreso_code")
	private Detalleingreso detalleingreso;

	//bi-directional many-to-one association to Transito
	@ManyToOne(fetch=FetchType.EAGER)
	private Transito transito;

    public Detalletransito() {
    }

	public Long getCode() {
		return this.code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public List<Detallerecepcion> getDetallerecepcions() {
		return this.detallerecepcions;
	}

	public void setDetallerecepcions(List<Detallerecepcion> detallerecepcions) {
		this.detallerecepcions = detallerecepcions;
	}
	
	public Detalleingreso getDetalleingreso() {
		return this.detalleingreso;
	}

	public void setDetalleingreso(Detalleingreso detalleingreso) {
		this.detalleingreso = detalleingreso;
	}
	
	public Transito getTransito() {
		return this.transito;
	}

	public void setTransito(Transito transito) {
		this.transito = transito;
	}
	
}