package entidad;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the inventario database table.
 * 
 */
@Entity
public class Inventario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long code;

	private String estado;

	//bi-directional many-to-one association to Baseproducto
	@ManyToOne(fetch=FetchType.LAZY)
	private Baseproducto baseproducto;

    public Inventario() {
    }

	public Long getCode() {
		return this.code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Baseproducto getBaseproducto() {
		return this.baseproducto;
	}

	public void setBaseproducto(Baseproducto baseproducto) {
		this.baseproducto = baseproducto;
	}
	
}