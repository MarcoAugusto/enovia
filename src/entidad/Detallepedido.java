package entidad;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the detallepedido database table.
 * 
 */
@Entity
public class Detallepedido implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long code;

	private Boolean activo;

	private Integer cantidad;

	private BigDecimal costo;

	private String estado;

	//bi-directional many-to-one association to Detalleingreso
	@OneToMany(mappedBy="detallepedido")
	private List<Detalleingreso> detalleingresos;

	//bi-directional many-to-one association to Pedido
	@ManyToOne(fetch=FetchType.EAGER)
	private Pedido pedido;

	//bi-directional many-to-one association to Producto
	@ManyToOne(fetch=FetchType.EAGER)
	private Producto producto;

    public Detallepedido() {
    }

	public Long getCode() {
		return this.code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Integer getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public BigDecimal getCosto() {
		return this.costo;
	}

	public void setCosto(BigDecimal costo) {
		this.costo = costo;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public List<Detalleingreso> getDetalleingresos() {
		return this.detalleingresos;
	}

	public void setDetalleingresos(List<Detalleingreso> detalleingresos) {
		this.detalleingresos = detalleingresos;
	}
	
	public Pedido getPedido() {
		return this.pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}
	
	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	
}