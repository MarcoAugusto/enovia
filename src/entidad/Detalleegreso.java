package entidad;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the detalleegreso database table.
 * 
 */
@Entity
public class Detalleegreso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long code;

	private Boolean activo;

	private Integer cantidad;

	private BigDecimal descuento;

	private BigDecimal precio;

	//bi-directional many-to-one association to Detallereserva
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="detreserva_code")
	private Detallereserva detallereserva;

	//bi-directional many-to-one association to Egreso
	@ManyToOne(fetch=FetchType.EAGER)
	private Egreso egreso;

	//bi-directional many-to-one association to Producto
	@ManyToOne(fetch=FetchType.EAGER)
	private Producto producto;

	//bi-directional many-to-one association to Detalleentrega
	@OneToMany(mappedBy="detalleegreso")
	private List<Detalleentrega> detalleentregas;

	//bi-directional many-to-many association to Devolucion
	@ManyToMany(mappedBy="detalleegresos")
	private List<Devolucion> devolucions;

    public Detalleegreso() {
    }

	public Long getCode() {
		return this.code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Integer getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public BigDecimal getDescuento() {
		return this.descuento;
	}

	public void setDescuento(BigDecimal descuento) {
		this.descuento = descuento;
	}

	public BigDecimal getPrecio() {
		return this.precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public Detallereserva getDetallereserva() {
		return this.detallereserva;
	}

	public void setDetallereserva(Detallereserva detallereserva) {
		this.detallereserva = detallereserva;
	}
	
	public Egreso getEgreso() {
		return this.egreso;
	}

	public void setEgreso(Egreso egreso) {
		this.egreso = egreso;
	}
	
	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	
	public List<Detalleentrega> getDetalleentregas() {
		return this.detalleentregas;
	}

	public void setDetalleentregas(List<Detalleentrega> detalleentregas) {
		this.detalleentregas = detalleentregas;
	}
	
	public List<Devolucion> getDevolucions() {
		return this.devolucions;
	}

	public void setDevolucions(List<Devolucion> devolucions) {
		this.devolucions = devolucions;
	}
	
}