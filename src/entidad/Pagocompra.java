package entidad;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the pagocompra database table.
 * 
 */
@Entity
public class Pagocompra implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String code;

	private Boolean activo;

	//bi-directional many-to-one association to Asientocontable
	@OneToMany(mappedBy="pagocompra")
	private List<Asientocontable> asientocontables;

	//bi-directional many-to-one association to Compra
	@ManyToOne(fetch=FetchType.LAZY)
	private Compra compra;

	//bi-directional many-to-one association to Pago
	@ManyToOne(fetch=FetchType.LAZY)
	private Pago pago;

	//bi-directional many-to-one association to Usuario
	@ManyToOne(fetch=FetchType.LAZY)
	private Usuario usuario;

    public Pagocompra() {
    }

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public List<Asientocontable> getAsientocontables() {
		return this.asientocontables;
	}

	public void setAsientocontables(List<Asientocontable> asientocontables) {
		this.asientocontables = asientocontables;
	}
	
	public Compra getCompra() {
		return this.compra;
	}

	public void setCompra(Compra compra) {
		this.compra = compra;
	}
	
	public Pago getPago() {
		return this.pago;
	}

	public void setPago(Pago pago) {
		this.pago = pago;
	}
	
	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}