package entidad;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the recepcion database table.
 * 
 */
@Entity
public class Recepcion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id	
	private String code;

	private Boolean activo;

	private String comentario;

	private Timestamp fecha;

	private BigDecimal total;

	//bi-directional many-to-one association to Detallerecepcion
	@OneToMany(mappedBy="recepcion")
	private List<Detallerecepcion> detallerecepcions;

	//bi-directional many-to-one association to Transito
	@ManyToOne(fetch=FetchType.EAGER)
	private Transito transito;

	//bi-directional many-to-one association to Traspaso
	@ManyToOne(fetch=FetchType.EAGER)
	private Traspaso traspaso;

	//bi-directional many-to-one association to Usuario
	@ManyToOne(fetch=FetchType.EAGER)
	private Usuario usuario;

    public Recepcion() {
    }

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getComentario() {
		return this.comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public Timestamp getFecha() {
		return this.fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public List<Detallerecepcion> getDetallerecepcions() {
		return this.detallerecepcions;
	}

	public void setDetallerecepcions(List<Detallerecepcion> detallerecepcions) {
		this.detallerecepcions = detallerecepcions;
	}
	
	public Transito getTransito() {
		return this.transito;
	}

	public void setTransito(Transito transito) {
		this.transito = transito;
	}
	
	public Traspaso getTraspaso() {
		return this.traspaso;
	}

	public void setTraspaso(Traspaso traspaso) {
		this.traspaso = traspaso;
	}
	
	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}