package entidad;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the transito database table.
 * 
 */
@Entity
public class Transito implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id	
	private String code;

	private Boolean activo;

	private String comentario;

	private Timestamp fecha;

	private BigDecimal total;

	@Column(name="transporte_empresa")
	private String transporteEmpresa;

	@Column(name="transporte_fecha")
	private String transporteFecha;

	@Column(name="transporte_fechallegada")
	private String transporteFechallegada;

	@Column(name="transporte_tipo")
	private String transporteTipo;

	//bi-directional many-to-one association to Detalletransito
	@OneToMany(mappedBy="transito")
	private List<Detalletransito> detalletransitos;

	//bi-directional many-to-one association to Recepcion
	@OneToMany(mappedBy="transito")
	private List<Recepcion> recepcions;

	//bi-directional many-to-one association to Compra
	@ManyToOne(fetch=FetchType.EAGER)
	private Compra compra;

	//bi-directional many-to-one association to Usuario
	@ManyToOne(fetch=FetchType.EAGER)
	private Usuario usuario;

    public Transito() {
    }

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getComentario() {
		return this.comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public Timestamp getFecha() {
		return this.fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public String getTransporteEmpresa() {
		return this.transporteEmpresa;
	}

	public void setTransporteEmpresa(String transporteEmpresa) {
		this.transporteEmpresa = transporteEmpresa;
	}

	public String getTransporteFecha() {
		return this.transporteFecha;
	}

	public void setTransporteFecha(String transporteFecha) {
		this.transporteFecha = transporteFecha;
	}

	public String getTransporteFechallegada() {
		return this.transporteFechallegada;
	}

	public void setTransporteFechallegada(String transporteFechallegada) {
		this.transporteFechallegada = transporteFechallegada;
	}

	public String getTransporteTipo() {
		return this.transporteTipo;
	}

	public void setTransporteTipo(String transporteTipo) {
		this.transporteTipo = transporteTipo;
	}

	public List<Detalletransito> getDetalletransitos() {
		return this.detalletransitos;
	}

	public void setDetalletransitos(List<Detalletransito> detalletransitos) {
		this.detalletransitos = detalletransitos;
	}
	
	public List<Recepcion> getRecepcions() {
		return this.recepcions;
	}

	public void setRecepcions(List<Recepcion> recepcions) {
		this.recepcions = recepcions;
	}
	
	public Compra getCompra() {
		return this.compra;
	}

	public void setCompra(Compra compra) {
		this.compra = compra;
	}
	
	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}