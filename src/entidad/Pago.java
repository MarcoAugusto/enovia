package entidad;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the pago database table.
 * 
 */
@Entity
public class Pago implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer code;

	private String banco;

	private String descripcion;

	private String factura;

	private Timestamp fecha;

	private String idtarjeta;

	@Column(name="monto_bs")
	private BigDecimal montoBs;

	@Column(name="monto_us")
	private BigDecimal montoUs;

	private String tipo;

	private String tipotarjeta;

	private String transaccion;

	//bi-directional many-to-one association to Asientocontable
	@OneToMany(mappedBy="pago")
	private List<Asientocontable> asientocontables;

	//bi-directional many-to-one association to Pagocompra
	@OneToMany(mappedBy="pago")
	private List<Pagocompra> pagocompras;

	//bi-directional many-to-one association to Pagoventa
	@OneToMany(mappedBy="pago")
	private List<Pagoventa> pagoventas;

    public Pago() {
    }

	public Integer getCode() {
		return this.code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getBanco() {
		return this.banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getFactura() {
		return this.factura;
	}

	public void setFactura(String factura) {
		this.factura = factura;
	}

	public Timestamp getFecha() {
		return this.fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public String getIdtarjeta() {
		return this.idtarjeta;
	}

	public void setIdtarjeta(String idtarjeta) {
		this.idtarjeta = idtarjeta;
	}

	public BigDecimal getMontoBs() {
		return this.montoBs;
	}

	public void setMontoBs(BigDecimal montoBs) {
		this.montoBs = montoBs;
	}

	public BigDecimal getMontoUs() {
		return this.montoUs;
	}

	public void setMontoUs(BigDecimal montoUs) {
		this.montoUs = montoUs;
	}

	public String getTipo() {
		return this.tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getTipotarjeta() {
		return this.tipotarjeta;
	}

	public void setTipotarjeta(String tipotarjeta) {
		this.tipotarjeta = tipotarjeta;
	}

	public String getTransaccion() {
		return this.transaccion;
	}

	public void setTransaccion(String transaccion) {
		this.transaccion = transaccion;
	}

	public List<Asientocontable> getAsientocontables() {
		return this.asientocontables;
	}

	public void setAsientocontables(List<Asientocontable> asientocontables) {
		this.asientocontables = asientocontables;
	}
	
	public List<Pagocompra> getPagocompras() {
		return this.pagocompras;
	}

	public void setPagocompras(List<Pagocompra> pagocompras) {
		this.pagocompras = pagocompras;
	}
	
	public List<Pagoventa> getPagoventas() {
		return this.pagoventas;
	}

	public void setPagoventas(List<Pagoventa> pagoventas) {
		this.pagoventas = pagoventas;
	}
	
}