package entidad;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the parametro database table.
 * 
 */
@Entity
public class Parametro implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer code;

	private String descripcion;

	private String llave;

	private String tipodato;

	private String tipovalor;

	private String valorcadena;

	private BigDecimal valornumerico;

    public Parametro() {
    }

	public Integer getCode() {
		return this.code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getLlave() {
		return this.llave;
	}

	public void setLlave(String llave) {
		this.llave = llave;
	}

	public String getTipodato() {
		return this.tipodato;
	}

	public void setTipodato(String tipodato) {
		this.tipodato = tipodato;
	}

	public String getTipovalor() {
		return this.tipovalor;
	}

	public void setTipovalor(String tipovalor) {
		this.tipovalor = tipovalor;
	}

	public String getValorcadena() {
		return this.valorcadena;
	}

	public void setValorcadena(String valorcadena) {
		this.valorcadena = valorcadena;
	}

	public BigDecimal getValornumerico() {
		return this.valornumerico;
	}

	public void setValornumerico(BigDecimal valornumerico) {
		this.valornumerico = valornumerico;
	}

}