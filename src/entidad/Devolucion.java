package entidad;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the devolucion database table.
 * 
 */
@Entity
public class Devolucion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String code;

	private Boolean activo;

	private Timestamp fecha;

	private String motivo;

	//bi-directional many-to-many association to Detalleegreso
    @ManyToMany
	@JoinTable(
		name="detalledevolucion"
		, joinColumns={
			@JoinColumn(name="devolucion_code")
			}
		, inverseJoinColumns={
			@JoinColumn(name="detegreso_code")
			}
		)
	private List<Detalleegreso> detalleegresos;

	//bi-directional many-to-one association to Egreso
	@ManyToOne(fetch=FetchType.LAZY)
	private Egreso egreso;

	//bi-directional many-to-one association to Usuario
	@ManyToOne(fetch=FetchType.LAZY)
	private Usuario usuario;

    public Devolucion() {
    }

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Timestamp getFecha() {
		return this.fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public String getMotivo() {
		return this.motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public List<Detalleegreso> getDetalleegresos() {
		return this.detalleegresos;
	}

	public void setDetalleegresos(List<Detalleegreso> detalleegresos) {
		this.detalleegresos = detalleegresos;
	}
	
	public Egreso getEgreso() {
		return this.egreso;
	}

	public void setEgreso(Egreso egreso) {
		this.egreso = egreso;
	}
	
	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}