package entidad;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the baseproducto database table.
 * 
 */
@Entity
public class Baseproducto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String code;

	private Boolean activo;

	private Integer cantidad;

	private String codigo;

	private Boolean esporcentaje;

	private Boolean espromo;

	private byte[] foto;

	@Column(name="foto_size")
	private Integer fotoSize;

	private BigDecimal precio;

	@Column(name="precio_promo")
	private BigDecimal precioPromo;

	//bi-directional many-to-one association to Estilo
	@ManyToOne(fetch=FetchType.EAGER)
	private Estilo estilo;

	//bi-directional many-to-one association to Marca
	@ManyToOne(fetch=FetchType.EAGER)
	private Marca marca;

	//bi-directional many-to-one association to Prenda
	@ManyToOne(fetch=FetchType.EAGER)
	private Prenda prenda;

	//bi-directional many-to-one association to Inventario
	@OneToMany(mappedBy="baseproducto")
	private List<Inventario> inventarios;

	//bi-directional many-to-one association to Producto
	@OneToMany(mappedBy="baseproducto")
	private List<Producto> productos;

    public Baseproducto() {
    }

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Integer getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Boolean getEsporcentaje() {
		return this.esporcentaje;
	}

	public void setEsporcentaje(Boolean esporcentaje) {
		this.esporcentaje = esporcentaje;
	}

	public Boolean getEspromo() {
		return this.espromo;
	}

	public void setEspromo(Boolean espromo) {
		this.espromo = espromo;
	}

	public byte[] getFoto() {
		return this.foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	public Integer getFotoSize() {
		return this.fotoSize;
	}

	public void setFotoSize(Integer fotoSize) {
		this.fotoSize = fotoSize;
	}

	public BigDecimal getPrecio() {
		return this.precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public BigDecimal getPrecioPromo() {
		return this.precioPromo;
	}

	public void setPrecioPromo(BigDecimal precioPromo) {
		this.precioPromo = precioPromo;
	}

	public Estilo getEstilo() {
		return this.estilo;
	}

	public void setEstilo(Estilo estilo) {
		this.estilo = estilo;
	}
	
	public Marca getMarca() {
		return this.marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}
	
	public Prenda getPrenda() {
		return this.prenda;
	}

	public void setPrenda(Prenda prenda) {
		this.prenda = prenda;
	}
	
	public List<Inventario> getInventarios() {
		return this.inventarios;
	}

	public void setInventarios(List<Inventario> inventarios) {
		this.inventarios = inventarios;
	}
	
	public List<Producto> getProductos() {
		return this.productos;
	}

	public void setProductos(List<Producto> productos) {
		this.productos = productos;
	}
	
}