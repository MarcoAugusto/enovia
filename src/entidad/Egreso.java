package entidad;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the egreso database table.
 * 
 */
@Entity
public class Egreso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id	
	private String code;

	private Boolean activo;

	private Timestamp fecha;

	private String tipoegreso;

	private BigDecimal total;

	private BigDecimal totaldescuento;

	//bi-directional many-to-one association to Detalleegreso
	@OneToMany(mappedBy="egreso")
	private List<Detalleegreso> detalleegresos;

	//bi-directional many-to-one association to Devolucion
	@OneToMany(mappedBy="egreso")
	private List<Devolucion> devolucions;

	//bi-directional many-to-one association to Almacen
	@ManyToOne(fetch=FetchType.EAGER)
	private Almacen almacen;

	//bi-directional many-to-one association to Usuario
	@ManyToOne(fetch=FetchType.EAGER)
	private Usuario usuario;

	//bi-directional many-to-one association to Traspaso
	@OneToMany(mappedBy="egreso")
	private List<Traspaso> traspasos;

	//bi-directional many-to-one association to Venta
	@OneToMany(mappedBy="egreso")
	private List<Venta> ventas;

    public Egreso() {
    }

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Timestamp getFecha() {
		return this.fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public String getTipoegreso() {
		return this.tipoegreso;
	}

	public void setTipoegreso(String tipoegreso) {
		this.tipoegreso = tipoegreso;
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getTotaldescuento() {
		return this.totaldescuento;
	}

	public void setTotaldescuento(BigDecimal totaldescuento) {
		this.totaldescuento = totaldescuento;
	}

	public List<Detalleegreso> getDetalleegresos() {
		return this.detalleegresos;
	}

	public void setDetalleegresos(List<Detalleegreso> detalleegresos) {
		this.detalleegresos = detalleegresos;
	}
	
	public List<Devolucion> getDevolucions() {
		return this.devolucions;
	}

	public void setDevolucions(List<Devolucion> devolucions) {
		this.devolucions = devolucions;
	}
	
	public Almacen getAlmacen() {
		return this.almacen;
	}

	public void setAlmacen(Almacen almacen) {
		this.almacen = almacen;
	}
	
	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public List<Traspaso> getTraspasos() {
		return this.traspasos;
	}

	public void setTraspasos(List<Traspaso> traspasos) {
		this.traspasos = traspasos;
	}
	
	public List<Venta> getVentas() {
		return this.ventas;
	}

	public void setVentas(List<Venta> ventas) {
		this.ventas = ventas;
	}
	
}