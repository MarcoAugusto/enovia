package entidad;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the producto database table.
 * 
 */
@Entity
public class Producto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String code;

	private Boolean activo;

	private String estado;

	private byte[] foto;

	@Column(name = "foto_size")
	private Integer fotoSize;

	private String nombre;

	// bi-directional many-to-one association to Controlmedida
	@OneToMany(mappedBy = "producto")
	private List<Controlmedida> controlmedidas;

	// bi-directional many-to-one association to Detalleegreso
	@OneToMany(mappedBy = "producto")
	private List<Detalleegreso> detalleegresos;

	// bi-directional many-to-one association to Detalleingreso
	@OneToMany(mappedBy = "producto")
	private List<Detalleingreso> detalleingresos;

	// bi-directional many-to-one association to Detallepedido
	@OneToMany(mappedBy = "producto")
	private List<Detallepedido> detallepedidos;

	// bi-directional many-to-one association to Detallereserva
	@OneToMany(mappedBy = "producto")
	private List<Detallereserva> detallereservas;

	// bi-directional many-to-one association to Almacen
	@ManyToOne(fetch = FetchType.EAGER)
	private Almacen almacen;

	// bi-directional many-to-one association to Baseproducto
	@ManyToOne(fetch = FetchType.EAGER)
	private Baseproducto baseproducto;

	// bi-directional many-to-one association to Color
	@ManyToOne(fetch = FetchType.EAGER)
	private Color color;

	// bi-directional many-to-one association to Talla
	@ManyToOne(fetch = FetchType.EAGER)
	private Talla talla;

	public Producto() {
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public byte[] getFoto() {
		return this.foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	public Integer getFotoSize() {
		return this.fotoSize;
	}

	public void setFotoSize(Integer fotoSize) {
		this.fotoSize = fotoSize;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Controlmedida> getControlmedidas() {
		return this.controlmedidas;
	}

	public void setControlmedidas(List<Controlmedida> controlmedidas) {
		this.controlmedidas = controlmedidas;
	}

	public List<Detalleegreso> getDetalleegresos() {
		return this.detalleegresos;
	}

	public void setDetalleegresos(List<Detalleegreso> detalleegresos) {
		this.detalleegresos = detalleegresos;
	}

	public List<Detalleingreso> getDetalleingresos() {
		return this.detalleingresos;
	}

	public void setDetalleingresos(List<Detalleingreso> detalleingresos) {
		this.detalleingresos = detalleingresos;
	}

	public List<Detallepedido> getDetallepedidos() {
		return this.detallepedidos;
	}

	public void setDetallepedidos(List<Detallepedido> detallepedidos) {
		this.detallepedidos = detallepedidos;
	}

	public List<Detallereserva> getDetallereservas() {
		return this.detallereservas;
	}

	public void setDetallereservas(List<Detallereserva> detallereservas) {
		this.detallereservas = detallereservas;
	}

	public Almacen getAlmacen() {
		return this.almacen;
	}

	public void setAlmacen(Almacen almacen) {
		this.almacen = almacen;
	}

	public Baseproducto getBaseproducto() {
		return this.baseproducto;
	}

	public void setBaseproducto(Baseproducto baseproducto) {
		this.baseproducto = baseproducto;
	}

	public Color getColor() {
		return this.color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Talla getTalla() {
		return this.talla;
	}

	public void setTalla(Talla talla) {
		this.talla = talla;
	}

}