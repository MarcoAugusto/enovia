package entidad;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the usuario database table.
 * 
 */
@Entity
public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer code;

	private Boolean activo;

	private String direccion;

	private String email;

	private String idexterno;

	private Boolean isexterno;

	private String login;

	private String nombre;

	private String password;

	private String telefono;

	private String tipoexterno;

	//bi-directional many-to-one association to Agenda
	@OneToMany(mappedBy="usuario")
	private List<Agenda> agendas;

	//bi-directional many-to-one association to Asientocontable
	@OneToMany(mappedBy="usuario")
	private List<Asientocontable> asientocontables;

	//bi-directional many-to-one association to Bitacora
	@OneToMany(mappedBy="usuario")
	private List<Bitacora> bitacoras;

	//bi-directional many-to-one association to Compra
	@OneToMany(mappedBy="usuario")
	private List<Compra> compras;

	//bi-directional many-to-one association to Controlmedida
	@OneToMany(mappedBy="usuario1")
	private List<Controlmedida> controlmedidas1;

	//bi-directional many-to-one association to Controlmedida
	@OneToMany(mappedBy="usuario2")
	private List<Controlmedida> controlmedidas2;

	//bi-directional many-to-one association to Detalleentrega
	@OneToMany(mappedBy="usuario")
	private List<Detalleentrega> detalleentregas;

	//bi-directional many-to-one association to Devolucion
	@OneToMany(mappedBy="usuario")
	private List<Devolucion> devolucions;

	//bi-directional many-to-one association to Devolucionventa
	@OneToMany(mappedBy="usuario")
	private List<Devolucionventa> devolucionventas;

	//bi-directional many-to-one association to Egreso
	@OneToMany(mappedBy="usuario")
	private List<Egreso> egresos;

	//bi-directional many-to-one association to Ingreso
	@OneToMany(mappedBy="usuario")
	private List<Ingreso> ingresos;

	//bi-directional many-to-one association to Pagocompra
	@OneToMany(mappedBy="usuario")
	private List<Pagocompra> pagocompras;

	//bi-directional many-to-one association to Pagoventa
	@OneToMany(mappedBy="usuario")
	private List<Pagoventa> pagoventas;

	//bi-directional many-to-one association to Pedido
	@OneToMany(mappedBy="usuario")
	private List<Pedido> pedidos;

	//bi-directional many-to-one association to Recepcion
	@OneToMany(mappedBy="usuario")
	private List<Recepcion> recepcions;

	//bi-directional many-to-one association to Reserva
	@OneToMany(mappedBy="usuario")
	private List<Reserva> reservas;

	//bi-directional many-to-one association to Transito
	@OneToMany(mappedBy="usuario")
	private List<Transito> transitos;

	//bi-directional many-to-one association to Traspaso
	@OneToMany(mappedBy="usuario")
	private List<Traspaso> traspasos;

	//bi-directional many-to-one association to Rol
	@ManyToOne(fetch=FetchType.EAGER)
	private Rol rol;

	//bi-directional many-to-one association to Sucursal
	@ManyToOne(fetch=FetchType.EAGER)
	private Sucursal sucursal;

	//bi-directional many-to-one association to Venta
	@OneToMany(mappedBy="usuario")
	private List<Venta> ventas;

    public Usuario() {
    }

	public Integer getCode() {
		return this.code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIdexterno() {
		return this.idexterno;
	}

	public void setIdexterno(String idexterno) {
		this.idexterno = idexterno;
	}

	public Boolean getIsexterno() {
		return this.isexterno;
	}

	public void setIsexterno(Boolean isexterno) {
		this.isexterno = isexterno;
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getTipoexterno() {
		return this.tipoexterno;
	}

	public void setTipoexterno(String tipoexterno) {
		this.tipoexterno = tipoexterno;
	}

	public List<Agenda> getAgendas() {
		return this.agendas;
	}

	public void setAgendas(List<Agenda> agendas) {
		this.agendas = agendas;
	}
	
	public List<Asientocontable> getAsientocontables() {
		return this.asientocontables;
	}

	public void setAsientocontables(List<Asientocontable> asientocontables) {
		this.asientocontables = asientocontables;
	}
	
	public List<Bitacora> getBitacoras() {
		return this.bitacoras;
	}

	public void setBitacoras(List<Bitacora> bitacoras) {
		this.bitacoras = bitacoras;
	}
	
	public List<Compra> getCompras() {
		return this.compras;
	}

	public void setCompras(List<Compra> compras) {
		this.compras = compras;
	}
	
	public List<Controlmedida> getControlmedidas1() {
		return this.controlmedidas1;
	}

	public void setControlmedidas1(List<Controlmedida> controlmedidas1) {
		this.controlmedidas1 = controlmedidas1;
	}
	
	public List<Controlmedida> getControlmedidas2() {
		return this.controlmedidas2;
	}

	public void setControlmedidas2(List<Controlmedida> controlmedidas2) {
		this.controlmedidas2 = controlmedidas2;
	}
	
	public List<Detalleentrega> getDetalleentregas() {
		return this.detalleentregas;
	}

	public void setDetalleentregas(List<Detalleentrega> detalleentregas) {
		this.detalleentregas = detalleentregas;
	}
	
	public List<Devolucion> getDevolucions() {
		return this.devolucions;
	}

	public void setDevolucions(List<Devolucion> devolucions) {
		this.devolucions = devolucions;
	}
	
	public List<Devolucionventa> getDevolucionventas() {
		return this.devolucionventas;
	}

	public void setDevolucionventas(List<Devolucionventa> devolucionventas) {
		this.devolucionventas = devolucionventas;
	}
	
	public List<Egreso> getEgresos() {
		return this.egresos;
	}

	public void setEgresos(List<Egreso> egresos) {
		this.egresos = egresos;
	}
	
	public List<Ingreso> getIngresos() {
		return this.ingresos;
	}

	public void setIngresos(List<Ingreso> ingresos) {
		this.ingresos = ingresos;
	}
	
	public List<Pagocompra> getPagocompras() {
		return this.pagocompras;
	}

	public void setPagocompras(List<Pagocompra> pagocompras) {
		this.pagocompras = pagocompras;
	}
	
	public List<Pagoventa> getPagoventas() {
		return this.pagoventas;
	}

	public void setPagoventas(List<Pagoventa> pagoventas) {
		this.pagoventas = pagoventas;
	}
	
	public List<Pedido> getPedidos() {
		return this.pedidos;
	}

	public void setPedidos(List<Pedido> pedidos) {
		this.pedidos = pedidos;
	}
	
	public List<Recepcion> getRecepcions() {
		return this.recepcions;
	}

	public void setRecepcions(List<Recepcion> recepcions) {
		this.recepcions = recepcions;
	}
	
	public List<Reserva> getReservas() {
		return this.reservas;
	}

	public void setReservas(List<Reserva> reservas) {
		this.reservas = reservas;
	}
	
	public List<Transito> getTransitos() {
		return this.transitos;
	}

	public void setTransitos(List<Transito> transitos) {
		this.transitos = transitos;
	}
	
	public List<Traspaso> getTraspasos() {
		return this.traspasos;
	}

	public void setTraspasos(List<Traspaso> traspasos) {
		this.traspasos = traspasos;
	}
	
	public Rol getRol() {
		return this.rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}
	
	public Sucursal getSucursal() {
		return this.sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}
	
	public List<Venta> getVentas() {
		return this.ventas;
	}

	public void setVentas(List<Venta> ventas) {
		this.ventas = ventas;
	}
	
}