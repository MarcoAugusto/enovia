package entidad;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the detalleingreso database table.
 * 
 */
@Entity
public class Detalleingreso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long code;

	private Boolean activo;

	private Integer cantidad;

	private BigDecimal precio;

	//bi-directional many-to-one association to Detallepedido
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="detpedido_code")
	private Detallepedido detallepedido;

	//bi-directional many-to-one association to Ingreso
	@ManyToOne(fetch=FetchType.EAGER)
	private Ingreso ingreso;

	//bi-directional many-to-one association to Producto
	@ManyToOne(fetch=FetchType.EAGER)
	private Producto producto;

	//bi-directional many-to-one association to Detallerecepcion
	@OneToMany(mappedBy="detalleingreso")
	private List<Detallerecepcion> detallerecepcions;

	//bi-directional many-to-one association to Detalletransito
	@OneToMany(mappedBy="detalleingreso")
	private List<Detalletransito> detalletransitos;

	//bi-directional many-to-many association to Devolucionventa
	@ManyToMany(mappedBy="detalleingresos")
	private List<Devolucionventa> devolucionventas;

    public Detalleingreso() {
    }

	public Long getCode() {
		return this.code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Integer getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public BigDecimal getPrecio() {
		return this.precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public Detallepedido getDetallepedido() {
		return this.detallepedido;
	}

	public void setDetallepedido(Detallepedido detallepedido) {
		this.detallepedido = detallepedido;
	}
	
	public Ingreso getIngreso() {
		return this.ingreso;
	}

	public void setIngreso(Ingreso ingreso) {
		this.ingreso = ingreso;
	}
	
	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	
	public List<Detallerecepcion> getDetallerecepcions() {
		return this.detallerecepcions;
	}

	public void setDetallerecepcions(List<Detallerecepcion> detallerecepcions) {
		this.detallerecepcions = detallerecepcions;
	}
	
	public List<Detalletransito> getDetalletransitos() {
		return this.detalletransitos;
	}

	public void setDetalletransitos(List<Detalletransito> detalletransitos) {
		this.detalletransitos = detalletransitos;
	}
	
	public List<Devolucionventa> getDevolucionventas() {
		return this.devolucionventas;
	}

	public void setDevolucionventas(List<Devolucionventa> devolucionventas) {
		this.devolucionventas = devolucionventas;
	}
	
}