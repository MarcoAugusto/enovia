package entidad;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the devolucionventa database table.
 * 
 */
@Entity
public class Devolucionventa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String code;

	private Boolean activo;

	private Timestamp fecha;

	private String motivo;

	//bi-directional many-to-many association to Detalleingreso
    @ManyToMany
	@JoinTable(
		name="devolucionventa_detalleingreso"
		, joinColumns={
			@JoinColumn(name="devolucionventa_code")
			}
		, inverseJoinColumns={
			@JoinColumn(name="detingreso_code")
			}
		)
	private List<Detalleingreso> detalleingresos;

	//bi-directional many-to-one association to Usuario
	@ManyToOne(fetch=FetchType.LAZY)
	private Usuario usuario;

	//bi-directional many-to-one association to Venta
	@ManyToOne(fetch=FetchType.LAZY)
	private Venta venta;

    public Devolucionventa() {
    }

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Timestamp getFecha() {
		return this.fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public String getMotivo() {
		return this.motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public List<Detalleingreso> getDetalleingresos() {
		return this.detalleingresos;
	}

	public void setDetalleingresos(List<Detalleingreso> detalleingresos) {
		this.detalleingresos = detalleingresos;
	}
	
	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public Venta getVenta() {
		return this.venta;
	}

	public void setVenta(Venta venta) {
		this.venta = venta;
	}
	
}