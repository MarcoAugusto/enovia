package entidad;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the detallereserva database table.
 * 
 */
@Entity
public class Detallereserva implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long code;

	private Boolean activo;

	private BigDecimal adelanto;

	private Integer cantidad;

	private BigDecimal precio;

	//bi-directional many-to-one association to Detalleegreso
	@OneToMany(mappedBy="detallereserva")
	private List<Detalleegreso> detalleegresos;

	//bi-directional many-to-one association to Producto
	@ManyToOne(fetch=FetchType.LAZY)
	private Producto producto;

	//bi-directional many-to-one association to Reserva
	@ManyToOne(fetch=FetchType.LAZY)
	private Reserva reserva;

    public Detallereserva() {
    }

	public Long getCode() {
		return this.code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public BigDecimal getAdelanto() {
		return this.adelanto;
	}

	public void setAdelanto(BigDecimal adelanto) {
		this.adelanto = adelanto;
	}

	public Integer getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public BigDecimal getPrecio() {
		return this.precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public List<Detalleegreso> getDetalleegresos() {
		return this.detalleegresos;
	}

	public void setDetalleegresos(List<Detalleegreso> detalleegresos) {
		this.detalleegresos = detalleegresos;
	}
	
	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	
	public Reserva getReserva() {
		return this.reserva;
	}

	public void setReserva(Reserva reserva) {
		this.reserva = reserva;
	}
	
}