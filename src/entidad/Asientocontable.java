package entidad;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the asientocontable database table.
 * 
 */
@Entity
public class Asientocontable implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long code;

	private String actividad;

	private Boolean activo;

	private String comentario;

	private String descripcion;

	private String tipo;

	//bi-directional many-to-one association to Pago
	@ManyToOne(fetch=FetchType.LAZY)
	private Pago pago;

	//bi-directional many-to-one association to Pagocompra
	@ManyToOne(fetch=FetchType.LAZY)
	private Pagocompra pagocompra;

	//bi-directional many-to-one association to Pagoventa
	@ManyToOne(fetch=FetchType.LAZY)
	private Pagoventa pagoventa;

	//bi-directional many-to-one association to Usuario
	@ManyToOne(fetch=FetchType.LAZY)
	private Usuario usuario;

    public Asientocontable() {
    }

	public Long getCode() {
		return this.code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public String getActividad() {
		return this.actividad;
	}

	public void setActividad(String actividad) {
		this.actividad = actividad;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getComentario() {
		return this.comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTipo() {
		return this.tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Pago getPago() {
		return this.pago;
	}

	public void setPago(Pago pago) {
		this.pago = pago;
	}
	
	public Pagocompra getPagocompra() {
		return this.pagocompra;
	}

	public void setPagocompra(Pagocompra pagocompra) {
		this.pagocompra = pagocompra;
	}
	
	public Pagoventa getPagoventa() {
		return this.pagoventa;
	}

	public void setPagoventa(Pagoventa pagoventa) {
		this.pagoventa = pagoventa;
	}
	
	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}