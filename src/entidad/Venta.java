package entidad;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the venta database table.
 * 
 */
@Entity
public class Venta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String code;

	private Boolean activo;

	private Boolean boda;

	private String comentario;

	private String estado;

	private Timestamp fecha;

	private Boolean quinse;

	private BigDecimal totalbob;

	private BigDecimal totaldolar;

	private Boolean vestirnovia;

	//bi-directional many-to-one association to Datosboda
	@OneToMany(mappedBy="venta")
	private List<Datosboda> datosbodas;

	//bi-directional many-to-one association to Devolucionventa
	@OneToMany(mappedBy="venta")
	private List<Devolucionventa> devolucionventas;

	//bi-directional many-to-one association to Pagoventa
	@OneToMany(mappedBy="venta")
	private List<Pagoventa> pagoventas;

	//bi-directional many-to-one association to Cliente
	@ManyToOne(fetch=FetchType.LAZY)
	private Cliente cliente;

	//bi-directional many-to-one association to Egreso
	@ManyToOne(fetch=FetchType.LAZY)
	private Egreso egreso;

	//bi-directional many-to-one association to Usuario
	@ManyToOne(fetch=FetchType.LAZY)
	private Usuario usuario;

    public Venta() {
    }

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Boolean getBoda() {
		return this.boda;
	}

	public void setBoda(Boolean boda) {
		this.boda = boda;
	}

	public String getComentario() {
		return this.comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Timestamp getFecha() {
		return this.fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public Boolean getQuinse() {
		return this.quinse;
	}

	public void setQuinse(Boolean quinse) {
		this.quinse = quinse;
	}

	public BigDecimal getTotalbob() {
		return this.totalbob;
	}

	public void setTotalbob(BigDecimal totalbob) {
		this.totalbob = totalbob;
	}

	public BigDecimal getTotaldolar() {
		return this.totaldolar;
	}

	public void setTotaldolar(BigDecimal totaldolar) {
		this.totaldolar = totaldolar;
	}

	public Boolean getVestirnovia() {
		return this.vestirnovia;
	}

	public void setVestirnovia(Boolean vestirnovia) {
		this.vestirnovia = vestirnovia;
	}

	public List<Datosboda> getDatosbodas() {
		return this.datosbodas;
	}

	public void setDatosbodas(List<Datosboda> datosbodas) {
		this.datosbodas = datosbodas;
	}
	
	public List<Devolucionventa> getDevolucionventas() {
		return this.devolucionventas;
	}

	public void setDevolucionventas(List<Devolucionventa> devolucionventas) {
		this.devolucionventas = devolucionventas;
	}
	
	public List<Pagoventa> getPagoventas() {
		return this.pagoventas;
	}

	public void setPagoventas(List<Pagoventa> pagoventas) {
		this.pagoventas = pagoventas;
	}
	
	public Cliente getCliente() {
		return this.cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Egreso getEgreso() {
		return this.egreso;
	}

	public void setEgreso(Egreso egreso) {
		this.egreso = egreso;
	}
	
	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}