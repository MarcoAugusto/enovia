/**
 * 
 */
package persistence.producto;

import bean.util.BaseSql;
import bean.util.Constantes;
import bean.util.IPaginationPersistence;
import entidad.Baseproducto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Named;

/**
 * @author penama
 * 
 */
@Named
public class BaseproductoPersistence extends BaseSql implements IPaginationPersistence {

	private String HQL_findall = " select c from Baseproducto c where c.activo = true ";	
	private String HQL_findall_count = " select count(c) from Baseproducto c where c.activo = true ";
	private String HQL_orderBy = " order by c.code asc ";
	private String HQL_generateCode = " select count(c) from Baseproducto c ";
	private String HQL_validarXduplicado = "select count(c) from Baseproducto c where c.activo = true and c.marca.code = :marca and c.estilo.code = :estilo and c.prenda.code = :prenda ";
	private String HQL_validarXduplicadoXid = "select count(c) from Baseproducto c where c.activo = true and c.marca.code = :marca and c.estilo.code = :estilo and c.prenda.code = :prenda and c.code <> :code ";	
	private String HQL_filtroCode = " lower(c.code) like lower(:code) ";
	private String HQL_filtroCodigo = " lower(c.codigo) like lower(:codigo) ";
	private String HQL_filtroEstilo = " c.estilo.code = :estilo ";
	private String HQL_filtroMarca = " c.marca.code = :marca ";
	private String HQL_filtroTipoPrenda = " c.prenda.code = :prenda ";
	
	private String HQL_prendas = "select DISTINCT c.prenda from Baseproducto c where c.activo = true ";
	private String HQL_estilos = "select DISTINCT c.estilo from Baseproducto c where c.activo = true and c.prenda.code = :prenda ";
	private String HQL_marcas = "select DISTINCT c.marca from Baseproducto c where c.activo = true and c.estilo.code = :estilo and c.prenda.code = :prenda ";
	private String HQL_Baseproducto = "select c from Baseproducto c where c.activo = true and c.estilo.code = :estilo and c.marca.code = :marca and c.prenda.code = :prenda ";
	
	public void init() {		
		this.iniLogs(this.getClass());
		mapaHqlCampos.clear();
		filtroBusqueda = "filtros";
		cadenaBusqueda = filtroBusqueda;
		mapaHqlCampos.put( "code" , HQL_filtroCode );
		mapaHqlCampos.put( "codigo" , HQL_filtroCodigo );		 
		mapaHqlCampos.put( "estilo" , HQL_filtroEstilo );
		mapaHqlCampos.put( "marca" , HQL_filtroMarca );
		mapaHqlCampos.put( "prenda" , HQL_filtroTipoPrenda );
	}

	public List<Baseproducto> getLista() throws Exception {		
		return getLista( "" );
	}

	public List<Baseproducto> getLista( String cadenaBusqueda ){
		hql = HQL_findall;
		hqlCount = HQL_findall_count;
		this.cadenaBusqueda = cadenaBusqueda;
		return buscar();
	}
	
	/**
	 * Obtiene el listado de la entidad en base a un mapa<Filtro, Valor>
	 * @param mapaAux
	 * @return
	 */
	public List<Baseproducto> getLista( Map<String, String> mapaAux ){
		mapaHqlValores.clear();		
		Iterator<Entry<String, String>> iterEntry = mapaAux.entrySet().iterator();
		while (iterEntry.hasNext()) {
			Entry<String, String> entry = iterEntry.next();			
			mapaHqlValores.put(entry.getKey(), entry.getValue());			
		}
		cadenaBusqueda = filtroBusqueda;
		hql = HQL_findall;
		hqlCount = HQL_findall_count;
		return buscar();
	}
	
	
	public void save(Baseproducto baseProducto) throws Exception {		
		String code = generateCode();		
		if ( code == null || code.trim().isEmpty() ){
			throw new Exception( this.getClass().getName() + ".save: No se pudo generar el code secuencial!!! " );
		}
		baseProducto.setCode( code );
		persistencia.save(baseProducto);		
	}

	/**
	 * Obtiene el code siguiente a asignar.
	 * @return
	 */
	public String generateCode(){
		try {
			Object obj = persistencia.Consulta_item(HQL_generateCode);
			if (obj != null) {
				return Constantes.PREFIJO_BASE_PRODUCTO + (Integer.parseInt( obj.toString() ) + 1) ;				
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".generateCode", e);
		}
		return null;
	}
	
	public void update(Baseproducto baseProducto) throws Exception {				
		persistencia.update(baseProducto);
	}

	
	public String validarCampos(Object entidad, boolean nuevo) {
		if (entidad != null && entidad instanceof Baseproducto) {
			StringBuilder sb = new StringBuilder();
			Baseproducto entidadBaseproducto = (Baseproducto) entidad;			
			if (entidadBaseproducto.getEstilo() == null ) {
				sb.append("Campo Estilo es requerido");
				sb.append(", ");
			}
			if ( entidadBaseproducto.getMarca() == null ){
				sb.append("Campo Marca es requerido");
				sb.append(", ");
			}
			if ( entidadBaseproducto.getPrenda() == null ){
				sb.append("Campo TipoPrenda es requerido");
				sb.append(", ");
			}
			if (nuevo) {
				if (existeXduplicado(entidadBaseproducto) ) {
					sb.append("Ya existe un baseProducto con las mismas caracteristicas");
				}
			} else {
				if (existeXduplicadoXid( entidadBaseproducto ) ) {
					sb.append("Ya existe un baseProducto con las mismas caracteristicas");
				}
			}
			log.info("cadena: " + sb.toString());
			return sb.toString();
		}
		return "";
	}

	public boolean existeXduplicado( Baseproducto entidad ) {
		mapa.clear();
		mapa.put("prenda", entidad.getPrenda().getCode());
		mapa.put("marca", entidad.getMarca().getCode());
		mapa.put("estilo", entidad.getEstilo().getCode());
		
		try {
			Object obj = persistencia.Consulta_item(HQL_validarXduplicado, mapa);
			if (obj != null) {				
				if (Integer.parseInt(obj.toString()) > 0) {
					return true;
				}
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".existeXduplicado", e);
		}
		return false;
	}
	
	public boolean existeXduplicadoXid( Baseproducto entidad ) {		
		mapa.clear();
		mapa.put("code", entidad.getCode());
		mapa.put("marca", entidad.getMarca().getCode());
		mapa.put("estilo", entidad.getEstilo().getCode());
		mapa.put("prenda", entidad.getPrenda().getCode());
		try {
			Object obj = persistencia.Consulta_item(HQL_validarXduplicadoXid, mapa);
			if (obj != null) {				
				if (Integer.parseInt(obj.toString()) > 0) {
					return true;
				}
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".existeXduplicadoXid", e);
		}
		return false;
	}
	
	/**
	 * M�todo buscar, realiza la busqueda seg�n los datos cargados previamente.
	 * Requisitos: setear las siguientes variables: hql, hqlCount
	 * Si lo que se requiere es obtener un listado de la entidad, dejar vacio la variable cadenaBusqueda
	 * @return
	 */
	public List<Baseproducto> buscar(){
		String hql_aux = "";
		String hql_filtro_aux = armarFiltroMultiple();		
		if ( !hql_filtro_aux.trim().isEmpty() ){
			hql_aux = Constantes.AND + Constantes.ABRIR_PARENTESIS + hql_filtro_aux + Constantes.CERRAR_PARENTESIS;
		}
//		hql_filtro_aux = ( hql_filtro_aux.trim().isEmpty() ? Constantes.VACIO : Constantes.WHERE ) + hql_filtro_aux;		
		hql = hql + hql_aux + HQL_orderBy;
		hqlCount = hqlCount + hql_aux;
		log.info( "hql: " +  hql );
		log.info( "hqlCount: " +  hqlCount );
		try {			
			consultaCount(hqlCount, mapa);			
			if ( cantidadRegistros == 0 ){
				pagina = 0;
				return new ArrayList<Baseproducto>();
			}
			pagina = 1;
			return Consulta(hql, pagina, mapa );
		} catch (Exception e) {
			log.info( "baseProducto.buscar", e );			
		}
		return new ArrayList<Baseproducto>();
	}
		
	
	@Override
	public List firstpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_FIRST));
	}

	@Override
	public List previouspage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_PREVIOUS));
	}

	@Override
	public List nextpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_NEXT));
	}

	@Override
	public List lastpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_LAST));
	}

}
