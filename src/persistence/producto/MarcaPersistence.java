/**
 * 
 */
package persistence.producto;

import bean.util.BaseSql;
import bean.util.Constantes;
import bean.util.IPaginationPersistence;
import bean.util.IValidator;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;
import javax.persistence.NamedQuery;

import entidad.Marca;

/**
 * @author penama
 * 
 */
@Named
public class MarcaPersistence extends BaseSql implements IValidator,IPaginationPersistence {

	private String HQL_findall = " select t from Marca t where t.activo = true ";
	private String HQL_findall_count = " select count(t) from Marca t where t.activo = true ";
	private String HQL_orderBy = " order by t.nombre asc ";
	private String HQL_validarXnombre = "select count(t) from Marca t where t.activo = true and lower(t.nombre) = lower(:nombre) ";
	private String HQL_validarXnombreDid = "select count(t) from Marca t where t.activo = true and lower(t.nombre) = lower(:nombre) and t.code <> :code ";
	private String HQL_filtroNombre = " and lower(t.nombre) like lower(:nombre) ";

	
	public void init() {
		this.iniLogs(this.getClass());
		mapaHqlCampos.clear();
		filtroBusqueda = "nombre";
		mapaHqlCampos.put( filtroBusqueda , HQL_filtroNombre );
	}

	public List<Marca> getLista() throws Exception {		
		return getLista( "" );
	}

	public List<Marca> getLista( String cadenaBusqueda ){
		hql = HQL_findall;
		hqlCount = HQL_findall_count;
		this.cadenaBusqueda = cadenaBusqueda;
		return buscar();
	}

	public void save(Marca marca) throws Exception {
		persistencia.save(marca);
//		try{
//			
//			Object cons = persistencia.Consulta_item(HQL_findall_count, null);
//			log.info( "cantidad: " +  Integer.parseInt(cons.toString()) );
//			
//			persistencia.beginTransaccion();
//			persistencia.persist( marca );
//			log.info( "marca.code: " + marca.getCode() );
//			
//			cons = persistencia.Consulta_item(HQL_findall_count, null);
//			log.info( "cantidad: " + Integer.parseInt(cons.toString()) );					
//			
//			persistencia.rollback();
//			
//			cons = persistencia.Consulta_item(HQL_findall_count, null);
//			log.info( "cantidad: " + Integer.parseInt(cons.toString()) );
//			
////			persistencia.commit();
//		}catch (Exception e) {
//			persistencia.rollback();
//		}
//		
	}

	public void update(Marca marca) throws Exception {
		persistencia.update(marca);
	}

	@Override
	public String validarCampos(Object entidad, boolean nuevo) {
		if (entidad != null && entidad instanceof Marca) {
			StringBuilder sb = new StringBuilder();
			Marca entidadColor = (Marca) entidad;
			log.info("EntidadMarca: " + entidadColor.getCode() + " " + entidadColor.getNombre() + " " + nuevo );
			if (entidadColor.getNombre() == null || entidadColor.getNombre().trim().isEmpty()) {
				sb.append("Campo nombre es requerido");
				sb.append(", ");
			}
			if (nuevo) {
				if (existeXnombre(entidadColor.getNombre())) {
					sb.append("Ya existe una talla igual");
				}
			} else {
				if (existeXnombreDid(entidadColor.getNombre(), entidadColor.getCode())) {
					sb.append("Ya existe una talla igual");
				}
			}
			log.info("cadena: " + sb.toString());
			return sb.toString();
		}
		return "";
	}

	@Override
	public boolean existeXnombre(String nombre) {
		mapa.clear();
		mapa.put("nombre", nombre);
		try {
			Object obj = persistencia.Consulta_item(HQL_validarXnombre, mapa);
			if (obj != null) {
				log.info("Object: " + obj + "  " + obj.toString() + " " + nombre );
				if (Integer.parseInt(obj.toString()) > 0) {
					return true;
				}
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".existeXnombre", e);
		}
		return false;
	}

	@Override
	public boolean existeXnombreDid(String nombre, long code) {
		log.info("EntidadMarca: " + code + " " + nombre);
		mapa.clear();
		mapa.put("code", (int) code);
		mapa.put("nombre", nombre);
		try {
			Object obj = persistencia
					.Consulta_item(HQL_validarXnombreDid, mapa);
			if (obj != null) {
				log.info("Object: " + obj + "  " + obj.toString() + " " + nombre );
				if (Integer.parseInt(obj.toString()) > 0) {
					return true;
				}
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".existeXnombreDid", e);
		}
		return false;
	}

	/**
	 * M�todo buscar, realiza la busqueda seg�n los datos cargados previamente.
	 * Requisitos: setear las siguientes variables: hql, hqlCount
	 * Si lo que se requiere es obtener un listado de la entidad, dejar vacio la variable cadenaBusqueda
	 * @return
	 */
	public List<Marca> buscar(){
		String hql_aux = "";
		String hql_filtro_aux = armarFiltro();		
		if ( !hql_filtro_aux.isEmpty() ){
			hql_aux = Constantes.AND + Constantes.ABRIR_PARENTESIS + hql_filtro_aux + Constantes.CERRAR_PARENTESIS;
		}
		hql = hql + hql_filtro_aux + HQL_orderBy;
		hqlCount = hqlCount + hql_filtro_aux;
		try {			
			consultaCount(hqlCount, mapa);			
			if ( cantidadRegistros == 0 ){
				pagina = 0;
				return new ArrayList<Marca>();
			}
			pagina = 1;
			return Consulta(hql, pagina, mapa );
		} catch (Exception e) {
			log.info( "Color.buscar", e );			
		}
		return new ArrayList<Marca>();
	}
	
	@Override
	public List firstpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_FIRST));
	}

	@Override
	public List previouspage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_PREVIOUS));
	}

	@Override
	public List nextpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_NEXT));
	}

	@Override
	public List lastpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_LAST));
	}

}
