/**
 * 
 */
package persistence.producto;

import bean.util.BaseBean;
import bean.util.BaseSql;
import bean.util.Constantes;
import bean.util.IPaginationPersistence;
import bean.util.IValidator;
import entidad.Bitacora;
import entidad.Producto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Named;
import javax.persistence.NamedQuery;

/**
 * @author penama
 * 
 */
@Named
public class ProductoPersistence extends BaseSql implements
		IPaginationPersistence {

	private String HQL_findall = " select c from Producto c where c.activo = true ";
	private String HQL_findall_count = " select count(c) from Producto c where c.activo = true ";
	private String HQL_orderBy = " order by c.code asc ";
	private String HQL_generateCode = " select count(c) from Producto c ";
	private String HQL_validarXduplicado = "select count(c) from Producto c where c.activo = true and c.marca.code = :marca and c.estilo.code = :estilo  ";
	private String HQL_validarXduplicadoXid = "select count(c) from Producto c where c.activo = true and c.marca.code = :marca and c.estilo.code = :estilo and c.tipoprenda.code = :tipoprenda and c.code <> :code ";
	private String HQL_filtroCode = " lower(c.code) like lower(:code) ";
	private String HQL_filtroColor = " c.color.code = :color ";
	private String HQL_filtroTalla = " c.talla.code = :talla ";
	private String HQL_filtroEstado = " lower(c.estado) like lower(:estado) ";

	public void init() {
		mapaHqlCampos.clear();
		filtroBusqueda = "filtros";
		cadenaBusqueda = filtroBusqueda;
		mapaHqlCampos.put("code", HQL_filtroCode);
		mapaHqlCampos.put("color", HQL_filtroColor);
		mapaHqlCampos.put("talla", HQL_filtroTalla);
		mapaHqlCampos.put("estado", HQL_filtroEstado);
	}

	public List<Producto> getLista() throws Exception {
		return getLista("");
	}

	public List<Producto> getLista(String cadenaBusqueda) {
		hql = HQL_findall;
		hqlCount = HQL_findall_count;
		this.cadenaBusqueda = cadenaBusqueda;
		return buscar();
	}

	/**
	 * Obtiene el listado de la entidad en base a un mapa<Filtro, Valor>
	 * 
	 * @param mapaAux
	 * @return
	 */
	public List<Producto> getLista(Map<String, String> mapaAux) {
		mapaHqlValores.clear();
		Iterator<Entry<String, String>> iterEntry = mapaAux.entrySet()
				.iterator();
		while (iterEntry.hasNext()) {
			Entry<String, String> entry = iterEntry.next();
			mapaHqlValores.put(entry.getKey(), entry.getValue());
		}
		cadenaBusqueda = filtroBusqueda;
		hql = HQL_findall;
		hqlCount = HQL_findall_count;
		return buscar();
	}

	public void save(Producto Producto) throws Exception {
		String code = generateCode();
		if (code == null || code.trim().isEmpty()) {
			throw new Exception(this.getClass().getName()
					+ ".save: No se pudo generar el code secuencial!!! ");
		}
		Producto.setCode(code);
		persistencia.save(Producto);
	}

	/**
	 * Obtiene el code siguiente a asignar.
	 * 
	 * @return
	 */
	public String generateCode() throws Exception {
		Object obj = persistencia.Consulta_item(HQL_generateCode);
		if (obj != null) {
			return Constantes.PREFIJO_PRODUCTO
					+ (Integer.parseInt(obj.toString()) + 1);
		}
		return null;
	}

	public void update(Producto baseProducto) throws Exception {
		persistencia.update(baseProducto);
	}

	public String validarCampos(Object entidad, boolean nuevo) {
		if (entidad != null && entidad instanceof Producto) {
			StringBuilder sb = new StringBuilder();
			Producto entidadProducto = (Producto) entidad;
			if (entidadProducto.getColor() == null) {
				sb.append("Campo Color es requerido");
				sb.append(", ");
			}
			if (entidadProducto.getTalla() == null) {
				sb.append("Campo Talla es requerido");
				sb.append(", ");
			}
			log.info("cadena: " + sb.toString());
			return sb.toString();
		}
		return "";
	}

	/**
	 * M�todo buscar, realiza la busqueda seg�n los datos cargados
	 * previamente. Requisitos: setear las siguientes variables: hql, hqlCount
	 * Si lo que se requiere es obtener un listado de la entidad, dejar vacio la
	 * variable cadenaBusqueda
	 * 
	 * @return
	 */
	public List<Producto> buscar() {
		String hql_aux = "";
		String hql_filtro_aux = armarFiltroMultiple();
		if (!hql_filtro_aux.trim().isEmpty()) {
			hql_aux = Constantes.AND + Constantes.ABRIR_PARENTESIS
					+ hql_filtro_aux + Constantes.CERRAR_PARENTESIS;
		}
		// hql_filtro_aux = ( hql_filtro_aux.trim().isEmpty() ? Constantes.VACIO
		// : Constantes.WHERE ) + hql_filtro_aux;
		hql = hql + hql_aux + HQL_orderBy;
		hqlCount = hqlCount + hql_aux;
		log.info("hql: " + hql);
		log.info("hqlCount: " + hqlCount);
		try {
			consultaCount(hqlCount, mapa);
			if (cantidadRegistros == 0) {
				pagina = 0;
				return new ArrayList<Producto>();
			}
			pagina = 1;
			return Consulta(hql, pagina, mapa);
		} catch (Exception e) {
			log.info("baseProducto.buscar", e);
		}
		return new ArrayList<Producto>();
	}

	@Override
	public List firstpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_FIRST));
	}

	@Override
	public List previouspage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_PREVIOUS));
	}

	@Override
	public List nextpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_NEXT));
	}

	@Override
	public List lastpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_LAST));
	}

}
