/**
 * 
 */
package persistence.producto;

import bean.util.BaseBean;
import bean.util.BaseSql;
import bean.util.Constantes;
import bean.util.IPaginationPersistence;
import bean.util.IValidator;
import entidad.Bitacora;
import entidad.Producto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.persistence.NamedQuery;

/**
 * @author penama
 * 
 */
@Named
public class ProductoInventarioPersistence extends BaseSql implements
		IPaginationPersistence {

	private String HQL_findall = " select c from Producto c where c.activo = true and c.estado = '" + Constantes.ESTADO_ENSTOCK + "' ";
	private String HQL_findall_count = " select count(c) from Producto c where c.activo = true and c.estado = '" + Constantes.ESTADO_ENSTOCK + "' ";
	private String HQL_findProductoCatalogo = " select c from Producto c where c.activo = true and c.baseproducto.code = :catalogo and c.almacen.nombre = :almacen and c.estado = '" + Constantes.ESTADO_ENSTOCK + "' ";
	private String HQL_orderBy = " order by c.code asc ";
	private String HQL_filtroAlmacen = " lower(c.almacen.nombre) like lower(:almacen) ";
	private String HQL_filtroCode = " lower(c.code) like lower(:code) ";
	private String HQL_filtroCatalogo = " lower(c.baseproducto.code) like lower(:catalogo) ";
	private String HQL_filtroPrenda = " lower(c.baseproducto.tipoprenda.nombre) like lower(:prenda) ";
	private String HQL_filtroEstilo = " lower(c.baseproducto.estilo.nombre) like lower(:estilo) ";
	private String HQL_filtroMarca = " lower(c.baseproducto.marca.nombre) like lower(:marca) ";
	private String HQL_filtroColor = " lower(c.color.nombre) like lower(:color) ";
	private String HQL_filtroTalla = " lower(c.talla.nombre) like lower(:talla) ";
	private String HQL_filtroNombre = " lower(c.nombre) like lower(:nombre) ";
	private String HQL_filtroEstado = " lower(c.estado) like lower(:estado) ";
	
	
	public void init() {
		iniLogs(this.getClass());
		mapaHqlCampos.clear();
		filtroBusqueda = "filtros";
		cadenaBusqueda = filtroBusqueda;
		mapaHqlCampos.put("code", HQL_filtroCode);
		mapaHqlCampos.put("catalogo", HQL_filtroCatalogo);
		mapaHqlCampos.put("prenda", HQL_filtroPrenda);
		mapaHqlCampos.put("estilo", HQL_filtroEstilo);
		mapaHqlCampos.put("marca", HQL_filtroMarca);
		mapaHqlCampos.put("color", HQL_filtroColor);
		mapaHqlCampos.put("talla", HQL_filtroTalla);
		mapaHqlCampos.put("nombre", HQL_filtroNombre);
		mapaHqlCampos.put("estado", HQL_filtroEstado);
		mapaHqlCampos.put("almacen", HQL_filtroAlmacen);
	}

	public List<Producto> getLista() throws Exception {
		return getLista("");
	}

	public List<Producto> getLista(String cadenaBusqueda) throws Exception {
		hql = HQL_findall;
		hqlCount = HQL_findall_count;
		this.cadenaBusqueda = cadenaBusqueda;
		return buscar();
	}

	/**
	 * Obtiene el listado de la entidad en base a un mapa<Filtro, Valor>
	 * 
	 * @param mapaAux
	 * @return
	 */
	public List<Producto> getLista(Map<String, String> mapaAux) throws Exception {
		mapaHqlValores.clear();
		Iterator<Entry<String, String>> iterEntry = mapaAux.entrySet().iterator();
		while (iterEntry.hasNext()) {
			Entry<String, String> entry = iterEntry.next();
			mapaHqlValores.put(entry.getKey(), entry.getValue());
		}
		cadenaBusqueda = filtroBusqueda;
		hql = HQL_findall;
		hqlCount = HQL_findall_count;
		return buscar();
	}

	/**
	 * Obtiene el listado de productos en base al c�digo del catalogo.
	 * @param catalogo
	 * @return
	 */
	public List<Producto> onProductoCatalogo( String catalogo, String almacen ) throws Exception {
		mapa.clear();
		mapa.put( "catalogo", catalogo );
		mapa.put( "almacen", almacen );
		log.info( "parametros: " + mapa.toString() );
		Object obj = null;
		try{
			obj = persistencia.Consulta(HQL_findProductoCatalogo, mapa);
			if ( obj != null ){
				return (List<Producto>)obj;
			}
		}catch (Exception e) {
			log.error( "Error: onProductoCatalogo ", e );
			throw e;
		}		
		return new ArrayList<Producto>();
	}
	
	/**
	 * M�todo buscar, realiza la busqueda seg�n los datos cargados
	 * previamente. Requisitos: setear las siguientes variables: hql, hqlCount
	 * Si lo que se requiere es obtener un listado de la entidad, dejar vacio la
	 * variable cadenaBusqueda
	 * 
	 * @return
	 */
	public List<Producto> buscar() {
		log.info( "ingreso a buscar productos." );
		String hql_aux = "";
		String hql_filtro_aux = armarFiltroMultiple();
		if (!hql_filtro_aux.trim().isEmpty()) {
			hql_aux = Constantes.AND + Constantes.ABRIR_PARENTESIS
					+ hql_filtro_aux + Constantes.CERRAR_PARENTESIS;
		}
		// hql_filtro_aux = ( hql_filtro_aux.trim().isEmpty() ? Constantes.VACIO
		// : Constantes.WHERE ) + hql_filtro_aux;
		hql = hql + hql_aux + HQL_orderBy;
		hqlCount = hqlCount + hql_aux;
		log.info("hql: " + hql);
		log.info("hqlCount: " + hqlCount);
		try {
			consultaCount(hqlCount, mapa);
			if (cantidadRegistros == 0) {
				pagina = 0;
				return new ArrayList<Producto>();
			}
			pagina = 1;
			return Consulta(hql, pagina, mapa);
		} catch (Exception e) {
			log.info("baseProducto.buscar", e);
		}
		return new ArrayList<Producto>();
	}

	@Override
	public List firstpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_FIRST));
	}

	@Override
	public List previouspage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_PREVIOUS));
	}

	@Override
	public List nextpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_NEXT));
	}

	@Override
	public List lastpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_LAST));
	}

}
