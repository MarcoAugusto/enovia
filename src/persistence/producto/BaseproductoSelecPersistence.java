/**
 * 
 */
package persistence.producto;

import bean.util.BaseBean;
import bean.util.BaseSql;
import bean.util.Constantes;
import bean.util.IPaginationPersistence;
import bean.util.IValidator;
import entidad.Baseproducto;
import entidad.Bitacora;
import entidad.Estilo;
import entidad.Marca;
import entidad.Prenda;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.faces.bean.CustomScoped;
import javax.inject.Named;
import javax.persistence.NamedQuery;

/**
 * @author penama
 * 
 */
@Named
public class BaseproductoSelecPersistence extends BaseSql implements Serializable {

	private String HQL_prendas = "select DISTINCT c.prenda from Baseproducto c where c.activo = true ";
	private String HQL_marcas = "select DISTINCT c.marca from Baseproducto c where c.activo = true and c.prenda.code = :prenda ";
	private String HQL_estilos = "select DISTINCT c.estilo from Baseproducto c where c.activo = true and c.prenda.code = :prenda and c.marca.code = :marca ";	
	private String HQL_Baseproducto = "select c from Baseproducto c where c.activo = true and c.prenda.code = :prenda and c.marca.code = :marca and c.estilo.code = :estilo ";
		
	public void init() {		
		this.iniLogs(this.getClass());			
	}
	
	/**
	 * Obtiene las prendas que se tiene asociadas a un base producto.
	 * @return
	 */
	public List<Prenda> obtenerPrendasAsociadas(){
		log.info( "Obteniendo prendas asociadas al Catalogo" );
		try {
			Object obj = persistencia.Consulta( HQL_prendas );
			if (obj != null) {
				return (List<Prenda>)obj;				
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".obtenerPrendasAsociadas", e);
		}
		return new ArrayList<Prenda>();
	}	
	
	/**
	 * obtiene las marcas asociados a un base producto, en base a un estilo seleccionado.
	 * @param estiloCode
	 * @return
	 */
	public List<Marca> obtenerMarcasAsociados( Integer prendaCode ){
		log.info( "Obteniendo marcas asociadas al Catalogo con la prenda: " + prendaCode  );
		mapa.clear();		
		mapa.put( "prenda", prendaCode );
		try {			
			Object obj = persistencia.Consulta( HQL_marcas, mapa );
			if (obj != null) {
				return (List<Marca>)obj;				
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".obtenerMarcasAsociados", e);
		}
		return new ArrayList<Marca>();
	}
	
	/**
	 * Obtiene los estilos que estan asociados en base producto
	 * @return
	 */
	public List<Estilo> obtenerEstilosAsociados( Integer prendaCode, Integer marcaCode ){
		log.info( "Obteniendo estilos asociados al Catalogo con la prenda: " + prendaCode + " marca: " + marcaCode );
		mapa.clear();
		mapa.put( "prenda", prendaCode );
		mapa.put( "marca", marcaCode );
		try {
			Object obj = persistencia.Consulta( HQL_estilos, mapa );
			if (obj != null) {
				return (List<Estilo>)obj;				
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".obtenerEstilosAsociados", e);
		}
		return new ArrayList<Estilo>();
	}
	
	public Baseproducto obtenerBaseProducto( Integer prendaCode, Integer estiloCode, Integer marcaCode ){
		log.info( "Obteniendo el catalodo con la prenda: " + prendaCode + " estilo: " + estiloCode + " marca: " + marcaCode );
		mapa.clear();
		mapa.put( "estilo", estiloCode );
		mapa.put( "marca", marcaCode );
		mapa.put( "prenda", prendaCode );
		try {			
			Object obj = persistencia.Consulta_item( HQL_Baseproducto, mapa );
			if (obj != null) {
				return (Baseproducto)obj;
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".obtenerBaseProducto", e);
		}
		return null;
	}
	
	
	
	

}
