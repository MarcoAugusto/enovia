/**
 * 
 */
package persistence.compra;

import bean.util.BaseBean;
import bean.util.BaseSql;
import bean.util.Constantes;
import bean.util.IPaginationPersistence;
import bean.util.IValidator;
import entidad.Bitacora;
import entidad.Detalleingreso;
import entidad.Producto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NamedQuery;

/**
 * @author penama
 * 
 */
@Named
public class TransitoSelectDetalleIngresoPersistence extends BaseSql implements
		IPaginationPersistence {

	private String HQL_findall = " select c from Detalleingreso c where c.activo = true and exists( select co from Compra co where co.activo = true and co.ingreso.code = c.ingreso.code and co.code = :compra ) ";
	private String HQL_findall_count = " select count(c) from Detalleingreso c where c.activo = true and exists( select co from Compra co where co.activo = true and co.ingreso.code = c.ingreso.code and co.code = :compra ) ";
	private String HQL_orderBy = " order by c.code asc ";	
	private String HQL_filtroCode = " lower(c.producto.code) like lower(:producto) ";
	private String HQL_filtroCatalogo = " lower(c.producto.baseproducto.code) like lower(:catalogo) ";
	private String HQL_filtroPrenda = " lower(c.producto.baseproducto.tipoprenda.nombre) like lower(:prenda) ";
	private String HQL_filtroEstilo = " lower(c.producto.baseproducto.estilo.nombre) like lower(:estilo) ";
	private String HQL_filtroMarca = " lower(c.producto.baseproducto.marca.nombre) like lower(:marca) ";
	private String HQL_filtroColor = " lower(c.producto.color.nombre) like lower(:color) ";
	private String HQL_filtroTalla = " lower(c.producto.talla.nombre) like lower(:talla) ";
	private String HQL_filtroNombre = " lower(c.producto.nombre) like lower(:nombre) ";
	private String HQL_filtroEstado = " lower(c.producto.estado) like lower(:estado) ";
	
	private String filtroCompra = "";
	
	@Inject
	private CompraListPersistence compralistpersistence;
	
	public void init() {
		iniLogs(this.getClass());
		mapaHqlCampos.clear();
		filtroBusqueda = "filtros";
		cadenaBusqueda = filtroBusqueda;
		mapaHqlCampos.put("code", HQL_filtroCode);
		mapaHqlCampos.put("catalogo", HQL_filtroCatalogo);
		mapaHqlCampos.put("prenda", HQL_filtroPrenda);
		mapaHqlCampos.put("estilo", HQL_filtroEstilo);
		mapaHqlCampos.put("marca", HQL_filtroMarca);
		mapaHqlCampos.put("color", HQL_filtroColor);
		mapaHqlCampos.put("talla", HQL_filtroTalla);
		mapaHqlCampos.put("nombre", HQL_filtroNombre);
		mapaHqlCampos.put("estado", HQL_filtroEstado);
		compralistpersistence.init();
	}

	public List<Detalleingreso> getLista() throws Exception {
		return getLista("");
	}

	public List<Detalleingreso> getLista(String cadenaBusqueda) {
		hql = HQL_findall;
		hqlCount = HQL_findall_count;
		this.cadenaBusqueda = cadenaBusqueda;
		return buscar();
	}

	/**
	 * Obtiene el listado de la entidad en base a un mapa<Filtro, Valor>
	 * 
	 * @param mapaAux
	 * @return
	 */
	public List<Detalleingreso> getLista(Map<String, String> mapaAux) {
		mapaHqlValores.clear();
		Iterator<Entry<String, String>> iterEntry = mapaAux.entrySet()
				.iterator();
		while (iterEntry.hasNext()) {
			Entry<String, String> entry = iterEntry.next();
			mapaHqlValores.put(entry.getKey(), entry.getValue());
		}
		cadenaBusqueda = filtroBusqueda;
		hql = HQL_findall;
		hqlCount = HQL_findall_count;
		return buscar();
	}
	
	/**
	 * M�todo buscar, realiza la busqueda seg�n los datos cargados
	 * previamente. Requisitos: setear las siguientes variables: hql, hqlCount
	 * Si lo que se requiere es obtener un listado de la entidad, dejar vacio la
	 * variable cadenaBusqueda
	 * 
	 * @return
	 */
	public List<Detalleingreso> buscar() {
		log.info( "ingreso a buscar productos." );
		String hql_aux = "";
		String hql_filtro_aux = armarFiltroMultiple();
		if (!hql_filtro_aux.trim().isEmpty()) {
			hql_aux = Constantes.AND + Constantes.ABRIR_PARENTESIS
					+ hql_filtro_aux + Constantes.CERRAR_PARENTESIS;
		}
		hql = hql + hql_aux + HQL_orderBy;
		// reemplazar el codigo de la compra antes de realizar la consulta.
		hql = hql.replace( ":compra" , "'" + filtroCompra + "'" );
		hqlCount = hqlCount.replace( ":compra" , "'" + filtroCompra + "'" );
		hqlCount = hqlCount + hql_aux;
		log.info("hql: " + hql);
		log.info("hqlCount: " + hqlCount);
		try {
			consultaCount(hqlCount, mapa);
			if (cantidadRegistros == 0) {
				pagina = 0;
				return new ArrayList<Detalleingreso>();
			}
			pagina = 1;
			return Consulta(hql, pagina, mapa);
		} catch (Exception e) {
			log.info("baseProducto.buscar", e);
		}
		return new ArrayList<Detalleingreso>();
	}

	@Override
	public List firstpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_FIRST));
	}

	@Override
	public List previouspage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_PREVIOUS));
	}

	@Override
	public List nextpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_NEXT));
	}

	@Override
	public List lastpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_LAST));
	}

	/**
	 * @return the filtroCompra
	 */
	public String getFiltroCompra() {
		return filtroCompra;
	}

	/**
	 * @param filtroCompra the filtroCompra to set
	 */
	public void setFiltroCompra(String filtroCompra) {
		this.filtroCompra = filtroCompra;
	}

}
