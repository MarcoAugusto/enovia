/**
 * 
 */
package persistence.compra;

import bean.compra.DetalleRecepcionItem;
import bean.util.BaseSql;
import bean.util.Constantes;
import entidad.Baseproducto;
import entidad.Detalleegreso;
import entidad.Detalleingreso;
import entidad.Detallerecepcion;
import entidad.Egreso;
import entidad.Ingreso;
import entidad.Recepcion;
import entidad.Producto;
import entidad.Traspaso;

import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import persistence.inventario.TraspasoPersistence;

/**
 * @author penama
 * 
 */
@Named
public class RecepcionPersistence extends BaseSql {

	private String HQL_generateCode = " select count(c) from Recepcion c ";

	private String HQL_findCountDetalleRecepcion = " select count(c) from Detallerecepcion c where c.detalletransito.code = :detalletransito ";

	@Inject
	private TraspasoPersistence traspasopersistence;

	
	public void init() {
		this.iniLogs(this.getClass());
		mapaHqlCampos.clear();
		filtroBusqueda = "filtros";
		cadenaBusqueda = filtroBusqueda;
		traspasopersistence.init();
	}

	/**
	 * Guardar recepción...
	 * @param recepcion
	 * @param detalleRecepcionList
	 * @param traspaso
	 * @param egreso
	 * @param detalleEgresoList
	 * @param ingreso
	 * @param detalleIngresoList
	 * @throws Exception
	 */
	public void save(Recepcion recepcion,
			List<DetalleRecepcionItem> detalleRecepcionList, Traspaso traspaso,
			Egreso egreso, List<Detalleegreso> detalleEgresoList,
			Ingreso ingreso, List<Detalleingreso> detalleIngresoList)
			throws Exception {
		try {
			persistencia.beginTransaccion();
			// persistir recepcion y su detalle
			persistirRecepcionIngresoDetalle(recepcion, detalleRecepcionList, traspaso, egreso, detalleEgresoList, ingreso, detalleIngresoList);
			// persistiendo todo.
			persistencia.commit();
			log.info("Commit al persistir recepcion.");
		} catch (Exception e) {
			persistencia.rollback();
			log.error("Rollback al persistir recepcion.");
			throw e;
		}

	}

	/**
	 * Persiste en la base la recepcion, el ingreso y su detalle de manera
	 * temporal, esperando el commit.
	 * 
	 * @param recepcion
	 * @param ingreso
	 * @param detalleIngresoList
	 * @throws Exception
	 */
	private void persistirRecepcionIngresoDetalle(Recepcion recepcion,
			List<DetalleRecepcionItem> detalleRecepcionList, Traspaso traspaso,
			Egreso egreso, List<Detalleegreso> detalleEgresoList,
			Ingreso ingreso, List<Detalleingreso> detalleIngresoList) throws Exception {
		
		// persistir transito y su detalle.
		traspasopersistence.persistirTransito(traspaso, egreso, detalleEgresoList, ingreso, detalleIngresoList );		
		
		String code = "";
		try {
			code = generateCode();
			if (code == null || code.trim().isEmpty()) {
				throw new Exception(
						"No se pudo generar el code secuencial de la recepción!!! ");
			}
		} catch (Exception e) {
			throw e;
		}
		recepcion.setCode(code);
		recepcion.setTraspaso( traspaso );
		// persistir recepcion
		persistencia.persist(recepcion);
		// persistir el ingreso y detalle.
		Detallerecepcion detalleRecepcion = null;
		for (Iterator iter = detalleRecepcionList.iterator(); iter.hasNext();) {
			DetalleRecepcionItem detalleRecepcionItem = (DetalleRecepcionItem) iter.next();
			if (!detalleRecepcionItem.isRecibir()) {
				continue;
			}
			detalleRecepcion = new Detallerecepcion();
			detalleRecepcion.setDetalleingreso( detalleRecepcionItem.getDetalleTransito().getDetalleingreso() );
			detalleRecepcion.setDetalletransito( detalleRecepcionItem.getDetalleTransito() );
			detalleRecepcion.setRecepcion(recepcion);
			persistencia.persist(detalleRecepcion);

			// cambiando de estado.
			Producto producto = detalleRecepcionItem.getDetalleTransito().getDetalleingreso().getProducto();
			if (producto.getEstado().equals(Constantes.ESTADO_TRANSITO)) {
				producto.setEstado(Constantes.ESTADO_ENSTOCK);
			}
			if (producto.getEstado().equals( Constantes.ESTADO_RESERVADO_TRANSITO )) {
				producto.setEstado(Constantes.ESTADO_RESERVADO);
			}
			persistencia.merge(producto);
			// cambiando precio del base producto.
			Baseproducto baseProducto = producto.getBaseproducto();
			baseProducto.setPrecio( detalleRecepcionItem.getCostoCatalogo() );
			log.info( "precio update : " + baseProducto.getCode() + "  " + baseProducto.getPrecio() );
			persistencia.merge( baseProducto );
		}
	}

	/**
	 * Obtiene el code siguiente a asignar.
	 * 
	 * @return
	 */
	public String generateCode() throws Exception {
		Object obj = persistencia.Consulta_item(HQL_generateCode);
		if (obj != null) {
			return Constantes.PREFIJO_RECEPCION
					+ (Integer.parseInt(obj.toString()) + 1);
		}
		return null;
	}

	/**
	 * verifica si el registro ya fue recepcionado.
	 * 
	 * @param detalleTransitoCode
	 * @return
	 * @throws Exception
	 */
	public boolean findItemDetalleTransitoRecepcion(Long detalleTransitoCode)
			throws Exception {
		mapa.clear();
		mapa.put("detalletransito", detalleTransitoCode);
		Object obj = persistencia.Consulta_item(HQL_findCountDetalleRecepcion,
				mapa);
		if (obj != null) {
			log.info("" + Integer.parseInt(obj.toString()) + " - "
					+ obj.toString() + " " + detalleTransitoCode);
			return Integer.parseInt(obj.toString()) > 0;
		}
		return false;
	}

}
