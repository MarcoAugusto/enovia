/**
 * 
 */
package persistence.compra;

import bean.util.BaseSql;
import entidad.Detallepedido;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;


/**
 * @author penama
 * 
 */
@Named
public class PedidoViewPersistence extends BaseSql {

	private String HQL_findDetalle = "select c from Detallepedido c where c.activo = true and c.pedido.code = :pedido ";
	private String HQL_findCompraPedido = "select c.code from Compra c where c.activo = true and c.pedido.code = :pedido ";
	/**
	 * Obtiene la cantidad de los detalles pedido que faltan por comprar.
	 */
	private String HQL_findCountDetpedido_X_Compra = " select count(dp) from Detallepedido dp where dp.activo = true and dp.pedido.code = :pedido " +  
														" and not exists( select di from Detalleingreso di where di.detallepedido.code = dp.code )";
	/**
	 * Obtiene los codigos de los detalles pedido que faltan por comprar.
	 */
	private String HQL_findCodeDetpedido_X_Compra = " select dp.code from Detallepedido dp where dp.activo = true and dp.pedido.code = :pedido " +  
													" and not exists( select di from Detalleingreso di where di.detallepedido.code = dp.code )";
	
	/**
	 * Obtiene los detalles pedido que faltan por comprar.
	 */
	private String HQL_findDetpedido_X_Compra = " select dp from Detallepedido dp where dp.activo = true and dp.pedido.code = :pedido " +  
													" and not exists( select di from Detalleingreso di where di.detallepedido.code = dp.code )";
	
	
	@PostConstruct
	public void init() {		
		this.iniLogs(this.getClass());			
	}

		
	/**
	 * Obtiene el listado de la entidad en base a un mapa<Filtro, Valor>
	 * @param mapaAux
	 * @return
	 */
	public List<Detallepedido> getDetallePedido( String pedido ) throws Exception {
		mapa.clear();
		mapa.put( "pedido", pedido );		
		try {
			Object obj = persistencia.Consulta(HQL_findDetalle, mapa );
			if (obj != null) {
				return (List<Detallepedido>)obj ;				
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".getDetallePedido", e);
			throw e;
		}
		return new ArrayList<Detallepedido>();
	}	
	
	/**
	 * Obtiene el c�digo de la compra en base al pedido.
	 * @param pedido
	 * @return code Compra
	 */
	public String getCompra( String pedido ) throws Exception {
		mapa.clear();
		mapa.put( "pedido", pedido );		
		try {
			Object obj = persistencia.Consulta(HQL_findCompraPedido, mapa );
			if (obj != null) {
				return String.valueOf( obj ) ;				
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".getCompra", e);
			throw e;
		}
		return "";
	}
	
	/**
	 * Obtiene la cantidad de registros que faltan por comprar
	 * @param pedido
	 * @return
	 * @throws Exception
	 */
	public int getCountDetpedido_X_Compra( String pedido ) throws Exception {
		mapa.clear();
		mapa.put( "pedido", pedido );		
		try {
			Object obj = persistencia.Consulta_item(HQL_findCountDetpedido_X_Compra, mapa );
			if (obj != null) {				
				return Integer.valueOf( obj.toString() ) ;				
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".getCompra", e);
			throw e;
		}
		return 0;
	}
	
	/**
	 * Obtiene los c�digos de los registros que faltan por comprar
	 * @param pedido
	 * @return
	 * @throws Exception
	 */
	public List<Long> getCodeDetpedido_X_Compra( String pedido ) throws Exception {
		mapa.clear();
		mapa.put( "pedido", pedido );		
		try {
			Object obj = persistencia.Consulta(HQL_findCodeDetpedido_X_Compra, mapa );
			log.info( "resp: " + obj );
			if (obj != null) {				
				return (ArrayList<Long>)obj;				
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".getCompra", e);
			throw e;
		}
		return new ArrayList<Long>();
	}
	
	/**
	 * Obtiene los detalles de pedido faltantes por comprar.
	 * @param pedido
	 * @return
	 * @throws Exception
	 */
	public List<Detallepedido> getDetpedido_X_Compra( String pedido ) throws Exception {
		mapa.clear();
		mapa.put( "pedido", pedido );		
		try {
			Object obj = persistencia.Consulta(HQL_findDetpedido_X_Compra, mapa );
			log.info( "resp: " + obj );
			if (obj != null) {				
				return (ArrayList<Detallepedido>)obj;				
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".getCompra", e);
			throw e;
		}
		return new ArrayList<Detallepedido>();
	}
}
