/**
 * 
 */
package persistence.compra;

import bean.util.BaseSql;
import bean.util.Constantes;
import bean.util.IPaginationPersistence;
import entidad.Detalleingreso;
import entidad.Detalletransito;
import entidad.Ingreso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.inject.Named;


/**
 * @author penama
 * 
 */
@Named
public class TransitoViewPersistence extends BaseSql {

	private String HQL_findDetalle = "select c from Detalletransito c where c.transito.code = :transito ";
	
	@PostConstruct
	public void init() {		
		this.iniLogs(this.getClass());			
	}

		
	/**
	 * Obtiene el listado de la entidad en base a un mapa<Filtro, Valor>
	 * @param mapaAux
	 * @return
	 */
	public List<Detalletransito> getDetalleTransito( String transito ) throws Exception {
		mapa.clear();
		mapa.put( "transito", transito );		
		try {
			Object obj = persistencia.Consulta(HQL_findDetalle, mapa );
			if (obj != null) {
				return (List<Detalletransito>)obj ;				
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".getDetalleTransito", e);
			throw e;
		}
		return new ArrayList<Detalletransito>();
	}	
		
	
}
