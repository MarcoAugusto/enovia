/**
 * 
 */
package persistence.compra;

import bean.util.BaseSql;
import bean.util.Constantes;
import bean.util.IPaginationPersistence;
import entidad.Pedido;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Named;

/**
 * @author penama
 * 
 */
@Named
public class PedidoListPersistence extends BaseSql implements IPaginationPersistence {

	private String HQL_findall = " select c from Pedido c where c.activo = true ";
	private String HQL_findall_count = " select count(c) from Pedido c where c.activo = true ";
	private String HQL_orderBy = " order by c.fecha desc ";
	private String HQL_findPedido = " select c from Pedido c where c.activo = true and c.code = :pedido ";	
	
	private String HQL_filtroCode = " lower(c.code) like lower(:code) ";
	private String HQL_filtroComentario = " lower(c.comentario) like lower(:comentario) ";
	private String HQL_filtroFecha = " to_char( c.fecha, 'dd/MM/yyyy HH:mm:ss' ) like :fecha ";
	private String HQL_filtroAlmacen = " lower( c.almacen.code ) like lower(:almacen) ";
	private String HQL_filtroProveedor = " lower( c.proveedor.code ) like lower(:proveedor) ";
	private String HQL_filtroUsuario = " lower( c.usuario.nombre ) like lower(:usuario) ";
	
	public void init() {		
		this.iniLogs(this.getClass());
		mapaHqlCampos.clear();
		filtroBusqueda = "filtros";
		cadenaBusqueda = filtroBusqueda;
		mapaHqlCampos.put( "code" , HQL_filtroCode );
		mapaHqlCampos.put( "comentario" , HQL_filtroComentario );		 
		mapaHqlCampos.put( "fecha" , HQL_filtroFecha );
		mapaHqlCampos.put( "almacen" , HQL_filtroAlmacen );
		mapaHqlCampos.put( "proveedor" , HQL_filtroProveedor );
		mapaHqlCampos.put( "usuario" , HQL_filtroUsuario );
	}

	public List<Pedido> getLista() throws Exception {		
		return getLista( "" );
	}

	public List<Pedido> getLista( String cadenaBusqueda ){
		hql = HQL_findall;
		hqlCount = HQL_findall_count;
		this.cadenaBusqueda = cadenaBusqueda;
		return buscar();
	}
	
	/**
	 * Obtiene el listado de la entidad en base a un mapa<Filtro, Valor>
	 * @param mapaAux
	 * @return
	 */
	public List<Pedido> getLista( Map<String, String> mapaAux ){
		mapaHqlValores.clear();		
		Iterator<Entry<String, String>> iterEntry = mapaAux.entrySet().iterator();
		while (iterEntry.hasNext()) {
			Entry<String, String> entry = iterEntry.next();			
			mapaHqlValores.put(entry.getKey(), entry.getValue());			
		}
		cadenaBusqueda = filtroBusqueda;
		hql = HQL_findall;
		hqlCount = HQL_findall_count;
		return buscar();
	}	
	
	/**
	 * M�todo buscar, realiza la busqueda seg�n los datos cargados previamente.
	 * Requisitos: setear las siguientes variables: hql, hqlCount
	 * Si lo que se requiere es obtener un listado de la entidad, dejar vacio la variable cadenaBusqueda
	 * @return
	 */
	public List<Pedido> buscar(){
		String hql_aux = "";
		String hql_filtro_aux = armarFiltroMultiple();		
		if ( !hql_filtro_aux.trim().isEmpty() ){
			hql_aux = Constantes.AND + Constantes.ABRIR_PARENTESIS + hql_filtro_aux + Constantes.CERRAR_PARENTESIS;
		}
//		hql_filtro_aux = ( hql_filtro_aux.trim().isEmpty() ? Constantes.VACIO : Constantes.WHERE ) + hql_filtro_aux;		
		hql = hql + hql_aux + HQL_orderBy;
		hqlCount = hqlCount + hql_aux;
		log.info( "hql: " +  hql );
		log.info( "hqlCount: " +  hqlCount );
		try {			
			consultaCount(hqlCount, mapa);			
			if ( cantidadRegistros == 0 ){
				pagina = 0;
				return new ArrayList<Pedido>();
			}
			pagina = 1;
			return Consulta(hql, pagina, mapa );
		} catch (Exception e) {
			log.info( "baseProducto.buscar", e );			
		}
		return new ArrayList<Pedido>();
	}
	
	/**
	 * Obtiene el objeto pedido.
	 * @param pedido
	 * @return
	 * @throws Exception
	 */
	public Pedido findPedido( String pedido ) throws Exception {
		if ( pedido == null || pedido.isEmpty() ){
			log.info( "c�digo del pedido es vacio o nulo" );
			return null;
		}
		mapa.clear();
		mapa.put( "pedido", pedido );
		Object obj = persistencia.Consulta_item(HQL_findPedido, mapa );
		if (obj != null) {
			return (Pedido)obj ;
		}
		return null;
	}
	
	@Override
	public List firstpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_FIRST));
	}

	@Override
	public List previouspage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_PREVIOUS));
	}

	@Override
	public List nextpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_NEXT));
	}

	@Override
	public List lastpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_LAST));
	}

}
