/**
 * 
 */
package persistence.compra;

import bean.compra.DetalleTransitoItem;
import bean.util.BaseSql;
import bean.util.Constantes;
import entidad.Detalletransito;
import entidad.Transito;
import entidad.Detalleingreso;
import entidad.Producto;

import java.util.Iterator;
import java.util.List;

import javax.inject.Named;

/**
 * @author penama
 * 
 */
@Named
public class TransitoPersistence extends BaseSql {

	private String HQL_findall = " select c from Detalleingreso c " +
			"where c.activo = true " +
			"and ( c.producto.estado = '" + Constantes.ESTADO_PEDIDO + "' or c.producto.estado = '" + Constantes.ESTADO_RESERVADO_PEDIDO + "' ) " + 
			"and exists( select co from Compra co " +
				"where co.activo = true and co.ingreso.code = c.ingreso.code and co.code = :compra ) ";
	
	private String HQL_findall2 = " select c from Detalleingreso c where c.activo = true " +
			"and exists( select co from Compra co " +
				"where co.activo = true and co.ingreso.code = c.ingreso.code and co.code = :compra ) ";
	
	private String HQL_findCountDetalleTransito = " select count(c) from Detalletransito c where c.detalleingreso.code = :detalleingreso ";
	
//	private String HQL_findall_count = " select count(c) from Detalleingreso c where c.activo = true and exists( select co from Compra co where co.activo = true and co.ingreso.code = c.ingreso.code and co.code = :compra ) ";
	private String HQL_generateCode = " select count(c) from Transito c ";
	
	
	
	public void init() {
		this.iniLogs(this.getClass());
		mapaHqlCampos.clear();
		filtroBusqueda = "filtros";
		cadenaBusqueda = filtroBusqueda;		
	}

	/**
	 * Guardar la transito
	 * @param transito
	 * @param compra
	 * @param detalleIngresoList
	 * @throws Exception
	 */
	public void save(Transito transito, List<DetalleTransitoItem> detalleTransitoList) throws Exception {
//		String code = generateCode();
//		if (code == null || code.trim().isEmpty()) {
//			throw new Exception( "No se pudo generar el code secuencial del transito!!! " );
//		}
//		transito.setCode(code);
		try {
			persistencia.beginTransaccion();
			// persistir la transito, la compra y el detalle sin commit.
			persistirTransitoIngresoDetalle(transito, detalleTransitoList);
			// persistiendo todo.
			persistencia.commit();
			log.info("Commit al persistir transito.");
		} catch (Exception e) {
			persistencia.rollback();
			log.error("Rollback al persistir transito.");
			throw e;
		}

	}

	/**
	 * Persiste en la base la transito, el ingreso y su detalle de manera temporal, esperando el commit.
	 * @param transito
	 * @param ingreso
	 * @param detalleIngresoList
	 * @throws Exception
	 */
	private void persistirTransitoIngresoDetalle(Transito transito, List<DetalleTransitoItem> detalleTransitoList) throws Exception {
		String code = "";
		try {
			code = generateCode();
			if (code == null || code.trim().isEmpty()) {
				throw new Exception( "No se pudo generar el code secuencial del ingreso!!! ");
			}
		} catch (Exception e) {
			throw e;
		}
		transito.setCode(code);
		// persistir transito
		persistencia.persist( transito );
		// persistir el detalle.
		Detalletransito detalleTransito = null;
		for (Iterator iter = detalleTransitoList.iterator(); iter.hasNext();) {
			DetalleTransitoItem detalleTransitoItem = (DetalleTransitoItem) iter.next();
//			if ( !detalleTransitoItem.isTransitoar() ){
//				continue;
//			}
			detalleTransito = new Detalletransito();
			detalleTransito.setDetalleingreso( detalleTransitoItem.getDetalleIngreso() );
			detalleTransito.setTransito( transito );
			
			persistencia.persist( detalleTransito );
			
			Producto producto = detalleTransitoItem.getDetalleIngreso().getProducto();
			if ( producto.getEstado().equals( Constantes.ESTADO_PEDIDO ) ){
				producto.setEstado( Constantes.ESTADO_TRANSITO );
			}
			if ( producto.getEstado().equals( Constantes.ESTADO_RESERVADO_PEDIDO ) ){
				producto.setEstado( Constantes.ESTADO_RESERVADO_TRANSITO );
			}
			persistencia.merge( producto );
		}		
	}
	
	/**
	 * Obtiene el code siguiente a asignar.
	 * 
	 * @return
	 */
	public String generateCode() throws Exception {
		Object obj = persistencia.Consulta_item(HQL_generateCode);
		if (obj != null) {
			return Constantes.PREFIJO_TRANSITO + (Integer.parseInt(obj.toString()) + 1);
		}
		return null;
	}
	
	/**
	 * Obtiene el listado de detalleingreso a partir del code de la compra.
	 * @param compra
	 * @return
	 * @throws Exception
	 */
	public List<Detalleingreso> findDetalleIngresoCompra( String compra ) throws Exception {
		mapa.clear();
		mapa.put( "compra", compra );
		Object obj = persistencia.Consulta( HQL_findall2, mapa );
		if (obj != null) {
			return (List<Detalleingreso>)obj;
		}
		return null;
	}
	
	/**
	 * Indica si el registro paso a estar en transito.
	 * @param detalleIngresoCode
	 * @return true si tiene registro.
	 * @throws Exception
	 */
	public boolean findItemDetalleIngresoTransito( Long detalleIngresoCode ) throws Exception {
		mapa.clear();
		mapa.put( "detalleingreso", detalleIngresoCode );
		Object obj = persistencia.Consulta_item( HQL_findCountDetalleTransito, mapa );
		if (obj != null) {
			log.info( "" +  Integer.parseInt(obj.toString()) + " - " + obj.toString() + " " + detalleIngresoCode  );
			return Integer.parseInt(obj.toString()) > 0 ;			
		}
		return false;
	}	

}
