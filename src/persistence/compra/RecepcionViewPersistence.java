/**
 * 
 */
package persistence.compra;

import bean.util.BaseSql;
import entidad.Detallerecepcion;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Named;


/**
 * @author penama
 * 
 */
@Named
public class RecepcionViewPersistence extends BaseSql {

	private String HQL_findDetalle = "select c from Detallerecepcion c where c.recepcion.code = :recepcion ";
	
	@PostConstruct
	public void init() {		
		this.iniLogs(this.getClass());			
	}

		
	/**
	 * Obtiene el listado de la entidad en base a un mapa<Filtro, Valor>
	 * @param mapaAux
	 * @return
	 */
	public List<Detallerecepcion> getDetalleRecepcion( String recibido ) throws Exception {
		mapa.clear();
		mapa.put( "recepcion", recibido );		
		try {
			Object obj = persistencia.Consulta(HQL_findDetalle, mapa );
			if (obj != null) {
				return (List<Detallerecepcion>)obj ;				
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".getDetalleRecepcion", e);
			throw e;
		}
		return new ArrayList<Detallerecepcion>();
	}	
		
	
}
