/**
 * 
 */
package persistence.compra;

import bean.util.BaseBean;
import bean.util.BaseSql;
import bean.util.Constantes;
import entidad.Baseproducto;
import entidad.Bitacora;
import entidad.Detallepedido;
import entidad.Pedido;
import entidad.Detalleingreso;
import entidad.Detallereserva;
import entidad.Ingreso;
import entidad.Producto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NamedQuery;

import persistence.inventario.IngresoPersistence;
import persistence.producto.ProductoPersistence;

/**
 * @author penama
 * 
 */
@Named
public class PedidoPersistence extends BaseSql {

	private String HQL_generateCode = " select count(c) from Pedido c ";

	@Inject
	private ProductoPersistence productopersistence;
	
	public void init() {
		this.iniLogs(this.getClass());
		mapaHqlCampos.clear();
		filtroBusqueda = "filtros";
		cadenaBusqueda = filtroBusqueda;
	}

	/**
	 * Guardar la pedido
	 * @param pedido
	 * @param ingreso
	 * @param detalleIngresoList
	 * @throws Exception
	 */
	public void save(Pedido pedido, List<Detallepedido> detallePedidoList) throws Exception {		
		try {
			persistencia.beginTransaccion();
			// persistir el pedido y el detalle sin commit.
			persistirPedidoDetalle(pedido, detallePedidoList);
			// persistiendo todo.
			persistencia.commit();
			log.info("Commit al persistir pedido.");
		} catch (Exception e) {
			persistencia.rollback();
			log.error("Rollback al persistir pedido.");
			throw e;
		}

	}

	/**
	 * Persiste en la base la pedido, el ingreso y su detalle de manera temporal, esperando el commit.
	 * @param pedido
	 * @param ingreso
	 * @param detallePedidoList
	 * @throws Exception
	 */
	private void persistirPedidoDetalle(Pedido pedido, List<Detallepedido> detallePedidoList) throws Exception {
		String code = "";
		try {
			code = generateCode();
			if (code == null || code.trim().isEmpty()) {
				throw new Exception( "No se pudo generar el code secuencial del pedido!!! ");
			}
		} catch (Exception e) {
			throw e;
		}
		pedido.setCode(code);
		// guardando pedido
		persistencia.persist(pedido);
		// persistir el detalle pedido.
		Detallepedido detallePedido = null;
		for (Iterator iterator = detallePedidoList.iterator(); iterator.hasNext();) {
			detallePedido = (Detallepedido) iterator.next();
			detallePedido.setActivo( true );
			detallePedido.setPedido( pedido );
			
			Producto producto = detallePedido.getProducto();
			String codeProducto = "";
			try {
				codeProducto = productopersistence.generateCode();
				if (codeProducto == null || codeProducto.trim().isEmpty()) {
					throw new Exception(
							"Error: No se pudo generar el code secuencial del producto! ");
				}
			} catch (Exception e) {
				throw e;
			}
			producto.setCode(codeProducto);
			producto.setAlmacen( pedido.getAlmacen() );
			persistencia.persist(producto);
			
			persistencia.persist( detallePedido );
		}
		
	}
	
	/**
	 * Obtiene el code siguiente a asignar.
	 * 
	 * @return
	 */
	public String generateCode() throws Exception {
		Object obj = persistencia.Consulta_item(HQL_generateCode);
		if (obj != null) {
			return Constantes.PREFIJO_PEDIDO + (Integer.parseInt(obj.toString()) + 1);
		}
		return null;
	}
	
	
//	public void updateDatellePedidoCompra( List<Detallepedido> detallePedidoList ) throws Exception{
//		fo
//	}
	

}
