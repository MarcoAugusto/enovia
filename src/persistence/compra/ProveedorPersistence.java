/**
 * 
 */
package persistence.compra;

import bean.util.BaseBean;
import bean.util.BaseSql;
import bean.util.Constantes;
import bean.util.IPaginationPersistence;
import bean.util.IValidator;
import entidad.Almacen;
import entidad.Bitacora;
import entidad.Proveedor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Named;
import javax.persistence.NamedQuery;

/**
 * @author penama
 * 
 */
@Named
public class ProveedorPersistence extends BaseSql implements IPaginationPersistence {

	private String HQL_findall = " select c from Proveedor c where c.activo = true ";
	private String HQL_findall_count = " select count(c) from Proveedor c where c.activo = true ";
	private String HQL_findCode = " select c from Proveedor c where c.activo = true and c.code = :code ";
	private String HQL_orderBy = " order by c.nombre asc ";	
	private String HQL_validarXnombre = "select count(c) from Proveedor c where c.activo = true and lower(c.nombre) = lower(:nombre) ";
	private String HQL_validarXnombreDid = "select count(c) from Proveedor c where c.activo = true and lower(c.nombre) = lower(:nombre) and c.code <> :code ";
	private String HQL_generateCode = " select count(c) from Proveedor c ";
	private String HQL_filtroCode = " lower(c.code) like lower(:code) ";
	private String HQL_filtroCodigo = " lower(c.codigo) like lower(:codigo) ";
	private String HQL_filtroNombre = " lower(c.nombre) like lower(:nombre) ";
	private String HQL_filtroContacto = " lower(c.contacto) like lower(:contacto) ";
	private String HQL_filtroDireccion = " lower(c.direccion) like lower(:direccion) ";
	private String HQL_filtroTelefono = " (lower(c.telefono) like lower(:telefono) or lower(c.contactoTelefono) like lower(:telefono)) ";	
	
	public void init() {		
		this.iniLogs(this.getClass());
		mapaHqlCampos.clear();
		filtroBusqueda = "filtros";
		cadenaBusqueda = filtroBusqueda;		
		mapaHqlCampos.put( "code" , HQL_filtroCode );
		mapaHqlCampos.put( "codigo" , HQL_filtroCodigo );
		mapaHqlCampos.put( "nombre" , HQL_filtroNombre );		
		mapaHqlCampos.put( "contacto" , HQL_filtroContacto );
		mapaHqlCampos.put( "direccion" , HQL_filtroDireccion );
		mapaHqlCampos.put( "telefono" , HQL_filtroTelefono );		
	}

	public List<Proveedor> getLista() throws Exception {		
		return getLista( "" );
	}

	public List<Proveedor> getLista( String cadenaBusqueda ){
		hql = HQL_findall;
		hqlCount = HQL_findall_count;
		this.cadenaBusqueda = cadenaBusqueda;
		return buscar();
	}
	
	/**
	 * Obtiene el listado de la entidad en base a un mapa<Filtro, Valor>
	 * @param mapaAux
	 * @return
	 */
	public List<Proveedor> getLista( Map<String, String> mapaAux ){
		mapaHqlValores.clear();		
		Iterator<Entry<String, String>> iterEntry = mapaAux.entrySet().iterator();
		while (iterEntry.hasNext()) {
			Entry<String, String> entry = iterEntry.next();			
			mapaHqlValores.put(entry.getKey(), entry.getValue());			
		}
		cadenaBusqueda = filtroBusqueda;
		hql = HQL_findall;
		hqlCount = HQL_findall_count;
		return buscar();
	}
	
	
	public void save(Proveedor proveedor) throws Exception {
		String code = generateCode();		
		if ( code == null || code.trim().isEmpty() ){
			throw new Exception( this.getClass().getName() + ".save: No se pudo generar el code secuencial!!! " );
		}
		proveedor.setCode( code );
		persistencia.save(proveedor);		
	}
	
	/**
	 * Obtiene el code siguiente a asignar.
	 * @return
	 */
	public String generateCode(){
		try {
			Object obj = persistencia.Consulta_item(HQL_generateCode);
			if (obj != null) {
				return Constantes.PREFIJO_PROVEEDOR + (Integer.parseInt( obj.toString() ) + 1) ;				
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".generateCode", e);
		}
		return null;
	}
	
	public void update(Proveedor proveedor) throws Exception {				
		persistencia.update(proveedor);
	}

	
	public String validarCampos(Object entidad, boolean nuevo) {
		if (entidad != null && entidad instanceof Proveedor) {
			StringBuilder sb = new StringBuilder();
			Proveedor entidadProveedor = (Proveedor) entidad;
			log.info("Entidadproveedor: " + entidadProveedor.getCode() + " " + entidadProveedor.getNombre() + " " + nuevo );
			if (entidadProveedor.getCodigo() == null || entidadProveedor.getCodigo().trim().isEmpty()) {
				sb.append("Campo codigo es requerido");
				sb.append(", ");
			}
			if (entidadProveedor.getNombre() == null || entidadProveedor.getNombre().trim().isEmpty()) {
				sb.append("Campo nombre es requerido");
				sb.append(", ");
			}
			if ( entidadProveedor.getDireccion() == null || entidadProveedor.getDireccion().trim().isEmpty() ){
				sb.append("Campo direcci�n es requerido");
				sb.append(", ");
			}
			if ( entidadProveedor.getTelefono() == null || entidadProveedor.getTelefono().trim().isEmpty() ){
				sb.append("Campo telefono es requerido");
				sb.append(", ");
			}
			if ( entidadProveedor.getContacto() == null || entidadProveedor.getContacto().trim().isEmpty() ){
				sb.append("Campo contacto es requerido");
				sb.append(", ");
			}
			if ( entidadProveedor.getContactoTelefono() == null || entidadProveedor.getContactoTelefono().trim().isEmpty() ){
				sb.append("Campo telefono del contacto es requerido");
				sb.append(", ");
			}
			if (nuevo) {
				if (existeXnombre(entidadProveedor.getNombre().trim())) {
					sb.append("Ya existe una proveedor con el mismo nombre");
				}
			} else {
				if (existeXnombreDid(entidadProveedor.getNombre().trim(), entidadProveedor.getCode())) {
					sb.append("Ya existe una proveedor con el mismo nombre");
				}
			}
			log.info("cadena: " + sb.toString());
			return sb.toString();
		}
		return "";
	}

	public boolean existeXnombre(String nombre) {
		mapa.clear();
		mapa.put("nombre", nombre);
		try {
			Object obj = persistencia.Consulta_item(HQL_validarXnombre, mapa);
			if (obj != null) {				
				if (Integer.parseInt(obj.toString()) > 1) {
					return true;
				}
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".existeXnombre", e);
		}
		return false;
	}
	
	public boolean existeXnombreDid(String nombre, String code) {
		log.info("Entidadproveedor: " + code + " " + nombre);
		mapa.clear();
		mapa.put("code", code );
		mapa.put("nombre", nombre);
		try {
			Object obj = persistencia.Consulta_item(HQL_validarXnombreDid, mapa);
			if (obj != null) {
				if (Integer.parseInt(obj.toString()) > 1) {
					return true;
				}
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".existeXnombreDid", e);
		}
		return false;
	}
	
	/**
	 * Obtener el proveedor apartir del codigo.
	 * @param code
	 * @return
	 */
	public Proveedor findProveedorCode( String code ) throws Exception {		
		mapa.clear();
		mapa.put("code", code );		
		try {
			Object obj = persistencia.Consulta_item(HQL_findCode, mapa);
			if (obj != null) {
				return (Proveedor)obj;
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".findProveedorCode", e);
			throw e;
		}
		return null;
	}
	
	/**
	 * M�todo buscar, realiza la busqueda seg�n los datos cargados previamente.
	 * Requisitos: setear las siguientes variables: hql, hqlCount
	 * Si lo que se requiere es obtener un listado de la entidad, dejar vacio la variable cadenaBusqueda
	 * @return
	 */
	public List<Proveedor> buscar(){
		String hql_aux = "";
		String hql_filtro_aux = armarFiltroMultiple();		
		if ( !hql_filtro_aux.trim().isEmpty() ){
			hql_aux = Constantes.AND + Constantes.ABRIR_PARENTESIS + hql_filtro_aux + Constantes.CERRAR_PARENTESIS;
		}
//		hql_filtro_aux = ( hql_filtro_aux.trim().isEmpty() ? Constantes.VACIO : Constantes.WHERE ) + hql_filtro_aux;		
		hql = hql + hql_aux + HQL_orderBy;
		hqlCount = hqlCount + hql_aux;
		log.info( "hql: " +  hql );
		log.info( "hqlCount: " +  hqlCount );
		try {			
			consultaCount(hqlCount, mapa);			
			if ( cantidadRegistros == 0 ){
				pagina = 0;
				return new ArrayList<Proveedor>();
			}
			pagina = 1;
			return Consulta(hql, pagina, mapa );
		} catch (Exception e) {
			log.info( "proveedor.buscar", e );			
		}
		return new ArrayList<Proveedor>();
	}
	
	@Override
	public List firstpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_FIRST));
	}

	@Override
	public List previouspage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_PREVIOUS));
	}

	@Override
	public List nextpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_NEXT));
	}

	@Override
	public List lastpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_LAST));
	}

}
