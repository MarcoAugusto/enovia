/**
 * 
 */
package persistence.venta;

import bean.util.BaseBean;
import bean.util.BaseSql;
import bean.util.Constantes;
import entidad.Baseproducto;
import entidad.Bitacora;
import entidad.Compra;
import entidad.Detalleingreso;
import entidad.Detallereserva;
import entidad.Ingreso;
import entidad.Producto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NamedQuery;

import persistence.inventario.IngresoPersistence;
import persistence.producto.ProductoPersistence;

/**
 * @author penama
 * 
 */
@Named
public class VentaPersistence extends BaseSql {

	private String HQL_generateCode = " select count(c) from Compra c ";

	@Inject
	private IngresoPersistence ingresopersistence;

	public void init() {
		this.iniLogs(this.getClass());
		mapaHqlCampos.clear();
		filtroBusqueda = "filtros";
		cadenaBusqueda = filtroBusqueda;
		ingresopersistence.init();
	}

	/**
	 * Guardar la compra
	 * @param compra
	 * @param ingreso
	 * @param detalleIngresoList
	 * @throws Exception
	 */
	public void save(Compra compra, Ingreso ingreso, List<Detalleingreso> detalleIngresoList) throws Exception {
		String code = generateCode();
		if (code == null || code.trim().isEmpty()) {
			throw new Exception( "No se pudo generar el code secuencial del compra!!! " );
		}
		compra.setCode(code);
		try {
			persistencia.beginTransaccion();
			// persistir la compra, el ingreso y el detalle sin commit.
			persistirCompraIngresoDetalle(compra, ingreso, detalleIngresoList);
			// persistiendo todo.
			persistencia.commit();
			log.info("Commit al persistir compra.");
		} catch (Exception e) {
			persistencia.rollback();
			log.error("Rollback al persistir compra.");
			throw e;
		}

	}

	/**
	 * Persiste en la base la compra, el ingreso y su detalle de manera temporal, esperando el commit.
	 * @param compra
	 * @param ingreso
	 * @param detalleIngresoList
	 * @throws Exception
	 */
	private void persistirCompraIngresoDetalle(Compra compra, Ingreso ingreso, List<Detalleingreso> detalleIngresoList) throws Exception {
		String code = "";
		try {
			code = generateCode();
			if (code == null || code.trim().isEmpty()) {
				throw new Exception( "No se pudo generar el code secuencial del ingreso!!! ");
			}
		} catch (Exception e) {
			throw e;
		}
		compra.setCode(code);
		// persistir el ingreso y detalle.
		ingresopersistence.persistirIngresoDetalle(ingreso, detalleIngresoList);
		// asignando el ingreso a la compra.
		compra.setIngreso(ingreso);
		// guardando compra
		persistencia.persist(compra);
	}
	
	/**
	 * Obtiene el code siguiente a asignar.
	 * 
	 * @return
	 */
	public String generateCode() throws Exception {
		Object obj = persistencia.Consulta_item(HQL_generateCode);
		if (obj != null) {
			return Constantes.PREFIJO_COMPRA + (Integer.parseInt(obj.toString()) + 1);
		}
		return null;
	}

}
