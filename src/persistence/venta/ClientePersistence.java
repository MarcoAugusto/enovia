/**
 * 
 */
package persistence.venta;

import bean.util.BaseBean;
import bean.util.BaseSql;
import bean.util.Constantes;
import bean.util.IPaginationPersistence;
import bean.util.IValidator;
import entidad.Almacen;
import entidad.Bitacora;
import entidad.Cliente;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Named;
import javax.persistence.NamedQuery;

/**
 * @author penama
 * 
 */
@Named
public class ClientePersistence extends BaseSql implements IPaginationPersistence {

	private String HQL_findall = " select c from Cliente c where c.activo = true ";
	private String HQL_findall_count = " select count(c) from Cliente c where c.activo = true ";
	private String HQL_findCode = " select c from Cliente c where c.activo = true and c.code = :code ";
	private String HQL_orderBy = " order by c.nombre asc ";	
	private String HQL_validarXnombre = "select count(c) from Cliente c where c.activo = true and lower(c.nombre) = lower(:nombre) ";
	private String HQL_validarXnombreDid = "select count(c) from Cliente c where c.activo = true and lower(c.nombre) = lower(:nombre) and c.code <> :code ";
	private String HQL_generateCode = " select count(c) from Cliente c ";
	private String HQL_filtroNombre = " lower(c.nombre) like lower(:nombre) ";
	private String HQL_filtroTelefono = " lower(c.telefono) like lower(:telefono) ";
	private String HQL_filtroOpciones = " lower(c.opciones) like lower(:opciones) ";
	
	public void init() {		
		this.iniLogs(this.getClass());
		mapaHqlCampos.clear();
		filtroBusqueda = "filtros";
		cadenaBusqueda = filtroBusqueda;		
		mapaHqlCampos.put( "nombre" , HQL_filtroNombre );
		mapaHqlCampos.put( "telefono" , HQL_filtroTelefono );
		mapaHqlCampos.put( "opciones" , HQL_filtroOpciones );
	}

	public List<Cliente> getLista() throws Exception {		
		return getLista( "" );
	}

	public List<Cliente> getLista( String cadenaBusqueda ){
		hql = HQL_findall;
		hqlCount = HQL_findall_count;
		this.cadenaBusqueda = cadenaBusqueda;
		return buscar();
	}
	
	/**
	 * Obtiene el listado de la entidad en base a un mapa<Filtro, Valor>
	 * @param mapaAux
	 * @return
	 */
	public List<Cliente> getLista( Map<String, String> mapaAux ){
		mapaHqlValores.clear();		
		Iterator<Entry<String, String>> iterEntry = mapaAux.entrySet().iterator();
		while (iterEntry.hasNext()) {
			Entry<String, String> entry = iterEntry.next();			
			mapaHqlValores.put(entry.getKey(), entry.getValue());			
		}
		cadenaBusqueda = filtroBusqueda;
		hql = HQL_findall;
		hqlCount = HQL_findall_count;
		return buscar();
	}
	
	
	public void save(Cliente cliente) throws Exception {
		if ( cliente == null ){
			throw new Exception( this.getClass().getName() + ".save: cliente nulo!! " );
		}
		persistencia.save(cliente);		
	}
	
	public void update(Cliente cliente) throws Exception {				
		persistencia.update(cliente);
	}

	
	public String validarCampos(Object entidad, boolean nuevo) {
		if (entidad != null && entidad instanceof Cliente) {
			StringBuilder sb = new StringBuilder();
			Cliente entidadCliente = (Cliente) entidad;
			log.info("Entidadcliente: " + entidadCliente.getCode() + " " + entidadCliente.getNombre() + " " + nuevo );
			if (entidadCliente.getNombre() == null || entidadCliente.getNombre().trim().isEmpty()) {
				sb.append("Campo nombre es requerido");
				sb.append(", ");
			}
			if (nuevo) {
				if (existeXnombre(entidadCliente.getNombre().trim())) {
					sb.append("Ya existe un cliente con el mismo nombre");
				}
			} else {
				if (existeXnombreDid(entidadCliente.getNombre().trim(), entidadCliente.getCode())) {
					sb.append("Ya existe un cliente con el mismo nombre");
				}
			}
			log.info("cadena: " + sb.toString());
			return sb.toString();
		}
		return "";
	}

	public boolean existeXnombre(String nombre) {
		mapa.clear();
		mapa.put("nombre", nombre);
		try {
			Object obj = persistencia.Consulta_item(HQL_validarXnombre, mapa);
			if (obj != null) {				
				if (Integer.parseInt(obj.toString()) > 1) {
					return true;
				}
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".existeXnombre", e);
		}
		return false;
	}
	
	public boolean existeXnombreDid(String nombre, Integer code) {
		log.info("Entidadcliente: " + code + " " + nombre);
		mapa.clear();
		mapa.put("code", code );
		mapa.put("nombre", nombre);
		try {
			Object obj = persistencia.Consulta_item(HQL_validarXnombreDid, mapa);
			if (obj != null) {
				if (Integer.parseInt(obj.toString()) > 1) {
					return true;
				}
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".existeXnombreDid", e);
		}
		return false;
	}
	
	/**
	 * Obtener el cliente apartir del codigo.
	 * @param code
	 * @return
	 */
	public Cliente findClienteCode( String code ) throws Exception {		
		mapa.clear();
		mapa.put("code", code );		
		try {
			Object obj = persistencia.Consulta_item(HQL_findCode, mapa);
			if (obj != null) {
				return (Cliente)obj;
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".findClienteCode", e);
			throw e;
		}
		return null;
	}
	
	/**
	 * M�todo buscar, realiza la busqueda seg�n los datos cargados previamente.
	 * Requisitos: setear las siguientes variables: hql, hqlCount
	 * Si lo que se requiere es obtener un listado de la entidad, dejar vacio la variable cadenaBusqueda
	 * @return
	 */
	public List<Cliente> buscar(){
		String hql_aux = "";
		String hql_filtro_aux = armarFiltroMultiple();		
		if ( !hql_filtro_aux.trim().isEmpty() ){
			hql_aux = Constantes.AND + Constantes.ABRIR_PARENTESIS + hql_filtro_aux + Constantes.CERRAR_PARENTESIS;
		}
//		hql_filtro_aux = ( hql_filtro_aux.trim().isEmpty() ? Constantes.VACIO : Constantes.WHERE ) + hql_filtro_aux;		
		hql = hql + hql_aux + HQL_orderBy;
		hqlCount = hqlCount + hql_aux;
		log.info( "hql: " +  hql );
		log.info( "hqlCount: " +  hqlCount );
		try {			
			consultaCount(hqlCount, mapa);			
			if ( cantidadRegistros == 0 ){
				pagina = 0;
				return new ArrayList<Cliente>();
			}
			pagina = 1;
			return Consulta(hql, pagina, mapa );
		} catch (Exception e) {
			log.info( "cliente.buscar", e );			
		}
		return new ArrayList<Cliente>();
	}
	
	@Override
	public List firstpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_FIRST));
	}

	@Override
	public List previouspage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_PREVIOUS));
	}

	@Override
	public List nextpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_NEXT));
	}

	@Override
	public List lastpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_LAST));
	}

}
