/**
 * 
 */
package persistence.venta;

import bean.util.BaseSql;
import bean.util.Constantes;
import bean.util.IPaginationPersistence;
import entidad.Controlmedida;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Named;

/**
 * @author penama
 * 
 */
@Named
public class ControlMedidasPersistence extends BaseSql implements IPaginationPersistence {

	private String HQL_findall = " select c from Controlmedida c where c.activo = true ";
	private String HQL_findall_count = " select count(c) from Controlmedida c where c.activo = true ";
	private String HQL_findCode = " select c from Controlmedida c where c.activo = true and c.code = :code ";
	private String HQL_orderBy = " order by c.fecha desc ";	
	
	public void init() {		
		this.iniLogs(this.getClass());
		mapaHqlCampos.clear();
		filtroBusqueda = "filtros";
		cadenaBusqueda = filtroBusqueda;		
	}

	public List<Controlmedida> getLista() throws Exception {		
		return getLista( "" );
	}

	public List<Controlmedida> getLista( String cadenaBusqueda ){
		hql = HQL_findall;
		hqlCount = HQL_findall_count;
		this.cadenaBusqueda = cadenaBusqueda;
		return buscar();
	}
	
	/**
	 * Obtiene el listado de la entidad en base a un mapa<Filtro, Valor>
	 * @param mapaAux
	 * @return
	 */
	public List<Controlmedida> getLista( Map<String, String> mapaAux ){
		mapaHqlValores.clear();		
		Iterator<Entry<String, String>> iterEntry = mapaAux.entrySet().iterator();
		while (iterEntry.hasNext()) {
			Entry<String, String> entry = iterEntry.next();			
			mapaHqlValores.put(entry.getKey(), entry.getValue());			
		}
		cadenaBusqueda = filtroBusqueda;
		hql = HQL_findall;
		hqlCount = HQL_findall_count;
		return buscar();
	}
	
	
	public void save(Controlmedida controlmedida) throws Exception {
		if ( controlmedida == null ){
			throw new Exception( this.getClass().getName() + ".save: controlmedida nulo!! " );
		}
		persistencia.save(controlmedida);		
	}
	
	public void update(Controlmedida controlmedida) throws Exception {				
		persistencia.update(controlmedida);
	}

	public String validarCampos(Object entidad, boolean nuevo) {
		if (entidad != null && entidad instanceof Controlmedida) {
			StringBuilder sb = new StringBuilder();
			Controlmedida entidadControlmedida = (Controlmedida) entidad;
			if (entidadControlmedida.getComentario() == null || entidadControlmedida.getComentario().trim().isEmpty()) {
				sb.append("Campo comentario es requerido");
				sb.append(", ");
			}
			return sb.toString();
		}
		return "";
	}

	/**
	 * Obtener el controlmedida apartir del codigo.
	 * @param code
	 * @return
	 */
	public Controlmedida findControlmedidaCode( int code ) throws Exception {		
		mapa.clear();
		mapa.put("code", code );		
		try {
			Object obj = persistencia.Consulta_item(HQL_findCode, mapa);
			if (obj != null) {
				return (Controlmedida)obj;
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".findControlmedidaCode", e);
			throw e;
		}
		return null;
	}
	
	/**
	 * M�todo buscar, realiza la busqueda seg�n los datos cargados previamente.
	 * Requisitos: setear las siguientes variables: hql, hqlCount
	 * Si lo que se requiere es obtener un listado de la entidad, dejar vacio la variable cadenaBusqueda
	 * @return
	 */
	public List<Controlmedida> buscar(){
		String hql_aux = "";
		String hql_filtro_aux = armarFiltroMultiple();		
		if ( !hql_filtro_aux.trim().isEmpty() ){
			hql_aux = Constantes.AND + Constantes.ABRIR_PARENTESIS + hql_filtro_aux + Constantes.CERRAR_PARENTESIS;
		}
//		hql_filtro_aux = ( hql_filtro_aux.trim().isEmpty() ? Constantes.VACIO : Constantes.WHERE ) + hql_filtro_aux;		
		hql = hql + hql_aux + HQL_orderBy;
		hqlCount = hqlCount + hql_aux;
		log.info( "hql: " +  hql );
		log.info( "hqlCount: " +  hqlCount );
		try {			
			consultaCount(hqlCount, mapa);			
			if ( cantidadRegistros == 0 ){
				pagina = 0;
				return new ArrayList<Controlmedida>();
			}
			pagina = 1;
			return Consulta(hql, pagina, mapa );
		} catch (Exception e) {
			log.info( "controlmedida.buscar", e );			
		}
		return new ArrayList<Controlmedida>();
	}
	
	@Override
	public List firstpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_FIRST));
	}

	@Override
	public List previouspage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_PREVIOUS));
	}

	@Override
	public List nextpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_NEXT));
	}

	@Override
	public List lastpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_LAST));
	}

}
