/**
 * 
 */
package persistence.contable;

import bean.util.BaseSql;
import entidad.Asientocontable;
import javax.inject.Named;


/**
 * @author penama
 * 
 */
@Named
public class AsientocontablePersistence extends BaseSql {

	private String HQL_generateCode = " select count(c) from Asientocontable c ";
	
	public void init() {
		this.iniLogs(this.getClass());
		mapaHqlCampos.clear();
		filtroBusqueda = "filtros";
		cadenaBusqueda = filtroBusqueda;
	}

	/**
	 * Guardar la asientocontable
	 * @param asientocontable
	 * @throws Exception
	 */
	public void save(Asientocontable asientocontable) throws Exception {
		if ( asientocontable == null ){
			throw new Exception( "El objeto asientocontable es nulo!" );
		}
		try {
			persistencia.beginTransaccion();
			// persistir el asientocontable
			persistencia.persist( asientocontable );			
			// persistiendo todo.
			persistencia.commit();
			log.info("Commit al persistir asientocontable.");
		} catch (Exception e) {
			persistencia.rollback();
			log.error("Rollback al persistir asientocontable.");
			throw e;
		}

	}

}
