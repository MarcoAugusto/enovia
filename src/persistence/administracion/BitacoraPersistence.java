/**
 * 
 */
package persistence.administracion;

import bean.util.BaseSql;
import bean.util.Constantes;
import bean.util.IPaginationPersistence;
import bean.util.IValidator;
import entidad.Bitacora;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Named;

/**
 * @author penama
 *
 */
@Named
public class BitacoraPersistence extends BaseSql implements IValidator, IPaginationPersistence {
	
	private String HQL_findall = " select b from Bitacora b ";
	private String HQL_findall_count = " select count(b) from Bitacora b ";
	private String HQL_orderBy = " order by b.fecha desc ";	
	private String HQL_filtroFecha = " to_char( b.fecha, 'dd/MM/yyyy HH:mm:ss' ) like :fecha ";
	private String HQL_filtroAccion = " lower(b.accion) like lower(:accion) ";
	private String HQL_filtroIp = " b.ip like :ip ";
	private String HQL_filtroMensaje = " lower(b.mensaje) like lower(:mensaje) ";
	private String HQL_filtroUsuario = " lower(b.usuario.nombre) like lower(:usuario) ";	
	
	
	public void init(){
		this.iniLogs(this.getClass());
		mapaHqlCampos.clear();
		filtroBusqueda = "filtros";
		cadenaBusqueda = filtroBusqueda;
		mapaHqlCampos.put( "fecha" , HQL_filtroFecha );
		mapaHqlCampos.put( "accion" , HQL_filtroAccion );
		mapaHqlCampos.put( "ip" , HQL_filtroIp );
		mapaHqlCampos.put( "mensaje" , HQL_filtroMensaje );
		mapaHqlCampos.put( "usuario" , HQL_filtroUsuario );
	}	
	
	/**
	 * Obtiene el listado de la entida.
	 * @return
	 * @throws Exception
	 */
	public List<Bitacora> getLista() throws Exception {		
		return getLista( "" );
	}

	/**
	 * Obtiene el listado de la entidad en base a la cadena de busqueda.
	 * @param cadenaBusqueda
	 * @return
	 */
	public List<Bitacora> getLista( String cadenaBusqueda ){
		hql = HQL_findall;
		hqlCount = HQL_findall_count;
		this.cadenaBusqueda = cadenaBusqueda;
		return buscar();
	}
	
	/**
	 * Obtiene el listado de la entidad en base a un mapa<Filtro, Valor>
	 * @param mapaAux
	 * @return
	 */
	public List<Bitacora> getLista( Map<String, String> mapaAux ){
		mapaHqlValores.clear();
		
		Iterator<Entry<String, String>> iterEntry = mapaAux.entrySet().iterator();
		while (iterEntry.hasNext()) {
			Entry<String, String> entry = iterEntry.next();			
			mapaHqlValores.put(entry.getKey(), entry.getValue());			
		}
		hql = HQL_findall;
		hqlCount = HQL_findall_count;		
		return buscar();
	}
	
	
	/**
	 * M�todo buscar, realiza la busqueda seg�n los datos cargados previamente.
	 * Requisitos: setear las siguientes variables: hql, hqlCount
	 * Si lo que se requiere es obtener un listado de la entidad, dejar vacio la variable cadenaBusqueda
	 * @return
	 */
	public List<Bitacora> buscar(){
		String hql_aux = "";
		String hql_filtro_aux = armarFiltroMultiple();		
		if ( !hql_filtro_aux.isEmpty() ){
			hql_aux = Constantes.AND + Constantes.ABRIR_PARENTESIS + hql_filtro_aux + Constantes.CERRAR_PARENTESIS;
		}
		hql_filtro_aux = ( hql_filtro_aux.trim().isEmpty() ? Constantes.VACIO : Constantes.WHERE ) + hql_filtro_aux;
		hql = hql + hql_filtro_aux + HQL_orderBy;
		hqlCount = hqlCount + hql_filtro_aux;
		try {			
			consultaCount(hqlCount, mapa);			
			if ( cantidadRegistros == 0 ){
				pagina = 0;
				return new ArrayList<Bitacora>();
			}
			pagina = 1;
			return Consulta(hql, pagina, mapa );
		} catch (Exception e) {
			log.info( "Rol.buscar", e );			
		}
		return new ArrayList<Bitacora>();
	}
	
	public void accion ( Bitacora entidad ) throws Exception{
		persistencia.save( entidad );
	}
	
	@Override
	public String validarCampos(Object entidad, boolean nuevo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean existeXnombre(String nombre) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean existeXnombreDid(String nombre, long code) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List firstpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_FIRST));
	}

	@Override
	public List previouspage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_PREVIOUS));
	}

	@Override
	public List nextpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_NEXT));
	}

	@Override
	public List lastpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_LAST));
	}
	
	
}
