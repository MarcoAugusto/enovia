/**
 * 
 */
package persistence.administracion;

import java.util.List;

import entidad.Usuario;

import bean.util.BaseSql;
import bean.util.IPaginationPersistence;
import bean.util.IValidator;
import javax.inject.Named;

/**
 * @author Marcus
 *
 */
@Named("loginpersistence")
public class LoginPersistence extends BaseSql {

	private String HQL_usuarioXnombre = "select u from Usuario u where u.activo = true and u.nombre = :nombre ";
	private String HQL_usuarioXpassword = "select u from Usuario u where u.activo = true and u.login = :login and u.password = :password ";
	
	
	public void init(){
		this.iniLogs( this.getClass() );
	}
	
	/**
	 * Obtiene el usuario apartir del nombre ingresado.
	 * @param nombre
	 * @return
	 * @throws Exception
	 */
	public Usuario getUsuariobyNombre( String nombre ) throws Exception{
		log.info( "Buscando usuario por el nombre: " + nombre );
		mapa.clear();
		mapa.put( "nombre", nombre);
		Object obj = null;
		try{
			obj = persistencia.Consulta_item( HQL_usuarioXnombre );
			if ( obj != null ){
				return (Usuario)obj;
			}
		}catch (Exception e) {
			log.error( this.getClass().getName(), e );
		}
		return null;
	}
	
	/**
	 * Obtiene el usuario apartir del login y del password.
	 * @param login
	 * @param password
	 * @return
	 */
	public Usuario validarPassword( String login, String password ){
		log.info( "Validando credenciales del usuario : " + login );
		mapa.clear();
		mapa.put( "login", login );
		mapa.put( "password", password );
		Object obj = null;
		try{
			obj = persistencia.Consulta_item( HQL_usuarioXpassword, mapa );
			if ( obj != null ){
				return (Usuario)obj;
			}			
		}catch (Exception e) {
			log.error( this.getClass().getName(), e );
		}
		return null;
	}
	

}
