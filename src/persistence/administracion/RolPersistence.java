/**
 * 
 */
package persistence.administracion;

import bean.util.BaseBean;
import bean.util.BaseSql;
import bean.util.Constantes;
import bean.util.IPaginationPersistence;
import bean.util.IValidator;
import entidad.Bitacora;
import entidad.Rol;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;
import javax.persistence.NamedQuery;

/**
 * @author penama
 * 
 */
@Named
public class RolPersistence extends BaseSql implements IValidator,IPaginationPersistence {

	private String HQL_findall = " select r from Rol r where r.activo = true ";
	private String HQL_findall_count = " select count(r) from Rol r where r.activo = true ";
	private String HQL_orderBy = " order by r.nombre asc ";
	private String HQL_validarXnombre = "select count(r) from Rol r where r.activo = true and lower(r.nombre) = lower(:nombre) ";
	private String HQL_validarXnombreDid = "select count(r) from Rol r where r.activo = true and lower(r.nombre) = lower(:nombre) and r.code <> :code ";
	private String HQL_filtroNombre = " and lower(r.nombre) like lower(:nombre) ";
	
	public void init() {		
		this.iniLogs(this.getClass());
		mapaHqlCampos.clear();
		filtroBusqueda = "nombre";
		mapaHqlCampos.put( filtroBusqueda , HQL_filtroNombre );		 
	}

	public List<Rol> getLista() throws Exception {		
		return getLista( "" );
	}

	public List<Rol> getLista( String cadenaBusqueda ){
		hql = HQL_findall;
		hqlCount = HQL_findall_count;
		this.cadenaBusqueda = cadenaBusqueda;
		return buscar();
	}
	
	public void save(Rol rol) throws Exception {
		persistencia.save(rol);		
	}

	public void update(Rol rol) throws Exception {
		persistencia.update(rol);
	}

	@Override
	public String validarCampos(Object entidad, boolean nuevo) {
		if (entidad != null && entidad instanceof Rol) {
			StringBuilder sb = new StringBuilder();
			Rol entidadRol = (Rol) entidad;
			log.info("EntidadRol: " + entidadRol.getCode() + " " + entidadRol.getNombre() + " " + nuevo );
			if (entidadRol.getNombre() == null || entidadRol.getNombre().trim().isEmpty()) {
				sb.append("Campo nombre es requerido");
				sb.append(", ");
			}
			if (nuevo) {
				if (existeXnombre(entidadRol.getNombre())) {
					sb.append("Ya existe un rol con el mismo nombre");
				}
			} else {
				if (existeXnombreDid(entidadRol.getNombre(), entidadRol.getCode())) {
					sb.append("Ya existe un rol con el mismo nombre");
				}
			}
			log.info("cadena: " + sb.toString());
			return sb.toString();
		}
		return "";
	}

	@Override
	public boolean existeXnombre(String nombre) {
		mapa.clear();
		mapa.put("nombre", nombre);
		try {
			Object obj = persistencia.Consulta_item(HQL_validarXnombre, mapa);
			if (obj != null) {
				log.info("Object: " + obj + "  " + obj.toString() + " " + nombre );
				if (Integer.parseInt(obj.toString()) > 1) {
					return true;
				}
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".existeXnombre", e);
		}
		return false;
	}

	@Override
	public boolean existeXnombreDid(String nombre, long code) {
		log.info("EntidadRol: " + code + " " + nombre);
		mapa.clear();
		mapa.put("code", (int) code);
		mapa.put("nombre", nombre);
		try {
			Object obj = persistencia
					.Consulta_item(HQL_validarXnombreDid, mapa);
			if (obj != null) {
				log.info("Object: " + obj + "  " + obj.toString() + " " + nombre );
				if (Integer.parseInt(obj.toString()) > 1) {
					return true;
				}
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".existeXnombreDid", e);
		}
		return false;
	}
	
	/**
	 * M�todo buscar, realiza la busqueda seg�n los datos cargados previamente.
	 * Requisitos: setear las siguientes variables: hql, hqlCount
	 * Si lo que se requiere es obtener un listado de la entidad, dejar vacio la variable cadenaBusqueda
	 * @return
	 */
	public List<Rol> buscar(){
		String hql_aux = "";
		String hql_filtro_aux = armarFiltro();		
		if ( !hql_filtro_aux.isEmpty() ){
			hql_aux = Constantes.AND + Constantes.ABRIR_PARENTESIS + hql_filtro_aux + Constantes.CERRAR_PARENTESIS;
		}
		hql = hql + hql_filtro_aux + HQL_orderBy;
		hqlCount = hqlCount + hql_filtro_aux;
		try {			
			consultaCount(hqlCount, mapa);			
			if ( cantidadRegistros == 0 ){
				pagina = 0;
				return new ArrayList<Rol>();
			}
			pagina = 1;
			return Consulta(hql, pagina, mapa );
		} catch (Exception e) {
			log.info( "Rol.buscar", e );			
		}
		return new ArrayList<Rol>();
	}
	
	@Override
	public List firstpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_FIRST));
	}

	@Override
	public List previouspage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_PREVIOUS));
	}

	@Override
	public List nextpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_NEXT));
	}

	@Override
	public List lastpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_LAST));
	}

}
