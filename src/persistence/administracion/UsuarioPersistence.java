/**
 * 
 */
package persistence.administracion;

import bean.util.BaseBean;
import bean.util.BaseSql;
import bean.util.Constantes;
import bean.util.IPaginationPersistence;
import bean.util.IValidator;
import entidad.Bitacora;
import entidad.Usuario;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Named;
import javax.persistence.NamedQuery;

/**
 * @author penama
 * 
 */
@Named
public class UsuarioPersistence extends BaseSql implements IPaginationPersistence {

	private String HQL_findall = " select c from Usuario c where c.activo = true ";
	private String HQL_findall_count = " select count(c) from Usuario c where c.activo = true ";
	private String HQL_orderBy = " order by c.nombre asc ";	
	private String HQL_validarXnombre = "select count(c) from Usuario c where c.activo = true and lower(c.nombre) = lower(:nombre) ";
	private String HQL_validarXnombreDid = "select count(c) from Usuario c where c.activo = true and lower(c.nombre) = lower(:nombre) and c.code <> :code ";	
	private String HQL_filtroNombre = " lower(c.nombre) like lower(:nombre) ";
	private String HQL_filtroLogin = " lower(c.login) like lower(:login) ";
	private String HQL_filtroDireccion = " lower(c.direccion) like lower(:direccion) ";
	private String HQL_filtroIsExterno = " c.isexterno = :responsable ";
	private String HQL_filtroTipoExterno = " lower(c.tipoexterno) like lower(:tipoexterno) ";
	private String HQL_filtroEmail = " lower(c.email) like lower(:email) ";
	
	public void init() {		
		this.iniLogs(this.getClass());
		mapaHqlCampos.clear();
		filtroBusqueda = "filtros";
		cadenaBusqueda = filtroBusqueda;		
		mapaHqlCampos.put( "nombre" , HQL_filtroNombre );		
		mapaHqlCampos.put( "login" , HQL_filtroLogin );
		mapaHqlCampos.put( "direccion" , HQL_filtroDireccion );
		mapaHqlCampos.put( "isexterno" , HQL_filtroIsExterno );
		mapaHqlCampos.put( "tipoexterno" , HQL_filtroTipoExterno );
		mapaHqlCampos.put( "email" , HQL_filtroEmail );
	}

	public List<Usuario> getLista() throws Exception {		
		return getLista( "" );
	}

	public List<Usuario> getLista( String cadenaBusqueda ){
		hql = HQL_findall;
		hqlCount = HQL_findall_count;
		this.cadenaBusqueda = cadenaBusqueda;
		return buscar();
	}
	
	/**
	 * Obtiene el listado de la entidad en base a un mapa<Filtro, Valor>
	 * @param mapaAux
	 * @return
	 */
	public List<Usuario> getLista( Map<String, String> mapaAux ){
		mapaHqlValores.clear();		
		Iterator<Entry<String, String>> iterEntry = mapaAux.entrySet().iterator();
		while (iterEntry.hasNext()) {
			Entry<String, String> entry = iterEntry.next();			
			mapaHqlValores.put(entry.getKey(), entry.getValue());			
		}
		cadenaBusqueda = filtroBusqueda;
		hql = HQL_findall;
		hqlCount = HQL_findall_count;
		return buscar();
	}
	
	
	public void save(Usuario usuario) throws Exception {				
		persistencia.save(usuario);		
	}
	
	public void update(Usuario usuario) throws Exception {				
		persistencia.update(usuario);
	}

	
	public String validarCampos(Object entidad, boolean nuevo) {
		if (entidad != null && entidad instanceof Usuario) {
			StringBuilder sb = new StringBuilder();
			Usuario entidadUsuario = (Usuario) entidad;
			log.info("Entidadusuario: " + entidadUsuario.getCode() + " " + entidadUsuario.getNombre() + " " + nuevo );
			if (entidadUsuario.getNombre() == null || entidadUsuario.getNombre().trim().isEmpty()) {
				sb.append("Campo nombre es requerido");
				sb.append(", ");
			}
			if ( entidadUsuario.getLogin() == null || entidadUsuario.getLogin().trim().isEmpty() ){
				sb.append("Campo direcci�n es requerido");
				sb.append(", ");
			}
			if ( entidadUsuario.getPassword() == null || entidadUsuario.getPassword().trim().isEmpty() ){
				sb.append("Campo responsable es requerido");
				sb.append(", ");
			}
			if ( entidadUsuario.getRol() == null ){
				sb.append("Campo rol es requerido");
				sb.append(", ");
			}
			if ( entidadUsuario.getSucursal() == null ){
				sb.append("Campo sucursal es requerido");
				sb.append(", ");
			}
			if (nuevo) {
				if (existeXnombre(entidadUsuario.getNombre().trim())) {
					sb.append("Ya existe una usuario con el mismo nombre");
				}
			} else {
				if (existeXnombreDid(entidadUsuario.getNombre().trim(), entidadUsuario.getCode())) {
					sb.append("Ya existe una usuario con el mismo nombre");
				}
			}
			log.info("cadena: " + sb.toString());
			return sb.toString();
		}
		return "";
	}

	public boolean existeXnombre(String nombre) {
		mapa.clear();
		mapa.put("nombre", nombre);
		try {
			Object obj = persistencia.Consulta_item(HQL_validarXnombre, mapa);
			if (obj != null) {				
				if (Integer.parseInt(obj.toString()) > 1) {
					return true;
				}
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".existeXnombre", e);
		}
		return false;
	}
	
	public boolean existeXnombreDid(String nombre, Integer code) {
		log.info("Entidadusuario: " + code + " " + nombre);
		mapa.clear();
		mapa.put("code", code );
		mapa.put("nombre", nombre);
		try {
			Object obj = persistencia.Consulta_item(HQL_validarXnombreDid, mapa);
			if (obj != null) {
				if (Integer.parseInt(obj.toString()) > 1) {
					return true;
				}
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".existeXnombreDid", e);
		}
		return false;
	}
	
	/**
	 * M�todo buscar, realiza la busqueda seg�n los datos cargados previamente.
	 * Requisitos: setear las siguientes variables: hql, hqlCount
	 * Si lo que se requiere es obtener un listado de la entidad, dejar vacio la variable cadenaBusqueda
	 * @return
	 */
	public List<Usuario> buscar(){
		String hql_aux = "";
		String hql_filtro_aux = armarFiltroMultiple();		
		if ( !hql_filtro_aux.trim().isEmpty() ){
			hql_aux = Constantes.AND + Constantes.ABRIR_PARENTESIS + hql_filtro_aux + Constantes.CERRAR_PARENTESIS;
		}
//		hql_filtro_aux = ( hql_filtro_aux.trim().isEmpty() ? Constantes.VACIO : Constantes.WHERE ) + hql_filtro_aux;		
		hql = hql + hql_aux + HQL_orderBy;
		hqlCount = hqlCount + hql_aux;
		log.info( "hql: " +  hql );
		log.info( "hqlCount: " +  hqlCount );
		try {			
			consultaCount(hqlCount, mapa);			
			if ( cantidadRegistros == 0 ){
				pagina = 0;
				return new ArrayList<Usuario>();
			}
			pagina = 1;
			return Consulta(hql, pagina, mapa );
		} catch (Exception e) {
			log.info( "usuario.buscar", e );			
		}
		return new ArrayList<Usuario>();
	}
	
	@Override
	public List firstpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_FIRST));
	}

	@Override
	public List previouspage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_PREVIOUS));
	}

	@Override
	public List nextpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_NEXT));
	}

	@Override
	public List lastpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_LAST));
	}

}
