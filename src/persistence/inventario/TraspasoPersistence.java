/**
 * 
 */
package persistence.inventario;

import java.util.List;

import bean.util.BaseSql;
import bean.util.Constantes;
import entidad.Detalleegreso;
import entidad.Detalleingreso;
import entidad.Egreso;
import entidad.Ingreso;
import entidad.Traspaso;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * @author penama
 * 
 */
@Named
public class TraspasoPersistence extends BaseSql {

	private String HQL_generateCode = " select count(c) from Traspaso c ";

	@Inject
	private IngresoPersistence ingresopersistence;

	@Inject
	private EgresoPersistence egresopersistence;

	public void init() {
		this.iniLogs(this.getClass());
		mapaHqlCampos.clear();
		filtroBusqueda = "filtros";
		cadenaBusqueda = filtroBusqueda;
		// inicializar las instancias.
		egresopersistence.init();
		ingresopersistence.init();
	}

	/**
	 * Persistir el egreso y su detalle.
	 * 
	 * @param traspaso
	 * @throws Exception
	 */
	public void save(Traspaso traspaso, Egreso egreso,
			List<Detalleegreso> detalleEgresoList, Ingreso ingreso,
			List<Detalleingreso> detalleIngresoList) throws Exception {
		try {
			persistencia.beginTransaccion();
			// persistiendo egreso y su detalle, ingreso y su detalle, y el traspaso.
			persistirTransito(traspaso, egreso, detalleEgresoList, ingreso, detalleIngresoList);
			// persistir todo.			
			persistencia.commit();
			log.info("Commit al persistir traspaso.");
		} catch (Exception e) {
			persistencia.rollback();
			log.error("Rollback al persistir traspaso" + e.getMessage(), e);
			throw e;
		}
	}

	public void persistirTransito(Traspaso traspaso, Egreso egreso,
			List<Detalleegreso> detalleEgresoList, Ingreso ingreso,
			List<Detalleingreso> detalleIngresoList) throws Exception {

		// guardando el egreso.
		egresopersistence.persistirEgresoDetalle(egreso, detalleEgresoList);
		// guardando el ingreso
		ingresopersistence.persistirIngresoDetalle(ingreso, detalleIngresoList);
		// actualizando los code del egreso e ingreso en el traspaso.
		traspaso.setEgreso(egreso);
		traspaso.setIngreso(ingreso);
		// asignando code al traspado.
		// guardar traspaso
		String code = "";
		try {
			code = generateCode();
			if (code == null || code.trim().isEmpty()) {
				throw new Exception(
						"No se pudo generar el code secuencial del traspaso!!! ");
			}
		} catch (Exception e) {
			throw e;
		}
		traspaso.setCode(code);
		persistencia.persist(traspaso);
	}

	/**
	 * Obtiene el code siguiente a asignar.
	 * 
	 * @return
	 */
	public String generateCode() throws Exception {
		Object obj = persistencia.Consulta_item(HQL_generateCode);
		if (obj != null) {
			return Constantes.PREFIJO_TRASPASO
					+ (Integer.parseInt(obj.toString()) + 1);
		}
		return null;
	}

}
