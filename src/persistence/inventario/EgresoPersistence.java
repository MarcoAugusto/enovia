/**
 * 
 */
package persistence.inventario;

import bean.util.BaseBean;
import bean.util.BaseSql;
import bean.util.Constantes;
import entidad.Baseproducto;
import entidad.Bitacora;
import entidad.Detalleegreso;
import entidad.Detallereserva;
import entidad.Egreso;
import entidad.Producto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NamedQuery;

import persistence.producto.ProductoPersistence;

/**
 * @author penama
 * 
 */
@Named
public class EgresoPersistence extends BaseSql {

	private String HQL_generateCode = " select count(c) from Egreso c ";

	public void init() {
		this.iniLogs(this.getClass());
		mapaHqlCampos.clear();
		filtroBusqueda = "filtros";
		cadenaBusqueda = filtroBusqueda;
	}

	/**
	 * Persistir el egreso y su detalle.
	 * 
	 * @param egreso
	 * @param detalleEgresoList
	 * @throws Exception
	 */
	public void save(Egreso egreso, List<Detalleegreso> detalleEgresoList) throws Exception {
		try {
			persistencia.beginTransaccion();
			// guardar egreso y detalle
			persistirEgresoDetalle(egreso, detalleEgresoList);
			// persistir todo.	
			persistencia.commit();
			log.info("Commit al persistir egreso.");
		} catch (Exception e) {
			persistencia.rollback();
			log.error("Rollback al persistir egreso" + e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * Guardar el egreso y su detalle, este m�todo solo persiste pero no
	 * realiza el commit.
	 * 
	 * @param egreso
	 * @param detalleEgresoList
	 * @throws Exception
	 */
	public void persistirEgresoDetalle(Egreso egreso, List<Detalleegreso> detalleEgresoList) throws Exception {
		String code = "";
		try{
			code = generateCode();
			if (code == null || code.trim().isEmpty()) {
				throw new Exception("No se pudo generar el code secuencial del egreso!!! ");
			}
		}catch (Exception e) {
			throw e;	
		}		
		egreso.setCode(code);
		// guardando el egreso
		persistencia.persist(egreso);
		// guardando el detalle del egreso.
		for (Iterator iter = detalleEgresoList.iterator(); iter.hasNext();) {
			Detalleegreso detalleegreso = (Detalleegreso) iter.next();
			// actualizando la cantidad del catalogo.
			Baseproducto baseProducto = detalleegreso.getProducto().getBaseproducto();
			baseProducto.setCantidad(baseProducto.getCantidad() - detalleegreso.getCantidad());
			persistencia.merge(baseProducto);
			// actualizando el estado del producto.
			Producto producto = detalleegreso.getProducto();
			persistencia.merge( producto );
			// guardando el detalle.
			detalleegreso.setEgreso(egreso);
			persistencia.persist(detalleegreso);			
		}
	}

	/**
	 * Obtiene el code siguiente a asignar.
	 * 
	 * @return
	 */
	public String generateCode() throws Exception {
		Object obj = persistencia.Consulta_item(HQL_generateCode);
		if (obj != null) {
			return Constantes.PREFIJO_INGRESO + (Integer.parseInt(obj.toString()) + 1);
		}
		return null;
	}

}
