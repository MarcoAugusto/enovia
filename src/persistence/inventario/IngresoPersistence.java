/**
 * 
 */
package persistence.inventario;

import bean.util.BaseBean;
import bean.util.BaseSql;
import bean.util.Constantes;
import entidad.Baseproducto;
import entidad.Bitacora;
import entidad.Detalleingreso;
import entidad.Detallereserva;
import entidad.Ingreso;
import entidad.Producto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NamedQuery;

import persistence.producto.ProductoPersistence;

/**
 * @author penama
 * 
 */
@Named
public class IngresoPersistence extends BaseSql {

	private String HQL_generateCode = " select count(c) from Ingreso c ";

	@Inject
	private ProductoPersistence productopersistence;

	public void init() {
		this.iniLogs(this.getClass());
		mapaHqlCampos.clear();
		filtroBusqueda = "filtros";
		cadenaBusqueda = filtroBusqueda;
		productopersistence.init();
	}

	/**
	 * Persistir el ingreso y su detalle.
	 * 
	 * @param ingreso
	 * @param detalleIngresoList
	 * @throws Exception
	 */
	public void save(Ingreso ingreso, List<Detalleingreso> detalleIngresoList) throws Exception {
		try {
			persistencia.beginTransaccion();
			// guardar ingreso y detalle
			persistirIngresoDetalle(ingreso, detalleIngresoList);
			// persistir todo.	
			persistencia.commit();
			log.info("Commit al persistir ingreso.");
		} catch (Exception e) {
			persistencia.rollback();
			log.error("Rollback al persistir ingreso" + e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * Guardar el ingreso y su detalle, este m�todo solo persiste pero no
	 * realiza el commit.
	 * 
	 * @param ingreso
	 * @param detalleIngresoList
	 * @throws Exception
	 */
	public void persistirIngresoDetalle(Ingreso ingreso, List<Detalleingreso> detalleIngresoList) throws Exception {
		String code = "";
		try{
			code = generateCode();
			if (code == null || code.trim().isEmpty()) {
				throw new Exception("No se pudo generar el code secuencial del ingreso!!! ");
			}
		}catch (Exception e) {
			throw e;	
		}		
		ingreso.setCode(code);
		// guardando el ingreso
		persistencia.persist(ingreso);
		// guardando el detalle del ingreso.
		for (Iterator iter = detalleIngresoList.iterator(); iter.hasNext();) {
			Detalleingreso detalleingreso = (Detalleingreso) iter.next();
			// guardando el producto.
			Producto producto = detalleingreso.getProducto();
			if ( producto.getCode() == null || producto.getCode().trim().isEmpty() ){
				String codeProducto = "";
				try {
					codeProducto = productopersistence.generateCode();
					if (codeProducto == null || codeProducto.trim().isEmpty()) {
						throw new Exception(
								"Error: No se pudo generar el code secuencial del producto! ");
					}
				} catch (Exception e) {
					throw e;
				}
				producto.setCode(codeProducto);
				producto.setAlmacen( ingreso.getAlmacen() );
				persistencia.persist(producto);
			} else {
				persistencia.merge(producto);
			}
			// actualizando la cantidad del catalogo.
			Baseproducto baseProducto = producto.getBaseproducto();
			baseProducto.setCantidad(baseProducto.getCantidad() + detalleingreso.getCantidad());
			persistencia.merge(baseProducto);
			// guardando el detalle.
			detalleingreso.setIngreso(ingreso);
			persistencia.persist(detalleingreso);
		}
	}

	/**
	 * Obtiene el code siguiente a asignar.
	 * 
	 * @return
	 */
	public String generateCode() throws Exception {
		Object obj = persistencia.Consulta_item(HQL_generateCode);
		if (obj != null) {
			return Constantes.PREFIJO_INGRESO + (Integer.parseInt(obj.toString()) + 1);
		}
		return null;
	}

}
