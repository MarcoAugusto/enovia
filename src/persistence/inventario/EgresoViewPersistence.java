/**
 * 
 */
package persistence.inventario;

import bean.util.BaseSql;
import entidad.Detalleegreso;
import entidad.Egreso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.inject.Named;


/**
 * @author penama
 * 
 */
@Named
public class EgresoViewPersistence extends BaseSql {

	private String HQL_findDetalle = "select c from Detalleegreso c where c.activo = true and c.egreso.code = :egreso ";
	
	@PostConstruct
	public void init() {		
		this.iniLogs(this.getClass());			
	}

		
	/**
	 * Obtiene el listado de la entidad en base a un mapa<Filtro, Valor>
	 * @param mapaAux
	 * @return
	 */
	public List<Detalleegreso> getDetalleEgreso( String egreso ) throws Exception {
		mapa.clear();
		mapa.put( "egreso", egreso );		
		try {
			Object obj = persistencia.Consulta(HQL_findDetalle, mapa );
			if (obj != null) {
				return (List<Detalleegreso>)obj ;				
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".getDetalleEgreso", e);
			throw e;
		}
		return new ArrayList<Detalleegreso>();
	}	
		
	
}
