/**
 * 
 */
package persistence.inventario;

import bean.util.BaseSql;
import bean.util.Constantes;
import bean.util.IPaginationPersistence;
import entidad.Traspaso;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Named;


/**
 * @author penama
 * 
 */
@Named
public class TraspasoListPersistence extends BaseSql implements IPaginationPersistence {

	private String HQL_findall = " select c from Traspaso c where c.activo = true ";
	private String HQL_findall_count = " select count(c) from Traspaso c where c.activo = true ";
	private String HQL_orderBy = " order by c.fecha desc ";
		
	private String HQL_filtroCode = " lower(c.code) like lower(:code) ";	
	private String HQL_filtroAlmacenIngreso = " lower( c.ingreso.almacen.code ) like lower(:almacenIngreso) ";
	private String HQL_filtroAlmacenEgreso = " lower( c.egreso.almacen.code ) like lower(:almacenEgreso) ";
	private String HQL_filtroUsuario = " lower( c.usuario.nombre ) like lower(:usuario) ";
	private String HQL_filtroMotivo = " lower( c.motivo ) like lower(:motivo) ";
	private String HQL_filtroComentario = " lower( c.comentario ) like lower(:comentario) ";
	
	
	public void init() {		
		this.iniLogs(this.getClass());
		mapaHqlCampos.clear();
		filtroBusqueda = "filtros";
		cadenaBusqueda = filtroBusqueda;
		mapaHqlCampos.put( "code" , HQL_filtroCode );		 
		mapaHqlCampos.put( "almacenIngreso" , HQL_filtroAlmacenIngreso );
		mapaHqlCampos.put( "almacenEgreso" , HQL_filtroAlmacenEgreso );
		mapaHqlCampos.put( "motivo" , HQL_filtroMotivo );
		mapaHqlCampos.put( "comentario" , HQL_filtroComentario );
		mapaHqlCampos.put( "usuario" , HQL_filtroUsuario );
	}

	public List<Traspaso> getLista() throws Exception {		
		return getLista( "" );
	}

	public List<Traspaso> getLista( String cadenaBusqueda ){
		hql = HQL_findall;
		hqlCount = HQL_findall_count;
		this.cadenaBusqueda = cadenaBusqueda;
		return buscar();
	}
	
	/**
	 * Obtiene el listado de la entidad en base a un mapa<Filtro, Valor>
	 * @param mapaAux
	 * @return
	 */
	public List<Traspaso> getLista( Map<String, String> mapaAux ){
		mapaHqlValores.clear();		
		Iterator<Entry<String, String>> iterEntry = mapaAux.entrySet().iterator();
		while (iterEntry.hasNext()) {
			Entry<String, String> entry = iterEntry.next();			
			mapaHqlValores.put(entry.getKey(), entry.getValue());			
		}
		cadenaBusqueda = filtroBusqueda;
		hql = HQL_findall;
		hqlCount = HQL_findall_count;
		return buscar();
	}	
	
	/**
	 * M�todo buscar, realiza la busqueda seg�n los datos cargados previamente.
	 * Requisitos: setear las siguientes variables: hql, hqlCount
	 * Si lo que se requiere es obtener un listado de la entidad, dejar vacio la variable cadenaBusqueda
	 * @return
	 */
	public List<Traspaso> buscar(){
		String hql_aux = "";
		String hql_filtro_aux = armarFiltroMultiple();		
		if ( !hql_filtro_aux.trim().isEmpty() ){
			hql_aux = Constantes.AND + Constantes.ABRIR_PARENTESIS + hql_filtro_aux + Constantes.CERRAR_PARENTESIS;
		}
//		hql_filtro_aux = ( hql_filtro_aux.trim().isEmpty() ? Constantes.VACIO : Constantes.WHERE ) + hql_filtro_aux;		
		hql = hql + hql_aux + HQL_orderBy;
		hqlCount = hqlCount + hql_aux;
		log.info( "hql: " +  hql );
		log.info( "hqlCount: " +  hqlCount );
		try {			
			consultaCount(hqlCount, mapa);			
			if ( cantidadRegistros == 0 ){
				pagina = 0;
				return new ArrayList<Traspaso>();
			}
			pagina = 1;
			return Consulta(hql, pagina, mapa );
		} catch (Exception e) {
			log.info( "baseProducto.buscar", e );			
		}
		return new ArrayList<Traspaso>();
	}
	
	@Override
	public List firstpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_FIRST));
	}

	@Override
	public List previouspage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_PREVIOUS));
	}

	@Override
	public List nextpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_NEXT));
	}

	@Override
	public List lastpage() throws Exception {
		return ConsultaPagina(NavegacionPagina(Constantes.PAGINA_LAST));
	}

}
