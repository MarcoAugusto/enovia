/**
 * 
 */
package persistence.inventario;

import bean.util.BaseSql;
import bean.util.Constantes;
import bean.util.IPaginationPersistence;
import entidad.Detalleingreso;
import entidad.Ingreso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.inject.Named;


/**
 * @author penama
 * 
 */
@Named
public class IngresoViewPersistence extends BaseSql {

	private String HQL_findDetalle = "select c from Detalleingreso c where c.activo = true and c.ingreso.code = :ingreso ";
	
	@PostConstruct
	public void init() {		
		this.iniLogs(this.getClass());			
	}

		
	/**
	 * Obtiene el listado de la entidad en base a un mapa<Filtro, Valor>
	 * @param mapaAux
	 * @return
	 */
	public List<Detalleingreso> getDetalleIngreso( String ingreso ) throws Exception {
		mapa.clear();
		mapa.put( "ingreso", ingreso );		
		try {
			Object obj = persistencia.Consulta(HQL_findDetalle, mapa );
			if (obj != null) {
				return (List<Detalleingreso>)obj ;				
			}
		} catch (Exception e) {
			log.error(getClass().getName() + ".getDetalleIngreso", e);
			throw e;
		}
		return new ArrayList<Detalleingreso>();
	}	
		
	
}
