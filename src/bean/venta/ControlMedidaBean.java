/**
 * 
 */
package bean.venta;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import persistence.venta.ControlMedidasPersistence;

import entidad.Controlmedida;

import bean.bitacora.BitacoraAccion;
import bean.util.BaseBean;
import bean.util.Constantes;
import bean.util.IGeneral;
import bean.util.IPagination;
import bean.util.Jutil;
import bean.util.Parametros;
import bean.util.SysMessage;


/**
 * @author Marcus
 *
 */
@ManagedBean(name="controlmedidabean")
@ViewScoped
public class ControlMedidaBean extends BaseBean<Controlmedida> implements IGeneral, IPagination {

	@Inject
	private ControlMedidasPersistence controlmedidapersistence;
	
	@Inject
	private BitacoraAccion bitacoraaccion;
	
	@Inject
	private Jutil jutil;
		
	public void init() { 
		iniLogs(this.getClass());
		controlmedidapersistence.init();
		cargarLista();
		nuevo();		
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void cargarLista(){
		try{
			this.list = controlmedidapersistence.getLista();			
		}catch (Exception e) {
			this.list = new ArrayList<Controlmedida>();
			log.error( "[ControlmedidaBean.cargarLista]", e );
		}
	}
	
	public void nuevo(){
		entidad = new Controlmedida();
		entidad.setActivo(true);
		nuevo = true;
		visibleNuevoEditar = false;
		selected = null;
	}
	
	public void nuevaEntidad() {
		nuevo();
		visibleNuevoEditar = true;
	}
	
	public void guardar() {
		String validacion = controlmedidapersistence.validarCampos(entidad, nuevo);
		if (validacion.trim().isEmpty()) {
			if (nuevo) {
				try {
					controlmedidapersistence.save( entidad );					
					log.info("[controlmedida: " + jutil.getUsuarioN() + ", controlmedida: " + entidad.getCode() + "] [Guardado correctamente]");					
					SysMessage.info("Guardado correctamente.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Se guardo correctamente el controlmedida: " + entidad.getCode() + ".");
					nuevo();
					cargarLista();
					visibleNuevoEditar = true;
				} catch (Exception e) {
					log.error("[controlmedida: " + jutil.getUsuarioN() + ", controlmedida: " + entidad.getCode() + "] [Fallo al guardar]", e);
					SysMessage.error("Fallo al guardar.");					
				}
			} else {
				try {
					controlmedidapersistence.update(entidad);					
					log.info("[controlmedida: " + jutil.getUsuarioN() + ", controlmedida: " + entidad.getCode() + "] [Guardado correctamente]");
					SysMessage.info("Actualizado correctamente.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Se actualizado correctamente el controlmedida: " + entidad.getCode() + ".");
					nuevo();
					cargarLista();
					visibleNuevoEditar = false;
				} catch (Exception e) {
					log.error("[controlmedida: " + jutil.getUsuarioN() + ", controlmedida: " + entidad.getCode() + "] [Fallo al guardar]", e);
					SysMessage.error("Fallo al guardar.");					
				}
			}
		} else {
			log.warn("[controlmedida: " + jutil.getUsuarioN() + ", controlmedida:" + "] [" + validacion + "]");
			SysMessage.warn(validacion);
		}
	}	

	public void editar() {
		if (selected != null) {
			entidad = selected;			
			nuevo = false;
			visibleNuevoEditar = true;
		} else {
			visibleNuevoEditar = false;
			log.warn("[controlmedida: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
		log.info( "entidad:" + entidad + " nuevo: " + nuevo );
	}

	public void editar(Controlmedida obj) {
		selected = obj;
		editar();
	}

	public void onRowSelect(org.primefaces.event.SelectEvent event) {
		selected = (Controlmedida)event.getObject();
		log.info( "selected: " + selected );
		editar();
    }
	
	public void eliminar() {
		if (selected != null) {			
			try {
				selected.setActivo( false );
				controlmedidapersistence.update(selected);				
				log.info("[controlmedida: " + jutil.getUsuarioN() + ", controlmedida: " + selected.getCode() + "] [Eliminado correctamente]");
				SysMessage.info("Eliminado correctamente.");
				bitacoraaccion.accion(selected.getClass().getName(), "Se elimino correctamente el controlmedida: " + selected.getCode() + ".");
				cargarLista();
				nuevo();
			} catch (Exception e) {
				log.error("[controlmedida: " + jutil.getUsuarioN() + ", controlmedida: " + selected.getCode() + "] [Fallo al eliminar]", e);
				SysMessage.info("Fallo al eliminar.");
				bitacoraaccion.accion(selected.getClass().getName(), "Fallo al eliminar el controlmedida: " + selected.getCode() + ".");
			}
		} else {
			log.warn("[controlmedida: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
	}
		
	public void eliminar(Controlmedida obj) {
		selected = obj;
		eliminar();
	}
	
	public void buscar(){
		try {
			list = controlmedidapersistence.getLista();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Obtiene al controlmedida de la lista cargada previamente, a partir del code 
	 * @param code
	 * @return
	 */
	public Controlmedida obtenerControlmedidaCode( Integer code ){					
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			Controlmedida controlmedida = (Controlmedida) iter.next();
			if ( controlmedida.getCode() == code ){
				return controlmedida;
			}
		}
		return null;
	}
	
	@Override
	public void firstpage() {
		try{
			list = controlmedidapersistence.firstpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "firstpage", e );
		}
	}

	@Override
	public void previouspage() {
		try{
			list = controlmedidapersistence.previouspage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "previouspage", e );
		}
	}

	@Override
	public void nextpage() {
		try{
			list = controlmedidapersistence.nextpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "nextpage", e );
		}
	}

	@Override
	public void lastpage() {
		try{
			list = controlmedidapersistence.lastpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "lastpage", e );
		}
	}

	public int getCantidadRegistros() {
		return controlmedidapersistence.getCantidadRegistros();
	}
	
	public int getCantidadPaginas() {
		return controlmedidapersistence.getCantidadPaginas();
	}
	
	public int getPagina() {
		return controlmedidapersistence.getPagina();
	}

	
	
}
