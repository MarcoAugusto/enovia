/**
 * 
 */
package bean.venta;

import java.math.BigDecimal;

import entidad.Detalleegreso;

/**
 * @author penama
 *
 */
public class DetalleEgresoItem {

	private Detalleegreso detalleEgreso;
	private BigDecimal total;
	
	/**
	 * @return the detalleEgreso
	 */
	public Detalleegreso getDetalleEgreso() {
		return detalleEgreso;
	}
	/**
	 * @param detalleEgreso the detalleEgreso to set
	 */
	public void setDetalleEgreso(Detalleegreso detalleEgreso) {
		this.detalleEgreso = detalleEgreso;
	}
	/**
	 * @return the total
	 */
	public BigDecimal getTotal() {
		return total;
	}
	/**
	 * @param total the total to set
	 */
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	
	
}
