/**
 * 
 */
package bean.venta;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import persistence.venta.ClientePersistence;

import entidad.Cliente;

import bean.bitacora.BitacoraAccion;
import bean.util.BaseBean;
import bean.util.Constantes;
import bean.util.IGeneral;
import bean.util.IPagination;
import bean.util.Jutil;
import bean.util.Parametros;
import bean.util.SysMessage;


/**
 * @author Marcus
 *
 */
@ManagedBean(name="clientebean")
@ViewScoped
public class ClienteBean extends BaseBean<Cliente> implements IGeneral, IPagination {

	@Inject
	private ClientePersistence clientepersistence;
	
	@Inject
	private BitacoraAccion bitacoraaccion;
	
	@Inject
	private Jutil jutil;
		
	private String filtroNombre;
	private String filtroTelefono;
	private String filtroOpciones;

	@Override
	@PostConstruct
	public void init() { 
		iniLogs(this.getClass());
		log.info("iniciliazando cliente");
		clientepersistence.init();
		initFiltros();
		cargarLista();
		nuevo();		
	}

	/**
	 * Inicializa en vacio los campos a utilizar como filtros.
	 */
	public void initFiltros(){
		filtroNombre = filtroTelefono = filtroOpciones = Constantes.VACIO;		
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void cargarLista(){
		try{
			this.list = clientepersistence.getLista();			
		}catch (Exception e) {
			this.list = new ArrayList<Cliente>();
			log.error( "[ClienteBean.cargarLista]", e );
		}
	}
	
	public void nuevo(){
		entidad = new Cliente();
		entidad.setActivo(true);
		entidad.setOpciones(Constantes.VACIO);
		entidad.setTelefono( Constantes.VACIO );
		nuevo = true;
		visibleNuevoEditar = false;
		selected = null;
		initFiltros();
	}
	
	public void nuevaEntidad() {
		nuevo();
		visibleNuevoEditar = true;
	}
	
	public void guardar() {
		String validacion = clientepersistence.validarCampos(entidad, nuevo);
		if (validacion.trim().isEmpty()) {
			if (nuevo) {
				try {
					clientepersistence.save( entidad );					
					log.info("[cliente: " + jutil.getUsuarioN() + ", cliente: " + entidad.getCode() + "|" + entidad.getNombre() + "] [Guardado correctamente]");					
					SysMessage.info("Guardado correctamente.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Se guardo correctamente el cliente: " + entidad.getCode() + "|" + entidad.getNombre() + ".");
					nuevo();
					cargarLista();
					visibleNuevoEditar = true;
				} catch (Exception e) {
					log.error("[cliente: " + jutil.getUsuarioN() + ", cliente: " + entidad.getCode() + "|" + entidad.getNombre() + "] [Fallo al guardar]", e);
					SysMessage.error("Fallo al guardar.");					
				}
			} else {
				try {
					clientepersistence.update(entidad);					
					log.info("[cliente: " + jutil.getUsuarioN() + ", cliente: " + entidad.getCode() + "|" + entidad.getNombre() + "] [Guardado correctamente]");
					SysMessage.info("Actualizado correctamente.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Se actualizado correctamente el cliente: " + entidad.getCode() + "|" + entidad.getNombre() + ".");
					nuevo();
					cargarLista();
					visibleNuevoEditar = false;
				} catch (Exception e) {
					log.error("[cliente: " + jutil.getUsuarioN() + ", cliente: " + entidad.getCode() + "|" + entidad.getNombre() + "] [Fallo al guardar]", e);
					SysMessage.error("Fallo al guardar.");					
				}
			}
		} else {
			log.warn("[cliente: " + jutil.getUsuarioN() + ", cliente:" + "] [" + validacion + "]");
			SysMessage.warn(validacion);
		}
	}	

	public void editar() {
		if (selected != null) {
			entidad = selected;			
			nuevo = false;
			visibleNuevoEditar = true;
		} else {
			visibleNuevoEditar = false;
			log.warn("[cliente: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
		log.info( "entidad:" + entidad + " nuevo: " + nuevo );
	}

	public void editar(Cliente obj) {
		log.info("intentando editar" + obj.getNombre() );
		selected = obj;
		editar();
	}

	public void onRowSelect(org.primefaces.event.SelectEvent event) {
		selected = (Cliente)event.getObject();
		log.info( "selected: " + selected );
		log.info( "Cliente seleccionado:" + selected.getNombre() );
		editar();
    }
	
	public void eliminar() {
		if (selected != null) {			
			try {
				selected.setActivo( false );
				clientepersistence.update(selected);				
				log.info("[cliente: " + jutil.getUsuarioN() + ", cliente: " + selected.getCode() + "|" + selected.getNombre() + "] [Eliminado correctamente]");
				SysMessage.info("Eliminado correctamente.");
				bitacoraaccion.accion(selected.getClass().getName(), "Se elimino correctamente el cliente: " + selected.getCode() + "|" + selected.getNombre() + ".");
				cargarLista();
				nuevo();
			} catch (Exception e) {
				log.error("[cliente: " + jutil.getUsuarioN() + ", cliente: " + selected.getCode() + "|" + selected.getNombre() + "] [Fallo al eliminar]", e);
				SysMessage.info("Fallo al eliminar.");
				bitacoraaccion.accion(selected.getClass().getName(), "Fallo al eliminar el cliente: " + selected.getCode() + "|" + selected.getNombre() + ".");
			}
		} else {
			log.warn("[cliente: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
	}
		
	public void eliminar(Cliente obj) {
		selected = obj;
		eliminar();
	}
	
	public void buscar(){
		Map<String, String> mapaValor = new HashMap<String, String>();		
		if ( !filtroNombre.trim().isEmpty() ){
			mapaValor.put( "nombre", filtroNombre );
		}		
		if ( !filtroTelefono.trim().isEmpty() ){
			mapaValor.put( "telefono", filtroTelefono );
		}
		if ( !filtroOpciones.trim().isEmpty() ){
			mapaValor.put( "opciones", filtroOpciones );
		}
		list = clientepersistence.getLista( mapaValor );		
	}
	
	/**
	 * Obtiene al cliente de la lista cargada previamente, a partir del code 
	 * @param code
	 * @return
	 */
	public Cliente obtenerClienteCode( Integer code ){					
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			Cliente cliente = (Cliente) iter.next();
			if ( cliente.getCode() == code ){
				return cliente;
			}
		}
		return null;
	}
	
	@Override
	public void firstpage() {
		try{
			list = clientepersistence.firstpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "firstpage", e );
		}
	}

	@Override
	public void previouspage() {
		try{
			list = clientepersistence.previouspage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "previouspage", e );
		}
	}

	@Override
	public void nextpage() {
		try{
			list = clientepersistence.nextpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "nextpage", e );
		}
	}

	@Override
	public void lastpage() {
		try{
			list = clientepersistence.lastpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "lastpage", e );
		}
	}

	public int getCantidadRegistros() {
		return clientepersistence.getCantidadRegistros();
	}
	
	public int getCantidadPaginas() {
		return clientepersistence.getCantidadPaginas();
	}
	
	public int getPagina() {
		return clientepersistence.getPagina();
	}

	/**
	 * @return the filtroNombre
	 */
	public String getFiltroNombre() {
		return filtroNombre;
	}

	/**
	 * @param filtroNombre the filtroNombre to set
	 */
	public void setFiltroNombre(String filtroNombre) {
		this.filtroNombre = filtroNombre;
	}
	
	/**
	 * @return the filtroTelefono
	 */
	public String getFiltroTelefono() {
		return filtroTelefono;
	}

	/**
	 * @param filtroTelefono the filtroTelefono to set
	 */
	public void setFiltroTelefono(String filtroTelefono) {
		this.filtroTelefono = filtroTelefono;
	}

	/**
	 * @return the filtroOpciones
	 */
	public String getFiltroOpciones() {
		return filtroOpciones;
	}

	/**
	 * @param filtroOpciones the filtroOpciones to set
	 */
	public void setFiltroOpciones(String filtroOpciones) {
		this.filtroOpciones = filtroOpciones;
	}

	
}
