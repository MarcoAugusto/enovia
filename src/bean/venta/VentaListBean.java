/**
 * 
 */
package bean.venta;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import persistence.venta.VentaListPersistence;

import entidad.Venta;
import entidad.Ingreso;
import entidad.Sucursal;

import bean.bitacora.BitacoraAccion;
import bean.util.BaseBean;
import bean.util.Constantes;
import bean.util.IGeneral;
import bean.util.IPagination;
import bean.util.Jutil;
import bean.util.SysMessage;


/**
 * @author Marcus
 *
 */
@ManagedBean(name="ventalistbean" )
@ViewScoped
public class VentaListBean extends BaseBean<Venta> implements IGeneral, IPagination {

	@Inject
	private VentaListPersistence ventalistpersistence;
	
	private String filtroCode;
	private String filtroComentario;
	private String filtroFecha;
	private String filtroCliente;
	private String filtroUsuario;
	
	
	@Override
	@PostConstruct
	public void init() { 
		iniLogs(this.getClass());
		ventalistpersistence.init();
		initFiltros();
		cargarLista();				
	}

	/**
	 * Inicializa en vacio los campos a utilizar como filtros.
	 */
	public void initFiltros(){
		filtroCode = filtroComentario = filtroFecha = filtroUsuario = filtroCliente = Constantes.VACIO;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void cargarLista(){
		try{
			this.list = ventalistpersistence.getLista();			
		}catch (Exception e) {
			this.list = new ArrayList();
			log.error( "[VentaBean.cargarLista]", e );
		}
	}		
		
	public void editar() {
		if (selected != null) {
			entidad = selected;			
			nuevo = false;
			visibleNuevoEditar = true;
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
		log.info( "entidad:" + entidad + " nuevo: " + nuevo );
	}

	public void editar(Venta obj) {
		log.info("intentando editar" + obj.getCode() );
		selected = obj;
		editar();
	}

	public void onRowSelect(org.primefaces.event.SelectEvent event) {
		selected = (Venta)event.getObject();
		log.info( "Venta seleccionado:" + selected.getCode() );
		editar();
    }	
	
	public void buscar(){
		Map<String, String> mapaValor = new HashMap<String, String>();
		if ( !filtroCode.trim().isEmpty() ){
			mapaValor.put( "code", filtroCode );
		}
		if ( !filtroComentario.trim().isEmpty() ){
			mapaValor.put( "comentario", filtroComentario );
		}
		if ( !filtroFecha.trim().isEmpty() ){
			mapaValor.put( "fecha", filtroFecha );
		}
		if ( !filtroCliente.trim().isEmpty() ){
			mapaValor.put( "proveedor", filtroCliente );
		}
		if ( !filtroUsuario.trim().isEmpty() ){
			mapaValor.put( "usuario", filtroUsuario );
		}		
		list = ventalistpersistence.getLista( mapaValor );		
	}
	
	/**
	 * Exporta el objeto Ingreso seleccionado.
	 */
	public void onVerVenta( Venta venta ){		
		jutil.addObjectParam( venta );
		log.info( "Venta ejectada: " + venta.getCode() );
	}
	
	@Override
	public void firstpage() {
		try{
			list = ventalistpersistence.firstpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "firstpage", e );
		}
	}

	@Override
	public void previouspage() {
		try{
			list = ventalistpersistence.previouspage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "previouspage", e );
		}
	}

	@Override
	public void nextpage() {
		try{
			list = ventalistpersistence.nextpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "nextpage", e );
		}
	}

	@Override
	public void lastpage() {
		try{
			list = ventalistpersistence.lastpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "lastpage", e );
		}
	}

	public int getCantidadRegistros() {
		return ventalistpersistence.getCantidadRegistros();
	}
	
	public int getCantidadPaginas() {
		return ventalistpersistence.getCantidadPaginas();
	}
	
	public int getPagina() {
		return ventalistpersistence.getPagina();
	}

	
	/**
	 * @return the filtroCode
	 */
	public String getFiltroCode() {
		return filtroCode;
	}

	/**
	 * @param filtroCode the filtroCode to set
	 */
	public void setFiltroCode(String filtroCode) {
		this.filtroCode = filtroCode;
	}	

	public String getFiltroCliente() {
		return filtroCliente;
	}

	public void setFiltroCliente(String filtroCliente) {
		this.filtroCliente = filtroCliente;
	}

	public String getFiltroUsuario() {
		return filtroUsuario;
	}

	public void setFiltroUsuario(String filtroUsuario) {
		this.filtroUsuario = filtroUsuario;
	}

	public String getFiltroComentario() {
		return filtroComentario;
	}

	public void setFiltroComentario(String filtroComentario) {
		this.filtroComentario = filtroComentario;
	}

	public String getFiltroFecha() {
		return filtroFecha;
	}

	public void setFiltroFecha(String filtroFecha) {
		this.filtroFecha = filtroFecha;
	}

	
	
}
