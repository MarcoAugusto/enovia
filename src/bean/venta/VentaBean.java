/**
 * 
 */
package bean.venta;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.apache.log4j.Logger;

import entidad.Detalleegreso;
import entidad.Egreso;
import entidad.Producto;
import entidad.Venta;

import persistence.venta.VentaPersistence;
import persistence.venta.ClientePersistence;
import persistence.inventario.AlmacenPersistence;
import persistence.inventario.IngresoPersistence;

import bean.bitacora.BitacoraAccion;
import bean.producto.BaseproductoBean;
import bean.producto.BaseproductoSelecBean;
import bean.util.BaseBean;
import bean.util.Constantes;
import bean.util.Jutil;
import bean.util.Parametros;
import bean.util.SysLog;
import bean.util.SysMessage;

/**
 * @author penama
 *
 */
@ManagedBean( name= "ventabean" )
@ViewScoped
public class VentaBean extends BaseBean<DetalleEgresoItem> implements Serializable {
		
	private String usuario = "";
	private String fecha = "";
	private Integer codeAlmacen;	
	private String egresoTipo = "";
	private String codeCliente = "";
	private String comentario = "";
	
	private Venta entidad;
	
	private BigDecimal montoTotal = new BigDecimal(0);
	private BigDecimal total = new BigDecimal(0);
	private BigDecimal desccuentoMontoTotal = new BigDecimal(0);

	private Egreso egreso = null;
	
	@Inject
	private VentaPersistence ventapersistence;

	@Inject
	private AlmacenPersistence almacenpersistence;
	
	@Inject
	private ClientePersistence clientepersistence;
	
	@Inject
	private BitacoraAccion bitacoraaccion;
	
	@PostConstruct
	public void init(){	
		iniLogs( this.getClass() );
		ventapersistence.init();
		clientepersistence.init();
		egresoTipo = Parametros.VENTA_EGRESOTIPO;
		iniNuevo();		
	}

	public void iniEditar(){
		
	}
	
	/**
	 * M�todo para cuando se tenga que realizar un nuevo ingreso.
	 */
	public void iniNuevo(){
		montoTotal = new BigDecimal( 0 );		
		fecha = jutil.getFecha( Parametros.FECHA_DDMMYYYY );
		usuario = jutil.getUsuarioN();
	}	
	
	/**
	 * Calcula el montoTotal del ingreso
	 */
	public void onUpdateMontoTotal(){	
		total = new BigDecimal(0);		
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			Detalleegreso detalleEgreso = (Detalleegreso) iter.next();			
			total = total.add( detalleEgreso.getPrecio() );			
		}		
	}		
	
	/**
	 * Adiciona un item a la lista, adem�s de actualizar los precios.
	 * @param producto
	 */
	public void onAddDetalleProducto( Producto producto ){
		if ( producto == null ){
			SysMessage.warn( "Debe seleccionar un producto!" );
			return;
		}
		if ( ExisteDuplicado(producto) ){
			SysMessage.info( "El producto " + producto.getCode() + " ya fue seleccionado.." );
			return;
		}
		Detalleegreso detalleEgreso = new Detalleegreso();
		detalleEgreso.setProducto( producto );
		detalleEgreso.setCantidad( 1 );
		detalleEgreso.setPrecio( producto.getBaseproducto().getPrecio() );
		detalleEgreso.setActivo( true );		
		onUpdateMontoTotal();
	}
				
	/**
	 * Valida que el m�todo no se encuentre duplicado en la lista.
	 * @param producto
	 * @return
	 */
	private boolean ExisteDuplicado( Producto producto ){
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			Detalleegreso detalleEgreso = (Detalleegreso) iter.next();
			if ( detalleEgreso.getProducto().getCode().equals( producto.getCode() ) ){
				return true;
			}
		}
		return false;
	}
		
	/**
	 * Quita un registro del listado...
	 */
	public void onQuitarItem( Producto producto ){
		list.remove( producto );
	}	
	
	/**
	 * Valida los datos del ingreso.
	 * @return
	 */
	private String onValidarVenta(){
		if ( codeAlmacen == null || codeAlmacen == 0 ){
			return "Debe seleccionar el almacen";
		}
		if ( codeCliente == null || codeCliente.isEmpty() ){
			return "Debe seleccionar el cliente";
		}
		if ( list.isEmpty() ){
			return "Debe adicionar al menos un catalogo...";
		}
		
//		for (Iterator iterCatalog = list.iterator(); iterCatalog.hasNext();) {			
//			DetalleIngresoItem detalleIngresoItem = (DetalleIngresoItem) iterCatalog.next();
//			if ( detalleIngresoItem.getDetalleIPIList() == null || detalleIngresoItem.getDetalleIPIList().isEmpty()  ){
//				return "Catalogo: " + detalleIngresoItem.getBaseProducto().getCode() + ", debe adicionar un producto!";
//			}
//			for (Iterator iterProducto = detalleIngresoItem.getDetalleIPIList().iterator(); iterProducto.hasNext();) {
//				DetalleIngresoProductoItem detalleIngresoProductoItem = (DetalleIngresoProductoItem) iterProducto.next();
//				if( detalleIngresoProductoItem.getColorCode() == null || detalleIngresoProductoItem.getColorCode() == 0 ){
//					return "Catalogo: " + detalleIngresoItem.getBaseProducto().getCode() + ", en uno de sus productos debe seleccionar el color!";
//				}
//				if( detalleIngresoProductoItem.getTallaCode() == null || detalleIngresoProductoItem.getTallaCode() == 0 ){
//					return "Catalogo: " + detalleIngresoItem.getBaseProducto().getCode() + ", en uno de sus productos debe seleccionar la talla!";
//				}
//				if( detalleIngresoProductoItem.getPrecio() == null ){
//					return "Catalogo: " + detalleIngresoItem.getBaseProducto().getCode() + ", en uno de sus productos debe ingresar el precio!";
//				}
//			}
//		}				
		return Constantes.VACIO;
	}
	
	/**
	 * Guardar el ingreso.
	 */
	public String save(){
		String redirect = "";
		String textValidar = onValidarVenta();
		if ( !textValidar.equals( Constantes.VACIO ) ){
			SysMessage.warn( textValidar );
			return redirect;
		}
		
//		List<Detalleingreso> detalleIngresoList = null;
//		try{
//			// preparar el ingreso.
//			prepararIngreso();
//			// preparar el detalle.
//			detalleIngresoList = prepararDetalleIngreso();
//			// preparar venta.
//			prepararVenta();
//			// persistir venta, ingreso y su detalle.
//			ventapersistence.save( entidad, egreso, detalleIngresoList );			
//			bitacoraaccion.accion(entidad.getClass().getName(), "Se guardo correctamente la venta: " + entidad.getCode() + ".");
//			SysMessage.info("Venta guardado correctamente.");
//			log.info( "redireccionado" );
//			jutil.addObjectParam( entidad );
//			redirect = "VentaView.xhtml" + Parametros.REDIRECT;
//		} catch (Exception e) {
//			log.error( "[Fallo al guardar]", e );
//			SysMessage.error("Fallo al guardar.");			
//		}
		return redirect;
	}
		
	private void prepararVenta() throws Exception {
		entidad = new Venta();
		entidad.setActivo( true );
		entidad.setFecha( new Timestamp( Calendar.getInstance().getTimeInMillis() ) );
		Object obj = clientepersistence.findClienteCode( codeCliente );
		log.info(obj);
//		entidad.setCliente( (Cliente)obj );
//		entidad.setTotalbob( montoTotal );
//		entidad.setTotaldolar( entidad.getTotalbob().divide( Parametros.DOLAR_COMPRA, BigDecimal.ROUND_HALF_UP ) );
//		entidad.setComentario(comentario);
//		entidad.setUsuario( (Usuario)jutil.getUsuarioO() );
	}
	
	/**
	 * completa los datos del ingreso antes de ser persistido.
	 * @throws Exception
	 */
	private void prepararEgreso() throws Exception {
		egreso = new Egreso();
		egreso.setActivo( true );
		egreso.setAlmacen( almacenpersistence.findAlmacenCode( codeAlmacen ) );
		egreso.setFecha(new Timestamp( Calendar.getInstance().getTimeInMillis() ) );
		egreso.setTipoegreso( egresoTipo );
		egreso.setTotal( montoTotal );
//		egreso.setUsuario( (Usuario)jutil.getUsuarioO() );
	}
	
	/**
	 * Prepar� el detalle del ingreso.
	 * @return
	 * @throws Exception
	 */
//	private List<Detalleingreso> prepararDetalleIngreso() throws Exception {
//		List<Detalleingreso> detalleIngresoList = new ArrayList<Detalleingreso>();
//		Detalleingreso detalleIngreso = null;
//		for (Iterator iterCatalogo = list.iterator(); iterCatalogo.hasNext();) {
//			DetalleIngresoItem detalleIngresoItem = (DetalleIngresoItem) iterCatalogo.next();
//			for (Iterator iterProducto = detalleIngresoItem.getDetalleIPIList().iterator(); iterProducto.hasNext();) {
//				DetalleIngresoProductoItem detalleIngresoProductoItem = (DetalleIngresoProductoItem) iterProducto.next();
//				detalleIngreso = new Detalleingreso();
//				detalleIngreso.setActivo(true);
//				detalleIngreso.setProducto( crearProducto( detalleIngresoItem.getBaseProducto(), detalleIngresoProductoItem) );
//				detalleIngreso.setCantidad( 1 );
//				detalleIngreso.setPrecio( detalleIngresoProductoItem.getPrecio() );
//				detalleIngresoList.add( detalleIngreso );
//			}
//		}
//		return detalleIngresoList;
//	}
//	
	/**
	 * Completa un producto en base a los campos completados en el detalle.
	 * @param baseProducto
	 * @param detalleIngresoProductoItem
	 * @return
	 * @throws Exception
	 */
//	private Producto crearProducto( Baseproducto baseProducto, DetalleIngresoProductoItem detalleIngresoProductoItem ) throws Exception {
//		Producto producto = new Producto();
//		producto.setActivo( true );
//		producto.setColor( colorpersistence.findColorCode( detalleIngresoProductoItem.getColorCode() ) );
//		producto.setTalla( tallapersistence.findTallaCode( detalleIngresoProductoItem.getTallaCode() ) );
//		producto.setBaseproducto( baseProducto );
//		producto.setEstado( detalleIngresoProductoItem.isPedido() ? Constantes.ESTADO_PEDIDO : Constantes.ESTADO_ENSTOCK );
////		producto.setFoto(null);
////		producto.setFotoSize(0);
//		producto.setNombre( detalleIngresoProductoItem.getNombre() );
//		return producto;
//	}
	
	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the codeAlmacen
	 */
	public Integer getCodeAlmacen() {
		return codeAlmacen;
	}

	/**
	 * @param codeAlmacen the codeAlmacen to set
	 */
	public void setCodeAlmacen(Integer codeAlmacen) {
		this.codeAlmacen = codeAlmacen;
	}

	/**
	 * @return the montoTotal
	 */
	public BigDecimal getMontoTotal() {
		return montoTotal;
	}

	/**
	 * @param montoTotal the montoTotal to set
	 */
	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}

	/**
	 * @return the ingresoTipo
	 */
	public String getIngresoTipo() {
		return egresoTipo;
	}

	/**
	 * @param ingresoTipo the ingresoTipo to set
	 */
	public void setIngresoTipo(String ingresoTipo) {
		this.egresoTipo = ingresoTipo;
	}

	/**
	 * @return the codeProveedor
	 */
	public String getCodeProveedor() {
		return codeCliente;
	}

	/**
	 * @param codeProveedor the codeProveedor to set
	 */
	public void setCodeProveedor(String codeProveedor) {
		this.codeCliente = codeProveedor;
	}

	/**
	 * @return the comentario
	 */
	public String getComentario() {
		return comentario;
	}

	/**
	 * @param comentario the comentario to set
	 */
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	/**
	 * @return the desccuentoMontoTotal
	 */
	public BigDecimal getDesccuentoMontoTotal() {
		return desccuentoMontoTotal;
	}

	/**
	 * @param desccuentoMontoTotal the desccuentoMontoTotal to set
	 */
	public void setDesccuentoMontoTotal(BigDecimal desccuentoMontoTotal) {
		this.desccuentoMontoTotal = desccuentoMontoTotal;
	}

	/**
	 * @return the total
	 */
	public BigDecimal getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	
	
}
