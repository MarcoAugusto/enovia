/**
 * 
 */
package bean.compra;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import persistence.compra.RecepcionViewPersistence;

import entidad.Detallerecepcion;
import entidad.Recepcion;

import bean.bitacora.BitacoraAccion;
import bean.util.Base;
import bean.util.Constantes;
import bean.util.IGeneral;
import bean.util.IPagination;
import bean.util.Jutil;
import bean.util.SysMessage;

/**
 * @author Marcus
 * 
 */
@ManagedBean(name = "recepcionviewbean")
@ViewScoped
public class RecepcionViewBean extends Base {

	@Inject
	private BitacoraAccion bitacoraaccion;

	@Inject
	private Jutil jutil;

	@Inject
	private RecepcionViewPersistence recepcionviewpersistence;

	private Recepcion recepcionView = null;

	private List<Detallerecepcion> detallerecepcionList = null;

	@PostConstruct
	public void init() {
		setup(this.getClass());
		onVerRecepcion();
	}

	/**
	 * Obtiene el recepcion a ver.
	 */
	public void onVerRecepcion() {
		Object obj = jutil.getObjectParam();		
		if (obj == null) {
			try {
				jutil.redirect("RecepcionList.xhtml");
			} catch (Exception e) {
				log.error("Redirect fallido", e);
			}
		}
		recepcionView = (Recepcion) obj;
		log.info("Recepcion injectado: " + recepcionView.getCode() );		
		try{
			detallerecepcionList = recepcionviewpersistence.getDetalleRecepcion(recepcionView.getCode());
		}catch (Exception e) {
			log.error( "Error al obtener detalle recepción", e );
			detallerecepcionList = new ArrayList<Detallerecepcion>();
		}
	}

	/**
	 * @return the recepcionView
	 */
	public Recepcion getRecepcionView() {
		return recepcionView;
	}

	/**
	 * @param recepcionView
	 *            the recepcionView to set
	 */
	public void setRecepcionView(Recepcion recepcionView) {
		this.recepcionView = recepcionView;
	}

	public List<Detallerecepcion> getDetallerecepcionList() {
		return detallerecepcionList;
	}

	public void setDetallerecepcionList(List<Detallerecepcion> detallerecepcionList) {
		this.detallerecepcionList = detallerecepcionList;
	}

}
