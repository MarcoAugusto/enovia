/**
 * 
 */
package bean.compra;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import persistence.compra.TransitoViewPersistence;

import entidad.Detalletransito;
import entidad.Detalletransito;
import entidad.Transito;

import bean.bitacora.BitacoraAccion;
import bean.util.Base;
import bean.util.Constantes;
import bean.util.IGeneral;
import bean.util.IPagination;
import bean.util.Jutil;
import bean.util.SysMessage;

/**
 * @author Marcus
 * 
 */
@ManagedBean(name = "transitoviewbean")
@ViewScoped
public class TransitoViewBean extends Base {

	@Inject
	private BitacoraAccion bitacoraaccion;

	@Inject
	private Jutil jutil;

	@Inject
	private TransitoViewPersistence transitoviewpersistence;

	private Transito transitoView = null;

	private List<Detalletransito> detalletransitoList = null;

	@PostConstruct
	public void init() {
		setup(this.getClass());
		onVerTransito();
	}

	/**
	 * Obtiene el transito a ver.
	 */
	public void onVerTransito() {
		Object obj = jutil.getObjectParam();		
		if (obj == null) {
			try {
				jutil.redirect("TransitoList.xhtml");
			} catch (Exception e) {
				log.error("Redirect fallido", e);
			}
		}
		transitoView = (Transito) obj;
		log.info("Transito injectado: " + transitoView.getCode() );		
		try{
			detalletransitoList = transitoviewpersistence.getDetalleTransito(transitoView.getCode());
		}catch (Exception e) {
			log.error( "Error al obtener detalle transito", e );
			detalletransitoList = new ArrayList<Detalletransito>();
		}
	}

	/**
	 * @return the transitoView
	 */
	public Transito getTransitoView() {
		return transitoView;
	}

	/**
	 * @param transitoView
	 *            the transitoView to set
	 */
	public void setTransitoView(Transito transitoView) {
		this.transitoView = transitoView;
	}

	/**
	 * @return the detalletransitoList
	 */
	public List<Detalletransito> getDetalletransitoList() {
		return detalletransitoList;
	}

	/**
	 * @param detalletransitoList the detalletransitoList to set
	 */
	public void setDetalletransitoList(List<Detalletransito> detalletransitoList) {
		this.detalletransitoList = detalletransitoList;
	}

}
