/**
 * 
 */
package bean.compra;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.apache.log4j.Logger;

import persistence.compra.CompraListPersistence;
import persistence.compra.TransitoListPersistence;
import persistence.compra.RecepcionPersistence;
import persistence.compra.TransitoPersistence;
import persistence.inventario.AlmacenPersistence;
import persistence.inventario.TraspasoPersistence;

import entidad.Almacen;
import entidad.Compra;
import entidad.Detalleegreso;
import entidad.Detalleingreso;
import entidad.Detallerecepcion;
import entidad.Detalletransito;
import entidad.Egreso;
import entidad.Ingreso;
import entidad.Producto;
import entidad.Transito;
import entidad.Traspaso;
//import entidad.Detallerecepcion;
import entidad.Recepcion;
//import entidad.Recepcion;
import entidad.Usuario;

import bean.bitacora.BitacoraAccion;
import bean.util.BaseBean;
import bean.util.Constantes;
import bean.util.Jutil;
import bean.util.Parametros;
import bean.util.SysLog;
import bean.util.SysMessage;

/**
 * @author penama
 *
 */
@ManagedBean( name= "recepcionbean" )
@ViewScoped
public class RecepcionBean extends BaseBean<DetalleRecepcionItem> implements Serializable {
		
	private String usuario = "";
	private String fecha = "";
	private String comentario = "";
	private Integer codeAlmacen;
	
	private Recepcion entidad;
	
	private Transito transito = null;
	
	private BigDecimal total = new BigDecimal(0);	

	@Inject
	private RecepcionPersistence recepcionpersistence;
	
	@Inject
	private TransitoListPersistence transitolistpersistence;
	
		
	@Inject
	private AlmacenPersistence almacenpersistence;
	
	@Inject
	private BitacoraAccion bitacoraaccion;
	
	@PostConstruct
	public void init(){	
		iniLogs( this.getClass() );
		recepcionpersistence.init();
		transitolistpersistence.init();
		almacenpersistence.init();
		iniNuevo();		
	}

	public void iniEditar(){
	}
	
	/**
	 * M�todo para cuando se tenga que realizar un nuevo ingreso.
	 */
	public void iniNuevo(){
		total = new BigDecimal( 0 );		
		fecha = jutil.getFecha( Parametros.FECHA_DDMMYYYY );
		usuario = jutil.getUsuarioN();
	}	
	
	/**
	 * Calcula el montoTotal del ingreso
	 */
	public void onUpdateTotal(){	
		total = new BigDecimal(0);		
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			DetalleRecepcionItem detalleRecepcionItem = (DetalleRecepcionItem) iter.next();
			if ( detalleRecepcionItem.isRecibir() ){				
				total = total.add( detalleRecepcionItem.getCostoCatalogo() );
			}						
		}		
	}	
				
	/**
	 * Valida que el m�todo no se encuentre duplicado en la lista.
	 * @param detalleTransito
	 * @return
	 */
	private boolean ExisteDuplicado( Detallerecepcion detalleTransito ){
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			DetalleRecepcionItem detalleRecepcionItem = (DetalleRecepcionItem) iter.next();
			if ( detalleRecepcionItem.getDetalleTransito().getCode() == detalleTransito.getCode() ){
				return true;
			}
		}
		return false;
	}
		
	/**
	 * Quita un registro del listado...
	 */
	public void onQuitarItem( DetalleRecepcionItem detalleRecepcionItem ){
		list.remove( detalleRecepcionItem );
		onUpdateTotal();
	}	
	
	/**
	 * Valida los datos del ingreso.
	 * @return
	 */
	private String onValidarRecepcion(){		
		if ( codeAlmacen == null || codeAlmacen == 0 ){
			return "Debe seleccionar un almacen destino!!";
		}
		if ( transito == null ){
			return "Debe seleccionar el c�digo de transito";
		}
		if ( transito.getCompra().getIngreso().getAlmacen().getCode() == codeAlmacen ){
			return "Debe seleccionar otro almacen distinto al origen!!";
		}
		if ( list.isEmpty() ){
			return "Debe adicionar al menos un producto a recepcionar...";
		}
		boolean selec = false;
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			DetalleRecepcionItem detalleRecepcionItem = (DetalleRecepcionItem) iter.next();
			selec = detalleRecepcionItem.isRecibir();
			if ( selec ){
				break;
			}
		}
		if ( !selec ){
			return "Debe seleccionar al menos un item.";
		}
		return Constantes.VACIO;
	}
	
	/**
	 * Guardar el ingreso.
	 */
	public String save(){
		String redirect = "";
		String textValidar = onValidarRecepcion();		
		if ( !textValidar.equals( Constantes.VACIO ) ){
			SysMessage.warn( textValidar );
			return redirect;
		}
		try{									 
			prepararRecepcion();
			// preparar el egreso y su detalle.
			Egreso egreso = prepararEgreso();
			List<Detalleegreso> detalleEgresoList = prepararDetalleEgreso();
			// preparar el ingreso y su detalle.
			Ingreso ingreso = prepararIngreso();
			List<Detalleingreso> detalleIngresoList = prepararDetalleIngreso();
			// preparar el traspaso.
			Traspaso traspaso = prepararTraspaso();			
			// persistir recepci�n y detalle.
			recepcionpersistence.save(entidad, list, traspaso, egreso, detalleEgresoList, ingreso, detalleIngresoList);
			bitacoraaccion.accion(entidad.getClass().getName(), "Se guardo correctamente la recepci�n: " + entidad.getCode() + ".");
			SysMessage.info("Recepcion guardado correctamente.");
			log.info( "redireccionado" );
			jutil.addObjectParam( entidad );			
			redirect = "RecepcionView.xhtml" + Parametros.REDIRECT;
		} catch (Exception e) {
			log.error( "[Fallo al guardar]", e );
			SysMessage.error("Fallo al guardar.");			
		}
		return redirect;
	}
		
	/**
	 * Prepara la recepci�n
	 * @throws Exception
	 */
	private void prepararRecepcion() throws Exception {
		entidad = new Recepcion();
		entidad.setActivo( true );
		entidad.setFecha( new Timestamp( Calendar.getInstance().getTimeInMillis() ) );		
		entidad.setTotal( total );		
		entidad.setComentario(comentario);
		entidad.setUsuario( (Usuario)jutil.getUsuarioO() );
		entidad.setTransito( transito );
	}
	
	/**
	 * prepara el traspaso
	 * 
	 * @throws Exception
	 */
	private Traspaso prepararTraspaso() throws Exception {		
		Traspaso traspaso = new Traspaso();		
		traspaso.setActivo(true);
		traspaso.setComentario( "RECEPION" );		
		traspaso.setFecha(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		traspaso.setMotivo( "RECEPCION" );		
		traspaso.setUsuario((Usuario) jutil.getUsuarioO());
		return traspaso;
	}
	
	/**
	 * prepara el egreso.
	 * 
	 * @throws Exception
	 */
	private Egreso prepararEgreso() throws Exception {		
		Egreso egreso = new Egreso();
		egreso.setActivo(true);
		egreso.setTotaldescuento(new BigDecimal(0));
		egreso.setAlmacen( transito.getCompra().getPedido().getAlmacen() );
		egreso.setFecha(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		egreso.setTipoegreso( "TRASPASO - RECEPCION" );
		egreso.setTotal( total );
		egreso.setUsuario((Usuario) jutil.getUsuarioO());
		return egreso;
	}
	
	/**
	 * Prepar� el detalle del egreso.
	 * 
	 * @return
	 * @throws Exception
	 */
	private List<Detalleegreso> prepararDetalleEgreso() throws Exception {
		List<Detalleegreso> detalleEgresoList = new ArrayList<Detalleegreso>();
		Detalleegreso detalleEgreso = null;
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			Producto producto = ((DetalleRecepcionItem) iter.next()).getDetalleTransito().getDetalleingreso().getProducto();
			detalleEgreso = new Detalleegreso();
			detalleEgreso.setActivo(true);			
			detalleEgreso.setProducto(producto);
			detalleEgreso.setCantidad(1);			
			// validar luego el calculo del costo.
			detalleEgreso.setPrecio( producto.getBaseproducto().getPrecio());
			detalleEgreso.setDescuento( new BigDecimal(0) );
			detalleEgresoList.add(detalleEgreso);
		}
		return detalleEgresoList;
	}


	/**
	 * Prepara el ingreso 
	 * @return
	 * @throws Exception
	 */
	private Ingreso prepararIngreso() throws Exception {
		Ingreso ingreso = new Ingreso();
		ingreso.setActivo( true );
		ingreso.setAlmacen( almacenpersistence.findAlmacenCode( codeAlmacen ) );
		ingreso.setFecha( new Timestamp(Calendar.getInstance().getTimeInMillis()) );
		ingreso.setTipoingreso( "TRASPASO" );
		ingreso.setTotal( total );
		ingreso.setUsuario( (Usuario)jutil.getUsuarioO() );
		return ingreso;
	}
	
	/**
	 * Prepar� el detalle del ingreso.
	 * 
	 * @return
	 * @throws Exception
	 */
	private List<Detalleingreso> prepararDetalleIngreso() throws Exception {
		List<Detalleingreso> detalleIngresoList = new ArrayList<Detalleingreso>();
		Detalleingreso detalleIngreso = null;
		Almacen almacenDestino = almacenpersistence.findAlmacenCode( codeAlmacen );
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			Producto producto = ((DetalleRecepcionItem) iter.next()).getDetalleTransito().getDetalleingreso().getProducto();
			producto.setAlmacen( almacenDestino );
			detalleIngreso = new Detalleingreso();
			detalleIngreso.setActivo(true);
			// ver el estado del producto.
			producto.setEstado( Constantes.ESTADO_ENSTOCK );
			detalleIngreso.setProducto(producto);
			detalleIngreso.setCantidad(1);
			// validar luego el calculo del costo.
			detalleIngreso.setPrecio(producto.getBaseproducto().getPrecio());
			detalleIngresoList.add(detalleIngreso);
		}
		return detalleIngresoList;
	}
	
	
	
	public void buscar(){
		if ( filtroBusqueda == null || filtroBusqueda.trim().isEmpty() ){
			SysMessage.info( "Debe ingresar el c�digo de transito" );
			list.clear();
			return;
		}
		String busqueda = Constantes.PREFIJO_TRANSITO + filtroBusqueda;
		list.clear();
		transito = null;
		try {
			Object obj = transitolistpersistence.findTransito( busqueda );
			if ( obj == null ){
				return;
			}
			transito = (Transito) obj;
			List<Detalletransito> detalleTransitoList = transitolistpersistence.findDetalleTransito( busqueda );			
			for (Iterator iterator = detalleTransitoList.iterator(); iterator.hasNext();) {
				Detalletransito detalleTransito = (Detalletransito) iterator.next();
				DetalleRecepcionItem detalleRecepcionItem = new DetalleRecepcionItem();
				detalleRecepcionItem.setRecibido( recepcionpersistence.findItemDetalleTransitoRecepcion( detalleTransito.getCode() ) );
				detalleRecepcionItem.setRecibir( false );
				detalleRecepcionItem.setDetalleTransito( detalleTransito );
				detalleRecepcionItem.setCostoCatalogo( detalleTransito.getDetalleingreso().getProducto().getBaseproducto().getPrecio() );
				list.add( detalleRecepcionItem );
			}
			log.info( list.size() );
			onUpdateTotal();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the comentario
	 */
	public String getComentario() {
		return comentario;
	}

	/**
	 * @param comentario the comentario to set
	 */
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	/**
	 * @return the total
	 */
	public BigDecimal getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	
	/**
	 * @return the transito
	 */
	public Transito getTransito() {
		return transito;
	}

	/**
	 * @param transito the transito to set
	 */
	public void setTransito(Transito transito) {
		this.transito = transito;
	}

	/**
	 * @return the codeAlmacen
	 */
	public Integer getCodeAlmacen() {
		return codeAlmacen;
	}

	/**
	 * @param codeAlmacen the codeAlmacen to set
	 */
	public void setCodeAlmacen(Integer codeAlmacen) {
		this.codeAlmacen = codeAlmacen;
	}

	
	
}
