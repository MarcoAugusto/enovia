/**
 * 
 */
package bean.compra;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import bean.util.SysMessage;
import entidad.Baseproducto;
import entidad.Producto;

/**
 * @author penama
 *
 */
public class DetallePedidoItem implements Serializable {
	
	private Integer codeDetalle;
	private Integer estiloCode;
	private Integer marcaCode;
	private Integer prendaCode;
	private Baseproducto baseProducto;
	private Producto producto;
	private BigDecimal precioTotal = new BigDecimal(0);
	
	private List<DetallePedidoProductoItem> detalleIPIList;
	
	public void updatePrecioTotal(){
		precioTotal = new BigDecimal(0);
		for (Iterator iter = detalleIPIList.iterator(); iter.hasNext();) {
			DetallePedidoProductoItem detalleIPI = (DetallePedidoProductoItem) iter.next();
			precioTotal = precioTotal.add( detalleIPI.getPrecio() );			
		}
	}
	
	/**
	 * Adiciona un item...
	 */
	public void addDetallePedidoProductoItem(){
		if ( baseProducto == null ){
			SysMessage.info( "Debe selecionar un base producto!!!" );
			return;
		}
		if ( detalleIPIList == null || detalleIPIList.isEmpty() ){
			detalleIPIList = new ArrayList<DetallePedidoProductoItem>();
		}
		DetallePedidoProductoItem detalleIPI = new DetallePedidoProductoItem();
		detalleIPI.setPrecio( baseProducto.getPrecio() );
		detalleIPIList.add( detalleIPI );
		updatePrecioTotal();
	}
	
	public void onQuitarItem( DetallePedidoProductoItem detalleIngresoProductoItem ){
		detalleIPIList.remove( detalleIngresoProductoItem );
	}
	
	/**
	 * @return the baseProducto
	 */
	public Baseproducto getBaseProducto() {
		return baseProducto;
	}
	/**
	 * @param baseProducto the baseProducto to set
	 */
	public void setBaseProducto(Baseproducto baseProducto) {
		this.baseProducto = baseProducto;		
	}
	/**
	 * @return the producto
	 */
	public Producto getProducto() {
		return producto;
	}
	/**
	 * @param producto the producto to set
	 */
	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	
	/**
	 * @return the codeDetalle
	 */
	public Integer getCodeDetalle() {
		return codeDetalle;
	}
	/**
	 * @param codeDetalle the codeDetalle to set
	 */
	public void setCodeDetalle(Integer codeDetalle) {
		this.codeDetalle = codeDetalle;
	}
	/**
	 * @return the estiloCode
	 */
	public Integer getEstiloCode() {
		return estiloCode;
	}
	/**
	 * @param estiloCode the estiloCode to set
	 */
	public void setEstiloCode(Integer estiloCode) {
		this.estiloCode = estiloCode;
	}
	/**
	 * @return the marcaCode
	 */
	public Integer getMarcaCode() {
		return marcaCode;
	}
	/**
	 * @param marcaCode the marcaCode to set
	 */
	public void setMarcaCode(Integer marcaCode) {
		this.marcaCode = marcaCode;
	}
	/**
	 * @return the prendaCode
	 */
	public Integer getPrendaCode() {
		return prendaCode;
	}
	/**
	 * @param prendaCode the prendaCode to set
	 */
	public void setPrendaCode(Integer prendaCode) {
		this.prendaCode = prendaCode;
	}

	/**
	 * @return the precioTotal
	 */
	public BigDecimal getPrecioTotal() {
		return precioTotal;
	}

	/**
	 * @param precioTotal the precioTotal to set
	 */
	public void setPrecioTotal(BigDecimal precioTotal) {
		this.precioTotal = precioTotal;
	}

	/**
	 * @return the detalleIPIList
	 */
	public List<DetallePedidoProductoItem> getDetalleIPIList() {
		return detalleIPIList;
	}

	/**
	 * @param detalleIPIList the detalleIPIList to set
	 */
	public void setDetalleIPIList(List<DetallePedidoProductoItem> detalleIPIList) {
		this.detalleIPIList = detalleIPIList;
	}
	

	
}
