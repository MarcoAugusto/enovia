/**
 * 
 */
package bean.compra;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author penama
 *
 */
public class DetallePedidoProductoItem implements Serializable{

	private Integer colorCode;
	private Integer tallaCode;
	private BigDecimal precio = new BigDecimal(0);	
	private String nombre;
	
	/**
	 * @return the colorCode
	 */
	public Integer getColorCode() {
		return colorCode;
	}
	/**
	 * @param colorCode the colorCode to set
	 */
	public void setColorCode(Integer colorCode) {
		this.colorCode = colorCode;
	}
	/**
	 * @return the tallaCode
	 */
	public Integer getTallaCode() {
		return tallaCode;
	}
	/**
	 * @param tallaCode the tallaCode to set
	 */
	public void setTallaCode(Integer tallaCode) {
		this.tallaCode = tallaCode;
	}
	/**
	 * @return the precio
	 */
	public BigDecimal getPrecio() {
		return precio;
	}
	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
}
