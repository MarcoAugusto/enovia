/**
 * 
 */
package bean.compra;

import java.math.BigDecimal;

import entidad.Detallerecepcion;
import entidad.Detalletransito;

/**
 * @author penama
 *
 */
public class DetalleRecepcionItem {

	private boolean recibido;
	private boolean recibir;
	private Detalletransito detalleTransito;
	private BigDecimal costoCatalogo;
	
	/**
	 * @return the recibir
	 */
	public boolean isRecibir() {
		return recibir;
	}
	/**
	 * @param recibir the recibir to set
	 */
	public void setRecibir(boolean recibir) {
		this.recibir = recibir;
	}
	/**
	 * @return the detalleTransito
	 */
	public Detalletransito getDetalleTransito() {
		return detalleTransito;
	}
	/**
	 * @param detalleTransito the detalleTransito to set
	 */
	public void setDetalleTransito(Detalletransito detalleTransito) {
		this.detalleTransito = detalleTransito;
	}
	/**
	 * @return the costoCatalogo
	 */
	public BigDecimal getCostoCatalogo() {
		return costoCatalogo;
	}
	/**
	 * @param costoCatalogo the costoCatalogo to set
	 */
	public void setCostoCatalogo(BigDecimal costoCatalogo) {
		this.costoCatalogo = costoCatalogo;
	}
	/**
	 * @return the recibido
	 */
	public boolean isRecibido() {
		return recibido;
	}
	/**
	 * @param recibido the recibido to set
	 */
	public void setRecibido(boolean recibido) {
		this.recibido = recibido;
	}
	
}
