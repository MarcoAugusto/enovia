/**
 * 
 */
package bean.compra;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.apache.log4j.Logger;

import persistence.compra.CompraListPersistence;
import persistence.compra.TransitoPersistence;

import entidad.Compra;
import entidad.Detalleingreso;
import entidad.Detalletransito;
import entidad.Transito;
import entidad.Usuario;

import bean.bitacora.BitacoraAccion;
import bean.util.BaseBean;
import bean.util.Constantes;
import bean.util.Jutil;
import bean.util.Parametros;
import bean.util.SysLog;
import bean.util.SysMessage;

/**
 * @author penama
 *
 */
@ManagedBean( name= "transitobean" )
@ViewScoped
public class TransitoBean extends BaseBean<DetalleTransitoItem> implements Serializable {
		
	private String usuario = "";
	private String fecha = "";
	private String comentario = "";	
	private String transporteEmpresa;
	private String transporteFecha;
	private String transporteFechaLlegada;
	private String transporteTipo;
	
	private Transito entidad;
		
	private Compra compra;
	
	private BigDecimal total = new BigDecimal(0);	

	private List<String> transporteTipolist = Parametros.TRANSITO_TRANSPORTE_TIPO_LIST;
	
	@Inject
	private TransitoPersistence transitopersistence;
	
	@Inject
	private CompraListPersistence compralistpersistence;
	
	@Inject
	private BitacoraAccion bitacoraaccion;
	
	@PostConstruct
	public void init(){	
		iniLogs( this.getClass() );
		transitopersistence.init();
		iniNuevo();		
	}

	public void iniEditar(){
		
	}
	
	/**
	 * M�todo para cuando se tenga que realizar un nuevo ingreso.
	 */
	public void iniNuevo(){
		total = new BigDecimal( 0 );		
		fecha = jutil.getFecha( Parametros.FECHA_DDMMYYYY );
		usuario = jutil.getUsuarioN();		
	}	
	
	/**
	 * Calcula el montoTotal del ingreso
	 */
	public void onUpdateTotal(){	
		total = new BigDecimal(0);		
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			DetalleTransitoItem detalleTransitoItem = (DetalleTransitoItem) iter.next();
			if ( detalleTransitoItem.isEnTransito() ){
				total = total.add( detalleTransitoItem.getDetalleIngreso().getPrecio() );
			}
		}		
	}	
				
	/**
	 * Valida que el m�todo no se encuentre duplicado en la lista.
	 * @param detalleIngreso
	 * @return
	 */
	private boolean ExisteDuplicado( Detalleingreso detalleIngreso ){
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			DetalleTransitoItem detalleTransitoItem = (DetalleTransitoItem) iter.next();
			if ( detalleTransitoItem.getDetalleIngreso().getCode() == detalleIngreso.getCode() ){
				return true;
			}
		}
		return false;
	}
		
	/**
	 * Quita un registro del listado...
	 */
	public void onQuitarItem( DetalleTransitoItem detalleTransitoItem ){
		list.remove( detalleTransitoItem );
		onUpdateTotal();
	}	
	
	/**
	 * Valida los datos del ingreso.
	 * @return
	 */
	private String onValidarTransito(){		
		if ( list.isEmpty() ){
			return "Debe adicionar al menos un producto a en transito...";
		}
		boolean select = false;
		for (Iterator<DetalleTransitoItem> iter = list.iterator(); iter.hasNext();) {
			DetalleTransitoItem detalleTransitoItem = iter.next();
			select = detalleTransitoItem.isEnTransito();
			if ( select ) {
				break;
			}
		}
		if ( !select ){
			return "Debe seleccionar al menos un item";
		}
		return Constantes.VACIO;
	}
	
	/**
	 * Guardar el ingreso.
	 */
	public String save(){
		String redirect = "";
		String textValidar = onValidarTransito();
		if ( !textValidar.equals( Constantes.VACIO ) ){
			SysMessage.warn( textValidar );
			return redirect;
		}
		try{						
			// preparar transito.
			prepararTransito();
			// persistir transito, ingreso y su detalle.
			List<DetalleTransitoItem> detalleTIList = preprarDetalle();
			transitopersistence.save( entidad, detalleTIList );			
			bitacoraaccion.accion(entidad.getClass().getName(), "Se guardo correctamente la transito: " + entidad.getCode() + ".");
			SysMessage.info("Transito guardado correctamente.");
			log.info( "redireccionado" );
			jutil.addObjectParam( entidad );
			redirect = "TransitoView.xhtml" + Parametros.REDIRECT;
		} catch (Exception e) {
			log.error( "[Fallo al guardar]", e );
			SysMessage.error("Fallo al guardar.");			
		}
		return redirect;
	}
		
	private void prepararTransito() throws Exception {
		entidad = new Transito();
		entidad.setActivo( true );
		entidad.setFecha( new Timestamp( Calendar.getInstance().getTimeInMillis() ) );		
		entidad.setTotal( total );		
		entidad.setComentario(comentario);
		entidad.setUsuario( (Usuario)jutil.getUsuarioO() );
		entidad.setCompra( compra );
		entidad.setTransporteEmpresa( transporteEmpresa );
		entidad.setTransporteFecha( transporteFecha );
		entidad.setTransporteFechallegada( transporteFechaLlegada );
		entidad.setTransporteTipo( transporteTipo );
	}

	public List<DetalleTransitoItem> preprarDetalle(){
		List<DetalleTransitoItem> detalleTIList = new ArrayList<>();
		DetalleTransitoItem detalleTI = null;
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			DetalleTransitoItem detalleTransitoItem = (DetalleTransitoItem) iter.next();
			if ( !detalleTransitoItem.isEnTransito() ){
				continue;
			}
			 detalleTI = new DetalleTransitoItem();
			 detalleTI.setDetalleIngreso( detalleTransitoItem.getDetalleIngreso() );
			 detalleTI.setEnTransito( detalleTransitoItem.isEnTransito() );
			 detalleTIList.add( detalleTI );
		}
		return detalleTIList; 
	}
	
	public void buscar(){
		if ( filtroBusqueda == null || filtroBusqueda.trim().isEmpty() ){
			SysMessage.info( "Debe ingresar la compra" );
			return;
		}
		String busqueda = Constantes.PREFIJO_COMPRA + filtroBusqueda;
		try {
			Object obj = compralistpersistence.findCompra( busqueda );
			if ( obj != null ){
				compra = (Compra) obj;
			}
			List<Detalleingreso> detalleIngresoList = transitopersistence.findDetalleIngresoCompra( busqueda );
			list.clear();
			for (Iterator iterator = detalleIngresoList.iterator(); iterator.hasNext();) {
				Detalleingreso detalleingreso = (Detalleingreso) iterator.next();
				DetalleTransitoItem detalleTransitoItem = new DetalleTransitoItem();
				detalleTransitoItem.setTransitando( transitopersistence.findItemDetalleIngresoTransito( detalleingreso.getCode() ) );
				detalleTransitoItem.setEnTransito( false );
				detalleTransitoItem.setDetalleIngreso( detalleingreso );
				list.add( detalleTransitoItem );
			}
			onUpdateTotal();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the comentario
	 */
	public String getComentario() {
		return comentario;
	}

	/**
	 * @param comentario the comentario to set
	 */
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	/**
	 * @return the total
	 */
	public BigDecimal getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	/**
	 * @return the compra
	 */
	public Compra getCompra() {
		return compra;
	}

	/**
	 * @param compra the compra to set
	 */
	public void setCompra(Compra compra) {
		this.compra = compra;
	}

	/**
	 * @return the transporteTipolist
	 */
	public List<String> getTransporteTipolist() {
		return transporteTipolist;
	}

	/**
	 * @param transporteTipolist the transporteTipolist to set
	 */
	public void setTransporteTipolist(List<String> transporteTipolist) {
		this.transporteTipolist = transporteTipolist;
	}

	/**
	 * @return the transporteEmpresa
	 */
	public String getTransporteEmpresa() {
		return transporteEmpresa;
	}

	/**
	 * @param transporteEmpresa the transporteEmpresa to set
	 */
	public void setTransporteEmpresa(String transporteEmpresa) {
		this.transporteEmpresa = transporteEmpresa;
	}

	/**
	 * @return the transporteFecha
	 */
	public String getTransporteFecha() {
		return transporteFecha;
	}

	/**
	 * @param transporteFecha the transporteFecha to set
	 */
	public void setTransporteFecha(String transporteFecha) {
		this.transporteFecha = transporteFecha;
	}

	/**
	 * @return the transporteFechaLlegada
	 */
	public String getTransporteFechaLlegada() {
		return transporteFechaLlegada;
	}

	/**
	 * @param transporteFechaLlegada the transporteFechaLlegada to set
	 */
	public void setTransporteFechaLlegada(String transporteFechaLlegada) {
		this.transporteFechaLlegada = transporteFechaLlegada;
	}

	/**
	 * @return the transporteTipo
	 */
	public String getTransporteTipo() {
		return transporteTipo;
	}

	/**
	 * @param transporteTipo the transporteTipo to set
	 */
	public void setTransporteTipo(String transporteTipo) {
		this.transporteTipo = transporteTipo;
	}

	
	
}
