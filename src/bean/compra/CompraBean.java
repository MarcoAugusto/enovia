/**
 * 
 */
package bean.compra;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import persistence.compra.CompraPersistence;
import persistence.compra.ProveedorPersistence;
import persistence.inventario.AlmacenPersistence;
import persistence.producto.ColorPersistence;
import persistence.producto.TallaPersistence;

import entidad.Almacen;
import entidad.Baseproducto;
import entidad.Compra;
import entidad.Detalleingreso;
import entidad.Ingreso;
import entidad.Producto;
import entidad.Proveedor;
import entidad.Usuario;

import bean.bitacora.BitacoraAccion;
import bean.inventario.DetalleIngresoItem;
import bean.inventario.DetalleIngresoProductoItem;
import bean.util.BaseBean;
import bean.util.Constantes;
import bean.util.Parametros;
import bean.util.SysMessage;

/**
 * @author penama
 *
 */
@ManagedBean( name= "comprabean" )
@ViewScoped
public class CompraBean extends BaseBean<DetalleIngresoItem> {
		
	private String usuario = "";
	private String fecha = "";
	private Integer codeAlmacen;	
	private String ingresoTipo = "";
	private String codeProveedor = "";
	private String comentario = "";
	
	private Compra entidad;
	
	private BigDecimal montoTotal = new BigDecimal(0);
	private BigDecimal total = new BigDecimal(0);

	private Ingreso ingreso = null;
	
	@Inject
	private CompraPersistence comprapersistence;

	@Inject
	private TallaPersistence tallapersistence;
	
	@Inject
	private ColorPersistence colorpersistence;
	
	@Inject
	private AlmacenPersistence almacenpersistence;
	
	@Inject
	private ProveedorPersistence proveedorpersistence;
	
	@Inject
	private BitacoraAccion bitacoraaccion;
	
	
	@PostConstruct
	public void init(){	
		iniLogs( this.getClass() );
		comprapersistence.init();
		proveedorpersistence.init();
		ingresoTipo = Parametros.COMPRA_INGRESOTIPO;
		iniNuevo();		
	}

	public void iniEditar(){
		
	}
	
	/**
	 * M�todo para cuando se tenga que realizar un nuevo ingreso.
	 */
	public void iniNuevo(){
		montoTotal = new BigDecimal( 0 );		
		fecha = jutil.getFecha( Parametros.FECHA_DDMMYYYY );
		usuario = jutil.getUsuarioN();
	}	
	
	/**
	 * Calcula el montoTotal del ingreso
	 */
	public void onUpdateMontoTotal(){	
		montoTotal = new BigDecimal(0);		
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			DetalleIngresoItem detalleIngresoItem = (DetalleIngresoItem) iter.next();
			detalleIngresoItem.updatePrecioTotal();
			montoTotal = montoTotal.add( detalleIngresoItem.getPrecioTotal() );			
		}	
	}	
	
	/**
	 * Adiciona un item a la lista, adem�s de actualizar los precios.
	 * @param detalleIngresoItem
	 */
	public void onAddDetalleIngresoProductoItem( DetalleIngresoItem detalleIngresoItem ){
		detalleIngresoItem.addDetalleIngresoProductoItem();
		onUpdateMontoTotal();
	}
		
	/**
	 * Adiciona un item al listado del detalle ingreso.
	 */
	public void onSelecBaseProducto( Baseproducto baseProducto ){		
		if ( baseProducto == null ){			
			SysMessage.warn( "Catalogo no Valido", "El Catalogo seleccionado no es valido" );			
			return ;
		}
		if ( ExisteDuplicado(baseProducto) ){			
			SysMessage.warn( "Catalogo Duplicado", "El catalogo ya se encuentra adicionado" );
			return ;
		}
		DetalleIngresoItem detalleIngresoItem = new DetalleIngresoItem();
		detalleIngresoItem.setBaseProducto( baseProducto );
		list.add( detalleIngresoItem );
		log.info( "Se adiciono al catalogo " + baseProducto.getCode() );
	}
	
	/**
	 * Valida que el m�todo no se encuentre duplicado en la lista.
	 * @param baseProducto
	 * @return
	 */
	private boolean ExisteDuplicado( Baseproducto baseProducto ){
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			DetalleIngresoItem detalleIngresoItem = (DetalleIngresoItem) iter.next();
			if ( detalleIngresoItem.getBaseProducto().getCode().equalsIgnoreCase( baseProducto.getCode() ) ){
				return true;
			}
		}
		return false;
	}
		
	/**
	 * Quita un registro del listado...
	 */
	public void onQuitarItem( DetalleIngresoItem detalleIngresoItem ){
		list.remove( detalleIngresoItem );
	}	
	
	/**
	 * Valida los datos del ingreso.
	 * @return
	 */
	private String onValidarCompra(){
		if ( codeAlmacen == null || codeAlmacen == 0 ){
			return "Debe seleccionar el almacen";
		}
		if ( codeProveedor == null || codeProveedor.isEmpty() ){
			return "Debe seleccionar el proveedor";
		}
		if ( list.isEmpty() ){
			return "Debe adicionar al menos un catalogo...";
		}
		
		for (Iterator iterCatalog = list.iterator(); iterCatalog.hasNext();) {			
			DetalleIngresoItem detalleIngresoItem = (DetalleIngresoItem) iterCatalog.next();
			if ( detalleIngresoItem.getDetalleIPIList() == null || detalleIngresoItem.getDetalleIPIList().isEmpty()  ){
				return "Catalogo: " + detalleIngresoItem.getBaseProducto().getCode() + ", debe adicionar un producto!";
			}
			for (Iterator iterProducto = detalleIngresoItem.getDetalleIPIList().iterator(); iterProducto.hasNext();) {
				DetalleIngresoProductoItem detalleIngresoProductoItem = (DetalleIngresoProductoItem) iterProducto.next();
				if( detalleIngresoProductoItem.getColorCode() == null || detalleIngresoProductoItem.getColorCode() == 0 ){
					return "Catalogo: " + detalleIngresoItem.getBaseProducto().getCode() + ", en uno de sus productos debe seleccionar el color!";
				}
				if( detalleIngresoProductoItem.getTallaCode() == null || detalleIngresoProductoItem.getTallaCode() == 0 ){
					return "Catalogo: " + detalleIngresoItem.getBaseProducto().getCode() + ", en uno de sus productos debe seleccionar la talla!";
				}
				if( detalleIngresoProductoItem.getPrecio() == null ){
					return "Catalogo: " + detalleIngresoItem.getBaseProducto().getCode() + ", en uno de sus productos debe ingresar el precio!";
				}
			}
		}				
		return Constantes.VACIO;
	}
	
	/**
	 * Guardar el ingreso.
	 */
	public String save(){
		String redirect = "";
		String textValidar = onValidarCompra();
		if ( !textValidar.equals( Constantes.VACIO ) ){
			SysMessage.warn( textValidar );
			return redirect;
		}
		
		List<Detalleingreso> detalleIngresoList = null;
		try{
			// preparar el ingreso.
			prepararIngreso();
			// preparar el detalle.
			detalleIngresoList = prepararDetalleIngreso();
			// preparar compra.
			prepararCompra();
			// persistir compra, ingreso y su detalle.
			comprapersistence.save( entidad, ingreso, detalleIngresoList );			
			bitacoraaccion.accion(entidad.getClass().getName(), "Se guardo correctamente la compra: " + entidad.getCode() + ".");
			SysMessage.info("Compra guardado correctamente.");
			log.info( "redireccionado" );
			jutil.addObjectParam( entidad );
			redirect = "CompraView.xhtml" + Parametros.REDIRECT;
		} catch (Exception e) {
			log.error( "[Fallo al guardar]", e );
			SysMessage.error("Fallo al guardar.");			
		}
		return redirect;
	}
		
	private void prepararCompra() throws Exception {
		entidad = new Compra();
		entidad.setActivo( true );
		entidad.setFecha( new Timestamp( Calendar.getInstance().getTimeInMillis() ) );
		Object obj = proveedorpersistence.findProveedorCode( codeProveedor );
		log.info(obj);
		entidad.setProveedor( (Proveedor)obj );
		entidad.setTotal( montoTotal );
		entidad.setComentario(comentario);
		entidad.setUsuario( (Usuario)jutil.getUsuarioO() );
	}
	
	/**
	 * completa los datos del ingreso antes de ser persistido.
	 * @throws Exception
	 */
	private void prepararIngreso() throws Exception {
		ingreso = new Ingreso();
		ingreso.setActivo( true );
		ingreso.setAlmacen( almacenpersistence.findAlmacenCode( codeAlmacen ) );
		ingreso.setFecha(new Timestamp( Calendar.getInstance().getTimeInMillis() ) );
		ingreso.setTipoingreso( ingresoTipo );
		ingreso.setTotal( montoTotal );
		ingreso.setUsuario( (Usuario)jutil.getUsuarioO() );
	}
	
	/**
	 * Prepar� el detalle del ingreso.
	 * @return
	 * @throws Exception
	 */
	private List<Detalleingreso> prepararDetalleIngreso() throws Exception {
		List<Detalleingreso> detalleIngresoList = new ArrayList<Detalleingreso>();
		Detalleingreso detalleIngreso = null;
		for (Iterator iterCatalogo = list.iterator(); iterCatalogo.hasNext();) {
			DetalleIngresoItem detalleIngresoItem = (DetalleIngresoItem) iterCatalogo.next();
			for (Iterator iterProducto = detalleIngresoItem.getDetalleIPIList().iterator(); iterProducto.hasNext();) {
				DetalleIngresoProductoItem detalleIngresoProductoItem = (DetalleIngresoProductoItem) iterProducto.next();
				detalleIngreso = new Detalleingreso();
				detalleIngreso.setActivo(true);
				detalleIngreso.setProducto( crearProducto( detalleIngresoItem.getBaseProducto(), detalleIngresoProductoItem) );
				detalleIngreso.setCantidad( 1 );
				detalleIngreso.setPrecio( detalleIngresoProductoItem.getPrecio() );
				detalleIngresoList.add( detalleIngreso );
			}
		}
		return detalleIngresoList;
	}
	
	/**
	 * Completa un producto en base a los campos completados en el detalle.
	 * @param baseProducto
	 * @param detalleIngresoProductoItem
	 * @return
	 * @throws Exception
	 */
	private Producto crearProducto( Baseproducto baseProducto, DetalleIngresoProductoItem detalleIngresoProductoItem ) throws Exception {
		Producto producto = new Producto();
		producto.setActivo( true );
		producto.setColor( colorpersistence.findColorCode( detalleIngresoProductoItem.getColorCode() ) );
		producto.setTalla( tallapersistence.findTallaCode( detalleIngresoProductoItem.getTallaCode() ) );
		producto.setBaseproducto( baseProducto );
		producto.setEstado( Constantes.ESTADO_ENSTOCK );
//		producto.setFoto(null);
//		producto.setFotoSize(0);
		producto.setNombre( detalleIngresoProductoItem.getNombre() );
		return producto;
	}
	
	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the codeAlmacen
	 */
	public Integer getCodeAlmacen() {
		return codeAlmacen;
	}

	/**
	 * @param codeAlmacen the codeAlmacen to set
	 */
	public void setCodeAlmacen(Integer codeAlmacen) {
		this.codeAlmacen = codeAlmacen;
	}

	/**
	 * @return the montoTotal
	 */
	public BigDecimal getMontoTotal() {
		return montoTotal;
	}

	/**
	 * @param montoTotal the montoTotal to set
	 */
	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}

	/**
	 * @return the ingresoTipo
	 */
	public String getIngresoTipo() {
		return ingresoTipo;
	}

	/**
	 * @param ingresoTipo the ingresoTipo to set
	 */
	public void setIngresoTipo(String ingresoTipo) {
		this.ingresoTipo = ingresoTipo;
	}

	/**
	 * @return the codeProveedor
	 */
	public String getCodeProveedor() {
		return codeProveedor;
	}

	/**
	 * @param codeProveedor the codeProveedor to set
	 */
	public void setCodeProveedor(String codeProveedor) {
		this.codeProveedor = codeProveedor;
	}

	/**
	 * @return the comentario
	 */
	public String getComentario() {
		return comentario;
	}

	/**
	 * @param comentario the comentario to set
	 */
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	/**
	 * @return the total
	 */
	public BigDecimal getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	
	
}
