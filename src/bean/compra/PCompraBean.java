/**
 * 
 */
package bean.compra;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import persistence.compra.CompraPersistence;
import persistence.compra.PedidoViewPersistence;
import persistence.inventario.IngresoViewPersistence;

import entidad.Compra;
import entidad.Detalleingreso;
import entidad.Detallepedido;
import entidad.Ingreso;
import entidad.Pedido;
import entidad.Producto;
import entidad.Proveedor;
import entidad.Usuario;

import bean.bitacora.BitacoraAccion;
import bean.inventario.DetalleIngresoItem;
import bean.inventario.DetalleIngresoProductoItem;
import bean.util.Base;
import bean.util.Constantes;
import bean.util.Jutil;
import bean.util.Parametros;
import bean.util.SysMessage;


/**
 * @author Marcus
 * 
 */
@ManagedBean(name = "pcomprabean")
@ViewScoped
public class PCompraBean extends Base {
	
	@Inject
	private Jutil jutil;
		
	private String comentario;
	private Pedido pedido = null;
	private List<DetallePedidoCompraItem> detallepedidocompraList = null;
	private BigDecimal total = null;
		
	@Inject
	private PedidoViewPersistence pedidoviewpersistence;
	
	@Inject
	private CompraPersistence comprapersistence;
	
	@Inject
	private BitacoraAccion bitacoraaccion;
	
	@PostConstruct
	public void init() {
		setup(this.getClass());
		onVerPedido();
		comprapersistence.init();
	}

	/**
	 * Obtiene el ingreso a ver.
	 */
	public void onVerPedido() {
		log.info("Obteniendo pedido a ver...");		
		Object obj = jutil.getObjectParam();
		log.info("obteniendo objeto injectado: " + obj);
		if (obj == null) {
			try {
				jutil.redirect("CompraList.xhtml");
			} catch (Exception e) {
				log.error("Redirect fallido", e);
			}
		}
		pedido = (Pedido)obj; 
		comentario = "Pedido[" + pedido.getCode() + "]";
		log.info("Pedido a ver: " + pedido.getCode() + " - Obteniendo detalle");
		List<Detallepedido> detallePedidoList = null;
		try{
//			detallePedidoList = pedidoviewpersistence.getDetallePedido( pedido.getCode() );
			detallePedidoList = pedidoviewpersistence.getDetpedido_X_Compra( pedido.getCode() );
		} catch (Exception e) {
			log.error( "Error al obtener detalle pedido..", e );
			detallePedidoList = new ArrayList<Detallepedido>();
		}
		DetallePedidoCompraItem dPCItem = null;
		total = new BigDecimal(0);
		detallepedidocompraList = new ArrayList<DetallePedidoCompraItem>();
		for (Iterator iter = detallePedidoList.iterator(); iter.hasNext();) {
			Detallepedido detallepedido = (Detallepedido) iter.next();
			dPCItem = new DetallePedidoCompraItem();
			dPCItem.setDetallePedido(detallepedido);
			dPCItem.setCosto( detallepedido.getCosto() );
			detallepedidocompraList.add( dPCItem );
			// actualizar el total.
			total = total.add( detallepedido.getCosto() );
		}
	}
		
	public void updateTotalDPC(){
		total = new BigDecimal(0);
		for (Iterator iter = detallepedidocompraList.iterator(); iter.hasNext();) {
			DetallePedidoCompraItem detallePCI = (DetallePedidoCompraItem) iter.next();
			total = total.add( detallePCI.getCosto() );
		}		
	}
	
	/**
	 * Proceso que realiza la compra de los pedidos seg�n los productos seleccionados.
	 */
	public String comprarPedido(){
		String redirect = "";
		String textValidar = onValidarCompra();
		if ( !textValidar.equals( Constantes.VACIO ) ){
			SysMessage.warn( textValidar );
			return redirect;
		}
		try{
			// preparar ingreso.
			Ingreso ingreso = prepararIngreso();
			// prepara compra.
			Compra compra = prepararCompra();
			// preparar detalle.
			List<Detalleingreso> detalleIngresoList = prepararDetalleIngreso();
			//guardando compra.
			comprapersistence.save(compra, ingreso, detalleIngresoList);			
			bitacoraaccion.accion(compra.getClass().getName(), "Se guardo correctamente la compra: " + compra.getCode() + ".");
			SysMessage.info("Compra guardado correctamente del pedido " + pedido.getCode());
			log.info( "redireccionado" );
			jutil.addObjectParam( compra );
			redirect = "CompraView.xhtml" + Parametros.REDIRECT;
		}catch (Exception e) {
			log.error( "[Fallo al guardar]", e );
			SysMessage.error("Fallo al guardar.");
		}
		return redirect;
	}

	/**
	 * Valida la compra a realizar
	 * @return
	 */
	private String onValidarCompra(){
		boolean selecItem = false;
		for (Iterator iterCatalog = detallepedidocompraList.iterator(); iterCatalog.hasNext();) {			
			DetallePedidoCompraItem detallePCI = (DetallePedidoCompraItem) iterCatalog.next();
			if ( detallePCI.isSelec() ){
				selecItem = true;
			}
			if ( detallePCI.getCosto().compareTo( new BigDecimal(0) ) < 0 || detallePCI.getCosto() == null ){
				return "Catalogo: " + detallePCI.getDetallePedido().getProducto().getBaseproducto().getCode() + ", Producto: " + detallePCI.getDetallePedido().getProducto().getCode() + ", debe ingresar el precio correcto";
			}			
		}	
		if ( !selecItem ){
			return "Catalogo: Debe seleccionar un producto!!!";
		}
		return Constantes.VACIO;
	}
	
	/**
	 * Prepara el detalle de la compra.
	 * @return
	 * @throws Exception
	 */
	private List<Detalleingreso> prepararDetalleIngreso() throws Exception {
		List<Detalleingreso> detalleIngresoList = new ArrayList<Detalleingreso>();
		Detalleingreso detalleIngreso = null;
		for (Iterator iterCatalogo = detallepedidocompraList.iterator(); iterCatalogo.hasNext();) {			
			DetallePedidoCompraItem detallePCI = (DetallePedidoCompraItem) iterCatalogo.next();		
			if ( !detallePCI.isSelec() ){
				continue;
			}
			detalleIngreso = new Detalleingreso();
			detalleIngreso.setActivo(true);
			Producto producto = detallePCI.getDetallePedido().getProducto();
//			log.info( "constante: [" + Constantes.ESTADO_PEDIDO + "]" );
//			log.info( "producto: " + producto.getCode() + " [" + producto.getEstado() + "] condicion: " + producto.getEstado().equalsIgnoreCase( Constantes.ESTADO_PEDIDO ));
//			if ( producto.getEstado().equalsIgnoreCase( Constantes.ESTADO_PEDIDO ) ){
//				producto.setEstado( Constantes.ESTADO_TRANSITO );
//			} else if ( producto.getEstado().equalsIgnoreCase( Constantes.ESTADO_RESERVADO_PEDIDO ) ){
//				producto.setEstado( Constantes.ESTADO_RESERVADO_TRANSITO );
//			}
//			log.info( "producto: " + producto.getCode() + " [" + producto.getEstado() + "] condicion: " + producto.getEstado().equalsIgnoreCase( Constantes.ESTADO_PEDIDO ));
			detalleIngreso.setProducto( producto );
			detalleIngreso.setCantidad( 1 );
			detalleIngreso.setPrecio( detallePCI.getCosto() );
			detalleIngreso.setDetallepedido( detallePCI.getDetallePedido() );
			
			detalleIngresoList.add( detalleIngreso );						
		}
		return detalleIngresoList;
	}
	
	/**
	 * Prepara la compra a realizar.
	 * @return
	 * @throws Exception
	 */
	private Compra prepararCompra() throws Exception {
		Compra compra = new Compra();
		compra.setActivo( true );
		compra.setFecha( new Timestamp( Calendar.getInstance().getTimeInMillis() ) );		
		compra.setProveedor( pedido.getProveedor() );
		compra.setTotal( total );
		compra.setComentario(comentario);
		compra.setUsuario( (Usuario)jutil.getUsuarioO() );
		compra.setPedido( pedido );
		return compra;
	}	
	
	/**
	 * Prepara el ingreso de la compra.
	 * @return
	 * @throws Exception
	 */
	private Ingreso prepararIngreso() throws Exception {
		Ingreso ingreso = new Ingreso();
		ingreso.setActivo( true );
		ingreso.setAlmacen( pedido.getAlmacen() );
		ingreso.setFecha(new Timestamp( Calendar.getInstance().getTimeInMillis() ) );
		ingreso.setTipoingreso( Parametros.COMPRA_INGRESOTIPO );
		ingreso.setTotal( total );
		ingreso.setUsuario( (Usuario)jutil.getUsuarioO() );
		return ingreso;
	}
	
	/**
	 * @return the pedido
	 */
	public Pedido getPedido() {
		return pedido;
	}

	/**
	 * @param pedido the pedido to set
	 */
	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	/**
	 * @return the detallepedidocompraList
	 */
	public List<DetallePedidoCompraItem> getDetallepedidocompraList() {
		return detallepedidocompraList;
	}

	/**
	 * @param detallepedidocompraList the detallepedidocompraList to set
	 */
	public void setDetallepedidocompraList(
			List<DetallePedidoCompraItem> detallepedidocompraList) {
		this.detallepedidocompraList = detallepedidocompraList;
	}

	/**
	 * @return the comentario
	 */
	public String getComentario() {
		return comentario;
	}

	/**
	 * @param comentario the comentario to set
	 */
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	/**
	 * @return the total
	 */
	public BigDecimal getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	
}
