/**
 * 
 */
package bean.compra;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import persistence.compra.PedidoViewPersistence;
import persistence.inventario.IngresoViewPersistence;

import entidad.Detallepedido;
import entidad.Pedido;
import entidad.Detalleingreso;

import bean.bitacora.BitacoraAccion;
import bean.util.Base;
import bean.util.Jutil;


/**
 * @author Marcus
 * 
 */
@ManagedBean(name = "pedidoviewbean")
@ViewScoped
public class PedidoViewBean extends Base {

	@Inject
	private BitacoraAccion bitacoraaccion;

	@Inject
	private Jutil jutil;

	@Inject
	private PedidoViewPersistence pedidoviewpersistence;

	private Pedido pedidoView = null;

	private List<Detallepedido> detallepedidoList = null;
	
	private boolean viewCompra = false;
	private List<Long> viewCompraList = null;

	@PostConstruct
	public void init() {
		setup(this.getClass());
		pedidoviewpersistence.init();
		onVerPedido();		
	}

	/**
	 * Obtiene el ingreso a ver.
	 */
	public void onVerPedido() {
		log.info("Obteniendo pedido a ver...");
		// Object obj =
		// FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(
		// "ingresoView" );
		Object obj = jutil.getObjectParam();
		log.info("obteniendo objeto injectado: " + obj);
		if (obj == null) {
			try {
				jutil.redirect("PedidoList.xhtml");
			} catch (Exception e) {
				log.error("Redirect fallido", e);
			}
		}
		pedidoView = (Pedido)obj;
		log.info("Pedido a ver: " + pedidoView.getCode() + " - Obteniendo detalle");		
		try{
			detallepedidoList = pedidoviewpersistence.getDetallePedido( pedidoView.getCode() );
			int countViewcompra = pedidoviewpersistence.getCountDetpedido_X_Compra( pedidoView.getCode() );
			log.info( "cantidad de registros pendientes por comprar: " +  countViewcompra );
			viewCompra = countViewcompra == 0;
			if ( countViewcompra > 0 ){
				viewCompraList = pedidoviewpersistence.getCodeDetpedido_X_Compra( pedidoView.getCode() );
			}
		} catch (Exception e) {
			log.error( "Error al obtener detalle pedido..", e );
			detallepedidoList = new ArrayList<Detallepedido>();
		}				
	}

	public void onComprar(){
		jutil.addObjectParam( pedidoView );
		log.info( "Eyectando pedido a comprar: " + pedidoView.getCode() );
	}
	
	public boolean viewItem( Long codeDetallePedido ){
		if ( viewCompraList == null ){
			return false;
		}
		return viewCompraList.contains( codeDetallePedido );
	}
	
	/**
	 * @return the ingresoView
	 */
	public Pedido getPedidoView() {
		return pedidoView;
	}

	/**
	 * @param ingresoView
	 *            the ingresoView to set
	 */
	public void setPedidoView(Pedido pedidoView) {
		this.pedidoView = pedidoView;
	}

	/**
	 * @return the detallepedidoList
	 */
	public List<Detallepedido> getDetallepedidoList() {
		return detallepedidoList;
	}

	/**
	 * @param detallepedidoList the detallepedidoList to set
	 */
	public void setDetallepedidoList(List<Detallepedido> detallepedidoList) {
		this.detallepedidoList = detallepedidoList;
	}

	/**
	 * @return the viewCompra
	 */
	public boolean isViewCompra() {
		return viewCompra;
	}

	/**
	 * @param viewCompra the viewCompra to set
	 */
	public void setViewCompra(boolean viewCompra) {
		this.viewCompra = viewCompra;
	}

	
	

}
