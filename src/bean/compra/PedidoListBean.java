/**
 * 
 */
package bean.compra;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import persistence.compra.PedidoListPersistence;
import persistence.compra.PedidoViewPersistence;

import entidad.Pedido;

import bean.util.BaseBean;
import bean.util.Constantes;
import bean.util.IGeneral;
import bean.util.IPagination;
import bean.util.SysMessage;


/**
 * @author Marcus
 *
 */
@ManagedBean(name="pedidolistbean" )
@ViewScoped
public class PedidoListBean extends BaseBean<Pedido> implements IGeneral, IPagination {

	@Inject
	private PedidoListPersistence pedidolistpersistence;
	
	@Inject
	private PedidoViewPersistence pedidoviewpersistence;
	
	private String filtroCode;	
	private String filtroComentario;
	private String filtroFecha;
	private String filtroAlmacen;
	private String filtroProveedor;
	private String filtroUsuario;
	
	
	@Override
	@PostConstruct
	public void init() { 
		iniLogs(this.getClass());
		pedidolistpersistence.init();
		initFiltros();
		cargarLista();				
	}

	/**
	 * Inicializa en vacio los campos a utilizar como filtros.
	 */
	public void initFiltros(){
		filtroCode = filtroAlmacen = filtroComentario = filtroFecha = filtroUsuario = filtroProveedor = Constantes.VACIO;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void cargarLista(){
		try{
			this.list = pedidolistpersistence.getLista();			
		}catch (Exception e) {
			this.list = new ArrayList();
			log.error( "[PedidoBean.cargarLista]", e );
		}
	}		
		
	public void editar() {
		if (selected != null) {
			entidad = selected;			
			nuevo = false;
			visibleNuevoEditar = true;
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
		log.info( "entidad:" + entidad + " nuevo: " + nuevo );
	}

	public void editar(Pedido obj) {
		log.info("intentando editar" + obj.getCode() );
		selected = obj;
		editar();
	}

	public void onRowSelect(org.primefaces.event.SelectEvent event) {
		selected = (Pedido)event.getObject();
		log.info( "Pedido seleccionado:" + selected.getCode() );
		editar();
    }	
	
	public void buscar(){
		Map<String, String> mapaValor = new HashMap<String, String>();
		if ( !filtroCode.trim().isEmpty() ){
			mapaValor.put( "code", filtroCode );
		}
		if ( !filtroComentario.trim().isEmpty() ){
			mapaValor.put( "comentario", filtroComentario );
		}
		if ( !filtroFecha.trim().isEmpty() ){
			mapaValor.put( "fecha", filtroFecha );
		}
		if ( !filtroProveedor.trim().isEmpty() ){
			mapaValor.put( "proveedor", filtroProveedor );
		}
		if ( !filtroUsuario.trim().isEmpty() ){
			mapaValor.put( "usuario", filtroUsuario );
		}		
		list = pedidolistpersistence.getLista( mapaValor );		
	}
	
	/**
	 * Exporta el objeto Ingreso seleccionado.
	 */
	public void onVerPedido( Pedido pedido ){		
		jutil.addObjectParam( pedido );
		log.info( "Eyectando pedido a ver: " + pedido.getCode() );
	}
	
	public void onComprar( Pedido pedido ){
		jutil.addObjectParam( pedido );
		log.info( "Eyectando pedido a comprar: " + pedido.getCode() );
	}
	
	/**
	 * indica si el pedido tiene alg�n pedido pendiente de comprar.
	 * @param pedido
	 * @return
	 */
	public boolean viewCompra( String pedido ){
		try{
			int count = pedidoviewpersistence.getCountDetpedido_X_Compra( pedido );
			return count == 0;
		}catch (Exception e) {
			log.error("viewCompra:" + pedido , e );
		}
		return false;
	}
	
	@Override
	public void firstpage() {
		try{
			list = pedidolistpersistence.firstpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "firstpage", e );
		}
	}

	@Override
	public void previouspage() {
		try{
			list = pedidolistpersistence.previouspage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "previouspage", e );
		}
	}

	@Override
	public void nextpage() {
		try{
			list = pedidolistpersistence.nextpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "nextpage", e );
		}
	}

	@Override
	public void lastpage() {
		try{
			list = pedidolistpersistence.lastpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "lastpage", e );
		}
	}

	public int getCantidadRegistros() {
		return pedidolistpersistence.getCantidadRegistros();
	}
	
	public int getCantidadPaginas() {
		return pedidolistpersistence.getCantidadPaginas();
	}
	
	public int getPagina() {
		return pedidolistpersistence.getPagina();
	}

	
	/**
	 * @return the filtroCode
	 */
	public String getFiltroCode() {
		return filtroCode;
	}

	/**
	 * @param filtroCode the filtroCode to set
	 */
	public void setFiltroCode(String filtroCode) {
		this.filtroCode = filtroCode;
	}	

	public String getFiltroProveedor() {
		return filtroProveedor;
	}

	public void setFiltroProveedor(String filtroProveedor) {
		this.filtroProveedor = filtroProveedor;
	}

	public String getFiltroUsuario() {
		return filtroUsuario;
	}

	public void setFiltroUsuario(String filtroUsuario) {
		this.filtroUsuario = filtroUsuario;
	}

	public String getFiltroComentario() {
		return filtroComentario;
	}

	public void setFiltroComentario(String filtroComentario) {
		this.filtroComentario = filtroComentario;
	}

	public String getFiltroFecha() {
		return filtroFecha;
	}

	public void setFiltroFecha(String filtroFecha) {
		this.filtroFecha = filtroFecha;
	}

	/**
	 * @return the filtroAlmacen
	 */
	public String getFiltroAlmacen() {
		return filtroAlmacen;
	}

	/**
	 * @param filtroAlmacen the filtroAlmacen to set
	 */
	public void setFiltroAlmacen(String filtroAlmacen) {
		this.filtroAlmacen = filtroAlmacen;
	}

	
	
}
