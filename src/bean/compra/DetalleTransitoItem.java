/**
 * 
 */
package bean.compra;

import entidad.Detalleingreso;

/**
 * @author penama
 *
 */
public class DetalleTransitoItem {

	private boolean transitando;
	private boolean enTransito;
	private Detalleingreso detalleIngreso;
	
	
	/**
	 * @return the detalleIngreso
	 */
	public Detalleingreso getDetalleIngreso() {
		return detalleIngreso;
	}
	/**
	 * @param detalleIngreso the detalleIngreso to set
	 */
	public void setDetalleIngreso(Detalleingreso detalleIngreso) {
		this.detalleIngreso = detalleIngreso;
	}
	/**
	 * @return the enTransito
	 */
	public boolean isEnTransito() {
		return enTransito;
	}
	/**
	 * @param enTransito the enTransito to set
	 */
	public void setEnTransito(boolean enTransito) {
		this.enTransito = enTransito;
	}
	/**
	 * @return the transitando
	 */
	public boolean isTransitando() {
		return transitando;
	}
	/**
	 * @param transitando the transitando to set
	 */
	public void setTransitando(boolean transitando) {
		this.transitando = transitando;
	}
	
	
	
		
}
