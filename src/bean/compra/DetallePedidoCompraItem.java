/**
 * 
 */
package bean.compra;

import java.math.BigDecimal;

import entidad.Detallepedido;

/**
 * @author penama
 *
 */
public class DetallePedidoCompraItem {

	private Detallepedido detallePedido;
	private BigDecimal costo;
	private boolean selec = false;
	
	/**
	 * @return the detallePedido
	 */
	public Detallepedido getDetallePedido() {
		return detallePedido;
	}
	/**
	 * @param detallePedido the detallePedido to set
	 */
	public void setDetallePedido(Detallepedido detallePedido) {
		this.detallePedido = detallePedido;
	}
	/**
	 * @return the selec
	 */
	public boolean isSelec() {
		return selec;
	}
	/**
	 * @param selec the selec to set
	 */
	public void setSelec(boolean selec) {
		this.selec = selec;
	}
	/**
	 * @return the costo
	 */
	public BigDecimal getCosto() {
		return costo;
	}
	/**
	 * @param costo the costo to set
	 */
	public void setCosto(BigDecimal costo) {
		this.costo = costo;
	}
	
	
	
}
