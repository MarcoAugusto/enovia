/**
 * 
 */
package bean.compra;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import persistence.compra.CompraListPersistence;

import entidad.Compra;
import entidad.Ingreso;
import entidad.Pedido;
import entidad.Sucursal;

import bean.bitacora.BitacoraAccion;
import bean.util.BaseBean;
import bean.util.Constantes;
import bean.util.IGeneral;
import bean.util.IPagination;
import bean.util.Jutil;
import bean.util.SysMessage;


/**
 * @author Marcus
 *
 */
@ManagedBean(name="compralistbean" )
@ViewScoped
public class CompraListBean extends BaseBean<Compra> implements IGeneral, IPagination {

	@Inject
	private CompraListPersistence compralistpersistence;
	
	private String filtroCode;
	private String filtroComentario;
	private String filtroFecha;
	private String filtroProveedor;
	private String filtroUsuario;
	
	
	@Override
	@PostConstruct
	public void init() { 
		iniLogs(this.getClass());
		compralistpersistence.init();
		initFiltros();
		cargarLista();				
	}

	/**
	 * Inicializa en vacio los campos a utilizar como filtros.
	 */
	public void initFiltros(){
		filtroCode = filtroComentario = filtroFecha = filtroUsuario = filtroProveedor = Constantes.VACIO;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void cargarLista(){
		try{
			this.list = compralistpersistence.getLista();			
		}catch (Exception e) {
			this.list = new ArrayList();
			log.error( "[CompraBean.cargarLista]", e );
		}
	}		
		
	public void editar() {
		if (selected != null) {
			entidad = selected;			
			nuevo = false;
			visibleNuevoEditar = true;
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
		log.info( "entidad:" + entidad + " nuevo: " + nuevo );
	}

	public void editar(Compra obj) {
		log.info("intentando editar" + obj.getCode() );
		selected = obj;
		editar();
	}

	public void onRowSelect(org.primefaces.event.SelectEvent event) {
		selected = (Compra)event.getObject();
		log.info( "Compra seleccionado:" + selected.getCode() );
		editar();
    }	
	
	public void buscar(){
		Map<String, String> mapaValor = new HashMap<String, String>();
		if ( !filtroCode.trim().isEmpty() ){
			mapaValor.put( "code", filtroCode );
		}
		if ( !filtroComentario.trim().isEmpty() ){
			mapaValor.put( "comentario", filtroComentario );
		}
		if ( !filtroFecha.trim().isEmpty() ){
			mapaValor.put( "fecha", filtroFecha );
		}
		if ( !filtroProveedor.trim().isEmpty() ){
			mapaValor.put( "proveedor", filtroProveedor );
		}
		if ( !filtroUsuario.trim().isEmpty() ){
			mapaValor.put( "usuario", filtroUsuario );
		}		
		list = compralistpersistence.getLista( mapaValor );		
	}
	
	/**
	 * Exporta el objeto Ingreso seleccionado.
	 */
	public void onVerCompra( Compra compra ){		
		jutil.addObjectParam( compra );
		log.info( "Compra ejectada: " + compra.getCode() );
	}
	
	public void OnVerPedido( Pedido pedido ){
		jutil.addObjectParam( pedido );
		log.info( "ver pedido: " + pedido.getCode() );
	}
	
	@Override
	public void firstpage() {
		try{
			list = compralistpersistence.firstpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "firstpage", e );
		}
	}

	@Override
	public void previouspage() {
		try{
			list = compralistpersistence.previouspage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "previouspage", e );
		}
	}

	@Override
	public void nextpage() {
		try{
			list = compralistpersistence.nextpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "nextpage", e );
		}
	}

	@Override
	public void lastpage() {
		try{
			list = compralistpersistence.lastpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "lastpage", e );
		}
	}

	public int getCantidadRegistros() {
		return compralistpersistence.getCantidadRegistros();
	}
	
	public int getCantidadPaginas() {
		return compralistpersistence.getCantidadPaginas();
	}
	
	public int getPagina() {
		return compralistpersistence.getPagina();
	}

	
	/**
	 * @return the filtroCode
	 */
	public String getFiltroCode() {
		return filtroCode;
	}

	/**
	 * @param filtroCode the filtroCode to set
	 */
	public void setFiltroCode(String filtroCode) {
		this.filtroCode = filtroCode;
	}	

	public String getFiltroProveedor() {
		return filtroProveedor;
	}

	public void setFiltroProveedor(String filtroProveedor) {
		this.filtroProveedor = filtroProveedor;
	}

	public String getFiltroUsuario() {
		return filtroUsuario;
	}

	public void setFiltroUsuario(String filtroUsuario) {
		this.filtroUsuario = filtroUsuario;
	}

	public String getFiltroComentario() {
		return filtroComentario;
	}

	public void setFiltroComentario(String filtroComentario) {
		this.filtroComentario = filtroComentario;
	}

	public String getFiltroFecha() {
		return filtroFecha;
	}

	public void setFiltroFecha(String filtroFecha) {
		this.filtroFecha = filtroFecha;
	}

	
	
}
