/**
 * 
 */
package bean.compra;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import persistence.inventario.IngresoViewPersistence;

import entidad.Compra;
import entidad.Detalleingreso;

import bean.bitacora.BitacoraAccion;
import bean.util.Base;
import bean.util.Jutil;


/**
 * @author Marcus
 * 
 */
@ManagedBean(name = "compraviewbean")
@ViewScoped
public class CompraViewBean extends Base {

	@Inject
	private BitacoraAccion bitacoraaccion;

	@Inject
	private Jutil jutil;

	@Inject
	private IngresoViewPersistence ingresoviewpersistence;

	private Compra compraView = null;

	private List<Detalleingreso> detalleingresoList = null;

	@PostConstruct
	public void init() {
		setup(this.getClass());
		ingresoviewpersistence.init();
		onVerCompra();		
	}

	/**
	 * Obtiene el ingreso a ver.
	 */
	public void onVerCompra() {
		log.info("Obteniendo compra a ver...");
		// Object obj =
		// FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(
		// "ingresoView" );
		Object obj = jutil.getObjectParam();
		log.info("obteniendo objeto injectado: " + obj);
		if (obj == null) {
			try {
				jutil.redirect("CompraList.xhtml");
			} catch (Exception e) {
				log.error("Redirect fallido", e);
			}
		}
		compraView = (Compra)obj;
		log.info("Compra a ver: " + compraView.getCode() + " - Obteniendo detalle");
		log.info("Ingreso: " + compraView.getIngreso() );
		log.info("Ingreso code: " + compraView.getIngreso().getCode() );
		try{
			detalleingresoList = ingresoviewpersistence.getDetalleIngreso(compraView.getIngreso().getCode());
		} catch (Exception e) {
			log.error( "Error al obtener detalle compra..", e );
			detalleingresoList = new ArrayList<Detalleingreso>();
		}		
		log.info("Detalle.size: " + detalleingresoList.size());
	}

	/**
	 * @return the ingresoView
	 */
	public Compra getCompraView() {
		return compraView;
	}

	/**
	 * @param ingresoView
	 *            the ingresoView to set
	 */
	public void setCompraView(Compra compraView) {
		this.compraView = compraView;
	}

	public List<Detalleingreso> getDetalleingresoList() {
		return detalleingresoList;
	}

	public void setDetalleingresoList(List<Detalleingreso> detalleingresoList) {
		this.detalleingresoList = detalleingresoList;
	}

}
