/**
 * 
 */
package bean.compra;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import persistence.compra.TransitoSelectDetalleIngresoPersistence;
import persistence.producto.ProductoInventarioPersistence;
import persistence.producto.ProductoPersistence;

import entidad.Baseproducto;
import entidad.Detalleingreso;
import entidad.Producto;
import entidad.Sucursal;

import bean.bitacora.BitacoraAccion;
import bean.util.BaseBean;
import bean.util.Constantes;
import bean.util.IGeneral;
import bean.util.IPagination;
import bean.util.Jutil;
import bean.util.SysMessage;
import bean.util.UploadBean;


/**
 * @author Marcus
 *
 */
@ManagedBean( name = "recepcionselectdetalleingresobean" )
@ViewScoped
public class TransitoSelectDetalleIngresoBean extends BaseBean<Detalleingreso> implements IGeneral, IPagination {

	@Inject
	private TransitoSelectDetalleIngresoPersistence recepcionselectdetalleingresopersistence;
	
	@Inject
	private Jutil jutil;
		
	private String filtroCompra;
	
	private String filtroCode;	
	private String filtroCatalogo;
	private String filtroPrenda;
	private String filtroEstilo;
	private String filtroMarca;
	private String filtroColor;
	private String filtroTalla;
	private String filtroNombre;
	private String filtroEstado;
	
	
	@Override
	@PostConstruct
	public void init() { 		
		iniLogs(this.getClass());
		recepcionselectdetalleingresopersistence.init();		
		initFiltros();
	}

	public void onResetVariables(){		
		initFiltros();		
		selected = null;
		list = new ArrayList<Detalleingreso>();
	}
	
	/**
	 * Inicializa en vacio los campos a utilizar como filtros.
	 */
	public void initFiltros(){
		filtroCode = filtroCatalogo = filtroPrenda = filtroEstilo = filtroMarca = filtroColor = filtroTalla = filtroNombre = filtroEstado = filtroCompra = Constantes.VACIO;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void cargarLista(){
		try{
			this.list = recepcionselectdetalleingresopersistence.getLista();			
		}catch (Exception e) {
			this.list = new ArrayList();
			log.error( "[ProductoInventarioBean.cargarLista]", e );
		}
	}
	
	public void onRowSelect(org.primefaces.event.SelectEvent event) {
		selected = (Detalleingreso)event.getObject();
		log.info( "DetalleIngreso seleccionado:" + selected.getCode() + " Producto: " + selected.getProducto().getCode() );	
    }
	
	
	public void buscar(){
		if ( filtroCompra.trim().isEmpty() ){
			SysMessage.info( "Debe indicar la compra a filtrar!!!" );
			list.clear();
			return;
		}
		// asignar el filtro para la compra
		recepcionselectdetalleingresopersistence.setFiltroCompra( "COM" + filtroCompra.trim() );
		Map<String, String> mapaValor = new HashMap<String, String>();		
		if ( !filtroCode.trim().isEmpty() ){
			mapaValor.put( "code", filtroCode );
		}		
		if ( !filtroCatalogo.trim().isEmpty() ){
			mapaValor.put( "catalogo", filtroCatalogo );
		}
		if ( !filtroPrenda.trim().isEmpty() ){
			mapaValor.put( "prenda", filtroPrenda );
		}
		if ( !filtroEstilo.trim().isEmpty() ){
			mapaValor.put( "estilo", filtroEstilo );
		}
		if ( !filtroMarca.trim().isEmpty() ){
			mapaValor.put( "marca", filtroMarca );
		}
		if ( !filtroColor.trim().isEmpty() ){
			mapaValor.put( "color", filtroColor );
		}
		if ( !filtroTalla.trim().isEmpty() ){
			mapaValor.put( "talla", filtroTalla );
		}
		if ( !filtroNombre.trim().isEmpty() ){
			mapaValor.put( "nombre", filtroNombre );
		}
		if ( !filtroEstado.trim().isEmpty() ){
			mapaValor.put( "estado", filtroEstado );
		}		
		list = recepcionselectdetalleingresopersistence.getLista( mapaValor );		
	}
	
	@Override
	public void firstpage() {
		try{
			list = recepcionselectdetalleingresopersistence.firstpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "firstpage", e );
		}
	}

	@Override
	public void previouspage() {
		try{
			list = recepcionselectdetalleingresopersistence.previouspage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "previouspage", e );
		}
	}

	@Override
	public void nextpage() {
		try{
			list = recepcionselectdetalleingresopersistence.nextpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "nextpage", e );
		}
	}

	@Override
	public void lastpage() {
		try{
			list = recepcionselectdetalleingresopersistence.lastpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "lastpage", e );
		}
	}

	public int getCantidadRegistros() {
		return recepcionselectdetalleingresopersistence.getCantidadRegistros();
	}
	
	public int getCantidadPaginas() {
		return recepcionselectdetalleingresopersistence.getCantidadPaginas();
	}
	
	public int getPagina() {
		return recepcionselectdetalleingresopersistence.getPagina();
	}

	
	/**
	 * @return the filtroCode
	 */
	public String getFiltroCode() {
		return filtroCode;
	}

	/**
	 * @param filtroCode the filtroCode to set
	 */
	public void setFiltroCode(String filtroCode) {
		this.filtroCode = filtroCode;
	}
	
	public String getFiltroColor() {
		return filtroColor;
	}

	public void setFiltroColor(String filtroColor) {
		this.filtroColor = filtroColor;
	}

	public String getFiltroTalla() {
		return filtroTalla;
	}

	public void setFiltroTalla(String filtroTalla) {
		this.filtroTalla = filtroTalla;
	}

	public String getFiltroEstado() {
		return filtroEstado;
	}

	public void setFiltroEstado(String filtroEstado) {
		this.filtroEstado = filtroEstado;
	}	

	/**
	 * @return the filtroCatalogo
	 */
	public String getFiltroCatalogo() {
		return filtroCatalogo;
	}

	/**
	 * @param filtroCatalogo the filtroCatalogo to set
	 */
	public void setFiltroCatalogo(String filtroCatalogo) {
		this.filtroCatalogo = filtroCatalogo;
	}

	/**
	 * @return the filtroPrenda
	 */
	public String getFiltroPrenda() {
		return filtroPrenda;
	}

	/**
	 * @param filtroPrenda the filtroPrenda to set
	 */
	public void setFiltroPrenda(String filtroPrenda) {
		this.filtroPrenda = filtroPrenda;
	}

	/**
	 * @return the filtroEstilo
	 */
	public String getFiltroEstilo() {
		return filtroEstilo;
	}

	/**
	 * @param filtroEstilo the filtroEstilo to set
	 */
	public void setFiltroEstilo(String filtroEstilo) {
		this.filtroEstilo = filtroEstilo;
	}

	/**
	 * @return the filtroMarca
	 */
	public String getFiltroMarca() {
		return filtroMarca;
	}

	/**
	 * @param filtroMarca the filtroMarca to set
	 */
	public void setFiltroMarca(String filtroMarca) {
		this.filtroMarca = filtroMarca;
	}

	/**
	 * @return the filtroNombre
	 */
	public String getFiltroNombre() {
		return filtroNombre;
	}

	/**
	 * @param filtroNombre the filtroNombre to set
	 */
	public void setFiltroNombre(String filtroNombre) {
		this.filtroNombre = filtroNombre;
	}

	/**
	 * @return the filtroCompra
	 */
	public String getFiltroCompra() {
		return filtroCompra;
	}

	/**
	 * @param filtroCompra the filtroCompra to set
	 */
	public void setFiltroCompra(String filtroCompra) {
		this.filtroCompra = filtroCompra;
	}	
	
}
