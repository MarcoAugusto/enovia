/**
 * 
 */
package bean.compra;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import persistence.compra.RecepcionListPersistence;

import entidad.Recepcion;

import bean.util.BaseBean;
import bean.util.Constantes;
import bean.util.IGeneral;
import bean.util.IPagination;
import bean.util.Jutil;
import bean.util.SysMessage;


/**
 * @author Marcus
 *
 */
@ManagedBean(name="recepcionlistbean" )
@ViewScoped
public class RecepcionListBean extends BaseBean<Recepcion> implements IGeneral, IPagination {

	@Inject
	private RecepcionListPersistence recepcionlistpersistence;
	
	private String filtroCode;
	private String filtroComentario;
	private String filtroFecha;
	private String filtroRecepcion;
	private String filtroUsuario;
	
	
	@Override
	@PostConstruct
	public void init() { 
		iniLogs(this.getClass());
		recepcionlistpersistence.init();
		initFiltros();
		cargarLista();		
	}

	/**
	 * Inicializa en vacio los campos a utilizar como filtros.
	 */
	public void initFiltros(){
		filtroCode = filtroComentario = filtroFecha = filtroUsuario = filtroRecepcion = Constantes.VACIO;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void cargarLista(){
		try{
			this.list = recepcionlistpersistence.getLista();			
		}catch (Exception e) {
			this.list = new ArrayList();
			log.error( "[RecepcionBean.cargarLista]", e );
		}
	}		
		
	public void editar() {
		if (selected != null) {
			entidad = selected;			
			nuevo = false;
			visibleNuevoEditar = true;
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
		log.info( "entidad:" + entidad + " nuevo: " + nuevo );
	}

	public void editar(Recepcion obj) {
		log.info("intentando editar" + obj.getCode() );
		selected = obj;
		editar();
	}

	public void onRowSelect(org.primefaces.event.SelectEvent event) {
		selected = (Recepcion)event.getObject();
		log.info( "Recepcion seleccionado:" + selected.getCode() );
		editar();
    }	
	
	public void buscar(){
		Map<String, String> mapaValor = new HashMap<String, String>();
		if ( !filtroCode.trim().isEmpty() ){
			mapaValor.put( "code", filtroCode );
		}
		if ( !filtroComentario.trim().isEmpty() ){
			mapaValor.put( "comentario", filtroComentario );
		}
		if ( !filtroFecha.trim().isEmpty() ){
			mapaValor.put( "fecha", filtroFecha );
		}
		if ( !filtroRecepcion.trim().isEmpty() ){
			mapaValor.put( "compra", filtroRecepcion );
		}
		if ( !filtroUsuario.trim().isEmpty() ){
			mapaValor.put( "usuario", filtroUsuario );
		}		
		list = recepcionlistpersistence.getLista( mapaValor );		
	}
	
	/**
	 * Exporta el objeto Ingreso seleccionado.
	 */
	public void onVerRecepcion( Recepcion recibido ){			
		jutil.addObjectParam( recibido );		
		log.info( "Recepcion ejectada : " + recibido.getCode() );
	}
	
	@Override
	public void firstpage() {
		try{
			list = recepcionlistpersistence.firstpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "firstpage", e );
		}
	}

	@Override
	public void previouspage() {
		try{
			list = recepcionlistpersistence.previouspage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "previouspage", e );
		}
	}

	@Override
	public void nextpage() {
		try{
			list = recepcionlistpersistence.nextpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "nextpage", e );
		}
	}

	@Override
	public void lastpage() {
		try{
			list = recepcionlistpersistence.lastpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "lastpage", e );
		}
	}

	public int getCantidadRegistros() {
		return recepcionlistpersistence.getCantidadRegistros();
	}
	
	public int getCantidadPaginas() {
		return recepcionlistpersistence.getCantidadPaginas();
	}
	
	public int getPagina() {
		return recepcionlistpersistence.getPagina();
	}

	
	/**
	 * @return the filtroCode
	 */
	public String getFiltroCode() {
		return filtroCode;
	}

	/**
	 * @param filtroCode the filtroCode to set
	 */
	public void setFiltroCode(String filtroCode) {
		this.filtroCode = filtroCode;
	}	
	
	public String getFiltroUsuario() {
		return filtroUsuario;
	}

	public void setFiltroUsuario(String filtroUsuario) {
		this.filtroUsuario = filtroUsuario;
	}

	public String getFiltroComentario() {
		return filtroComentario;
	}

	public void setFiltroComentario(String filtroComentario) {
		this.filtroComentario = filtroComentario;
	}

	public String getFiltroFecha() {
		return filtroFecha;
	}

	public void setFiltroFecha(String filtroFecha) {
		this.filtroFecha = filtroFecha;
	}

	/**
	 * @return the filtroRecepcion
	 */
	public String getFiltroRecepcion() {
		return filtroRecepcion;
	}

	/**
	 * @param filtroRecepcion the filtroRecepcion to set
	 */
	public void setFiltroRecepcion(String filtroRecepcion) {
		this.filtroRecepcion = filtroRecepcion;
	}
	
}
