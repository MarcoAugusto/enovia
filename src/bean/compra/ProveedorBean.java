/**
 * 
 */
package bean.compra;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import persistence.compra.ProveedorPersistence;

import entidad.Proveedor;

import bean.bitacora.BitacoraAccion;
import bean.util.BaseBean;
import bean.util.Constantes;
import bean.util.IGeneral;
import bean.util.IPagination;
import bean.util.Jutil;
import bean.util.Parametros;
import bean.util.SysMessage;


/**
 * @author Marcus
 *
 */
@ManagedBean(name="proveedorbean")
@ViewScoped
public class ProveedorBean extends BaseBean<Proveedor> implements IGeneral, IPagination {

	@Inject
	private ProveedorPersistence proveedorpersistence;
	
	@Inject
	private BitacoraAccion bitacoraaccion;
	
	@Inject
	private Jutil jutil;
		
	private String filtroCode;
	private String filtroNombre;
	private String filtroCodigo;	
	private String filtroContacto;
	private String filtroTelefono;
	private String filtroDireccion;		
	
	@Override
	@PostConstruct
	public void init() { 
		iniLogs(this.getClass());
		log.info("iniciliazando proveedor");
		proveedorpersistence.init();
		initFiltros();
		cargarLista();
		nuevo();		
	}

	/**
	 * Inicializa en vacio los campos a utilizar como filtros.
	 */
	public void initFiltros(){
		filtroCode = filtroDireccion = filtroNombre = filtroCodigo = filtroContacto = filtroTelefono = filtroTelefono  = Constantes.VACIO;		
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void cargarLista(){
		try{
			this.list = proveedorpersistence.getLista();			
		}catch (Exception e) {
			this.list = new ArrayList<Proveedor>();
			log.error( "[ProveedorBean.cargarLista]", e );
		}
	}
	
	public void nuevo(){
		entidad = new Proveedor();
		entidad.setActivo(true);
		entidad.setSitio( Constantes.VACIO );
		entidad.setContactoEmail( Constantes.VACIO );
		nuevo = true;
		visibleNuevoEditar = false;
		selected = null;
		initFiltros();
	}
	
	public void nuevaEntidad() {
		nuevo();
		visibleNuevoEditar = true;
	}
	
	public void guardar() {
		String validacion = proveedorpersistence.validarCampos(entidad, nuevo);
		if (validacion.trim().isEmpty()) {
			if (nuevo) {
				try {
					proveedorpersistence.save( entidad );					
					log.info("[proveedor: " + jutil.getUsuarioN() + ", proveedor: " + entidad.getCode() + "|" + entidad.getNombre() + "] [Guardado correctamente]");					
					SysMessage.info("Guardado correctamente.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Se guardo correctamente el proveedor: " + entidad.getCode() + "|" + entidad.getNombre() + ".");
					nuevo();
					cargarLista();
					visibleNuevoEditar = true;
				} catch (Exception e) {
					log.error("[proveedor: " + jutil.getUsuarioN() + ", proveedor: " + entidad.getCode() + "|" + entidad.getNombre() + "] [Fallo al guardar]", e);
					SysMessage.error("Fallo al guardar.");					
				}
			} else {
				try {
					proveedorpersistence.update(entidad);					
					log.info("[proveedor: " + jutil.getUsuarioN() + ", proveedor: " + entidad.getCode() + "|" + entidad.getNombre() + "] [Guardado correctamente]");
					SysMessage.info("Actualizado correctamente.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Se actualizado correctamente el proveedor: " + entidad.getCode() + "|" + entidad.getNombre() + ".");
					nuevo();
					cargarLista();
					visibleNuevoEditar = false;
				} catch (Exception e) {
					log.error("[proveedor: " + jutil.getUsuarioN() + ", proveedor: " + entidad.getCode() + "|" + entidad.getNombre() + "] [Fallo al guardar]", e);
					SysMessage.error("Fallo al guardar.");					
				}
			}
		} else {
			log.warn("[proveedor: " + jutil.getUsuarioN() + ", proveedor:" + "] [" + validacion + "]");
			SysMessage.warn(validacion);
		}
	}	

	public void editar() {
		if (selected != null) {
			entidad = selected;			
			nuevo = false;
			visibleNuevoEditar = true;
		} else {
			visibleNuevoEditar = false;
			log.warn("[proveedor: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
		log.info( "entidad:" + entidad + " nuevo: " + nuevo );
	}

	public void editar(Proveedor obj) {
		log.info("intentando editar" + obj.getNombre() );
		selected = obj;
		editar();
	}

	public void onRowSelect(org.primefaces.event.SelectEvent event) {
		selected = (Proveedor)event.getObject();
		log.info( "selected: " + selected );
		log.info( "Proveedor seleccionado:" + selected.getNombre() );
		editar();
    }
	
	public void eliminar() {
		if (selected != null) {			
			try {
				selected.setActivo( false );
				proveedorpersistence.update(selected);				
				log.info("[proveedor: " + jutil.getUsuarioN() + ", proveedor: " + selected.getCode() + "|" + selected.getNombre() + "] [Eliminado correctamente]");
				SysMessage.info("Eliminado correctamente.");
				bitacoraaccion.accion(selected.getClass().getName(), "Se elimino correctamente el proveedor: " + selected.getCode() + "|" + selected.getNombre() + ".");
				cargarLista();
				nuevo();
			} catch (Exception e) {
				log.error("[proveedor: " + jutil.getUsuarioN() + ", proveedor: " + selected.getCode() + "|" + selected.getNombre() + "] [Fallo al eliminar]", e);
				SysMessage.info("Fallo al eliminar.");
				bitacoraaccion.accion(selected.getClass().getName(), "Fallo al eliminar el proveedor: " + selected.getCode() + "|" + selected.getNombre() + ".");
			}
		} else {
			log.warn("[proveedor: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
	}
		
	public void eliminar(Proveedor obj) {
		selected = obj;
		eliminar();
	}
	
	public void buscar(){
		Map<String, String> mapaValor = new HashMap<String, String>();		
		if ( !filtroNombre.trim().isEmpty() ){
			mapaValor.put( "nombre", filtroNombre );
		}
		if ( !filtroCode.trim().isEmpty() ){
			mapaValor.put( "code", filtroCode );
		}
		if ( !filtroCodigo.trim().isEmpty() ){
			mapaValor.put( "codigo", filtroCodigo );
		}
		if ( !filtroDireccion.trim().isEmpty() ){
			mapaValor.put( "direccion", filtroDireccion );
		}
		if ( !filtroContacto.trim().isEmpty() ){
			mapaValor.put( "contacto", filtroContacto );
		}				
		if ( !filtroTelefono.trim().isEmpty() ){
			mapaValor.put( "telefono", filtroTelefono );
		}		
		list = proveedorpersistence.getLista( mapaValor );		
	}
	
	/**
	 * Obtiene al proveedor de la lista cargada previamente, a partir del code 
	 * @param code
	 * @return
	 */
	public Proveedor obtenerProveedorCode( String code ){					
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			Proveedor proveedor = (Proveedor) iter.next();
			if ( proveedor.getCode().equalsIgnoreCase( code ) ){
				return proveedor;
			}
		}
		return null;
	}
	
	@Override
	public void firstpage() {
		try{
			list = proveedorpersistence.firstpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "firstpage", e );
		}
	}

	@Override
	public void previouspage() {
		try{
			list = proveedorpersistence.previouspage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "previouspage", e );
		}
	}

	@Override
	public void nextpage() {
		try{
			list = proveedorpersistence.nextpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "nextpage", e );
		}
	}

	@Override
	public void lastpage() {
		try{
			list = proveedorpersistence.lastpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "lastpage", e );
		}
	}

	public int getCantidadRegistros() {
		return proveedorpersistence.getCantidadRegistros();
	}
	
	public int getCantidadPaginas() {
		return proveedorpersistence.getCantidadPaginas();
	}
	
	public int getPagina() {
		return proveedorpersistence.getPagina();
	}

	/**
	 * @return the filtroNombre
	 */
	public String getFiltroNombre() {
		return filtroNombre;
	}

	/**
	 * @param filtroNombre the filtroNombre to set
	 */
	public void setFiltroNombre(String filtroNombre) {
		this.filtroNombre = filtroNombre;
	}
	
	/**
	 * @return the filtroTelefono
	 */
	public String getFiltroTelefono() {
		return filtroTelefono;
	}

	/**
	 * @param filtroTelefono the filtroTelefono to set
	 */
	public void setFiltroTelefono(String filtroTelefono) {
		this.filtroTelefono = filtroTelefono;
	}

	public String getFiltroCode() {
		return filtroCode;
	}

	public void setFiltroCode(String filtroCode) {
		this.filtroCode = filtroCode;
	}

	public String getFiltroCodigo() {
		return filtroCodigo;
	}

	public void setFiltroCodigo(String filtroCodigo) {
		this.filtroCodigo = filtroCodigo;
	}

	public String getFiltroContacto() {
		return filtroContacto;
	}

	public void setFiltroContacto(String filtroContacto) {
		this.filtroContacto = filtroContacto;
	}

	public String getFiltroDireccion() {
		return filtroDireccion;
	}

	public void setFiltroDireccion(String filtroDireccion) {
		this.filtroDireccion = filtroDireccion;
	}
	
}
