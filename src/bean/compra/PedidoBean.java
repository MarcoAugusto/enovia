/**
 * 
 */
package bean.compra;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import persistence.compra.PedidoPersistence;
import persistence.compra.ProveedorPersistence;
import persistence.inventario.AlmacenPersistence;
import persistence.producto.ColorPersistence;
import persistence.producto.TallaPersistence;

import entidad.Baseproducto;
import entidad.Pedido;
import entidad.Detallepedido;
import entidad.Producto;
import entidad.Proveedor;
import entidad.Usuario;
import bean.bitacora.BitacoraAccion;
import bean.util.BaseBean;
import bean.util.Constantes;
import bean.util.Parametros;
import bean.util.SysMessage;

/**
 * @author penama
 *
 */
@ManagedBean( name= "pedidobean" )
@ViewScoped
public class PedidoBean extends BaseBean<DetallePedidoItem> {
		
	private String usuario = "";
	private String fecha = "";
	private Integer codeAlmacen;		
	private String codeProveedor = "";
	private String comentario = "";
	
	private Pedido entidad;
		
	private BigDecimal total = new BigDecimal(0);
	
	@Inject
	private PedidoPersistence pedidopersistence;

	@Inject
	private TallaPersistence tallapersistence;
	
	@Inject
	private ColorPersistence colorpersistence;
	
	@Inject
	private AlmacenPersistence almacenpersistence;
	
	@Inject
	private ProveedorPersistence proveedorpersistence;
	
	@Inject
	private BitacoraAccion bitacoraaccion;
	
	
	@PostConstruct
	public void init(){	
		iniLogs( this.getClass() );
		pedidopersistence.init();
//		proveedorpersistence.init();		
		iniNuevo();		
	}

	public void iniEditar(){
		
	}
	
	/**
	 * M�todo para cuando se tenga que realizar un nuevo pedido.
	 */
	public void iniNuevo(){
		total = new BigDecimal( 0 );		
		fecha = jutil.getFecha( Parametros.FECHA_DDMMYYYY );
		usuario = jutil.getUsuarioN();
	}	
	
	/**
	 * Calcula el montoTotal del pedido
	 */
	public void onUpdateMontoTotal(){	
		total = new BigDecimal(0);		
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			DetallePedidoItem detallePedidoItem = (DetallePedidoItem) iter.next();
			detallePedidoItem.updatePrecioTotal();
			total = total.add( detallePedidoItem.getPrecioTotal() );			
		}			
	}	
	
	/**
	 * Adiciona un item a la lista, adem�s de actualizar los precios.
	 * @param detallePedidoItem
	 */
	public void onAddDetallePedidoProductoItem( DetallePedidoItem detallePedidoItem ){
		detallePedidoItem.addDetallePedidoProductoItem();
		onUpdateMontoTotal();
	}
		
	/**
	 * Adiciona un item al listado del detalle pedido.
	 */
	public void onSelecBaseProducto( Baseproducto baseProducto ){		
		if ( baseProducto == null ){			
			SysMessage.warn( "Catalogo no Valido", "El Catalogo seleccionado no es valido" );			
			return ;
		}
		if ( ExisteDuplicado(baseProducto) ){			
			SysMessage.warn( "Catalogo Duplicado", "El catalogo ya se encuentra adicionado" );
			return ;
		}
		DetallePedidoItem detallePedidoItem = new DetallePedidoItem();
		detallePedidoItem.setBaseProducto( baseProducto );
		list.add( detallePedidoItem );
		log.info( "Se adiciono al catalogo " + baseProducto.getCode() );
	}
	
	/**
	 * Valida que el m�todo no se encuentre duplicado en la lista.
	 * @param baseProducto
	 * @return
	 */
	private boolean ExisteDuplicado( Baseproducto baseProducto ){
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			DetallePedidoItem detallePedidoItem = (DetallePedidoItem) iter.next();
			if ( detallePedidoItem.getBaseProducto().getCode().equalsIgnoreCase( baseProducto.getCode() ) ){
				return true;
			}
		}
		return false;
	}
		
	/**
	 * Quita un registro del listado...
	 */
	public void onQuitarItem( DetallePedidoItem detallePedidoItem ){
		list.remove( detallePedidoItem );
	}	
	
	/**
	 * Valida los datos del pedido.
	 * @return
	 */
	private String onValidarPedido(){
		if ( codeAlmacen == null || codeAlmacen == 0 ){
			return "Debe seleccionar el almacen";
		}
		if ( codeProveedor == null || codeProveedor.isEmpty() ){
			return "Debe seleccionar el proveedor";
		}
		if ( list.isEmpty() ){
			return "Debe adicionar al menos un catalogo...";
		}
		
		for (Iterator iterCatalog = list.iterator(); iterCatalog.hasNext();) {			
			DetallePedidoItem detallePedidoItem = (DetallePedidoItem) iterCatalog.next();
			if ( detallePedidoItem.getDetalleIPIList() == null || detallePedidoItem.getDetalleIPIList().isEmpty()  ){
				return "Catalogo: " + detallePedidoItem.getBaseProducto().getCode() + ", debe adicionar un producto!";
			}
			for (Iterator iterProducto = detallePedidoItem.getDetalleIPIList().iterator(); iterProducto.hasNext();) {
				DetallePedidoProductoItem detallePedidoProductoItem = (DetallePedidoProductoItem) iterProducto.next();
				if( detallePedidoProductoItem.getColorCode() == null || detallePedidoProductoItem.getColorCode() == 0 ){
					return "Catalogo: " + detallePedidoItem.getBaseProducto().getCode() + ", en uno de sus productos debe seleccionar el color!";
				}
				if( detallePedidoProductoItem.getTallaCode() == null || detallePedidoProductoItem.getTallaCode() == 0 ){
					return "Catalogo: " + detallePedidoItem.getBaseProducto().getCode() + ", en uno de sus productos debe seleccionar la talla!";
				}
				if( detallePedidoProductoItem.getPrecio() == null ){
					return "Catalogo: " + detallePedidoItem.getBaseProducto().getCode() + ", en uno de sus productos debe ingresar el precio!";
				}
			}
		}				
		return Constantes.VACIO;
	}
	
	/**
	 * Guardar el pedido.
	 */
	public String save(){
		String redirect = "";
		String textValidar = onValidarPedido();
		if ( !textValidar.equals( Constantes.VACIO ) ){
			SysMessage.warn( textValidar );
			return redirect;
		}
		
		List<Detallepedido> detallePedidoList = null;
		try{
			// preparar el pedido.
			prepararPedido();
			// preparar el detalle.
			detallePedidoList = prepararDetallePedido();
			// persistir pedido, pedido y su detalle.
			pedidopersistence.save( entidad, detallePedidoList );			
			bitacoraaccion.accion(entidad.getClass().getName(), "Se guardo correctamente el pedido: " + entidad.getCode() + ".");
			SysMessage.info("Pedido [" + entidad.getCode() + "] guardado correctamente.");			
			jutil.addObjectParam( entidad );
			redirect = "PedidoView.xhtml" + Parametros.REDIRECT;
		} catch (Exception e) {
			log.error( "[Fallo al guardar]", e );
			SysMessage.error("Fallo al guardar.");			
		}
		return redirect;
	}
		
	/**
	 * completa los datos del pedido antes de ser persistido.
	 * @throws Exception
	 */
	private void prepararPedido() throws Exception {
		entidad = new Pedido();
		entidad.setActivo( true );
		entidad.setComentario(comentario);
		entidad.setAlmacen( almacenpersistence.findAlmacenCode( codeAlmacen ) );
		entidad.setFecha(new Timestamp( Calendar.getInstance().getTimeInMillis() ) );
		entidad.setProveedor( proveedorpersistence.findProveedorCode( codeProveedor ) );		
		entidad.setTotal( total );
		entidad.setUsuario( (Usuario)jutil.getUsuarioO() );
	}
	
	/**
	 * Prepar� el detalle del pedido.
	 * @return
	 * @throws Exception
	 */
	private List<Detallepedido> prepararDetallePedido() throws Exception {
		List<Detallepedido> detallePedidoList = new ArrayList<Detallepedido>();
		Detallepedido detallePedido = null;
		for (Iterator iterCatalogo = list.iterator(); iterCatalogo.hasNext();) {
			DetallePedidoItem detallePedidoItem = (DetallePedidoItem) iterCatalogo.next();
			for (Iterator iterProducto = detallePedidoItem.getDetalleIPIList().iterator(); iterProducto.hasNext();) {
				DetallePedidoProductoItem detallePedidoProductoItem = (DetallePedidoProductoItem) iterProducto.next();
				detallePedido = new Detallepedido();
				detallePedido.setActivo(true);
				detallePedido.setProducto( crearProducto( detallePedidoItem.getBaseProducto(), detallePedidoProductoItem) );
				detallePedido.setCantidad( 1 );
				detallePedido.setCosto( detallePedidoProductoItem.getPrecio() );
				detallePedido.setEstado( Parametros.MODPEDIDO_ESTADO_PEDIDO );
				detallePedidoList.add( detallePedido );
			}
		}
		return detallePedidoList;
	}
	
	/**
	 * Completa un producto en base a los campos completados en el detalle.
	 * @param baseProducto
	 * @param detallePedidoProductoItem
	 * @return
	 * @throws Exception
	 */
	private Producto crearProducto( Baseproducto baseProducto, DetallePedidoProductoItem detallePedidoProductoItem ) throws Exception {
		Producto producto = new Producto();
		producto.setActivo( true );
		producto.setColor( colorpersistence.findColorCode( detallePedidoProductoItem.getColorCode() ) );
		producto.setTalla( tallapersistence.findTallaCode( detallePedidoProductoItem.getTallaCode() ) );
		producto.setBaseproducto( baseProducto );
		producto.setEstado( Constantes.ESTADO_PEDIDO );
//		producto.setFoto(null);
//		producto.setFotoSize(0);
		producto.setNombre( detallePedidoProductoItem.getNombre() );
		return producto;
	}
	
	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the codeAlmacen
	 */
	public Integer getCodeAlmacen() {
		return codeAlmacen;
	}

	/**
	 * @param codeAlmacen the codeAlmacen to set
	 */
	public void setCodeAlmacen(Integer codeAlmacen) {
		this.codeAlmacen = codeAlmacen;
	}

	/**
	 * @return the codeProveedor
	 */
	public String getCodeProveedor() {
		return codeProveedor;
	}

	/**
	 * @param codeProveedor the codeProveedor to set
	 */
	public void setCodeProveedor(String codeProveedor) {
		this.codeProveedor = codeProveedor;
	}

	/**
	 * @return the comentario
	 */
	public String getComentario() {
		return comentario;
	}

	/**
	 * @param comentario the comentario to set
	 */
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	/**
	 * @return the total
	 */
	public BigDecimal getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	
	
}
