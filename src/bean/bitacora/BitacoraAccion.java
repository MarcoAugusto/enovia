/**
 * 
 */
package bean.bitacora;

import java.sql.Timestamp;
import java.util.Calendar;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import persistence.administracion.BitacoraPersistence;


import bean.util.BaseBean;
import bean.util.Jutil;
import entidad.Bitacora;
import entidad.Usuario;

/**
 * @author Marcus
 *
 */
@Named
public class BitacoraAccion {

	@Inject
	private Jutil jutil;
	
	@Inject
	private BitacoraPersistence bitacorapersistence;
	
	private static Logger log = Logger.getLogger(BitacoraAccion.class);
	
	public void accion( String accion, String mensaje  ){
		try{
			Bitacora bitacora = new Bitacora();
			bitacora.setFecha( new Timestamp(Calendar.getInstance().getTimeInMillis()) );
			bitacora.setAccion(accion);
			bitacora.setMensaje(mensaje);
			bitacora.setIp(jutil.getIp());
			bitacora.setUsuario( (Usuario)jutil.getUsuarioO() );
			bitacorapersistence.accion(bitacora);
		}catch( Exception e ){
			log.error("[BitacoraAccion]", e);
		}
		
	}
	
}
