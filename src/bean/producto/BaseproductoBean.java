/**
 * 
 */
package bean.producto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import persistence.producto.BaseproductoPersistence;

import entidad.Baseproducto;
import entidad.Sucursal;

import bean.bitacora.BitacoraAccion;
import bean.util.BaseBean;
import bean.util.Constantes;
import bean.util.IGeneral;
import bean.util.IPagination;
import bean.util.Jutil;
import bean.util.SysMessage;


/**
 * @author Marcus
 *
 */
@ManagedBean(name="baseproductobean" )
@ViewScoped
public class BaseproductoBean extends BaseBean<Baseproducto> implements IGeneral, IPagination {

	@Inject
	private BaseproductoPersistence baseproductopersistence;
	
	@Inject
	private BitacoraAccion bitacoraaccion;
	
	@Inject
	private MarcaBean marcabean;
	
	@Inject
	private EstiloBean estilobean;
	
	@Inject
	private PrendaBean prendabean;
			
	@Inject
	private Jutil jutil;
	
	private int marcaCode;
	private int estiloCode;
	private int prendaCode;
	
	private String filtroCode;
	private String filtroCodigo;
	private String filtroEstilo;
	private String filtroMarca;
	private String filtroPrenda;
	
	
	@Override
	@PostConstruct
	public void init() { 
		iniLogs(this.getClass());
		baseproductopersistence.init();
		initFiltros();
		cargarLista();
		nuevo();
		marcabean.init();
		estilobean.init();
		prendabean.init();
	}
	
	/**
	 * Inicializa en vacio los campos a utilizar como filtros.
	 */
	public void initFiltros(){
		filtroCodigo = filtroEstilo = filtroMarca = filtroPrenda = Constantes.VACIO;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void cargarLista(){
		try{
			this.list = baseproductopersistence.getLista();			
		}catch (Exception e) {
			this.list = new ArrayList();
			log.error( "[BaseproductoBean.cargarLista]", e );
		}
	}
	
	public void nuevo(){
		entidad = new Baseproducto();
		entidad.setActivo(true);
		entidad.setEsporcentaje( false );
		entidad.setEspromo( false );
		entidad.setPrecio( new BigDecimal(0) );
		entidad.setPrecioPromo( new BigDecimal(0) );
		entidad.setCantidad( 0 );
		prendaCode = estiloCode = marcaCode = 0;
		nuevo = true;
		visibleNuevoEditar = false;
		selected = null;
		initFiltros();
	}
	
	public void nuevaEntidad() {
		nuevo();
		visibleNuevoEditar = true;
	}
	
	public void guardar() {		
		entidad.setPrenda( prendabean.obtenerPrendaCode( prendaCode ) );
		entidad.setMarca( marcabean.obtenerMarcaCode( marcaCode ) );
		entidad.setEstilo( estilobean.obtenerEstiloCode( estiloCode ) );
		String validacion = baseproductopersistence.validarCampos(entidad, nuevo);
		if (validacion.trim().isEmpty()) {
			if (nuevo) {
				try {
					baseproductopersistence.save( entidad );					
					log.info("[usuario: " + jutil.getUsuarioN() + ", baseProducto: " + entidad.getCode()  + "] [Guardado correctamente]");					
					SysMessage.info("Guardado correctamente.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Se guardo correctamente la baseProducto: " + entidad.getCode()  + ".");
					nuevo();
					cargarLista();
					visibleNuevoEditar = true;
				} catch (Exception e) {
					log.error("[usuario: " + jutil.getUsuarioN() + ", baseProducto: " + entidad.getCode() + "] [Fallo al guardar]", e);
					SysMessage.error("Fallo al guardar.");					
				}
			} else {
				try {
					baseproductopersistence.update(entidad);					
					log.info("[usuario: " + jutil.getUsuarioN() + ", baseProducto: " + entidad.getCode() + "] [Guardado correctamente]");
					SysMessage.info("Actualizado correctamente.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Se actualizado correctamente la baseProducto: " + entidad.getCode() + ".");
					nuevo();
					cargarLista();
					visibleNuevoEditar = false;
				} catch (Exception e) {
					log.error("[usuario: " + jutil.getUsuarioN() + ", baseProducto: " + entidad.getCode()  + "] [Fallo al guardar]", e);
					SysMessage.error("Fallo al guardar.");					
				}
			}
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + ", baseProducto:" + "] [" + validacion + "]");
			SysMessage.warn(validacion);
		}
	}	

	public void editar() {
		if (selected != null) {
			entidad = selected;
			prendaCode = entidad.getPrenda().getCode();
			estiloCode = entidad.getEstilo().getCode();
			marcaCode = entidad.getMarca().getCode();
			nuevo = false;
			visibleNuevoEditar = true;
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
		log.info( "entidad:" + entidad + " nuevo: " + nuevo );
	}

	public void editar(Baseproducto obj) {
		log.info("intentando editar" + obj.getCode() );
		selected = obj;
		editar();
	}

	public void onRowSelect(org.primefaces.event.SelectEvent event) {
		selected = (Baseproducto)event.getObject();
		log.info( "Baseproducto seleccionado:" + selected.getCode() );
		editar();
    }
	
	public void eliminar() {
		if (selected != null) {			
			try {
				selected.setActivo( false );
				baseproductopersistence.update(selected);				
				log.info("[usuario: " + jutil.getUsuarioN() + ", baseProducto: " + selected.getCode() + "] [Eliminado correctamente]");
				SysMessage.info("Eliminado correctamente.");
				bitacoraaccion.accion(selected.getClass().getName(), "Se elimino correctamente la baseProducto: " + selected.getCode() + ".");
				cargarLista();
				nuevo();
			} catch (Exception e) {
				log.error("[usuario: " + jutil.getUsuarioN() + ", baseProducto: " + selected.getCode() + "] [Fallo al eliminar]", e);
				SysMessage.info("Fallo al eliminar.");
				bitacoraaccion.accion(selected.getClass().getName(), "Fallo al eliminar la baseProducto: " + selected.getCode()  + ".");
			}
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
	}
		
	public void eliminar(Baseproducto obj) {
		selected = obj;
		eliminar();
	}
	
	public void buscar(){
		Map<String, String> mapaValor = new HashMap<String, String>();
		if ( !filtroCode.trim().isEmpty() ){
			mapaValor.put( "code", filtroCode );
		}
		if ( !filtroCodigo.trim().isEmpty() ){
			mapaValor.put( "codigo", filtroCodigo );
		}
		if ( !filtroEstilo.trim().isEmpty() ){
			mapaValor.put( "estilo", filtroEstilo );
		}
		if ( !filtroMarca.trim().isEmpty() ){
			mapaValor.put( "marca", filtroMarca );
		}
		if ( !filtroPrenda.trim().isEmpty() ){
			mapaValor.put( "prenda", filtroPrenda );
		}
		list = baseproductopersistence.getLista( mapaValor );		
	}
	
	
	
	@Override
	public void firstpage() {
		try{
			list = baseproductopersistence.firstpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "firstpage", e );
		}
	}

	@Override
	public void previouspage() {
		try{
			list = baseproductopersistence.previouspage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "previouspage", e );
		}
	}

	@Override
	public void nextpage() {
		try{
			list = baseproductopersistence.nextpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "nextpage", e );
		}
	}

	@Override
	public void lastpage() {
		try{
			list = baseproductopersistence.lastpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "lastpage", e );
		}
	}

	public int getCantidadRegistros() {
		return baseproductopersistence.getCantidadRegistros();
	}
	
	public int getCantidadPaginas() {
		return baseproductopersistence.getCantidadPaginas();
	}
	
	public int getPagina() {
		return baseproductopersistence.getPagina();
	}

	
	/**
	 * @return the filtroCode
	 */
	public String getFiltroCode() {
		return filtroCode;
	}

	/**
	 * @param filtroCode the filtroCode to set
	 */
	public void setFiltroCode(String filtroCode) {
		this.filtroCode = filtroCode;
	}

	public String getFiltroCodigo() {
		return filtroCodigo;
	}

	public void setFiltroCodigo(String filtroCodigo) {
		this.filtroCodigo = filtroCodigo;
	}

	public String getFiltroEstilo() {
		return filtroEstilo;
	}

	public void setFiltroEstilo(String filtroEstilo) {
		this.filtroEstilo = filtroEstilo;
	}

	public String getFiltroMarca() {
		return filtroMarca;
	}

	public void setFiltroMarca(String filtroMarca) {
		this.filtroMarca = filtroMarca;
	}

	public String getFiltroPrenda() {
		return filtroPrenda;
	}

	public void setFiltroPrenda(String filtroPrenda) {
		this.filtroPrenda = filtroPrenda;
	}

	public int getMarcaCode() {
		return marcaCode;
	}

	public void setMarcaCode(int marcaCode) {
		this.marcaCode = marcaCode;
	}

	public int getEstiloCode() {
		return estiloCode;
	}

	public void setEstiloCode(int estiloCode) {
		this.estiloCode = estiloCode;
	}

	/**
	 * @return the prendaCode
	 */
	public int getPrendaCode() {
		return prendaCode;
	}

	/**
	 * @param prendaCode the prendaCode to set
	 */
	public void setPrendaCode(int prendaCode) {
		this.prendaCode = prendaCode;
	}

		
	
}
