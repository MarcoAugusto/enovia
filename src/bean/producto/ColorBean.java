/**
 * 
 */
package bean.producto;

import java.util.ArrayList;
import java.util.Iterator;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import persistence.producto.ColorPersistence;

import entidad.Color;

import bean.bitacora.BitacoraAccion;
import bean.util.BaseBean;
import bean.util.IGeneral;
import bean.util.IPagination;
import bean.util.Jutil;
import bean.util.SysMessage;


/**
 * @author Marcus
 *
 */
@ManagedBean(name="colorbean")
@ViewScoped
public class ColorBean extends BaseBean<Color> implements IGeneral, IPagination {

	@Inject
	private ColorPersistence colorpersistence;
	
	@Inject
	private BitacoraAccion bitacoraaccion;
	
	@Inject
	private Jutil jutil;
		
	
	@Override
	@PostConstruct
	public void init() { 
		iniLogs(this.getClass());
		colorpersistence.init();
		cargarLista();
		nuevo();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void cargarLista(){
		try{
			this.list = colorpersistence.getLista();			
		}catch (Exception e) {
			this.list = new ArrayList();
			log.error( "[ColorBean.cargarLista]", e );
		}
	}
	
	public void nuevo(){
		entidad = new Color();
		entidad.setActivo(true);		
		nuevo = true;
		visibleNuevoEditar = false;
		selected = null;
	}
	
	public void nuevaEntidad() {
		nuevo();
		visibleNuevoEditar = true;
	}
	
	public void guardar() {
		String validacion = colorpersistence.validarCampos(entidad, nuevo);
		if (validacion.trim().isEmpty()) {
			if (nuevo) {
				try {
					colorpersistence.save( entidad );					
					log.info("[usuario: " + jutil.getUsuarioN() + ", color: " + entidad.getNombre() + "] [Guardado correctamente]");					
					SysMessage.info("Guardado correctamente.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Se guardo correctamente el color: " + entidad.getNombre() + ".");
					nuevo();
					cargarLista();
					visibleNuevoEditar = true;
				} catch (Exception e) {
					log.error("[usuario: " + jutil.getUsuarioN() + ", color: " + entidad.getNombre() + "] [Fallo al guardar]", e);
					SysMessage.error("Fallo al guardar.");
					bitacoraaccion.accion(getClass().getName(), "Fallo al guardar el color: " + entidad.getNombre() + ".");
				}
			} else {
				try {
					colorpersistence.update(entidad);					
					log.info("[usuario: " + jutil.getUsuarioN() + ", color: " + entidad.getNombre() + "] [Guardado correctamente]");
					SysMessage.info("Actualizado correctamente.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Se actualizado correctamente el color: " + entidad.getNombre() + ".");
					nuevo();
					cargarLista();
					visibleNuevoEditar = false;
				} catch (Exception e) {
					log.error("[usuario: " + jutil.getUsuarioN() + ", color: " + entidad.getNombre() + "] [Fallo al guardar]", e);
					SysMessage.error("Fallo al guardar.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Fallo al actualizar el color: " + entidad.getNombre() + ".");
				}
			}
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + ", color: " + entidad.getNombre() + "] [" + validacion + "]");
			SysMessage.warn(validacion);
		}
	}	

	public void editar() {
		if (selected != null) {
			entidad = selected;			
			nuevo = false;
			visibleNuevoEditar = true;
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
		log.info( "entidad:" + entidad + " nuevo: " + nuevo );
	}

	public void editar(Color obj) {
		log.info("intentando editar" + obj.getNombre() );
		selected = obj;
		editar();
	}

	public void onRowSelect(org.primefaces.event.SelectEvent event) {
		selected = (Color)event.getObject();
		log.info( "Color seleccionado:" + selected.getNombre() );
		editar();
    }
	
	public void eliminar() {
		if (selected != null) {			
			try {
				selected.setActivo( false );
				colorpersistence.update(selected);				
				log.info("[usuario: " + jutil.getUsuarioN() + ", color: " + selected.getNombre() + "] [Eliminado correctamente]");
				SysMessage.info("Eliminado correctamente.");
				bitacoraaccion.accion(selected.getClass().getName(), "Se elimino correctamente el color: " + selected.getNombre() + ".");
				cargarLista();
				nuevo();
			} catch (Exception e) {
				log.error("[usuario: " + jutil.getUsuarioN() + ", color: " + selected.getNombre() + "] [Fallo al eliminar]", e);
				SysMessage.info("Fallo al eliminar.");
				bitacoraaccion.accion(selected.getClass().getName(), "Fallo al eliminar el color: " + selected.getNombre() + ".");
			}
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
	}
		
	public void eliminar(Color obj) {
		selected = obj;
		eliminar();
	}
	
	public void buscar(){
		list = colorpersistence.getLista( cadenaBusqueda );		
	}
	
	/**
	 * Obtiene el color de la lista previamente cargada.
	 * @param code
	 * @return
	 */
	public Color obtenerColorCode( Integer code ){					
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			Color color = (Color) iter.next();
			if ( color.getCode() == code ){
				return color;
			}
		}
		return null;
	}
	
	@Override
	public void firstpage() {
		try{
			list = colorpersistence.firstpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "firstpage", e );
		}
	}

	@Override
	public void previouspage() {
		try{
			list = colorpersistence.previouspage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "previouspage", e );
		}
	}

	@Override
	public void nextpage() {
		try{
			list = colorpersistence.nextpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "nextpage", e );
		}
	}

	@Override
	public void lastpage() {
		try{
			list = colorpersistence.lastpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "lastpage", e );
		}
	}

	public int getCantidadRegistros() {
		return colorpersistence.getCantidadRegistros();
	}
	
	public int getCantidadPaginas() {
		return colorpersistence.getCantidadPaginas();
	}
	
	public int getPagina() {
		return colorpersistence.getPagina();
	}
	
	
}
