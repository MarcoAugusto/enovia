/**
 * 
 */
package bean.producto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.CustomScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import persistence.producto.BaseproductoSelecPersistence;

import entidad.Baseproducto;
import entidad.Estilo;
import entidad.Marca;
import entidad.Prenda;

import bean.util.Base;


/**
 * @author Marcus
 *
 */
@ManagedBean( name = "baseproductoselecbean" )
@ViewScoped
public class BaseproductoSelecBean extends Base implements Serializable {
	
	@Inject
	private BaseproductoSelecPersistence baseproductoselecpersistence;
					
	private Integer prendaCode = null;
	private Integer estiloCode = null;
	private Integer marcaCode = null;
	
	private List<Prenda> prendaList;
	private List<Estilo> estiloList;
	private List<Marca> marcaList;	

	private Baseproducto baseProducto;
	
	@PostConstruct
	public void init() {		
		super.setup( this.getClass() );
//		baseproductoselecpersistence = new BaseproductoSelecPersistence();
		baseproductoselecpersistence.init();
		onResetearVariables();			
		log.info( "BaseProductoSelecBean: iniciado" );		
	}
	
	public void onResetearVariables(){
		baseProducto = null;
		prendaList = null;
		estiloList = null;
		marcaList = null;
		prendaCode = null;
		estiloCode = null;
		marcaCode = null;
		onUpdatePrendas();
	}
	
	/**
	 * Actualiza la lista de prendas que se tiene registrados en el baseProducto.
	 */
	public void onUpdatePrendas(){
		prendaList = baseproductoselecpersistence.obtenerPrendasAsociadas();
		baseProducto = null;
		log.info( "onUpdatePrendas" );
		log.info( "baseProducto: " + baseProducto );
	}
	
	
	/**
	 * actualiza la lista de marcas a partir de la seleccion de un estilo.
	 * @param detalleIngresoItem
	 */
	public void onUpdateMarcas(){
		if ( prendaCode == null ){
			return;
		}
		baseProducto = null;
		marcaList = baseproductoselecpersistence.obtenerMarcasAsociados( prendaCode );
		log.info( "onUpdateMarcas" );
		log.info( "baseProducto: " + baseProducto );
	}

	/**
	 * Actualiza la lista de estilos en base a la prenda seleccionada.
	 * @param detalleIngresoItem
	 */
	public void onUpdateEstilos(){		
		if ( prendaCode == null || marcaCode == null ){
			return;
		}
		baseProducto = null;
		estiloList = baseproductoselecpersistence.obtenerEstilosAsociados( prendaCode, marcaCode );
		log.info( "onUpdateEstilos" );
		log.info( "baseProducto: " + baseProducto );
	}
	
	/**
	 * Selecciona un baseProducto en base a la prenda, estilo y marca...
	 */
	public void onUpdateBaseproductoSelected(){
		if ( prendaCode == null || marcaCode == null || estiloCode == null ){
			log.info( "prendacode: "+ prendaCode + " marcacode: " + marcaCode + " estilocode: " + estiloCode );
			return;
		}
		baseProducto = baseproductoselecpersistence.obtenerBaseProducto( prendaCode, estiloCode, marcaCode );
		log.info( "onUpdateBaseproductoSelected" );
		log.info( "baseProducto: " + baseProducto.getCode() );
	}
	
	/**
	 * @return the estiloList
	 */
	public List<Estilo> getEstiloList() {
		return estiloList;
	}

	/**
	 * @param estiloList the estiloList to set
	 */
	public void setEstiloList(List<Estilo> estiloList) {
		this.estiloList = estiloList;
	}

	/**
	 * @return the marcaList
	 */
	public List<Marca> getMarcaList() {
		return marcaList;
	}

	/**
	 * @param marcaList the marcaList to set
	 */
	public void setMarcaList(List<Marca> marcaList) {
		this.marcaList = marcaList;
	}

	/**
	 * @return the prendaList
	 */
	public List<Prenda> getPrendaList() {
		return prendaList;
	}

	/**
	 * @param prendaList the prendaList to set
	 */
	public void setPrendaList(List<Prenda> prendaList) {
		this.prendaList = prendaList;
	}

	/**
	 * @return the prendaCode
	 */
	public Integer getPrendaCode() {
		return prendaCode;
	}

	/**
	 * @param prendaCode the prendaCode to set
	 */
	public void setPrendaCode(Integer prendaCode) {
		this.prendaCode = prendaCode;
		if ( prendaCode == 0  ){
			this.prendaCode = null;
		}		
		this.estiloList = null;
		this.estiloCode = null;
		this.marcaList = null;
		this.marcaCode = null;
		this.baseProducto = null;
		log.info( "setPrendaCode" );
	}

	/**
	 * @return the estiloCode
	 */
	public Integer getEstiloCode() {
		return estiloCode;
	}

	/**
	 * @param estiloCode the estiloCode to set
	 */
	public void setEstiloCode(Integer estiloCode) {
		this.estiloCode = estiloCode;		
	}

	/**
	 * @return the marcaCode
	 */
	public Integer getMarcaCode() {
		return marcaCode;
	}

	/**
	 * @param marcaCode the marcaCode to set
	 */
	public void setMarcaCode(Integer marcaCode) {
		this.marcaCode = marcaCode;	
		if ( marcaCode == 0 ){
			marcaCode = null;
		}
		this.estiloList = null;
		this.estiloCode = null;
		this.baseProducto = null;
		log.info( "setMarcaCode" );
	}

	public Baseproducto getBaseProducto() {
		return baseProducto;
	}

	public void setBaseProducto(Baseproducto baseProducto) {
		this.baseProducto = baseProducto;
	}
	
}
