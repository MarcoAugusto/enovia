/**
 * 
 */
package bean.producto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import persistence.producto.ProductoInventarioPersistence;

import entidad.Baseproducto;
import entidad.Producto;

import bean.util.BaseBean;
import bean.util.Constantes;
import bean.util.IGeneral;
import bean.util.IPagination;
import bean.util.Jutil;
import bean.util.SysMessage;


/**
 * @author Marcus
 *
 */
@ManagedBean( name = "productoinventariobean" )
@ViewScoped
public class ProductoInventarioBean extends BaseBean<Producto> implements IGeneral, IPagination {

	@Inject
	private ProductoInventarioPersistence productoinventariopersistence;

	@Inject
	private Jutil jutil;
	
	private String filtroCompra;
	
	private String filtroCode;
	private String filtroCatalogo;
	private String filtroPrenda;
	private String filtroEstilo;
	private String filtroMarca;
	private String filtroColor;
	private String filtroTalla;
	private String filtroNombre;
	private String filtroEstado;
	
	private String filtroBusqueda;
	private String filtroAlmacen;
	
	private Map<String, String> mapaValor = new HashMap<String, String>();
	
	private boolean ini = false;
	
	@Override
	@PostConstruct
	public void init() { 		
		if ( ini ){
			return;
		}
		iniLogs(this.getClass());
		productoinventariopersistence.init();		
		initFiltros();
		ini = true;
	}

	
	/**
	 * Carga los productos seg�n el almacen seleccionado en el egreso.
	 * @param almacenCode
	 */
	public void onResetVariables( String almacenNombre ){
		initFiltros();				
		log.info( "almacenNombre: " + almacenNombre );
		if ( almacenNombre == null || almacenNombre.trim().isEmpty() ){
			filtroBusqueda = "";
			log.info( "mapa: " + mapaValor.toString() );
			cargarLista( mapaValor );
		} else {
			mapaValor.clear();
			mapaValor.put( "almacen", "" + almacenNombre );
			log.info( "mapa: " + mapaValor.toString() );
			filtroAlmacen = almacenNombre;
			cargarLista( mapaValor );
		}		
		selected = null;
	}
	
	/**
	 * Inicializa en vacio los campos a utilizar como filtros.
	 */
	public void initFiltros(){
		filtroCompra = filtroCode = filtroCatalogo = filtroPrenda = filtroEstilo = filtroMarca = filtroColor = filtroTalla = filtroNombre = filtroEstado = Constantes.VACIO;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void cargarLista(){
		try{
			log.info( "cargarLista" );
			this.list = productoinventariopersistence.getLista();			
		}catch (Exception e) {
			this.list = new ArrayList();
			log.error( "[ProductoInventarioBean.cargarLista]", e );
		}
	}
	
	public void cargarLista( Map<String, String> mapa ){
		try{
			log.info( "cargarListaMap" );
			this.list = productoinventariopersistence.getLista( mapa );			
		}catch (Exception e) {
			this.list = new ArrayList();
			log.error( "[ProductoInventarioBean.cargarLista(map)]", e );
		}
	}
	
	public void onRowSelect(org.primefaces.event.SelectEvent event) {
		selected = (Producto)event.getObject();
		log.info( "Producto seleccionado:" + selected.getCode() );	
    }
	
	/**
	 * Obtiene el listado de productos relacionados con el catalogo.
	 * @param catalogo
	 * @return
	 */
	public List<Producto> onSelectedCatalogo( Baseproducto catalogo ){
		if ( catalogo == null ){
			SysMessage.warn( "Debe seleccionar un catalogo." );
			log.warn( "Debe seleccionar un catalogo." );
			return null;
		}
		log.info( "Obteniendo los productos del catalogo: " + catalogo.getCode() );
		List<Producto> productoList = new ArrayList<Producto>();
		try{
			productoList = productoinventariopersistence.onProductoCatalogo( catalogo.getCode(), filtroAlmacen );
		}catch (Exception e) {
			log.error( "Error: onSelectedCatalogo", e );
			SysMessage.error( "No se pudo obtener los productos del cat�logo..." );
		}
		log.info( "Productos Obtenidos: " + productoList.size() );
		return productoList;
	}
	
	public void buscar() throws Exception {
		Map<String, String> mapaValor = new HashMap<String, String>();
		if ( !filtroCode.trim().isEmpty() ){
			mapaValor.put( "code", filtroCode );
		}		
		if ( !filtroCatalogo.trim().isEmpty() ){
			mapaValor.put( "catalogo", filtroCatalogo );
		}
		if ( !filtroPrenda.trim().isEmpty() ){
			mapaValor.put( "prenda", filtroPrenda );
		}
		if ( !filtroEstilo.trim().isEmpty() ){
			mapaValor.put( "estilo", filtroEstilo );
		}
		if ( !filtroMarca.trim().isEmpty() ){
			mapaValor.put( "marca", filtroMarca );
		}
		if ( !filtroColor.trim().isEmpty() ){
			mapaValor.put( "color", filtroColor );
		}
		if ( !filtroTalla.trim().isEmpty() ){
			mapaValor.put( "talla", filtroTalla );
		}
		if ( !filtroNombre.trim().isEmpty() ){
			mapaValor.put( "nombre", filtroNombre );
		}
		if ( !filtroEstado.trim().isEmpty() ){
			mapaValor.put( "estado", filtroEstado );
		}
		if ( !filtroAlmacen.trim().isEmpty() ){
			mapaValor.put( "almacen", filtroAlmacen );
		}
		list = productoinventariopersistence.getLista( mapaValor );		
	}
	
	@Override
	public void firstpage() {
		try{
			list = productoinventariopersistence.firstpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "firstpage", e );
		}
	}

	@Override
	public void previouspage() {
		try{
			list = productoinventariopersistence.previouspage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "previouspage", e );
		}
	}

	@Override
	public void nextpage() {
		try{
			list = productoinventariopersistence.nextpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "nextpage", e );
		}
	}

	@Override
	public void lastpage() {
		try{
			list = productoinventariopersistence.lastpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "lastpage", e );
		}
	}

	public int getCantidadRegistros() {
		return productoinventariopersistence.getCantidadRegistros();
	}
	
	public int getCantidadPaginas() {
		return productoinventariopersistence.getCantidadPaginas();
	}
	
	public int getPagina() {
		return productoinventariopersistence.getPagina();
	}

	
	/**
	 * @return the filtroCode
	 */
	public String getFiltroCode() {
		return filtroCode;
	}

	/**
	 * @param filtroCode the filtroCode to set
	 */
	public void setFiltroCode(String filtroCode) {
		this.filtroCode = filtroCode;
	}
	
	public String getFiltroColor() {
		return filtroColor;
	}

	public void setFiltroColor(String filtroColor) {
		this.filtroColor = filtroColor;
	}

	public String getFiltroTalla() {
		return filtroTalla;
	}

	public void setFiltroTalla(String filtroTalla) {
		this.filtroTalla = filtroTalla;
	}

	public String getFiltroEstado() {
		return filtroEstado;
	}

	public void setFiltroEstado(String filtroEstado) {
		this.filtroEstado = filtroEstado;
	}

	/**
	 * @return the filtroCatalogo
	 */
	public String getFiltroCatalogo() {
		return filtroCatalogo;
	}

	/**
	 * @param filtroCatalogo the filtroCatalogo to set
	 */
	public void setFiltroCatalogo(String filtroCatalogo) {
		this.filtroCatalogo = filtroCatalogo;
	}

	/**
	 * @return the filtroPrenda
	 */
	public String getFiltroPrenda() {
		return filtroPrenda;
	}

	/**
	 * @param filtroPrenda the filtroPrenda to set
	 */
	public void setFiltroPrenda(String filtroPrenda) {
		this.filtroPrenda = filtroPrenda;
	}

	/**
	 * @return the filtroEstilo
	 */
	public String getFiltroEstilo() {
		return filtroEstilo;
	}

	/**
	 * @param filtroEstilo the filtroEstilo to set
	 */
	public void setFiltroEstilo(String filtroEstilo) {
		this.filtroEstilo = filtroEstilo;
	}

	/**
	 * @return the filtroMarca
	 */
	public String getFiltroMarca() {
		return filtroMarca;
	}

	/**
	 * @param filtroMarca the filtroMarca to set
	 */
	public void setFiltroMarca(String filtroMarca) {
		this.filtroMarca = filtroMarca;
	}

	/**
	 * @return the filtroNombre
	 */
	public String getFiltroNombre() {
		return filtroNombre;
	}

	/**
	 * @param filtroNombre the filtroNombre to set
	 */
	public void setFiltroNombre(String filtroNombre) {
		this.filtroNombre = filtroNombre;
	}	
	
}
