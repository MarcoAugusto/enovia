/**
 * 
 */
package bean.producto;

import java.util.ArrayList;
import java.util.Iterator;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import persistence.producto.EstiloPersistence;

import entidad.Estilo;

import bean.bitacora.BitacoraAccion;
import bean.util.BaseBean;
import bean.util.IGeneral;
import bean.util.IPagination;
import bean.util.Jutil;
import bean.util.SysMessage;


/**
 * @author Marcus
 *
 */
@ManagedBean(name="estilobean")
@ViewScoped
public class EstiloBean extends BaseBean<Estilo> implements IGeneral, IPagination {

	@Inject
	private EstiloPersistence estilopersistence;
	
	@Inject
	private BitacoraAccion bitacoraaccion;
	
	@Inject
	private Jutil jutil;
		
	
	@Override
	@PostConstruct
	public void init() { 
		iniLogs(this.getClass());
		estilopersistence.init();
		cargarLista();
		nuevo();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void cargarLista(){
		try{
			this.list = estilopersistence.getLista();			
		}catch (Exception e) {
			this.list = new ArrayList();
			log.error( "[ColorBean.cargarLista]", e );
		}
	}
	
	public void nuevo(){
		entidad = new Estilo();
		entidad.setActivo(true);		
		nuevo = true;
		visibleNuevoEditar = false;
		selected = null;
	}
	
	public void nuevaEntidad() {
		nuevo();
		visibleNuevoEditar = true;
	}
	
	public void guardar() {
		String validacion = estilopersistence.validarCampos(entidad, nuevo);
		if (validacion.trim().isEmpty()) {
			if (nuevo) {
				try {
					estilopersistence.save( entidad );					
					log.info("[usuario: " + jutil.getUsuarioN() + ", estilo: " + entidad.getNombre() + "] [Guardado correctamente]");					
					SysMessage.info("Guardado correctamente.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Se guardo correctamente el estilo: " + entidad.getNombre() + ".");
					nuevo();
					cargarLista();
					visibleNuevoEditar = true;
				} catch (Exception e) {
					log.error("[usuario: " + jutil.getUsuarioN() + ", estilo: " + entidad.getNombre() + "] [Fallo al guardar]", e);
					SysMessage.error("Fallo al guardar.");
					bitacoraaccion.accion(getClass().getName(), "Fallo al guardar el estilo: " + entidad.getNombre() + ".");
				}
			} else {
				try {
					estilopersistence.update(entidad);					
					log.info("[usuario: " + jutil.getUsuarioN() + ", estilo: " + entidad.getNombre() + "] [Guardado correctamente]");
					SysMessage.info("Actualizado correctamente.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Se actualizado correctamente la estilo: " + entidad.getNombre() + ".");
					nuevo();
					cargarLista();
					visibleNuevoEditar = false;
				} catch (Exception e) {
					log.error("[usuario: " + jutil.getUsuarioN() + ", estilo: " + entidad.getNombre() + "] [Fallo al guardar]", e);
					SysMessage.error("Fallo al guardar.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Fallo al actualizar el estilo: " + entidad.getNombre() + ".");
				}
			}
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + ", estilo: " + entidad.getNombre() + "] [" + validacion + "]");
			SysMessage.warn(validacion);
		}
	}	

	public void editar() {
		if (selected != null) {
			entidad = selected;			
			nuevo = false;
			visibleNuevoEditar = true;
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
		log.info( "entidad:" + entidad + " nuevo: " + nuevo );
	}

	public void editar(Estilo obj) {
		log.info("intentando editar" + obj.getNombre() );
		selected = obj;
		editar();
	}

	public void onRowSelect(org.primefaces.event.SelectEvent event) {
		selected = (Estilo)event.getObject();
		log.info( "Color seleccionado:" + selected.getNombre() );
		editar();
    }
	
	public void eliminar() {
		if (selected != null) {			
			try {
				selected.setActivo( false );
				estilopersistence.update(selected);				
				log.info("[usuario: " + jutil.getUsuarioN() + ", estilo: " + selected.getNombre() + "] [Eliminado correctamente]");
				SysMessage.info("Eliminado correctamente.");
				bitacoraaccion.accion(selected.getClass().getName(), "Se elimino correctamente el estilo: " + selected.getNombre() + ".");
				cargarLista();
				nuevo();
			} catch (Exception e) {
				log.error("[usuario: " + jutil.getUsuarioN() + ", estilo: " + selected.getNombre() + "] [Fallo al eliminar]", e);
				SysMessage.info("Fallo al eliminar.");
				bitacoraaccion.accion(selected.getClass().getName(), "Fallo al eliminar el estilo: " + selected.getNombre() + ".");
			}
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
	}
		
	public void eliminar(Estilo obj) {
		selected = obj;
		eliminar();
	}
	
	public void buscar(){
		list = estilopersistence.getLista( cadenaBusqueda );		
	}
	
	/**
	 * Obtiene el estilo de la lista previamente cargada.
	 * @param code
	 * @return
	 */
	public Estilo obtenerEstiloCode( Integer code ){					
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			Estilo estilo = (Estilo) iter.next();
			if ( estilo.getCode() == code ){
				return estilo;
			}
		}
		return null;
	}
	
	@Override
	public void firstpage() {
		try{
			list = estilopersistence.firstpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "firstpage", e );
		}
	}

	@Override
	public void previouspage() {
		try{
			list = estilopersistence.previouspage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "previouspage", e );
		}
	}

	@Override
	public void nextpage() {
		try{
			list = estilopersistence.nextpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "nextpage", e );
		}
	}

	@Override
	public void lastpage() {
		try{
			list = estilopersistence.lastpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "lastpage", e );
		}
	}

	public int getCantidadRegistros() {
		return estilopersistence.getCantidadRegistros();
	}
	
	public int getCantidadPaginas() {
		return estilopersistence.getCantidadPaginas();
	}
	
	public int getPagina() {
		return estilopersistence.getPagina();
	}
	
	
}
