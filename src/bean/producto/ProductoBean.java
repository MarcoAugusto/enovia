/**
 * 
 */
package bean.producto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import persistence.producto.ProductoPersistence;

import entidad.Producto;
import entidad.Sucursal;

import bean.bitacora.BitacoraAccion;
import bean.util.BaseBean;
import bean.util.Constantes;
import bean.util.IGeneral;
import bean.util.IPagination;
import bean.util.Jutil;
import bean.util.SysMessage;
import bean.util.UploadBean;


/**
 * @author Marcus
 *
 */
@ManagedBean(name="productobean" )
@ViewScoped
public class ProductoBean extends BaseBean<Producto> implements IGeneral, IPagination {

	@Inject
	private ProductoPersistence productopersistence;
	
	@Inject
	private BaseproductoBean baseproductobean;
	
	@Inject
	private BitacoraAccion bitacoraaccion;
	
	@Inject
	private ColorBean colorbean;
	
	@Inject
	private TallaBean tallabean;	
			
	@Inject
	private Jutil jutil;
	
	@Inject
	private UploadBean uploadbean;
	
	private int colorCode;
	private int tallaCode;	
	
	private String filtroCode;	
	private String filtroColor;
	private String filtroTalla;	
	private String filtroEstado;
	
	
	@Override
	@PostConstruct
	public void init() { 
		iniLogs(this.getClass());
		productopersistence.init();
		baseproductobean.init();
		initFiltros();
		cargarLista();
		nuevo();
		colorbean.init();
		tallabean.init();
	}

	/**
	 * Inicializa en vacio los campos a utilizar como filtros.
	 */
	public void initFiltros(){
		filtroCode = filtroColor = filtroTalla = filtroEstado = Constantes.VACIO;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void cargarLista(){
		try{
			this.list = productopersistence.getLista();			
		}catch (Exception e) {
			this.list = new ArrayList();
			log.error( "[ProductoBean.cargarLista]", e );
		}
	}
	
	public void nuevo(){
		entidad = new Producto();
		entidad.setActivo(true);				
		nuevo = true;
		visibleNuevoEditar = false;
		selected = null;
		initFiltros();
	}
	
	public void nuevaEntidad() {
		nuevo();
		visibleNuevoEditar = true;
	}
	
	public void guardar() {
		entidad.setColor( colorbean.obtenerColorCode( colorCode ) );
		entidad.setTalla( tallabean.obtenerTallaCode( tallaCode ) );		
		String validacion = productopersistence.validarCampos(entidad, nuevo);
		if (validacion.trim().isEmpty()) {
			if (nuevo) {
				try {
					productopersistence.save( entidad );					
					log.info("[usuario: " + jutil.getUsuarioN() + ", Producto: " + entidad.getCode()  + "] [Guardado correctamente]");					
					SysMessage.info("Guardado correctamente.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Se guardo correctamente el Producto: " + entidad.getCode()  + ".");
					nuevo();
					cargarLista();
					visibleNuevoEditar = true;
				} catch (Exception e) {
					log.error("[usuario: " + jutil.getUsuarioN() + ", Producto: " + entidad.getCode() + "] [Fallo al guardar]", e);
					SysMessage.error("Fallo al guardar.");					
				}
			} else {
				try {
					productopersistence.update(entidad);					
					log.info("[usuario: " + jutil.getUsuarioN() + ", Producto: " + entidad.getCode() + "] [Guardado correctamente]");
					SysMessage.info("Actualizado correctamente.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Se actualizado correctamente el Producto: " + entidad.getCode() + ".");
					nuevo();
					cargarLista();
					visibleNuevoEditar = false;
				} catch (Exception e) {
					log.error("[usuario: " + jutil.getUsuarioN() + ", Producto: " + entidad.getCode()  + "] [Fallo al guardar]", e);
					SysMessage.error("Fallo al guardar.");					
				}
			}
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + ", Producto:" + "] [" + validacion + "]");
			SysMessage.warn(validacion);
		}
	}	

	public void editar() {
		if (selected != null) {
			entidad = selected;			
			nuevo = false;
			visibleNuevoEditar = true;
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
		log.info( "entidad:" + entidad + " nuevo: " + nuevo );
	}

	public void editar(Producto obj) {
		log.info("intentando editar" + obj.getCode() );
		selected = obj;
		editar();
	}

	public void onRowSelect(org.primefaces.event.SelectEvent event) {
		selected = (Producto)event.getObject();
		log.info( "Producto seleccionado:" + selected.getCode() );
		editar();
    }
	
	public void eliminar() {
		if (selected != null) {			
			try {
				selected.setActivo( false );
				productopersistence.update(selected);				
				log.info("[usuario: " + jutil.getUsuarioN() + ", Producto: " + selected.getCode() + "] [Eliminado correctamente]");
				SysMessage.info("Eliminado correctamente.");
				bitacoraaccion.accion(selected.getClass().getName(), "Se elimino correctamente el Producto: " + selected.getCode() + ".");
				cargarLista();
				nuevo();
			} catch (Exception e) {
				log.error("[usuario: " + jutil.getUsuarioN() + ", Producto: " + selected.getCode() + "] [Fallo al eliminar]", e);
				SysMessage.info("Fallo al eliminar.");
				bitacoraaccion.accion(selected.getClass().getName(), "Fallo al eliminar el Producto: " + selected.getCode()  + ".");
			}
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
	}
		
	public void eliminar(Producto obj) {
		selected = obj;
		eliminar();
	}
	
	public void buscar(){
		Map<String, String> mapaValor = new HashMap<String, String>();
		if ( !filtroCode.trim().isEmpty() ){
			mapaValor.put( "code", filtroCode );
		}		
		if ( !filtroColor.trim().isEmpty() ){
			mapaValor.put( "color", filtroColor );
		}
		if ( !filtroTalla.trim().isEmpty() ){
			mapaValor.put( "talla", filtroTalla );
		}
		if ( !filtroEstado.trim().isEmpty() ){
			mapaValor.put( "estado", filtroEstado );
		}
		list = productopersistence.getLista( mapaValor );		
	}
	
	@Override
	public void firstpage() {
		try{
			list = productopersistence.firstpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "firstpage", e );
		}
	}

	@Override
	public void previouspage() {
		try{
			list = productopersistence.previouspage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "previouspage", e );
		}
	}

	@Override
	public void nextpage() {
		try{
			list = productopersistence.nextpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "nextpage", e );
		}
	}

	@Override
	public void lastpage() {
		try{
			list = productopersistence.lastpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "lastpage", e );
		}
	}

	public int getCantidadRegistros() {
		return productopersistence.getCantidadRegistros();
	}
	
	public int getCantidadPaginas() {
		return productopersistence.getCantidadPaginas();
	}
	
	public int getPagina() {
		return productopersistence.getPagina();
	}

	
	/**
	 * @return the filtroCode
	 */
	public String getFiltroCode() {
		return filtroCode;
	}

	/**
	 * @param filtroCode the filtroCode to set
	 */
	public void setFiltroCode(String filtroCode) {
		this.filtroCode = filtroCode;
	}

	public UploadBean getUploadbean() {
		return uploadbean;
	}

	public void setUploadbean(UploadBean uploadbean) {
		this.uploadbean = uploadbean;
	}

	public String getFiltroColor() {
		return filtroColor;
	}

	public void setFiltroColor(String filtroColor) {
		this.filtroColor = filtroColor;
	}

	public String getFiltroTalla() {
		return filtroTalla;
	}

	public void setFiltroTalla(String filtroTalla) {
		this.filtroTalla = filtroTalla;
	}

	public String getFiltroEstado() {
		return filtroEstado;
	}

	public void setFiltroEstado(String filtroEstado) {
		this.filtroEstado = filtroEstado;
	}

	/**
	 * @return the colorCode
	 */
	public int getColorCode() {
		return colorCode;
	}

	/**
	 * @param colorCode the colorCode to set
	 */
	public void setColorCode(int colorCode) {
		this.colorCode = colorCode;
	}

	/**
	 * @return the tallaCode
	 */
	public int getTallaCode() {
		return tallaCode;
	}

	/**
	 * @param tallaCode the tallaCode to set
	 */
	public void setTallaCode(int tallaCode) {
		this.tallaCode = tallaCode;
	}	
	
}
