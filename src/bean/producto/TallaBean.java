/**
 * 
 */
package bean.producto;

import java.util.ArrayList;
import java.util.Iterator;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import persistence.producto.TallaPersistence;

import entidad.Talla;

import bean.bitacora.BitacoraAccion;
import bean.util.BaseBean;
import bean.util.IGeneral;
import bean.util.IPagination;
import bean.util.Jutil;
import bean.util.SysMessage;


/**
 * @author Marcus
 *
 */
@ManagedBean(name="tallabean")
@ViewScoped
public class TallaBean extends BaseBean<Talla> implements IGeneral, IPagination {

	@Inject
	private TallaPersistence tallapersistence;
	
	@Inject
	private BitacoraAccion bitacoraaccion;
	
	@Inject
	private Jutil jutil;
		
	
	@Override
	@PostConstruct
	public void init() { 
		iniLogs(this.getClass());
		tallapersistence.init();
		cargarLista();
		nuevo();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void cargarLista(){
		try{
			this.list = tallapersistence.getLista();			
		}catch (Exception e) {
			this.list = new ArrayList();
			log.error( "[ColorBean.cargarLista]", e );
		}
	}
	
	public void nuevo(){
		entidad = new Talla();
		entidad.setActivo(true);		
		nuevo = true;
		visibleNuevoEditar = false;
		selected = null;
	}
	
	public void nuevaEntidad() {
		nuevo();
		visibleNuevoEditar = true;
	}
	
	public void guardar() {
		String validacion = tallapersistence.validarCampos(entidad, nuevo);
		if (validacion.trim().isEmpty()) {
			if (nuevo) {
				try {
					tallapersistence.save( entidad );					
					log.info("[usuario: " + jutil.getUsuarioN() + ", talla: " + entidad.getNombre() + "] [Guardado correctamente]");					
					SysMessage.info("Guardado correctamente.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Se guardo correctamente la talla: " + entidad.getNombre() + ".");
					nuevo();
					cargarLista();
					visibleNuevoEditar = true;
				} catch (Exception e) {
					log.error("[usuario: " + jutil.getUsuarioN() + ", talla: " + entidad.getNombre() + "] [Fallo al guardar]", e);
					SysMessage.error("Fallo al guardar.");
					bitacoraaccion.accion(getClass().getName(), "Fallo al guardar la talla: " + entidad.getNombre() + ".");
				}
			} else {
				try {
					tallapersistence.update(entidad);					
					log.info("[usuario: " + jutil.getUsuarioN() + ", talla: " + entidad.getNombre() + "] [Guardado correctamente]");
					SysMessage.info("Actualizado correctamente.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Se actualizado correctamente la talla: " + entidad.getNombre() + ".");
					nuevo();
					cargarLista();
					visibleNuevoEditar = false;
				} catch (Exception e) {
					log.error("[usuario: " + jutil.getUsuarioN() + ", talla: " + entidad.getNombre() + "] [Fallo al guardar]", e);
					SysMessage.error("Fallo al guardar.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Fallo al actualizar la talla: " + entidad.getNombre() + ".");
				}
			}
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + ", talla: " + entidad.getNombre() + "] [" + validacion + "]");
			SysMessage.warn(validacion);
		}
	}	

	public void editar() {
		if (selected != null) {
			entidad = selected;			
			nuevo = false;
			visibleNuevoEditar = true;
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
		log.info( "entidad:" + entidad + " nuevo: " + nuevo );
	}

	public void editar(Talla obj) {
		log.info("intentando editar" + obj.getNombre() );
		selected = obj;
		editar();
	}

	public void onRowSelect(org.primefaces.event.SelectEvent event) {
		selected = (Talla)event.getObject();
		log.info( "Color seleccionado:" + selected.getNombre() );
		editar();
    }
	
	public void eliminar() {
		if (selected != null) {			
			try {
				selected.setActivo( false );
				tallapersistence.update(selected);				
				log.info("[usuario: " + jutil.getUsuarioN() + ", talla: " + selected.getNombre() + "] [Eliminado correctamente]");
				SysMessage.info("Eliminado correctamente.");
				bitacoraaccion.accion(selected.getClass().getName(), "Se elimino correctamente la talla: " + selected.getNombre() + ".");
				cargarLista();
				nuevo();
			} catch (Exception e) {
				log.error("[usuario: " + jutil.getUsuarioN() + ", talla: " + selected.getNombre() + "] [Fallo al eliminar]", e);
				SysMessage.info("Fallo al eliminar.");
				bitacoraaccion.accion(selected.getClass().getName(), "Fallo al eliminar la talla: " + selected.getNombre() + ".");
			}
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
	}
		
	public void eliminar(Talla obj) {
		selected = obj;
		eliminar();
	}
	
	public void buscar(){
		list = tallapersistence.getLista( cadenaBusqueda );		
	}
	
	/**
	 * Obtiene la talla de la lista previamente cargada.
	 * @param code
	 * @return
	 */
	public Talla obtenerTallaCode( Integer code ){					
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			Talla talla = (Talla) iter.next();
			if ( talla.getCode() == code ){
				return talla;
			}
		}
		return null;
	}
	
	
	@Override
	public void firstpage() {
		try{
			list = tallapersistence.firstpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "firstpage", e );
		}
	}

	@Override
	public void previouspage() {
		try{
			list = tallapersistence.previouspage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "previouspage", e );
		}
	}

	@Override
	public void nextpage() {
		try{
			list = tallapersistence.nextpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "nextpage", e );
		}
	}

	@Override
	public void lastpage() {
		try{
			list = tallapersistence.lastpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "lastpage", e );
		}
	}

	public int getCantidadRegistros() {
		return tallapersistence.getCantidadRegistros();
	}
	
	public int getCantidadPaginas() {
		return tallapersistence.getCantidadPaginas();
	}
	
	public int getPagina() {
		return tallapersistence.getPagina();
	}
	
	
}
