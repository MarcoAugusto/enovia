/**
 * 
 */
package bean.producto;

import java.util.ArrayList;
import java.util.Iterator;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import persistence.producto.PrendaPersistence;

import entidad.Prenda;

import bean.bitacora.BitacoraAccion;
import bean.util.BaseBean;
import bean.util.IGeneral;
import bean.util.IPagination;
import bean.util.Jutil;
import bean.util.SysMessage;


/**
 * @author Marcus
 *
 */
@ManagedBean(name="prendabean")
@ViewScoped
public class PrendaBean extends BaseBean<Prenda> implements IGeneral, IPagination {

	@Inject
	private PrendaPersistence prendapersistence;
	
	@Inject
	private BitacoraAccion bitacoraaccion;
	
	@Inject
	private Jutil jutil;
		
	
	@Override
	@PostConstruct
	public void init() { 
		iniLogs(this.getClass());
		prendapersistence.init();
		cargarLista();
		nuevo();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void cargarLista(){
		try{
			this.list = prendapersistence.getLista();			
		}catch (Exception e) {
			this.list = new ArrayList();
			log.error( "[ColorBean.cargarLista]", e );
		}
	}
	
	public void nuevo(){
		entidad = new Prenda();
		entidad.setActivo(true);		
		nuevo = true;
		visibleNuevoEditar = false;
		selected = null;
	}
	
	public void nuevaEntidad() {
		nuevo();
		visibleNuevoEditar = true;
	}
	
	public void guardar() {
		String validacion = prendapersistence.validarCampos(entidad, nuevo);
		if (validacion.trim().isEmpty()) {
			if (nuevo) {
				try {
					prendapersistence.save( entidad );					
					log.info("[usuario: " + jutil.getUsuarioN() + ", TipoPrenda: " + entidad.getNombre() + "] [Guardado correctamente]");					
					SysMessage.info("Guardado correctamente.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Se guardo correctamente el TipoPrenda: " + entidad.getNombre() + ".");
					nuevo();
					cargarLista();
					visibleNuevoEditar = true;
				} catch (Exception e) {
					log.error("[usuario: " + jutil.getUsuarioN() + ", TipoPrenda: " + entidad.getNombre() + "] [Fallo al guardar]", e);
					SysMessage.error("Fallo al guardar.");
					bitacoraaccion.accion(getClass().getName(), "Fallo al guardar la prenda: " + entidad.getNombre() + ".");
				}
			} else {
				try {
					prendapersistence.update(entidad);					
					log.info("[usuario: " + jutil.getUsuarioN() + ", TipoPrenda: " + entidad.getNombre() + "] [Guardado correctamente]");
					SysMessage.info("Actualizado correctamente.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Se actualizado correctamente el TipoPrenda: " + entidad.getNombre() + ".");
					nuevo();
					cargarLista();
					visibleNuevoEditar = false;
				} catch (Exception e) {
					log.error("[usuario: " + jutil.getUsuarioN() + ", TipoPrenda: " + entidad.getNombre() + "] [Fallo al guardar]", e);
					SysMessage.error("Fallo al guardar.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Fallo al actualizar el TipoPrenda: " + entidad.getNombre() + ".");
				}
			}
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + ", color: " + entidad.getNombre() + "] [" + validacion + "]");
			SysMessage.warn(validacion);
		}
	}	

	public void editar() {
		if (selected != null) {
			entidad = selected;			
			nuevo = false;
			visibleNuevoEditar = true;
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
		log.info( "entidad:" + entidad + " nuevo: " + nuevo );
	}

	public void editar(Prenda obj) {
		log.info("intentando editar" + obj.getNombre() );
		selected = obj;
		editar();
	}

	public void onRowSelect(org.primefaces.event.SelectEvent event) {
		selected = (Prenda)event.getObject();
		log.info( "Color seleccionado:" + selected.getNombre() );
		editar();
    }
	
	public void eliminar() {
		if (selected != null) {			
			try {
				selected.setActivo( false );
				prendapersistence.update(selected);				
				log.info("[usuario: " + jutil.getUsuarioN() + ", TipoPrenda: " + selected.getNombre() + "] [Eliminado correctamente]");
				SysMessage.info("Eliminado correctamente.");
				bitacoraaccion.accion(selected.getClass().getName(), "Se elimino correctamente el TipoPrenda: " + selected.getNombre() + ".");
				cargarLista();
				nuevo();
			} catch (Exception e) {
				log.error("[usuario: " + jutil.getUsuarioN() + ", color: " + selected.getNombre() + "] [Fallo al eliminar]", e);
				SysMessage.info("Fallo al eliminar.");
				bitacoraaccion.accion(selected.getClass().getName(), "Fallo al eliminar el TipoPrenda: " + selected.getNombre() + ".");
			}
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
	}
		
	public void eliminar(Prenda obj) {
		selected = obj;
		eliminar();
	}
	
	public void buscar(){
		list = prendapersistence.getLista( cadenaBusqueda );		
	}
	
	/**
	 * Obtiene la prenda de la lista previamente cargada.
	 * @param code
	 * @return
	 */
	public Prenda obtenerPrendaCode( Integer code ){					
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			Prenda prenda = (Prenda) iter.next();
			if ( prenda.getCode() == code ){
				return prenda;
			}
		}
		return null;
	}
	
	@Override
	public void firstpage() {
		try{
			list = prendapersistence.firstpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "firstpage", e );
		}
	}

	@Override
	public void previouspage() {
		try{
			list = prendapersistence.previouspage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "previouspage", e );
		}
	}

	@Override
	public void nextpage() {
		try{
			list = prendapersistence.nextpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "nextpage", e );
		}
	}

	@Override
	public void lastpage() {
		try{
			list = prendapersistence.lastpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "lastpage", e );
		}
	}

	public int getCantidadRegistros() {
		return prendapersistence.getCantidadRegistros();
	}
	
	public int getCantidadPaginas() {
		return prendapersistence.getCantidadPaginas();
	}
	
	public int getPagina() {
		return prendapersistence.getPagina();
	}
	
	
}
