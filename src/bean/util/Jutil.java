/**
 * 
 */
package bean.util;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import entidad.Usuario;

/**
 * @author Marcus
 *
 */
@Named
public class Jutil implements Serializable {

	
	/**
	 * retorna la ip del cliente.
	 * @return
	 */
	public String getIp(){		
		String remoteAddr = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRemoteAddr();
		return remoteAddr;
	}
	
	/**
	 * Setea el objeto usuario en session.
	 * @param usuario
	 */
	public void setUsuarioSession( Usuario usuario ){
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		request.getSession().setAttribute( "TEMP$USER", usuario);		
		request.getSession().setAttribute( "TEMP$USER_NAME", usuario.getNombre());
	}
	
	/**
	 * retorna el usuario logeado.
	 * @return
	 */
	public Object getUsuarioO(){
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		Object usuario = request.getSession().getAttribute("TEMP$USER");
		return usuario;
	}
	
	public String getUsuarioN(){
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		Object usuario = request.getSession().getAttribute("TEMP$USER_NAME");
		return ( usuario == null ? "[?]" : usuario.toString() );		
	}
	
	/**
	 * guarda un objeto temporalmente
	 * @param obj
	 */
	public void addObjectParam( Object obj ){
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		request.getSession().setAttribute( "TEMP$OBJ", obj );
	}
	
	/**
	 * Obtiene el objeto y lo elimina.
	 * @return
	 */
	public Object getObjectParam(){
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		Object obj = request.getSession().getAttribute("TEMP$OBJ");
//		request.getSession().removeAttribute( "TEMP$OBJ" );
		return obj;
	}
	
	/**
	 * M�todo para redireccionar desde un bean.
	 * Nota: El m�todo adicionar la cadena '?faces-redirect=true'
	 * @param url
	 * @throws Exception
	 */
	public void redirect( String url ) throws Exception {
		String redirect = "";
		FacesContext.getCurrentInstance().getExternalContext().redirect( url + redirect );
	}
	
	/**
	 * retorna la fecha actual en base a un formato.
	 * @param formato
	 * @return
	 */
	public String getFecha( String formato ){
		Calendar calendario = Calendar.getInstance();
		SimpleDateFormat formatofecha = new SimpleDateFormat( formato );
		return formatofecha.format( calendario.getTime() );
	}
	
	/**
	 * Convierte la fecha de Date a String en el formato indicado.
	 * @param formato
	 * @param fecha
	 * @return
	 */
	public String FechaToString( String formato, Date fecha ){		
		SimpleDateFormat formatofecha = new SimpleDateFormat( formato );
		return formatofecha.format( fecha );
	}
	
}
