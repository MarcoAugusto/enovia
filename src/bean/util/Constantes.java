package bean.util;

import javax.inject.Named;

@Named
public class Constantes {
	
	public static String PAGINA_FIRST = "FIRST";
	public static String PAGINA_PREVIOUS = "PREVIOUS";
	public static String PAGINA_NEXT = "NEXT";
	public static String PAGINA_LAST = "LAST";
	public static String VACIO = "";
	public static String LIKE = "%";
	public static String OR = " or ";	
	public static String AND = " and ";	
	public static String WHERE = " where ";
	public static String ABRIR_PARENTESIS = " ( ";
	public static String CERRAR_PARENTESIS = " ) ";
	public static String COMA = ",";
	public static String ESTADO_PEDIDO = "PEDIDO";
	public static String ESTADO_RESERVADO_PEDIDO = "RESERVADO_PEDIDO";	
	public static String ESTADO_TRANSITO = "TRANSITO";
	public static String ESTADO_RESERVADO_TRANSITO = "RESERVADO_TRANSITO";
	public static String ESTADO_ENSTOCK = "EN STOCK";
	public static String ESTADO_RESERVADO = "RESERVADO";
	public static String ESTADO_EGRESADO = "EGRESADO";

	public static String PREFIJO_SUCURSAL = "SUC";
	public static String PREFIJO_BASE_PRODUCTO = "BPROD";
	public static String PREFIJO_PRODUCTO = "PROD";
	public static String PREFIJO_PROVEEDOR = "PROV";
	public static String PREFIJO_INGRESO = "ING";
	public static String PREFIJO_EGRESO = "EGR";
	public static String PREFIJO_TRASPASO = "TRAS";
	public static String PREFIJO_PEDIDO = "PED";
	public static String PREFIJO_COMPRA = "COM";	
	public static String PREFIJO_TRANSITO = "TRAN";
	public static String PREFIJO_RECEPCION = "RECP";
	
	
}
