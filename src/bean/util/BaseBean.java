/**
 * 
 */
package bean.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;

/**
 * @author Marcus
 *
 */
public class BaseBean<T> extends Base implements Serializable {

	
	protected T entidad = null;
	
	protected T selected = null;
	
	/**
	 * Lista donde se cargar� el listado de los datos a mostrar...
	 */
	protected List<T> list = new ArrayList<T>();	
	
	/**
	 * Indica si se va a crear un nuevo registro.
	 */
	protected boolean nuevo = true;
	
	/**
	 * Inidca si se va a editar el registro.
	 */
	protected boolean visibleNuevoEditar = true;	
	
	
	// **************************** BUSQUEDA ****************************************
//	/**
//	 * Cantidad de filas, cantidad de registros de la consulta..
//	 */
//	protected int cantidadRegistros = 0;
//	/**
//	 * Cantidad de p�ginas, valor calculado entre cantidadRegistros y la
//	 * cantidad de registros a mostrar por p�gina.
//	 */
//	protected int cantidadPaginas = 0;
//	/**
//	 * p�gina en la cual se encuentra actualmente la consulta...
//	 */
//	protected int pagina = 1;
	/**
	 * Valor que el usuario introduce para realizar la busqueda
	 */
	protected String cadenaBusqueda = "";
	/**
	 * Valor del combo que selecciona por que filtro se va a aplicar la
	 * busqueda...
	 */
	protected String filtroBusqueda = "";
	
	
	public void iniLogs( Class clase ){
		//log = Logger.getLogger(clase);
		super.setup( clase );
	}
	
	/**
	 * @return the cadenaBusqueda
	 */
	public String getCadenaBusqueda() {
		return cadenaBusqueda;
	}

	/**
	 * @param cadenaBusqueda the cadenaBusqueda to set
	 */
	public void setCadenaBusqueda(String cadenaBusqueda) {
		this.cadenaBusqueda = cadenaBusqueda;
	}

	/**
	 * @return the filtroBusqueda
	 */
	public String getFiltroBusqueda() {
		return filtroBusqueda;
	}

	/**
	 * @param filtroBusqueda the filtroBusqueda to set
	 */
	public void setFiltroBusqueda(String filtroBusqueda) {
		this.filtroBusqueda = filtroBusqueda;
	}

	public T getEntidad() {
		return entidad;
	}



	public void setEntidad(T entidad) {
		this.entidad = entidad;
	}



	public List<T> getList() {
		return list;
	}



	public void setList(List<T> list) {
		this.list = list;
	}



	public boolean isNuevo() {
		return nuevo;
	}



	public void setNuevo(boolean nuevo) {
		this.nuevo = nuevo;
	}



	public boolean isVisibleNuevoEditar() {
		return visibleNuevoEditar;
	}



	public void setVisibleNuevoEditar(boolean visibleNuevoEditar) {
		this.visibleNuevoEditar = visibleNuevoEditar;
	}



	/**
	 * @return the selected
	 */
	public T getSelected() {
		return selected;
	}



	/**
	 * @param selected the selected to set
	 */
	public void setSelected(T selected) {
		this.selected = selected;
	}
		
	
	
}
