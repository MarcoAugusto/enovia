package bean.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

public class Parametros {

	static {
		try {
			init();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static int CANTIDAD_REGISTRO_PAGINA;
	public static String USUARIO_SOPORTE;
	public static String PASSWORD_SOPORTE;
	public static String IMAGEN_DEPOSITO;
	public static String IMAGEN_DEFAULT;	
	public static int IMAGEN_WIDTH;
	public static int IMAGEN_HEIGHT;
	
	public static String FECHA_DDMMYYYY;
	public static String FECHA_DDMMYYYYHMS;
	
	public static List<String> USUARIOS_TIPO_EXTERNOS_LIST;
	public static List<String> INGRESO_TIPO_INGRESO_LIST;
	public static List<String> EGRESO_TIPO_INGRESO_LIST;
	public static List<String> TRANSITO_TRANSPORTE_TIPO_LIST;
	
	public static String MONEDA;
	public static String REDIRECT;
	public static String COMPRA_INGRESOTIPO;	
	public static String VENTA_EGRESOTIPO;
	public static BigDecimal DOLAR_COMPRA;
	public static BigDecimal DOLAR_VENTA;
	
	public static String MODPEDIDO_ESTADO_PEDIDO;
	public static String MODPEDIDO_ESTADO_COMPRADO;
	public static String MODPEDIDO_ESTADO_RECHAZADO;
	
	
	public static void init(){
		
		String configFile = "messages";
		ResourceBundle rb = ResourceBundle.getBundle(configFile);
		
		CANTIDAD_REGISTRO_PAGINA = Integer.parseInt( rb.getString( "CANTIDAD_REGISTRO_PAGINA" ) );
		USUARIO_SOPORTE = rb.getString( "usuario_soporte" );
		PASSWORD_SOPORTE = rb.getString( "password_soporte" );
		
		StringTokenizer token = new StringTokenizer(rb.getString( "usuario_tipoexterno" ), Constantes.COMA );
		USUARIOS_TIPO_EXTERNOS_LIST = new ArrayList<String>();
		while( token.hasMoreElements() ){
			USUARIOS_TIPO_EXTERNOS_LIST.add( token.nextToken() );
		}
		
		IMAGEN_DEPOSITO = rb.getString( "imgdeposito" );
		IMAGEN_DEFAULT = rb.getString( "imgdefault" );
		IMAGEN_WIDTH = Integer.parseInt( rb.getString( "imgwidth" ) );
		IMAGEN_HEIGHT = Integer.parseInt( rb.getString( "imgheight" ) );
		
		FECHA_DDMMYYYY = rb.getString( "fecha_ddMMyyyy" );
		FECHA_DDMMYYYYHMS = rb.getString( "fecha_ddMMyyyyhms" );
		
		token = new StringTokenizer(rb.getString( "ingreso_tipos" ), Constantes.COMA );		
		INGRESO_TIPO_INGRESO_LIST = new ArrayList<String>();
		while( token.hasMoreElements() ){
			INGRESO_TIPO_INGRESO_LIST.add( token.nextToken() );
		}
		
		token = new StringTokenizer(rb.getString( "egreso_tipos" ), Constantes.COMA );		
		EGRESO_TIPO_INGRESO_LIST = new ArrayList<String>();
		while( token.hasMoreElements() ){
			EGRESO_TIPO_INGRESO_LIST.add( token.nextToken() );
		}			
		
//		MONEDA = rb.getString( "imgdeposito" );
		DOLAR_COMPRA = new BigDecimal( rb.getString( "moneda_Dolar_compra" ) );
		DOLAR_VENTA = new BigDecimal(  rb.getString( "moneda_Dolar_venta" ) );
		REDIRECT = rb.getString( "redirect" );
		COMPRA_INGRESOTIPO = rb.getString( "compra_ingresotipo" );		
		VENTA_EGRESOTIPO = rb.getString( "venta_egresotipo" );
		
		// valores para los estados del pedido.
		MODPEDIDO_ESTADO_PEDIDO = rb.getString( "modpedido_estado_pedido" );
		MODPEDIDO_ESTADO_COMPRADO = rb.getString( "modpedido_estado_comprado" );
		MODPEDIDO_ESTADO_RECHAZADO = rb.getString( "modpedido_estado_rechazado" );
		
		token = new StringTokenizer(rb.getString( "transito_transporte_tipo" ), Constantes.COMA );		
		TRANSITO_TRANSPORTE_TIPO_LIST = new ArrayList<String>();
		while( token.hasMoreElements() ){
			TRANSITO_TRANSPORTE_TIPO_LIST.add( token.nextToken() );
		}
		
		
	}
		
	
}
