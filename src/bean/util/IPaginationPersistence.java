/**
 * 
 */
package bean.util;

import java.util.List;

/**
 * @author Marcus
 *
 */
public interface IPaginationPersistence {

	/**
	 * Obtiene la primera p�gina de la consulta ya realizada.
	 * @return
	 */
	public List firstpage() throws Exception;
	/**
	 * Obtiene la anterior p�gina de la consulta.
	 * @return
	 */
	public List previouspage() throws Exception;
	/**
	 * Obtiene la siguiente p�gina de la consulta.
	 * @return
	 */
	public List nextpage() throws Exception;
	/**
	 * Obtiene la �ltima p�gina de la consulta.
	 * @return
	 */
	public List lastpage() throws Exception;
	
}
