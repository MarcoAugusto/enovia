/**
 * 
 */
package bean.util;

import java.util.List;

/**
 * @author Marcus
 *
 */
public interface IPagination {

	/**
	 * Obtiene la primera p�gina de la consulta ya realizada.
	 * @return
	 */
	public void firstpage() throws Exception;
	/**
	 * Obtiene la anterior p�gina de la consulta.
	 * @return
	 */
	public void previouspage() throws Exception;
	/**
	 * Obtiene la siguiente p�gina de la consulta.
	 * @return
	 */
	public void nextpage() throws Exception;
	/**
	 * Obtiene la �ltima p�gina de la consulta.
	 * @return
	 */
	public void lastpage() throws Exception;
	
}
