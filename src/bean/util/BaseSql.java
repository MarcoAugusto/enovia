/**
 * 
 */
package bean.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Inject;

import org.apache.log4j.Logger;

/**
 * @author penama
 *
 */
public class BaseSql extends Base implements Serializable {

	@Inject
	protected Persistencia persistencia;
		
	/**
	 * Variable que almacena la ultima consulta realizada
	 */
	protected String hql = "";
	
	/**
	 * Variable que almacena la ultima consulta realizada
	 */
	protected String hqlCount = "";
	
	protected int cantidadRegistros = 0;
	protected int cantidadPaginas = 0;
	protected int pagina = 0;
	protected String cadenaBusqueda = "";
	protected String filtroBusqueda = "";
	
	
	/**
	 * Variable que indica si las consultas se van a realizar con paginado...
	 */
	protected boolean paginado = true;
	
	/**
	 * Estructura map para poder almacenar los parametros y su valor de las
	 * consultas....
	 */
	protected Map<String, Object> mapa = new HashMap<String, Object>();

	/**
	 * Estructura map para almacenar las consultas de los campos para los
	 * filtros que se realicen...
	 */
	protected Map<String, String> mapaHqlCampos = new HashMap<String, String>();
	
	/**
	 * Estructura map para almacenar los valores de los campos para los
	 * filtros que se realicen...
	 */
	protected Map<String, String> mapaHqlValores = new HashMap<String, String>();
	
	
	// **********************************METODOS******************************************

	public void iniLogs( Class clase ){		
		super.setup(clase);
	}
	
	/**
	 * M�todo que realiza el calculo de la cantidad de tuplas y cantidad de
	 * p�ginas que tiene la consulta a obtener...
	 * 
	 * @param hqlAux
	 *            Consulta a realizar...
	 * @param mapaAux
	 *            Estructura donde se almacena los datos de la consulta..
	 */
	public void consultaCount(String hqlAux, Map<String, Object> mapaAux) throws Exception {
		Object cons = persistencia.Consulta_item(hqlAux, mapaAux);
		cantidadRegistros = Integer.parseInt(cons.toString());
		cantidadPaginas = cantidadRegistros == 0 ? 0 : 1;
		if (paginado) {
			cantidadPaginas = (cantidadRegistros / Parametros.CANTIDAD_REGISTRO_PAGINA)
					+ (cantidadRegistros % Parametros.CANTIDAD_REGISTRO_PAGINA == 0 ? 0 : 1);
		}
	}

	public List Consulta( String hqlAux ) throws Exception {
		return Consulta( hqlAux, -1, null );
	}
	
	/**
	 * M�todo para realizar la consulta
	 * 
	 * @param hqlAux
	 * @param paginaAux
	 * @param mapaAux
	 * @return
	 */
	public List Consulta(String hqlAux, int paginaAux, Map<String, Object> mapaAux) throws Exception {
		int initPagina = 0;
		if (paginaAux > 0 && paginaAux <= cantidadPaginas) {
			initPagina = (paginaAux - 1) * Parametros.CANTIDAD_REGISTRO_PAGINA;
		}
		if (paginado) {
			return persistencia.Consulta(hqlAux, initPagina, Parametros.CANTIDAD_REGISTRO_PAGINA, mapaAux);
		} else {
			return persistencia.Consulta(hqlAux, mapaAux);
		}

	}

	/**
	 * m�todo que devuelve la p�gina indicada
	 * @param pagina
	 * @return
	 * @throws Exception
	 */
	public List ConsultaPagina( int pagina ) throws Exception{
		return Consulta(hql, pagina, mapa);
	}
	
	/**
	 * M�todo para direccionar la busqueda, primero, anterior, siguiente,
	 * ultimo...
	 * 
	 * @param filtro
	 *            recibe los siguientes parametros: first, previous, next, last
	 * @param hqlAux
	 *            parametro de la consulta a realizar...
	 */
	public List paginaTabla(String hqlAux, String filtro) throws Exception {
		int nroPagina = NavegacionPagina(filtro);
		return Consulta(hqlAux, nroPagina, mapa);
	}

	/**
	 * M�todo que verifica y calcula el valor de la siguiente o anterior pagina
	 * a mostra (p�gina de datos, osea cantidad de items por p�gina).
	 * 
	 * @param paginaAux
	 *            Filtro indicando en que direcci�n indico..
	 * @return
	 */
	public int NavegacionPagina(String paginaAux) {
		if (paginaAux.equalsIgnoreCase( Constantes.PAGINA_FIRST )) {
			pagina = 1;
			return pagina;
		} else if (paginaAux.equalsIgnoreCase( Constantes.PAGINA_PREVIOUS )) {
			return (pagina - 1 == 0 ? pagina : --pagina);
		} else if (paginaAux.equalsIgnoreCase(Constantes.PAGINA_NEXT)) {
			return (pagina + 1 <= cantidadPaginas ? ++pagina : pagina);
		} else if (paginaAux.equalsIgnoreCase(Constantes.PAGINA_LAST)) {
			pagina = cantidadPaginas;
			return pagina;
		}
		return pagina;
	}

	/**
	 * M�todo que arma los filtros para la consulta. Esto de acuerdo a lo que el
	 * usuario a seleccionado e ingresado
	 * 
	 * @return Cadena que se adicionar� a la consulta...
	 */
	public String armarFiltro() {
		cadenaBusqueda = cadenaBusqueda.trim().toLowerCase();
		mapa.clear();
		if (cadenaBusqueda.trim().isEmpty()) {			
			return Constantes.VACIO;
		}
		String cadena = Constantes.LIKE + cadenaBusqueda + Constantes.LIKE;
		StringBuffer hqlAux = new StringBuffer( Constantes.VACIO );
		if (filtroBusqueda.equalsIgnoreCase("todos")) {
			Iterator<Entry<String, String>> iterEntry = mapaHqlCampos.entrySet().iterator();
			while (iterEntry.hasNext()) {
				Entry<String, String> entry = iterEntry.next();
				hqlAux.append(entry.getValue() + (iterEntry.hasNext() ? Constantes.OR : Constantes.VACIO));
				mapa.put(entry.getKey(), cadena);
			}
		} else {
			String aux = mapaHqlCampos.get(filtroBusqueda);			
			if (aux != null) {
				mapa.put(filtroBusqueda, cadena);
				hqlAux.append(aux);				
			}		
		}		
		return hqlAux.toString();
	}

	/**
	 * Obtiene los filtros a aplicar segun los campos completados para la entidad.
	 * Previo a esta m�todo debe tener cargado el mapaHQLValores<keyFiltro, valor>
	 * @return
	 */
	public String armarFiltroMultiple() {		
		cadenaBusqueda = cadenaBusqueda.trim().toLowerCase();
		mapa.clear();
		log.info("cadenaBusqueda :" + cadenaBusqueda );
		if (cadenaBusqueda.trim().isEmpty()) {			
			return Constantes.VACIO;
		}		
		String cadena = Constantes.LIKE + cadenaBusqueda + Constantes.LIKE;
		StringBuffer hqlAux = new StringBuffer( Constantes.VACIO );
		if (filtroBusqueda.equalsIgnoreCase("todos")) {
			Iterator<Entry<String, String>> iterEntry = mapaHqlCampos.entrySet().iterator();
			while (iterEntry.hasNext()) {
				Entry<String, String> entry = iterEntry.next();
				hqlAux.append(entry.getValue() + (iterEntry.hasNext() ? Constantes.OR : Constantes.VACIO));
				mapa.put(entry.getKey(), cadena);
			}
		} if ( filtroBusqueda.equalsIgnoreCase( "filtros" ) ) {			
			Iterator<Entry<String, String>> iterEntry = mapaHqlValores.entrySet().iterator();
			while( iterEntry.hasNext() ){
				Entry<String, String> entry = iterEntry.next();
				hqlAux.append(mapaHqlCampos.get(entry.getKey()) + (iterEntry.hasNext() ? Constantes.AND : Constantes.VACIO));
				mapa.put(entry.getKey(), Constantes.LIKE + entry.getValue() + Constantes.LIKE );
			}			
		} else {
			String aux = mapaHqlCampos.get(filtroBusqueda);			
			if (aux != null) {
				mapa.put(filtroBusqueda, cadena);
				hqlAux.append(aux);				
			}		
		}		
		return hqlAux.toString();
	}
	
	public int getCantidadRegistros() {
		return cantidadRegistros;
	}

	public void setCantidadRegistros(int cantidadRegistros) {
		this.cantidadRegistros = cantidadRegistros;
	}

	public int getCantidadPaginas() {
		return cantidadPaginas;
	}

	public void setCantidadPaginas(int cantidadPaginas) {
		this.cantidadPaginas = cantidadPaginas;
	}

	public int getPagina() {
		return pagina;
	}

	public void setPagina(int pagina) {
		this.pagina = pagina;
	}

	public String getCadenaBusqueda() {
		return cadenaBusqueda;
	}

	public void setCadenaBusqueda(String cadenaBusqueda) {
		this.cadenaBusqueda = cadenaBusqueda;
	}

	public String getFiltroBusqueda() {
		return filtroBusqueda;
	}

	public void setFiltroBusqueda(String filtroBusqueda) {
		this.filtroBusqueda = filtroBusqueda;
	}

	public boolean isPaginado() {
		return paginado;
	}

	public void setPaginado(boolean paginado) {
		this.paginado = paginado;
	}

	/**
	 * @return the mapaHqlValores
	 */
	public Map<String, String> getMapaHqlValores() {
		return mapaHqlValores;
	}

	/**
	 * @param mapaHqlValores the mapaHqlValores to set
	 */
	public void setMapaHqlValores(Map<String, String> mapaHqlValores) {
		this.mapaHqlValores = mapaHqlValores;
	}

	
	
}
