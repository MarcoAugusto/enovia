/**
 * 
 */
package bean.util;

import java.util.List;

import javax.annotation.PostConstruct;

/**
 * @author Marcus
 *
 */
public interface IGeneral {
	
	/**
	 * M�todo iniciador de las varibles generales del bean
	 */	
	public void init();
	/**
	 * carga la lista inicial del bean
	 * @throws Exception
	 */
	public void cargarLista() throws Exception;		
}
