/**
 * 
 */
package bean.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;

/**
 * @author Marcus
 *
 */
@Named
public class Persistencia implements Serializable {

	@PersistenceContext(unitName = "ENovia_Ds")
	private transient EntityManager entityManager;
	 
	@Resource
	private transient UserTransaction transaction;
	
	//****************************************************************************
	//     	PERSITENCIA GRUPAL
	//****************************************************************************
	public void beginTransaccion() throws Exception {
		transaction.begin();
		entityManager.joinTransaction();
	}
	
	public void persist( Object entidad ) throws Exception {
		if (entidad == null) {
			throw new Exception("La entidad a persistir es null");
		}
		entityManager.persist( entidad );
	}
	
	public void merge( Object entidad ) throws Exception {
		if (entidad == null) {
			throw new Exception("La entidad a actualizar es null");
		}
		entityManager.merge( entidad );
	}
	
	public void commit() throws Exception {
		transaction.commit();
	}
	
	public void rollback() throws Exception{
		transaction.rollback();
	}
	
	
	//****************************************************************************
	//     	PERSITENCIA GRUPAL END
	//****************************************************************************
	
	/**
	 * Persistir una entidad a la base de datos.
	 * @param entidad
	 * @throws Exception
	 */
	public void save(Object entidad) throws Exception {
		if (entidad == null) {
			throw new Exception("La entidad a persistir es null");
		}
		transaction.begin();
		entityManager.joinTransaction();
		try{
			entityManager.persist( entidad );
			transaction.commit();
		}catch (Exception e) {
			transaction.rollback();
			throw e;
		} 
	}

	
	/**
	 * Actualizar la entidad a la base de datos.
	 * @param entidad
	 * @throws Exception
	 */
	public void update(Object entidad) throws Exception {
		if (entidad == null) {
			throw new Exception("La entidad a actualizar es null");
		}
		transaction.begin();
		entityManager.joinTransaction();		
		try{
			entityManager.merge( entidad );
			transaction.commit();
		}catch (Exception e) {
			transaction.rollback();
			throw e;
		} 
	}

	
	public void remove(Object entidad) throws Exception {
		if (entidad == null) {
			throw new Exception("La entidad a eliminar es null");
		}		
		transaction.begin();
		entityManager.joinTransaction();		
		try{
			entityManager.remove( entidad );
			transaction.commit();
		}catch (Exception e) {
			transaction.rollback();
			throw e;
		} 
	}

	/**
	 * Vacia el buffer de transacciones de hibernate.
	 */
	public void limpiarCanal() {
		entityManager.flush();
	}

	/**
	 * Realiza la consulta obteniendo una lista de objetos resultado de la consulta enviada.
	 * 
	 * @param sql
	 *            	Consulta.
	 * @return Lista 
	 * 				de objetos con tipo de datos relativo a la consulta enviada...
	 * @throws Exception
	 */
	public List Consulta(String sql) throws Exception {
		return Consulta(sql, null);
	}

	/**
	 * Realiza la consulta obteniendo una lista de objetos resultado de la consulta enviada
	 * con paginaci�n.
	 * 
	 * @param sql
	 * 				Consulta
	 * @param init_pagina
	 * 				indice a partir de que tupla se devolveran la lista.
	 * @param cantItemsPaginas
	 * 				Cantidad de items a obtener para la p�gina
	 * @return Lista
	 * 				de objetos con tipo de datos relativo a la consulta enviada...
	 * @throws Exception
	 */
	public List Consulta(String sql, int init_pagina, int cantItemsPaginas ) throws Exception {
		return Consulta(sql, init_pagina, cantItemsPaginas, null);
	}

	/**
	 * Realiza la consulta obteniendo una lista de objetos resultado de la consulta enviada
	 * con parametros.
	 * </br></br>
	 * Si la cadena es vacia o si ocurre algun problema en el flujo del m�todo 
	 * se genera una excepci�n indicando el problema...
	 * 
	 * @param sql
	 *            	Consulta.
	 * @param mapa
	 * 			 	Parametros de la consulta	
	 * @return Lista 
	 * 				de objetos con tipo de datos relativo a la consulta enviada...
	 * @throws Exception
	 */
	public List Consulta(String sql, Map<String, Object> mapa) throws Exception {		
		return Consulta(sql, -1, 0, mapa);		
	}

	/**
	 * Realiza la consulta obteniendo una lista de objetos resultado de la consulta enviada
	 * con parametros y paginaci�n.
	 * </br></br>
	 * Si la cadena es vacia o si ocurre algun problema en el flujo del m�todo 
	 * se genera una excepci�n indicando el problema...
	 * @param sql
	 *            	Consulta.
	 * @param init_pagina
	 *            	indice a partir de que tupla se devolveran la lista.
	 * @param cantItemsPaginas
	 *            	Cantidad de items a obtener para la p�gina
	 * @param mapa
	 * 			  	Parametros de la consulta            
	 * @return Lista 
	 * 				de objetos con tipo de datos relativo a la consulta enviada...
	 * @throws Exception
	 */
	public List Consulta(String sql, int init_pagina, int cantItemsPaginas, 
			Map<String, Object> mapa) throws Exception {
		
		if ( sql.isEmpty() ){
			throw new Exception( "Cadena de consulta esta vacia [" + sql + "]" );
		}
		Query q = completarQuery(sql, mapa);
		if ( init_pagina != -1 ){
			q.setMaxResults(cantItemsPaginas).setFirstResult(init_pagina);
		}				
		return q.getResultList();
	}

	/**
	 * Realiza la consulta obteniendo un objeto como resultado de la consulta enviada..
	 * </br></br>
	 * Si la cadena es vacia o si ocurre algun problema en el flujo del m�todo 
	 * se genera una excepci�n indicando el problema...
	 * 
	 * @param sql
	 *            Consulta.
	 * @return Objeto 
	 * 			  Con tipo de datos relativo a la consulta enviada...
	 *  @throws Exception
	 */
	public Object Consulta_item(String sql) throws Exception {
		return Consulta_item(sql, null);
	}

	/**
	 * Realiza la consulta obteniendo un objeto como resultado de la consulta enviada con parametros..
	 * </br></br>
	 * Si la cadena es vacia o si ocurre algun problema en el flujo del m�todo 
	 * se genera una excepci�n indicando el problema...  
	 * @param sql
	 * 				Consulta
	 * @param mapa
	 * 				Parametros de la consulta
	 * @return Object 
	 * 				Con tipo de datos relativo a la consulta enviada...
	 * @throws Exception
	 */
	public Object Consulta_item(String sql, Map<String, Object> mapa) throws Exception{
		if ( sql.isEmpty() ){
			throw new Exception( "Cadena de consulta esta vacia [" + sql + "]" );
		}		
		try{
			Query q = completarQuery(sql, mapa);
			return q.getSingleResult();
		}catch ( NoResultException e) {								
			return null;
		}
				
	}


	/**
	 * Completa los parametros de la consulta enviada...
	 * </br></br>
	 * En caso se ocurra algun problema se genera una excepci�n...
	 * @param sql
	 *            Cadena de la consulta con parametros por completar </br> Ej:
	 *            From Area a where a.nombre = :nombre
	 * @param mapa
	 *            Valores con su respectivo nombre de la cadena </br> En el
	 *            ejemplo del parametro se tiene la variable
	 *            <strong>:nombre</strong> </br> en el mapa se tendra que
	 *            adicionar <strong>nombre</strong> como llave y <strong>valor
	 *            a asignar al nombre</strong> como valor de la variable
	 * @return Query con los parametros completados.
	 * @throws Exception
	 */
	private Query completarQuery(String sql, Map<String, Object> mapa) throws Exception {
		Query q = entityManager.createQuery(sql);		
		mapa = (mapa == null ? new HashMap<String, Object>() : mapa);
		if (!mapa.isEmpty()) {
			Iterator<Entry<String, Object>> iter = mapa.entrySet().iterator();
			while (iter.hasNext()) {
				Entry<String, Object> entry = iter.next();
				q.setParameter(entry.getKey(), entry.getValue());
			}
		}
		return q;
	}
	
}
