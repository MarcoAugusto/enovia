/**
 * 
 */
package bean.util;

import java.io.FileInputStream;
import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.swing.ImageIcon;

import org.primefaces.model.UploadedFile;

/**
 * @author penama
 * 
 */
@ManagedBean(name = "uploadbean")
@ViewScoped
public class UploadBean implements Serializable {

	private UploadedFile file;

	public byte[] getImageByte() {
		return file.getContents();
	}

	public ImageIcon getImage() {
		return new ImageIcon(getImageByte());
	}

//	public void handleFileUpload(FileUploadEvent event) {
//
//		FacesContext facesContext = FacesContext.getCurrentInstance();
//		ExternalContext externalContext = facesContext.getExternalContext();
//		HttpServletResponse response = (HttpServletResponse) externalContext
//				.getResponse();
//
//		System.out.println("path:" + externalContext.getRealPath("/upload/"));
//
//		System.out.println("file solo:" + event.getFile().getFileName());
//
//		File result = new File(externalContext.getRealPath("/upload/")
//				+ File.separator + event.getFile().getFileName());
//
//		System.out.println("final file:" + result.getName());
//
//		current.setEmpLog(event.getFile().getFileName());
//		try {
//
//			FileOutputStream fileOutputStream = new FileOutputStream(result);
//
//			byte[] buffer = new byte[BUFFER_SIZE];
//
//			int bulk;
//
//			// Here you get uploaded picture bytes, while debugging you can see
//			// that 34818
//			InputStream inputStream = event.getFile().getInputstream();
//
//			while (true) {
//
//				bulk = inputStream.read(buffer);
//
//				if (bulk < 0) {
//
//					break;
//
//				} // end of if
//
//				fileOutputStream.write(buffer, 0, bulk);
//				fileOutputStream.flush();
//
//			} // end fo while(true)
//
//			fileOutputStream.close();
//			inputStream.close();
//
//			FacesMessage msg = new FacesMessage("Succesful", event.getFile()
//					.getFileName() + " is uploaded.");
//			FacesContext.getCurrentInstance().addMessage(null, msg);
//
//		} catch (IOException e) {
//
//			e.printStackTrace();
//
//			FacesMessage error = new FacesMessage(
//					"The files were not uploaded!");
//			FacesContext.getCurrentInstance().addMessage(null, error);
//
//		}
//
//	}

	/**
	 * @return the file
	 */
	public UploadedFile getFile() {
		return file;
	}

	/**
	 * @param file
	 *            the file to set
	 */
	public void setFile(UploadedFile file) {
		this.file = file;
	}

}
