/**
 * 
 */
package bean.util;

/**
 * @author Marcus
 *
 */
public interface IValidator {

	/**
	 * M�todo que verifica los campos obligatorios de la entidad.
	 * @param entidad
	 * @param nuevo
	 * @return
	 */
	public String validarCampos( Object entidad, boolean nuevo );
	
	/**
	 * M�todo que verifica si existe el nombre en otra entidad.
	 * @param nombre
	 * @return
	 */
	public boolean existeXnombre( String nombre );
	
	/**
	 * M�todo que verifica si existe el nombre en otra entidad diferente al que tiene el nombre.
	 * @param nombre
	 * @param code
	 * @return
	 */
	public boolean existeXnombreDid( String nombre, long code );
	
}
