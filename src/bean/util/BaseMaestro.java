/**
 * 
 */
package bean.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Inject;
import javax.inject.Named;


/**
 * @author Marcus
 * 
 */
@Named
public class BaseMaestro<T> extends BaseBean {

	@Inject
	protected Persistencia persistencia;
	
	/**
	 * Variable que indica si las consultas se van a realizar con paginado...
	 */
	protected boolean paginado = true;
	
	/**
	 * Estructura map para poder almacenar los parametros y su valor de las
	 * consultas....
	 */
	protected Map<String, Object> mapa = new HashMap<String, Object>();

	/**
	 * Estructura map para almacenar las consultas de los campos para los
	 * filtros que se realicen...
	 */
	protected Map<String, String> mapaHqlCampos = new HashMap<String, String>();

	/**
	 * Lista donde se cargar� el listado de los datos a mostrar...
	 */
	protected List<T> list = new ArrayList<T>();

	
	// **************************** BUSQUEDA ****************************************
	/**
	 * Cantidad de filas, cantidad de registros de la consulta..
	 */
	protected int cantidadRegistros = 0;
	/**
	 * Cantidad de p�ginas, valor calculado entre cantidadRegistros y la
	 * cantidad de registros a mostrar por p�gina.
	 */
	protected int cantidadPaginas = 0;
	/**
	 * p�gina en la cual se encuentra actualmente la consulta...
	 */
	protected int pagina = 1;
	/**
	 * Valor que el usuario introduce para realizar la busqueda
	 */
	protected String cadenaBusqueda = "";
	/**
	 * Valor del combo que selecciona por que filtro se va a aplicar la
	 * busqueda...
	 */
	protected String filtroBusqueda = "";
	/**
	 * Variable que almacena la ultima consulta realizada
	 */
	protected String hql = "";
	/**
	 * Variable que almacena la ultima consulta realizada
	 */
	protected String hqlCount = "";

	// **********************************METODOS******************************************

	/**
	 * M�todo que realiza el calculo de la cantidad de tuplas y cantidad de
	 * p�ginas que tiene la consulta a obtener...
	 * 
	 * @param hqlAux
	 *            Consulta a realizar...
	 * @param mapaAux
	 *            Estructura donde se almacena los datos de la consulta..
	 */
	public void calcularConsulta(String hqlAux, Map<String, Object> mapaAux) throws Exception {
		Object cons = persistencia.Consulta_item(hqlAux, mapaAux);
		cantidadRegistros = Integer.parseInt(cons.toString());
		cantidadPaginas = cantidadRegistros == 0 ? 0 : 1;
		if (paginado) {
			cantidadPaginas = (cantidadRegistros / Parametros.CANTIDAD_REGISTRO_PAGINA)
					+ (cantidadRegistros % Parametros.CANTIDAD_REGISTRO_PAGINA == 0 ? 0 : 1);
		}
	}

	/**
	 * M�todo para realizar la consulta
	 * 
	 * @param hqlAux
	 * @param paginaAux
	 * @param mapaAux
	 * @return
	 */
	public List realizarConsulta(String hqlAux, int paginaAux, Map<String, Object> mapaAux) throws Exception {
		int initPagina = 0;
		if (paginaAux > 0 && paginaAux <= cantidadPaginas) {
			initPagina = (paginaAux - 1) * Parametros.CANTIDAD_REGISTRO_PAGINA;
		}
		if (paginado) {
			return persistencia.Consulta(hqlAux, initPagina, Parametros.CANTIDAD_REGISTRO_PAGINA, mapaAux);
		} else {
			return persistencia.Consulta(hqlAux, mapaAux);
		}

	}

	/**
	 * M�todo para direccionar la busqueda, primero, anterior, siguiente,
	 * ultimo...
	 * 
	 * @param filtro
	 *            recibe los siguientes parametros: first, previous, next, last
	 * @param hqlAux
	 *            parametro de la consulta a realizar...
	 */
	public List paginaTabla(String hqlAux, String filtro) throws Exception {
		int nroPagina = calcularNavegacionPagina(filtro);
		return realizarConsulta(hqlAux, nroPagina, mapa);
	}

	/**
	 * M�todo que verifica y calcula el valor de la siguiente o anterior pagina
	 * a mostra (p�gina de datos, osea cantidad de items por p�gina).
	 * 
	 * @param paginaAux
	 *            Filtro indicando en que direcci�n indico..
	 * @return
	 */
	public int calcularNavegacionPagina(String paginaAux) {
		if (paginaAux.equalsIgnoreCase( Constantes.PAGINA_FIRST )) {
			pagina = 1;
			return pagina;
		} else if (paginaAux.equalsIgnoreCase( Constantes.PAGINA_PREVIOUS )) {
			return (pagina - 1 == 0 ? pagina : --pagina);
		} else if (paginaAux.equalsIgnoreCase(Constantes.PAGINA_NEXT)) {
			return (pagina + 1 <= cantidadPaginas ? ++pagina : pagina);
		} else if (paginaAux.equalsIgnoreCase(Constantes.PAGINA_LAST)) {
			pagina = cantidadPaginas;
			return pagina;
		}
		return pagina;
	}

	/**
	 * M�todo que arma los filtros para la consulta. Esto de acuerdo a lo que el
	 * usuario a seleccionado e ingresado
	 * 
	 * @return Cadena que se adicionar� a la consulta...
	 */
	public String armarFiltro() {
		cadenaBusqueda = cadenaBusqueda.trim().toLowerCase();
		mapa.clear();
		if (cadenaBusqueda.trim().isEmpty()) {			
			return Constantes.VACIO;
		}
		String cadena = Constantes.LIKE + cadenaBusqueda + Constantes.LIKE;
		StringBuffer hqlAux = new StringBuffer( Constantes.VACIO );
		if (filtroBusqueda.equalsIgnoreCase("todos")) {
			Iterator<Entry<String, String>> iterEntry = mapaHqlCampos.entrySet().iterator();
			while (iterEntry.hasNext()) {
				Entry<String, String> entry = iterEntry.next();
				hqlAux.append(entry.getValue() + (iterEntry.hasNext() ? Constantes.OR : Constantes.VACIO));
				mapa.put(entry.getKey(), cadena);
			}
		} else {
			String aux = mapaHqlCampos.get(filtroBusqueda);			
			if (aux != null) {
				mapa.put(filtroBusqueda, cadena);
				hqlAux.append(aux);
			}		
		}		
		return hqlAux.toString();
	}

}
