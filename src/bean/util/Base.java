/**
 * 
 */
package bean.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;

/**
 * @author Marcus
 *
 */
public class Base implements Serializable {
	
	@Inject
	protected SysLog log;
		
	@Inject
	protected Jutil jutil;	
	
	
	
	public void setup( Class clase ){
		//log = Logger.getLogger(clase);
		log.setup( clase , jutil.getUsuarioN() );
	}	
	
}
