/**
 * 
 */
package bean.util;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.apache.log4j.Logger;

/**
 * @author Marcus
 *
 */
public class SysLog implements Serializable {

	private static final long serialVersionUID = 1L;

	private Logger log = null;
	private String usuario = ""; 
	
	/**
	 * M�todo que debe ser iniciado al momento de iniciar el bean.
	 * @param clase
	 * @param usuario
	 */
	public void setup( Class clase, String usuario ){
		log = Logger.getLogger(clase);
		this.usuario = usuario;
	}
	
	public void info( Object msj ){
		log.info( "[" + usuario + "] " + msj);
	}
	
	public void info( Object msj, Throwable t ){
		log.info( "[" + usuario + "] " + msj, t);
	}
	
	public void warn( Object msj ){
		log.warn( "[" + usuario + "] " + msj);
	}
	
	public void warn( Object msj, Throwable t ){
		log.warn( "[" + usuario + "] " + msj, t);
	}
	
	public void error( Object msj ){
		log.error( "[" + usuario + "] " + msj);
	}
	
	public void error( Object msj, Throwable t ){
		log.error( "[" + usuario + "] " + msj, t);
	}	

}