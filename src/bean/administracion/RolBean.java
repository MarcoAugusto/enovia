/**
 * 
 */
package bean.administracion;

import java.util.ArrayList;
import java.util.Iterator;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import persistence.administracion.RolPersistence;

import entidad.Rol;
import entidad.Sucursal;

import bean.bitacora.BitacoraAccion;
import bean.util.BaseBean;
import bean.util.IGeneral;
import bean.util.IPagination;
import bean.util.Jutil;
import bean.util.SysMessage;


/**
 * @author Marcus
 *
 */
@ManagedBean(name="rolbean")
@ViewScoped
public class RolBean extends BaseBean<Rol> implements IGeneral, IPagination {

	@Inject
	private RolPersistence rolpersistence;
	
	@Inject
	private BitacoraAccion bitacoraaccion;
	
	@Inject
	private Jutil jutil;
		
	
	@Override
	@PostConstruct
	public void init() { 
		iniLogs(this.getClass());
		rolpersistence.init();
		cargarLista();
		nuevo();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void cargarLista(){
		try{
			this.list = rolpersistence.getLista();			
		}catch (Exception e) {
			this.list = new ArrayList();
			log.error( "[RolBean.cargarLista]", e );
		}
	}
	
	public void nuevo(){
		entidad = new Rol();
		entidad.setActivo(true);		
		nuevo = true;
		visibleNuevoEditar = false;
		selected = null;
	}
	
	public void nuevaEntidad() {
		nuevo();
		visibleNuevoEditar = true;
	}
	
	public void guardar() {
		String validacion = rolpersistence.validarCampos(entidad, nuevo);
		if (validacion.trim().isEmpty()) {
			if (nuevo) {
				try {
					rolpersistence.save( entidad );					
					log.info("[usuario: " + jutil.getUsuarioN() + ", rol: " + entidad.getNombre() + "] [Guardado correctamente]");					
					SysMessage.info("Guardado correctamente.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Se guardo correctamente el rol: " + entidad.getNombre() + ".");
					nuevo();
					cargarLista();
					visibleNuevoEditar = true;
				} catch (Exception e) {
					log.error("[usuario: " + jutil.getUsuarioN() + ", rol: " + entidad.getNombre() + "] [Fallo al guardar]", e);
					SysMessage.error("Fallo al guardar.");
					bitacoraaccion.accion(getClass().getName(), "Fallo al guardar el rol: " + entidad.getNombre() + ".");
				}
			} else {
				try {
					rolpersistence.update(entidad);					
					log.info("[usuario: " + jutil.getUsuarioN() + ", rol: " + entidad.getNombre() + "] [Guardado correctamente]");
					SysMessage.info("Actualizado correctamente.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Se actualizado correctamente el rol: " + entidad.getNombre() + ".");
					nuevo();
					cargarLista();
					visibleNuevoEditar = false;
				} catch (Exception e) {
					log.error("[usuario: " + jutil.getUsuarioN() + ", rol: " + entidad.getNombre() + "] [Fallo al guardar]", e);
					SysMessage.error("Fallo al guardar.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Fallo al actualizar el rol: " + entidad.getNombre() + ".");
				}
			}
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + ", rol: " + entidad.getNombre() + "] [" + validacion + "]");
			SysMessage.warn(validacion);
		}
	}	

	public void editar() {
		if (selected != null) {
			entidad = selected;			
			nuevo = false;
			visibleNuevoEditar = true;
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
		log.info( "entidad:" + entidad + " nuevo: " + nuevo );
	}

	public void editar(Rol obj) {
		log.info("intentando editar" + obj.getNombre() );
		selected = obj;
		editar();
	}

	public void onRowSelect(org.primefaces.event.SelectEvent event) {
		selected = (Rol)event.getObject();
		log.info( "Rol seleccionado:" + selected.getNombre() );
		editar();
    }
	
	public void eliminar() {
		if (selected != null) {			
			try {
				selected.setActivo( false );
				rolpersistence.update(selected);				
				log.info("[usuario: " + jutil.getUsuarioN() + ", rol: " + selected.getNombre() + "] [Eliminado correctamente]");
				SysMessage.info("Eliminado correctamente.");
				bitacoraaccion.accion(selected.getClass().getName(), "Se elimino correctamente el rol: " + selected.getNombre() + ".");
				cargarLista();
				nuevo();
			} catch (Exception e) {
				log.error("[usuario: " + jutil.getUsuarioN() + ", rol: " + selected.getNombre() + "] [Fallo al eliminar]", e);
				SysMessage.info("Fallo al eliminar.");
				bitacoraaccion.accion(selected.getClass().getName(), "Fallo al eliminar el rol: " + selected.getNombre() + ".");
			}
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
	}
		
	public void eliminar(Rol obj) {
		selected = obj;
		eliminar();
	}
	
	public Rol obtenerRolCode( Integer code ){		
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			Rol rol = (Rol) iter.next();
			if ( rol.getCode()  == code ){
				return rol;
			}
		}
		return null;
	}
	
	public void buscar(){
		list = rolpersistence.getLista( cadenaBusqueda );		
	}
	
	@Override
	public void firstpage() {
		try{
			list = rolpersistence.firstpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "firstpage", e );
		}
	}

	@Override
	public void previouspage() {
		try{
			list = rolpersistence.previouspage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "previouspage", e );
		}
	}

	@Override
	public void nextpage() {
		try{
			list = rolpersistence.nextpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "nextpage", e );
		}
	}

	@Override
	public void lastpage() {
		try{
			list = rolpersistence.lastpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "lastpage", e );
		}
	}

	public int getCantidadRegistros() {
		return rolpersistence.getCantidadRegistros();
	}
	
	public int getCantidadPaginas() {
		return rolpersistence.getCantidadPaginas();
	}
	
	public int getPagina() {
		return rolpersistence.getPagina();
	}
	
	
}
