/**
 * 
 */
package bean.administracion;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;

import org.apache.log4j.Logger;

import entidad.Usuario;

import bean.bitacora.BitacoraAccion;
import bean.util.Jutil;
import bean.util.Parametros;
import bean.util.SysMessage;

import persistence.administracion.LoginPersistence;

/**
 * @author Marcus
 *
 */
@ManagedBean(name="loginbean")
@SessionScoped
public class LoginBean implements Serializable {

	private String login;
	private String password;
	
	public static Logger log = null;
	
	@Inject
	private LoginPersistence loginpersistence;
	
	@Inject 
	private BitacoraAccion bitacoraaccion;
	
	@Inject
	private Jutil jutil;
	
	@PostConstruct
	public void init(){
		log = Logger.getLogger(LoginBean.class);
		loginpersistence.init();
	}
		
	public String verificarLogin(){
		Usuario usuario = loginpersistence.validarPassword( login, password);
		if ( usuario == null ){
			SysMessage.error( "Login o Password incorrecto!!!" );
//			return "login?faces-redirect=true";
			return "login" + Parametros.REDIRECT;
		}	
		SysMessage.info( "Bienvenido: " + login );
		jutil.setUsuarioSession( usuario );
		return "index?faces-redirect=true";
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
