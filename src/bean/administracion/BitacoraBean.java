/**
 * 
 */
package bean.administracion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.apache.log4j.Logger;

import persistence.administracion.BitacoraPersistence;


import entidad.Bitacora;
import bean.util.BaseBean;
import bean.util.BaseMaestro;
import bean.util.IGeneral;
import bean.util.IPagination;
import bean.util.Jutil;

/**
 * @author Marcus
 *
 */
@ManagedBean( name = "bitacorabean" )
@ViewScoped
public class BitacoraBean extends BaseBean<Bitacora> implements IGeneral, IPagination  {

	@Inject
	private Jutil jutil;
	
	@Inject
	private BitacoraPersistence bitacorapersistence;
	
	private String filtroFecha = "";
	private String filtroAccion = "";
	private String filtroIp = "";
	private String filtroMensaje = "";
	private String filtroUsuario = "";
	
	@Override
	@PostConstruct
	public void init(){
		iniLogs( this.getClass() );	
		bitacorapersistence.init();
		cargarLista();	
	}		
		
	@Override
	public void cargarLista(){		
		try {
			this.list = bitacorapersistence.getLista();
		} catch (Exception e) {
			this.list = new ArrayList();
			log.error( "BitacoraBean.cargarlista", e );
		}
	}
	
	public void buscar(){
		Map<String, String> mapaValor = new HashMap<String, String>();
		if ( !filtroFecha.trim().isEmpty() ){
			mapaValor.put( "fecha", filtroFecha );
		}
		if ( !filtroAccion.trim().isEmpty() ){
			mapaValor.put( "accion", filtroAccion );
		}
		if ( !filtroIp.trim().isEmpty() ){
			mapaValor.put( "ip", filtroIp );
		}
		if ( !filtroMensaje.trim().isEmpty() ){
			mapaValor.put( "mensaje", filtroMensaje );
		}
		if ( !filtroUsuario.trim().isEmpty() ){
			mapaValor.put( "usuario", filtroUsuario );
		}
		log.info( "mapaValor: " + mapaValor.size() );
		list = bitacorapersistence.getLista( mapaValor );		
	}
	
	@Override
	public void firstpage() {
		try{
			list = bitacorapersistence.firstpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "firstpage", e );
		}
	}

	@Override
	public void previouspage() {
		try{
			list = bitacorapersistence.previouspage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "previouspage", e );
		}
	}

	@Override
	public void nextpage() {
		try{
			list = bitacorapersistence.nextpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "nextpage", e );
		}
	}

	@Override
	public void lastpage() {
		try{
			list = bitacorapersistence.lastpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "lastpage", e );
		}
	}

	public int getCantidadRegistros() {
		return bitacorapersistence.getCantidadRegistros();
	}
	
	public int getCantidadPaginas() {
		return bitacorapersistence.getCantidadPaginas();
	}
	
	public int getPagina() {
		return bitacorapersistence.getPagina();
	}

	/**
	 * @return the filtroFecha
	 */
	public String getFiltroFecha() {
		return filtroFecha;
	}

	/**
	 * @param filtroFecha the filtroFecha to set
	 */
	public void setFiltroFecha(String filtroFecha) {
		this.filtroFecha = filtroFecha;
	}

	/**
	 * @return the filtroAccion
	 */
	public String getFiltroAccion() {
		return filtroAccion;
	}

	/**
	 * @param filtroAccion the filtroAccion to set
	 */
	public void setFiltroAccion(String filtroAccion) {
		this.filtroAccion = filtroAccion;
	}

	/**
	 * @return the filtroIp
	 */
	public String getFiltroIp() {
		return filtroIp;
	}

	/**
	 * @param filtroIp the filtroIp to set
	 */
	public void setFiltroIp(String filtroIp) {
		this.filtroIp = filtroIp;
	}

	/**
	 * @return the filtroMensaje
	 */
	public String getFiltroMensaje() {
		return filtroMensaje;
	}

	/**
	 * @param filtroMensaje the filtroMensaje to set
	 */
	public void setFiltroMensaje(String filtroMensaje) {
		this.filtroMensaje = filtroMensaje;
	}

	/**
	 * @return the filtroUsuario
	 */
	public String getFiltroUsuario() {
		return filtroUsuario;
	}

	/**
	 * @param filtroUsuario the filtroUsuario to set
	 */
	public void setFiltroUsuario(String filtroUsuario) {
		this.filtroUsuario = filtroUsuario;
	}

	
	
}
