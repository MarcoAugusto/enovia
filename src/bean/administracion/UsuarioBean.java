/**
 * 
 */
package bean.administracion;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import persistence.administracion.UsuarioPersistence;

import entidad.Usuario;

import bean.bitacora.BitacoraAccion;
import bean.inventario.SucursalBean;
import bean.util.BaseBean;
import bean.util.Constantes;
import bean.util.IGeneral;
import bean.util.IPagination;
import bean.util.Jutil;
import bean.util.Parametros;
import bean.util.SysMessage;


/**
 * @author Marcus
 *
 */
@ManagedBean(name="usuariobean")
@ViewScoped
public class UsuarioBean extends BaseBean<Usuario> implements IGeneral, IPagination {

	@Inject
	private UsuarioPersistence usuariopersistence;
	
	@Inject
	private BitacoraAccion bitacoraaccion;

	@Inject
	private SucursalBean sucursalbean;
	
	private String sucursalCode = "";
	
	@Inject
	private RolBean rolbean;
	
	private Integer rolCode;
	
	@Inject
	private Jutil jutil;
		
	private String filtroNombre;
	private String filtroLogin;	
	private String filtroDireccion;
	private boolean filtroIsExterno;
	private String filtroTipoExterno;
	private String filtroEmail;
	private String filtroTelefono;
	
	public List<String> tipsexternolist = null;
	
	
	@Override
	@PostConstruct
	public void init() { 
		iniLogs(this.getClass());
		log.info("iniciliazando usuario");
		usuariopersistence.init();
		initFiltros();
		cargarLista();
		nuevo();
		tipsexternolist = Parametros.USUARIOS_TIPO_EXTERNOS_LIST;
	}

	/**
	 * Inicializa en vacio los campos a utilizar como filtros.
	 */
	public void initFiltros(){
		filtroLogin = filtroDireccion = filtroNombre = filtroTipoExterno = filtroEmail  = Constantes.VACIO;
		filtroIsExterno = false;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void cargarLista(){
		try{
			this.list = usuariopersistence.getLista();			
		}catch (Exception e) {
			this.list = new ArrayList<Usuario>();
			log.error( "[UsuarioBean.cargarLista]", e );
		}
	}
	
	public void nuevo(){
		entidad = new Usuario();
		entidad.setActivo(true);	
		entidad.setIsexterno( false );
		nuevo = true;
		visibleNuevoEditar = false;
		selected = null;
		initFiltros();
		sucursalCode = "";
		rolCode = 0;
		
	}
	
	public void nuevaEntidad() {
		nuevo();
		visibleNuevoEditar = true;
	}
	
	public void guardar() {
		entidad.setRol( rolbean.obtenerRolCode( rolCode ) );
		
		String validacion = usuariopersistence.validarCampos(entidad, nuevo);
		if (validacion.trim().isEmpty()) {
			if (nuevo) {
				try {
					usuariopersistence.save( entidad );
					log.info("[usuario: " + jutil.getUsuarioN() + ", usuario: " + entidad.getCode() + "|" + entidad.getNombre() + "] [Guardado correctamente]");					
					SysMessage.info("Guardado correctamente.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Se guardo correctamente la usuario: " + entidad.getCode() + "|" + entidad.getNombre() + ".");
					nuevo();
					cargarLista();
					visibleNuevoEditar = true;
				} catch (Exception e) {
					log.error("[usuario: " + jutil.getUsuarioN() + ", usuario: " + entidad.getCode() + "|" + entidad.getNombre() + "] [Fallo al guardar]", e);
					SysMessage.error("Fallo al guardar.");					
				}
			} else {
				try {
					usuariopersistence.update(entidad);					
					log.info("[usuario: " + jutil.getUsuarioN() + ", usuario: " + entidad.getCode() + "|" + entidad.getNombre() + "] [Guardado correctamente]");
					SysMessage.info("Actualizado correctamente.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Se actualizado correctamente la usuario: " + entidad.getCode() + "|" + entidad.getNombre() + ".");
					nuevo();
					cargarLista();
					visibleNuevoEditar = false;
				} catch (Exception e) {
					log.error("[usuario: " + jutil.getUsuarioN() + ", usuario: " + entidad.getCode() + "|" + entidad.getNombre() + "] [Fallo al guardar]", e);
					SysMessage.error("Fallo al guardar.");					
				}
			}
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + ", usuario:" + "] [" + validacion + "]");
			SysMessage.warn(validacion);
		}
	}	

	public void editar() {
		if (selected != null) {
			entidad = selected;			
			nuevo = false;
			visibleNuevoEditar = true;
			sucursalCode = entidad.getSucursal().getCode();
			rolCode = entidad.getRol().getCode();
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
		log.info( "entidad:" + entidad + " nuevo: " + nuevo );
	}

	public void editar(Usuario obj) {
		log.info("intentando editar" + obj.getNombre() );
		selected = obj;
		editar();
	}

	public void onRowSelect(org.primefaces.event.SelectEvent event) {
		selected = (Usuario)event.getObject();
		log.info( "Usuario seleccionado:" + selected.getNombre() );
		editar();
    }
	
	public void eliminar() {
		if (selected != null) {			
			try {
				selected.setActivo( false );
				usuariopersistence.update(selected);				
				log.info("[usuario: " + jutil.getUsuarioN() + ", usuario: " + selected.getCode() + "|" + selected.getNombre() + "] [Eliminado correctamente]");
				SysMessage.info("Eliminado correctamente.");
				bitacoraaccion.accion(selected.getClass().getName(), "Se elimino correctamente la usuario: " + selected.getCode() + "|" + selected.getNombre() + ".");
				cargarLista();
				nuevo();
			} catch (Exception e) {
				log.error("[usuario: " + jutil.getUsuarioN() + ", usuario: " + selected.getCode() + "|" + selected.getNombre() + "] [Fallo al eliminar]", e);
				SysMessage.info("Fallo al eliminar.");
				bitacoraaccion.accion(selected.getClass().getName(), "Fallo al eliminar la usuario: " + selected.getCode() + "|" + selected.getNombre() + ".");
			}
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
	}
		
	public void eliminar(Usuario obj) {
		selected = obj;
		eliminar();
	}
	
	public void buscar(){
		Map<String, String> mapaValor = new HashMap<String, String>();		
		if ( !filtroNombre.trim().isEmpty() ){
			mapaValor.put( "nombre", filtroNombre );
		}
		if ( !filtroLogin.trim().isEmpty() ){
			mapaValor.put( "login", filtroLogin );
		}
		if ( !filtroDireccion.trim().isEmpty() ){
			mapaValor.put( "direccion", filtroDireccion );
		}
		if ( filtroIsExterno == true ){
			mapaValor.put( "isexterno", "true" );
		}				
		if ( !filtroTipoExterno.trim().isEmpty() ){
			mapaValor.put( "tipoexterno", filtroTipoExterno );
		}
		if ( !filtroEmail.trim().isEmpty() ){
			mapaValor.put( "email", filtroEmail );
		}
		list = usuariopersistence.getLista( mapaValor );		
	}
	
	@Override
	public void firstpage() {
		try{
			list = usuariopersistence.firstpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "firstpage", e );
		}
	}

	@Override
	public void previouspage() {
		try{
			list = usuariopersistence.previouspage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "previouspage", e );
		}
	}

	@Override
	public void nextpage() {
		try{
			list = usuariopersistence.nextpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "nextpage", e );
		}
	}

	@Override
	public void lastpage() {
		try{
			list = usuariopersistence.lastpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "lastpage", e );
		}
	}

	public int getCantidadRegistros() {
		return usuariopersistence.getCantidadRegistros();
	}
	
	public int getCantidadPaginas() {
		return usuariopersistence.getCantidadPaginas();
	}
	
	public int getPagina() {
		return usuariopersistence.getPagina();
	}

	/**
	 * @return the filtroNombre
	 */
	public String getFiltroNombre() {
		return filtroNombre;
	}

	/**
	 * @param filtroNombre the filtroNombre to set
	 */
	public void setFiltroNombre(String filtroNombre) {
		this.filtroNombre = filtroNombre;
	}

	/**
	 * @return the filtroLogin
	 */
	public String getFiltroLogin() {
		return filtroLogin;
	}

	/**
	 * @param filtroLogin the filtroLogin to set
	 */
	public void setFiltroLogin(String filtroLogin) {
		this.filtroLogin = filtroLogin;
	}

	/**
	 * @return the filtroDireccion
	 */
	public String getFiltroDireccion() {
		return filtroDireccion;
	}

	/**
	 * @param filtroDireccion the filtroDireccion to set
	 */
	public void setFiltroDireccion(String filtroDireccion) {
		this.filtroDireccion = filtroDireccion;
	}

	/**
	 * @return the filtroIsExterno
	 */
	public boolean isFiltroIsExterno() {
		return filtroIsExterno;
	}

	/**
	 * @param filtroIsExterno the filtroIsExterno to set
	 */
	public void setFiltroIsExterno(boolean filtroIsExterno) {
		this.filtroIsExterno = filtroIsExterno;
	}

	/**
	 * @return the filtroTipoExterno
	 */
	public String getFiltroTipoExterno() {
		return filtroTipoExterno;
	}

	/**
	 * @param filtroTipoExterno the filtroTipoExterno to set
	 */
	public void setFiltroTipoExterno(String filtroTipoExterno) {
		this.filtroTipoExterno = filtroTipoExterno;
	}

	/**
	 * @return the filtroEmail
	 */
	public String getFiltroEmail() {
		return filtroEmail;
	}

	/**
	 * @param filtroEmail the filtroEmail to set
	 */
	public void setFiltroEmail(String filtroEmail) {
		this.filtroEmail = filtroEmail;
	}

	/**
	 * @return the filtroTelefono
	 */
	public String getFiltroTelefono() {
		return filtroTelefono;
	}

	/**
	 * @param filtroTelefono the filtroTelefono to set
	 */
	public void setFiltroTelefono(String filtroTelefono) {
		this.filtroTelefono = filtroTelefono;
	}

	/**
	 * @return the tipsexternolist
	 */
	public List<String> getTipsexternolist() {
		return tipsexternolist;
	}

	/**
	 * @param tipsexternolist the tipsexternolist to set
	 */
	public void setTipsexternolist(List<String> tipsexternolist) {
		this.tipsexternolist = tipsexternolist;
	}

	/**
	 * @return the sucursalCode
	 */
	public String getSucursalCode() {
		return sucursalCode;
	}

	/**
	 * @param sucursalCode the sucursalCode to set
	 */
	public void setSucursalCode(String sucursalCode) {
		this.sucursalCode = sucursalCode;
	}

	/**
	 * @return the rolCode
	 */
	public Integer getRolCode() {
		return rolCode;
	}

	/**
	 * @param rolCode the rolCode to set
	 */
	public void setRolCode(Integer rolCode) {
		this.rolCode = rolCode;
	}

		
}
