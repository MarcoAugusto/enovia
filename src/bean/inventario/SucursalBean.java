/**
 * 
 */
package bean.inventario;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import persistence.inventario.SucursalPersistence;

import entidad.Sucursal;

import bean.bitacora.BitacoraAccion;
import bean.util.BaseBean;
import bean.util.Constantes;
import bean.util.IGeneral;
import bean.util.IPagination;
import bean.util.Jutil;
import bean.util.SysMessage;


/**
 * @author Marcus
 *
 */
@ManagedBean(name="sucursalbean")
@ViewScoped
public class SucursalBean extends BaseBean<Sucursal> implements IGeneral, IPagination {

	@Inject
	private SucursalPersistence sucursalpersistence;
	
	@Inject
	private BitacoraAccion bitacoraaccion;
	
	@Inject
	private Jutil jutil;
	
	private String filtroCode;
	private String filtroNombre;
	private String filtroDireccion;
	private String filtroResponsable;
	
	
	@Override
	@PostConstruct
	public void init() { 
		iniLogs(this.getClass());
		sucursalpersistence.init();
		initFiltros();
		cargarLista();
		nuevo();
	}

	/**
	 * Inicializa en vacio los campos a utilizar como filtros.
	 */
	public void initFiltros(){
		filtroCode = filtroDireccion = filtroNombre = filtroResponsable = Constantes.VACIO;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void cargarLista(){
		try{
			this.list = sucursalpersistence.getLista();			
		}catch (Exception e) {
			this.list = new ArrayList();
			log.error( "[SucursalBean.cargarLista]", e );
		}
	}
	
	public void nuevo(){
		entidad = new Sucursal();
		entidad.setActivo(true);		
		nuevo = true;
		visibleNuevoEditar = false;
		selected = null;
		initFiltros();
	}
	
	public void nuevaEntidad() {
		nuevo();
		visibleNuevoEditar = true;
	}
	
	public void guardar() {
		String validacion = sucursalpersistence.validarCampos(entidad, nuevo);
		if (validacion.trim().isEmpty()) {
			if (nuevo) {
				try {
					sucursalpersistence.save( entidad );					
					log.info("[usuario: " + jutil.getUsuarioN() + ", sucursal: " + entidad.getCode() + "|" + entidad.getNombre() + "] [Guardado correctamente]");					
					SysMessage.info("Guardado correctamente.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Se guardo correctamente la sucursal: " + entidad.getCode() + "|" + entidad.getNombre() + ".");
					nuevo();
					cargarLista();
					visibleNuevoEditar = true;
				} catch (Exception e) {
					log.error("[usuario: " + jutil.getUsuarioN() + ", sucursal: " + entidad.getCode() + "|" + entidad.getNombre() + "] [Fallo al guardar]", e);
					SysMessage.error("Fallo al guardar.");					
				}
			} else {
				try {
					sucursalpersistence.update(entidad);					
					log.info("[usuario: " + jutil.getUsuarioN() + ", sucursal: " + entidad.getCode() + "|" + entidad.getNombre() + "] [Guardado correctamente]");
					SysMessage.info("Actualizado correctamente.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Se actualizado correctamente la sucursal: " + entidad.getCode() + "|" + entidad.getNombre() + ".");
					nuevo();
					cargarLista();
					visibleNuevoEditar = false;
				} catch (Exception e) {
					log.error("[usuario: " + jutil.getUsuarioN() + ", sucursal: " + entidad.getCode() + "|" + entidad.getNombre() + "] [Fallo al guardar]", e);
					SysMessage.error("Fallo al guardar.");					
				}
			}
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + ", sucursal:" + "] [" + validacion + "]");
			SysMessage.warn(validacion);
		}
	}	

	public void editar() {
		if (selected != null) {
			entidad = selected;			
			nuevo = false;
			visibleNuevoEditar = true;
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
		log.info( "entidad:" + entidad + " nuevo: " + nuevo );
	}

	public void editar(Sucursal obj) {
		log.info("intentando editar" + obj.getNombre() );
		selected = obj;
		editar();
	}

	public void onRowSelect(org.primefaces.event.SelectEvent event) {
		selected = (Sucursal)event.getObject();
		log.info( "Sucursal seleccionado:" + selected.getNombre() );
		editar();
    }
	
	public void eliminar() {
		if (selected != null) {			
			try {
				selected.setActivo( false );
				sucursalpersistence.update(selected);				
				log.info("[usuario: " + jutil.getUsuarioN() + ", sucursal: " + selected.getCode() + "|" + selected.getNombre() + "] [Eliminado correctamente]");
				SysMessage.info("Eliminado correctamente.");
				bitacoraaccion.accion(selected.getClass().getName(), "Se elimino correctamente la sucursal: " + selected.getCode() + "|" + selected.getNombre() + ".");
				cargarLista();
				nuevo();
			} catch (Exception e) {
				log.error("[usuario: " + jutil.getUsuarioN() + ", sucursal: " + selected.getCode() + "|" + selected.getNombre() + "] [Fallo al eliminar]", e);
				SysMessage.info("Fallo al eliminar.");
				bitacoraaccion.accion(selected.getClass().getName(), "Fallo al eliminar la sucursal: " + selected.getCode() + "|" + selected.getNombre() + ".");
			}
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
	}
		
	public void eliminar(Sucursal obj) {
		selected = obj;
		eliminar();
	}
	
	public void buscar(){
		Map<String, String> mapaValor = new HashMap<String, String>();
		if ( !filtroCode.trim().isEmpty() ){
			mapaValor.put( "code", filtroCode );
		}
		if ( !filtroNombre.trim().isEmpty() ){
			mapaValor.put( "nombre", filtroNombre );
		}
		if ( !filtroDireccion.trim().isEmpty() ){
			mapaValor.put( "direccion", filtroDireccion );
		}
		if ( !filtroResponsable.trim().isEmpty() ){
			mapaValor.put( "responsable", filtroResponsable );
		}				
		list = sucursalpersistence.getLista( mapaValor );		
	}
	
	/**
	 * Obtiene la sucursal apartir del codigo.
	 * Nota: solo recorre la lista cargada previamente.
	 * @param code
	 * @return
	 */
	public Sucursal obtenerSecursalCode( String code ){
		if( code == null || code.trim().isEmpty() ){
			return null;
		}				
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			Sucursal sucursal = (Sucursal) iter.next();
			if ( sucursal.getCode().trim().equalsIgnoreCase( code ) ){
				return sucursal;
			}
		}
		return null;
	}
	
	@Override
	public void firstpage() {
		try{
			list = sucursalpersistence.firstpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "firstpage", e );
		}
	}

	@Override
	public void previouspage() {
		try{
			list = sucursalpersistence.previouspage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "previouspage", e );
		}
	}

	@Override
	public void nextpage() {
		try{
			list = sucursalpersistence.nextpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "nextpage", e );
		}
	}

	@Override
	public void lastpage() {
		try{
			list = sucursalpersistence.lastpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "lastpage", e );
		}
	}

	public int getCantidadRegistros() {
		return sucursalpersistence.getCantidadRegistros();
	}
	
	public int getCantidadPaginas() {
		return sucursalpersistence.getCantidadPaginas();
	}
	
	public int getPagina() {
		return sucursalpersistence.getPagina();
	}

	/**
	 * @return the filtroNombre
	 */
	public String getFiltroNombre() {
		return filtroNombre;
	}

	/**
	 * @param filtroNombre the filtroNombre to set
	 */
	public void setFiltroNombre(String filtroNombre) {
		this.filtroNombre = filtroNombre;
	}

	/**
	 * @return the filtroDireccion
	 */
	public String getFiltroDireccion() {
		return filtroDireccion;
	}

	/**
	 * @param filtroDireccion the filtroDireccion to set
	 */
	public void setFiltroDireccion(String filtroDireccion) {
		this.filtroDireccion = filtroDireccion;
	}

	/**
	 * @return the filtroResponsable
	 */
	public String getFiltroResponsable() {
		return filtroResponsable;
	}

	/**
	 * @param filtroResponsable the filtroResponsable to set
	 */
	public void setFiltroResponsable(String filtroResponsable) {
		this.filtroResponsable = filtroResponsable;
	}

	/**
	 * @return the filtroCode
	 */
	public String getFiltroCode() {
		return filtroCode;
	}

	/**
	 * @param filtroCode the filtroCode to set
	 */
	public void setFiltroCode(String filtroCode) {
		this.filtroCode = filtroCode;
	}
	
	
}
