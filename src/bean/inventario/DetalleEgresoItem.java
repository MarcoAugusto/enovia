/**
 * 
 */
package bean.inventario;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import bean.util.SysMessage;

import entidad.Baseproducto;
import entidad.Producto;

/**
 * @author penama
 *
 */
public class DetalleEgresoItem implements Serializable {

	private Integer codeDetalle;
	private Integer estiloCode;
	private Integer marcaCode;
	private Integer prendaCode;
	private Baseproducto baseProducto;
	private Producto producto;
	private BigDecimal precioTotal = new BigDecimal(0);
	private List<DetalleEgresoProductoItem> detalleIPIList;
	
	public void updatePrecioTotal(){
		precioTotal = new BigDecimal(0);
		for (Iterator iter = detalleIPIList.iterator(); iter.hasNext();) {
			DetalleEgresoProductoItem detalleIPI = (DetalleEgresoProductoItem) iter.next();
			precioTotal = precioTotal.add( detalleIPI.getPrecio() );			
		}
	}
	
	/**
	 * Adiciona un item...
	 */
	public void addDetalleEgresoProductoItem(){
		if ( baseProducto == null ){
			SysMessage.info( "Debe selecionar un base producto!!!" );
			return;
		}
		if ( detalleIPIList == null || detalleIPIList.isEmpty() ){
			detalleIPIList = new ArrayList<DetalleEgresoProductoItem>();
		}
		DetalleEgresoProductoItem detalleIPI = new DetalleEgresoProductoItem();
		detalleIPI.setPrecio( baseProducto.getPrecio() );
		detalleIPIList.add( detalleIPI );
		updatePrecioTotal();
	}
	
	public void onQuitarItem( DetalleEgresoProductoItem detalleEgresoProductoItem ){
		detalleIPIList.remove( detalleEgresoProductoItem );
	}
	
	/**
	 * @return the baseProducto
	 */
	public Baseproducto getBaseProducto() {
		return baseProducto;
	}
	/**
	 * @param baseProducto the baseProducto to set
	 */
	public void setBaseProducto(Baseproducto baseProducto) {
		this.baseProducto = baseProducto;		
	}
	/**
	 * @return the producto
	 */
	public Producto getProducto() {
		return producto;
	}
	/**
	 * @param producto the producto to set
	 */
	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	
	/**
	 * @return the codeDetalle
	 */
	public Integer getCodeDetalle() {
		return codeDetalle;
	}
	/**
	 * @param codeDetalle the codeDetalle to set
	 */
	public void setCodeDetalle(Integer codeDetalle) {
		this.codeDetalle = codeDetalle;
	}
	/**
	 * @return the estiloCode
	 */
	public Integer getEstiloCode() {
		return estiloCode;
	}
	/**
	 * @param estiloCode the estiloCode to set
	 */
	public void setEstiloCode(Integer estiloCode) {
		this.estiloCode = estiloCode;
	}
	/**
	 * @return the marcaCode
	 */
	public Integer getMarcaCode() {
		return marcaCode;
	}
	/**
	 * @param marcaCode the marcaCode to set
	 */
	public void setMarcaCode(Integer marcaCode) {
		this.marcaCode = marcaCode;
	}
	/**
	 * @return the prendaCode
	 */
	public Integer getPrendaCode() {
		return prendaCode;
	}
	/**
	 * @param prendaCode the prendaCode to set
	 */
	public void setPrendaCode(Integer prendaCode) {
		this.prendaCode = prendaCode;
	}

	/**
	 * @return the precioTotal
	 */
	public BigDecimal getPrecioTotal() {
		return precioTotal;
	}

	/**
	 * @param precioTotal the precioTotal to set
	 */
	public void setPrecioTotal(BigDecimal precioTotal) {
		this.precioTotal = precioTotal;
	}

	/**
	 * @return the detalleIPIList
	 */
	public List<DetalleEgresoProductoItem> getDetalleIPIList() {
		return detalleIPIList;
	}

	/**
	 * @param detalleIPIList the detalleIPIList to set
	 */
	public void setDetalleIPIList(List<DetalleEgresoProductoItem> detalleIPIList) {
		this.detalleIPIList = detalleIPIList;
	}
	
	
}
