/**
 * 
 */
package bean.inventario;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import persistence.inventario.AlmacenPersistence;
import persistence.inventario.EgresoPersistence;
import entidad.Almacen;
import entidad.Detalleegreso;
import entidad.Egreso;
import entidad.Producto;
import entidad.Usuario;

import bean.bitacora.BitacoraAccion;
import bean.util.BaseBean;
import bean.util.Constantes;
import bean.util.Parametros;
import bean.util.SysMessage;

/**
 * @author penama
 * 
 */
@ManagedBean(name = "egresobean")
@ViewScoped
public class EgresoBean extends BaseBean<Producto> implements Serializable {

	private List<String> tipoEgresolist = null;

	private String usuario = "";
	private String fecha = "";
	private Integer codeAlmacen;
	private Almacen almacen;
	private String tipoEgreso = "";

	private Egreso entidad;

	private BigDecimal montoTotal;

	@Inject
	private EgresoPersistence egresopersistence;

	@Inject
	private AlmacenPersistence almacenpersistence;

	@Inject
	private BitacoraAccion bitacoraaccion;

	@PostConstruct
	public void init() {
		iniLogs(this.getClass());
		// baseproductopersistence.init();
		egresopersistence.init();
		tipoEgresolist = Parametros.EGRESO_TIPO_INGRESO_LIST;
		// prendaList = baseproductopersistence.obtenerPrendasAsociadas();
		iniNuevo();
	}

	public void iniEditar() {

	}

	/**
	 * M�todo para cuando se tenga que realizar un nuevo egreso.
	 */
	public void iniNuevo() {
		montoTotal = new BigDecimal(0);
		fecha = jutil.getFecha(Parametros.FECHA_DDMMYYYY);
		usuario = jutil.getUsuarioN();
		// productopersistence.setup( this.getClass() );
	}

	/**
	 * Calcula el montoTotal del egreso
	 */
	public void onUpdateMontoTotal() {
		montoTotal = new BigDecimal(0);
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			Producto producto = (Producto) iter.next();
			// luego validar realizar el calculo del porcentaje y precio promo.
			montoTotal = montoTotal.add(producto.getBaseproducto().getPrecio());
		}
	}

	/**
	 * Adiciona un item a la lista, adem�s de actualizar los precios.
	 * 
	 * @param detalleEgresoItem
	 */
	public void onAddDetalleEgresoProductoItem(
			DetalleEgresoItem detalleEgresoItem) {
		detalleEgresoItem.addDetalleEgresoProductoItem();
		onUpdateMontoTotal();
	}
	
	/**
	 * Al realizar el cambio del almacen, la lista debe limpiarse, el egreso solo se hace por almacen.
	 */
	public void onCambioAlmacen(){
		this.list.clear();
		this.montoTotal = new BigDecimal(0);
	}

	/**
	 * Valida que el m�todo no se encuentre duplicado en la lista.
	 * 
	 * @param baseProducto
	 * @return
	 */
	private boolean ExisteDuplicado(Producto producto) throws Exception {
		if (producto == null) {
			throw new Exception("Producto nulo");
		}

		for (Iterator iter = list.iterator(); iter.hasNext();) {
			Producto productoItem = (Producto) iter.next();
			if (productoItem.getCode().equals(producto.getCode())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Quita un registro del listado...
	 */
	public void onQuitarItem(Producto producto) {
		list.remove(producto);
	}

	/**
	 * Valida los datos del egreso.
	 * 
	 * @return
	 */
	private String onValidarEgreso() {
		if (codeAlmacen == null || codeAlmacen == 0) {
			return "Debe seleccionar el almacen";
		}
		if (tipoEgreso == null || tipoEgreso.isEmpty()) {
			return "Debe seleccionar un tipo de egreso.";
		}
		if (list.isEmpty()) {
			return "Debe seleccionar al menos un producto...";
		}

		return Constantes.VACIO;
	}

	/**
	 * Guardar el egreso.
	 */
	public String save() {
		String redirect = "";
		String textValidar = onValidarEgreso();
		if (!textValidar.equals(Constantes.VACIO)) {
			SysMessage.warn(textValidar);
			return redirect;
		}

		List<Detalleegreso> detalleEgresoList = null;
		try {
			// preparar el egreso.
			prepararEgreso();
			// preparar el detalle.
			detalleEgresoList = prepararDetalleEgreso();
			// persistir egreso y su detalle.
			egresopersistence.save(entidad, detalleEgresoList);
			bitacoraaccion.accion(entidad.getClass().getName(),
					"Se guardo correctamente el egreso: " + entidad.getCode()
							+ ".");
			SysMessage.info("Egreso guardado correctamente.");
			log.info("redireccionado");
			jutil.addObjectParam(entidad);
			redirect = "EgresoView.xhtml" + Parametros.REDIRECT;
		} catch (Exception e) {
			log.error("[Fallo al guardar]", e);
			SysMessage.error("Fallo al guardar.");
		}
		return redirect;
	}

	/**
	 * completa los datos del egreso antes de ser persistido.
	 * 
	 * @throws Exception
	 */
	private void prepararEgreso() throws Exception {
		entidad = new Egreso();
		entidad.setActivo(true);
		entidad.setTotaldescuento(new BigDecimal(0));
		entidad.setAlmacen(almacenpersistence.findAlmacenCode(codeAlmacen));
		entidad.setFecha(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		entidad.setTipoegreso(tipoEgreso);
		entidad.setTotal(montoTotal);
		entidad.setTotaldescuento( new BigDecimal(0) );
		entidad.setUsuario((Usuario) jutil.getUsuarioO());
	}

	/**
	 * Prepar� el detalle del egreso.
	 * 
	 * @return
	 * @throws Exception
	 */
	private List<Detalleegreso> prepararDetalleEgreso() throws Exception {
		List<Detalleegreso> detalleEgresoList = new ArrayList<Detalleegreso>();
		Detalleegreso detalleEgreso = null;
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			Producto producto = (Producto) iter.next();
			detalleEgreso = new Detalleegreso();
			detalleEgreso.setActivo(true);
			//cambiando estado del producto.
			producto.setEstado( Constantes.ESTADO_EGRESADO );
			detalleEgreso.setProducto(producto);
			detalleEgreso.setCantidad(1);
			detalleEgreso.setDescuento( new BigDecimal(0) );
			// validar luego el calculo del costo.
			detalleEgreso.setPrecio(producto.getBaseproducto().getPrecio());
			detalleEgresoList.add(detalleEgreso);
		}
		return detalleEgresoList;
	}

	/**
	 * Adiciona un producto a la lista.
	 * 
	 * @param producto
	 */
	public void adicionarProducto(Producto producto) {
		try {
			if (ExisteDuplicado(producto)) {
				SysMessage.warn("Producto " + producto.getCode() + " duplicado!!!");
				return;
			}
		} catch (Exception e) {
			SysMessage.error("Debe seleccionar un producto");
			log.error(e);
		}
		montoTotal = montoTotal.add( producto.getBaseproducto().getPrecio() );
		list.add(producto);
	}

	/**
	 * Adiciona un listado de productos a la lista.
	 * 
	 * @param productoList
	 */
	public void adicionarProductos(List<Producto> productoList) {
		if (productoList.isEmpty()) {
			SysMessage.warn("Lista de productos esta vacia.");
			return;
		}		
		for (Iterator iter = productoList.iterator(); iter.hasNext();) {
			Producto producto = (Producto) iter.next();
			try {
				if (ExisteDuplicado(producto)) {
					SysMessage.info("Producto " + producto.getCode() + " duplicado!!!");
					continue;
				}
			} catch (Exception e) {
				SysMessage.error(e.getMessage());
			}
			montoTotal = montoTotal.add( producto.getBaseproducto().getPrecio() );
			list.add(producto);
		}
	}

	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario
	 *            the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * @param fecha
	 *            the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the codeAlmacen
	 */
	public Integer getCodeAlmacen() {
		return codeAlmacen;
	}

	/**
	 * @param codeAlmacen
	 *            the codeAlmacen to set
	 */
	public void setCodeAlmacen(Integer codeAlmacen) {
		this.codeAlmacen = codeAlmacen;	
		this.almacen = almacenpersistence.findAlmacenCode( this.codeAlmacen );
	}

	/**
	 * @return the tipoEgreso
	 */
	public String getTipoEgreso() {
		return tipoEgreso;
	}

	/**
	 * @param tipoEgreso
	 *            the tipoEgreso to set
	 */
	public void setTipoEgreso(String tipoEgreso) {
		this.tipoEgreso = tipoEgreso;
	}

	/**
	 * @return the tipoEgresolist
	 */
	public List<String> getTipoEgresolist() {
		return tipoEgresolist;
	}

	/**
	 * @param tipoEgresolist
	 *            the tipoEgresolist to set
	 */
	public void setTipoEgresolist(List<String> tipoEgresolist) {
		this.tipoEgresolist = tipoEgresolist;
	}

	/**
	 * @return the montoTotal
	 */
	public BigDecimal getMontoTotal() {
		return montoTotal;
	}

	/**
	 * @param montoTotal
	 *            the montoTotal to set
	 */
	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}

	/**
	 * @return the almacen
	 */
	public Almacen getAlmacen() {
		return almacen;
	}

	/**
	 * @param almacen the almacen to set
	 */
	public void setAlmacen(Almacen almacen) {
		this.almacen = almacen;
	}

}
