/**
 * 
 */
package bean.inventario;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import persistence.inventario.IngresoListPersistence;

import entidad.Ingreso;

import bean.bitacora.BitacoraAccion;
import bean.util.BaseBean;
import bean.util.Constantes;
import bean.util.IGeneral;
import bean.util.IPagination;
import bean.util.Jutil;
import bean.util.SysMessage;


/**
 * @author Marcus
 *
 */
@ManagedBean(name="ingresolistbean" )
@ViewScoped
public class IngresoListBean extends BaseBean<Ingreso> implements IGeneral, IPagination {

	@Inject
	private IngresoListPersistence ingresolistpersistence;
	
	@Inject
	private BitacoraAccion bitacoraaccion;
	
	@Inject
	private AlmacenBean almacenbean;
	
	@Inject
	private Jutil jutil;	
	
	private String filtroCode;
	private String filtroTipoingreso;
	private String filtroFechaIni;
	private String filtroFechaFin;
	private String filtroAlmacen;
	private String filtroUsuario;
	
	
	@Override
	@PostConstruct
	public void init() { 
		iniLogs(this.getClass());
		ingresolistpersistence.init();
		initFiltros();
		cargarLista();		
		almacenbean.init();
	}

	/**
	 * Inicializa en vacio los campos a utilizar como filtros.
	 */
	public void initFiltros(){
		filtroCode = filtroTipoingreso = filtroFechaIni = filtroFechaFin = filtroUsuario = filtroAlmacen = Constantes.VACIO;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void cargarLista(){
		try{
			this.list = ingresolistpersistence.getLista();			
		}catch (Exception e) {
			this.list = new ArrayList();
			log.error( "[IngresoBean.cargarLista]", e );
		}
	}		
		
	public void editar() {
		if (selected != null) {
			entidad = selected;			
			nuevo = false;
			visibleNuevoEditar = true;
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
		log.info( "entidad:" + entidad + " nuevo: " + nuevo );
	}

	public void editar(Ingreso obj) {
		log.info("intentando editar" + obj.getCode() );
		selected = obj;
		editar();
	}

	public void onRowSelect(org.primefaces.event.SelectEvent event) {
		selected = (Ingreso)event.getObject();
		log.info( "Ingreso seleccionado:" + selected.getCode() );
		editar();
    }	
	
	public void buscar(){
		Map<String, String> mapaValor = new HashMap<String, String>();
		if ( !filtroCode.trim().isEmpty() ){
			mapaValor.put( "code", filtroCode );
		}
		if ( !filtroTipoingreso.trim().isEmpty() ){
			mapaValor.put( "tipoingreso", filtroTipoingreso );
		}
		if ( !filtroFechaIni.trim().isEmpty() ){
			mapaValor.put( "fechaini", filtroFechaIni );
		}
		if ( !filtroFechaFin.trim().isEmpty() ){
			mapaValor.put( "fechafin", filtroFechaFin );
		}
		if ( !filtroAlmacen.trim().isEmpty() ){
			mapaValor.put( "almacen", filtroAlmacen );
		}
		if ( !filtroUsuario.trim().isEmpty() ){
			mapaValor.put( "usuario", filtroUsuario );
		}		
		list = ingresolistpersistence.getLista( mapaValor );		
	}
	
	/**
	 * Exporta el objeto Ingreso seleccionado.
	 */
	public void onVerIngreso( Ingreso ingreso ){
		log.info( "Ejectando: ingresoView = " + ingreso + " [" + ingreso.getCode() + "]" );		
//		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put( "ingresoView", ingreso );
		jutil.addObjectParam( ingreso );
		log.info( "objeto Ejectado" );
	}
	
	@Override
	public void firstpage() {
		try{
			list = ingresolistpersistence.firstpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "firstpage", e );
		}
	}

	@Override
	public void previouspage() {
		try{
			list = ingresolistpersistence.previouspage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "previouspage", e );
		}
	}

	@Override
	public void nextpage() {
		try{
			list = ingresolistpersistence.nextpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "nextpage", e );
		}
	}

	@Override
	public void lastpage() {
		try{
			list = ingresolistpersistence.lastpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "lastpage", e );
		}
	}

	public int getCantidadRegistros() {
		return ingresolistpersistence.getCantidadRegistros();
	}
	
	public int getCantidadPaginas() {
		return ingresolistpersistence.getCantidadPaginas();
	}
	
	public int getPagina() {
		return ingresolistpersistence.getPagina();
	}

	
	/**
	 * @return the filtroCode
	 */
	public String getFiltroCode() {
		return filtroCode;
	}

	/**
	 * @param filtroCode the filtroCode to set
	 */
	public void setFiltroCode(String filtroCode) {
		this.filtroCode = filtroCode;
	}	

	public String getFiltroUsuario() {
		return filtroUsuario;
	}

	public void setFiltroUsuario(String filtroUsuario) {
		this.filtroUsuario = filtroUsuario;
	}

	/**
	 * @return the filtroTipoingreso
	 */
	public String getFiltroTipoingreso() {
		return filtroTipoingreso;
	}

	/**
	 * @param filtroTipoingreso the filtroTipoingreso to set
	 */
	public void setFiltroTipoingreso(String filtroTipoingreso) {
		this.filtroTipoingreso = filtroTipoingreso;
	}

	/**
	 * @return the filtroFechaIni
	 */
	public String getFiltroFechaIni() {
		return filtroFechaIni;
	}

	/**
	 * @param filtroFechaIni the filtroFechaIni to set
	 */
	public void setFiltroFechaIni(String filtroFechaIni) {
		this.filtroFechaIni = filtroFechaIni;
	}

	/**
	 * @return the filtroFechaFin
	 */
	public String getFiltroFechaFin() {
		return filtroFechaFin;
	}

	/**
	 * @param filtroFechaFin the filtroFechaFin to set
	 */
	public void setFiltroFechaFin(String filtroFechaFin) {
		this.filtroFechaFin = filtroFechaFin;
	}

	/**
	 * @return the filtroAlmacen
	 */
	public String getFiltroAlmacen() {
		return filtroAlmacen;
	}

	/**
	 * @param filtroAlmacen the filtroAlmacen to set
	 */
	public void setFiltroAlmacen(String filtroAlmacen) {
		this.filtroAlmacen = filtroAlmacen;
	}

	
	
}
