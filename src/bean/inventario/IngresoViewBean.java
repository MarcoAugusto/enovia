/**
 * 
 */
package bean.inventario;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import persistence.inventario.IngresoListPersistence;
import persistence.inventario.IngresoViewPersistence;

import entidad.Detalleingreso;
import entidad.Ingreso;

import bean.bitacora.BitacoraAccion;
import bean.util.Base;
import bean.util.Constantes;
import bean.util.IGeneral;
import bean.util.IPagination;
import bean.util.Jutil;
import bean.util.SysMessage;

/**
 * @author Marcus
 * 
 */
@ManagedBean(name = "ingresoviewbean")
@ViewScoped
public class IngresoViewBean extends Base {

	@Inject
	private BitacoraAccion bitacoraaccion;

	@Inject
	private Jutil jutil;

	@Inject
	private IngresoViewPersistence ingresoviewpersistence;

	private Ingreso ingresoView = null;

	private List<Detalleingreso> detalleingresoList = null;

	@PostConstruct
	public void init() {
		setup(this.getClass());
		onVerIngreso();
	}

	/**
	 * Obtiene el ingreso a ver.
	 */
	public void onVerIngreso() {
		log.info("Obteniendo ingreso a ver...");
		// Object obj =
		// FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(
		// "ingresoView" );
		Object obj = jutil.getObjectParam();
		log.info("obteniendo objeto injectado: " + obj);
		if (obj == null) {
			try {
				jutil.redirect("IngresoList.xhtml");
			} catch (Exception e) {
				log.error("Redirect fallido", e);
			}
		}
		ingresoView = (Ingreso) obj;
		log.info("Ingreso a ver: " + ingresoView.getCode() + " - Obteniendo detalle");
		try{
			detalleingresoList = ingresoviewpersistence.getDetalleIngreso(ingresoView.getCode());
		}catch (Exception e) {
			log.error( "Error al obtener detalle ingreso", e );
			detalleingresoList = new ArrayList<Detalleingreso>();
		}
		
		log.info("Detalle.size: " + detalleingresoList.size());
	}

	/**
	 * @return the ingresoView
	 */
	public Ingreso getIngresoView() {
		return ingresoView;
	}

	/**
	 * @param ingresoView
	 *            the ingresoView to set
	 */
	public void setIngresoView(Ingreso ingresoView) {
		this.ingresoView = ingresoView;
	}

	public List<Detalleingreso> getDetalleingresoList() {
		return detalleingresoList;
	}

	public void setDetalleingresoList(List<Detalleingreso> detalleingresoList) {
		this.detalleingresoList = detalleingresoList;
	}

}
