/**
 * 
 */
package bean.inventario;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import persistence.inventario.AlmacenPersistence;

import entidad.Almacen;
import entidad.Sucursal;

import bean.bitacora.BitacoraAccion;
import bean.util.BaseBean;
import bean.util.Constantes;
import bean.util.IGeneral;
import bean.util.IPagination;
import bean.util.Jutil;
import bean.util.SysMessage;


/**
 * @author Marcus
 *
 */
@ManagedBean(name="almacenbean")
@ViewScoped
public class AlmacenBean extends BaseBean<Almacen> implements IGeneral, IPagination {

	@Inject
	private AlmacenPersistence almacenpersistence;
	
	@Inject
	private BitacoraAccion bitacoraaccion;
	
	@Inject
	private Jutil jutil;
	
	@Inject
	private SucursalBean sucursalbean;
		
	private String sucursalCode = ""; 
	
	private String filtroNombre;
	private String filtroDireccion;		
	private String filtroTipo;
	private String filtroSucursal;
	
	@Override
	@PostConstruct
	public void init() { 
		iniLogs(this.getClass());
		almacenpersistence.init();
		initFiltros();
		cargarLista();
		nuevo();
//		sucursalbean.init();		
	}	
	
	/**
	 * Inicializa en vacio los campos a utilizar como filtros.
	 */
	public void initFiltros(){
		filtroDireccion = filtroTipo = filtroNombre = filtroSucursal = Constantes.VACIO;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void cargarLista(){
		try{
			this.list = almacenpersistence.getLista();			
		}catch (Exception e) {
			this.list = new ArrayList();
			log.error( "[AlmacenBean.cargarLista]", e );
		}
	}
	
	public void nuevo(){
		entidad = new Almacen();
		entidad.setActivo(true);		
		nuevo = true;
		visibleNuevoEditar = false;
		selected = null;
		initFiltros();
		sucursalCode = "";
	}
	
	public void nuevaEntidad() {
		nuevo();
		visibleNuevoEditar = true;
	}
	
	public void guardar() {
		entidad.setSucursal( sucursalbean.obtenerSecursalCode( sucursalCode ) );
		String validacion = almacenpersistence.validarCampos(entidad, nuevo);
		if (validacion.trim().isEmpty()) {
			if (nuevo) {
				try {
					almacenpersistence.save( entidad );					
					log.info("[usuario: " + jutil.getUsuarioN() + ", almacen: " + entidad.getCode() + "|" + entidad.getNombre() + "] [Guardado correctamente]");					
					SysMessage.info("Guardado correctamente.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Se guardo correctamente el almacen: " + entidad.getCode() + "|" + entidad.getNombre() + ".");
					nuevo();
					cargarLista();
					visibleNuevoEditar = true;
				} catch (Exception e) {
					log.error("[usuario: " + jutil.getUsuarioN() + ", almacen: " + entidad.getCode() + "|" + entidad.getNombre() + "] [Fallo al guardar]", e);
					SysMessage.error("Fallo al guardar.");					
				}
			} else {
				try {
					almacenpersistence.update(entidad);					
					log.info("[usuario: " + jutil.getUsuarioN() + ", almacen: " + entidad.getCode() + "|" + entidad.getNombre() + "] [Guardado correctamente]");
					SysMessage.info("Actualizado correctamente.");
					bitacoraaccion.accion(entidad.getClass().getName(), "Se actualizado correctamente el almacen: " + entidad.getCode() + "|" + entidad.getNombre() + ".");
					nuevo();
					cargarLista();
					visibleNuevoEditar = false;
				} catch (Exception e) {
					log.error("[usuario: " + jutil.getUsuarioN() + ", almacen: " + entidad.getCode() + "|" + entidad.getNombre() + "] [Fallo al guardar]", e);
					SysMessage.error("Fallo al guardar.");					
				}
			}
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + ", almacen:" + "] [" + validacion + "]");
			SysMessage.warn(validacion);
		}
	}	

	public void editar() {
		if (selected != null) {
			entidad = selected;			
			nuevo = false;
			visibleNuevoEditar = true;
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
		log.info( "entidad:" + entidad + " nuevo: " + nuevo );
	}

	public void editar(Almacen obj) {
		log.info("intentando editar" + obj.getNombre() );
		selected = obj;
		editar();
	}

	public void onRowSelect(org.primefaces.event.SelectEvent event) {
		selected = (Almacen)event.getObject();
		log.info( "Almacen seleccionado:" + selected.getNombre() );
		editar();
    }
	
	public void eliminar() {
		if (selected != null) {			
			try {
				selected.setActivo( false );
				almacenpersistence.update(selected);				
				log.info("[usuario: " + jutil.getUsuarioN() + ", almacen: " + selected.getCode() + "|" + selected.getNombre() + "] [Eliminado correctamente]");
				SysMessage.info("Eliminado correctamente.");
				bitacoraaccion.accion(selected.getClass().getName(), "Se elimino correctamente el almacen: " + selected.getCode() + "|" + selected.getNombre() + ".");
				cargarLista();
				nuevo();
			} catch (Exception e) {
				log.error("[usuario: " + jutil.getUsuarioN() + ", almacen: " + selected.getCode() + "|" + selected.getNombre() + "] [Fallo al eliminar]", e);
				SysMessage.info("Fallo al eliminar.");
				bitacoraaccion.accion(selected.getClass().getName(), "Fallo al eliminar el almacen: " + selected.getCode() + "|" + selected.getNombre() + ".");
			}
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
	}
		
	public void eliminar(Almacen obj) {
		selected = obj;
		eliminar();
	}
	
	public void buscar(){
		Map<String, String> mapaValor = new HashMap<String, String>();		
		if ( !filtroNombre.trim().isEmpty() ){
			mapaValor.put( "nombre", filtroNombre );
		}
		if ( !filtroDireccion.trim().isEmpty() ){
			mapaValor.put( "direccion", filtroDireccion );
		}					
		if ( !filtroSucursal.trim().isEmpty() ){
			mapaValor.put( "sucursal", filtroSucursal );
		}
		list = almacenpersistence.getLista( mapaValor );		
	}
	
	@Override
	public void firstpage() {
		try{
			list = almacenpersistence.firstpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "firstpage", e );
		}
	}

	@Override
	public void previouspage() {
		try{
			list = almacenpersistence.previouspage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "previouspage", e );
		}
	}

	@Override
	public void nextpage() {
		try{
			list = almacenpersistence.nextpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "nextpage", e );
		}
	}

	@Override
	public void lastpage() {
		try{
			list = almacenpersistence.lastpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "lastpage", e );
		}
	}

	public int getCantidadRegistros() {
		return almacenpersistence.getCantidadRegistros();
	}
	
	public int getCantidadPaginas() {
		return almacenpersistence.getCantidadPaginas();
	}
	
	public int getPagina() {
		return almacenpersistence.getPagina();
	}

	/**
	 * @return the filtroNombre
	 */
	public String getFiltroNombre() {
		return filtroNombre;
	}

	/**
	 * @param filtroNombre the filtroNombre to set
	 */
	public void setFiltroNombre(String filtroNombre) {
		this.filtroNombre = filtroNombre;
	}

	/**
	 * @return the filtroDireccion
	 */
	public String getFiltroDireccion() {
		return filtroDireccion;
	}

	/**
	 * @param filtroDireccion the filtroDireccion to set
	 */
	public void setFiltroDireccion(String filtroDireccion) {
		this.filtroDireccion = filtroDireccion;
	}

	/**
	 * @return the sucursalCode
	 */
	public String getSucursalCode() {
		return sucursalCode;
	}

	/**
	 * @param sucursalCode the sucursalCode to set
	 */
	public void setSucursalCode(String sucursalCode) {
		this.sucursalCode = sucursalCode;
	}

	/**
	 * @return the filtroSucursal
	 */
	public String getFiltroSucursal() {
		return filtroSucursal;
	}

	/**
	 * @param filtroSucursal the filtroSucursal to set
	 */
	public void setFiltroSucursal(String filtroSucursal) {
		this.filtroSucursal = filtroSucursal;
	}

	/**
	 * @return the filtroTipo
	 */
	public String getFiltroTipo() {
		return filtroTipo;
	}

	/**
	 * @param filtroTipo the filtroTipo to set
	 */
	public void setFiltroTipo(String filtroTipo) {
		this.filtroTipo = filtroTipo;
	}
	

}
