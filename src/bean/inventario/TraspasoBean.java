/**
 * 
 */
package bean.inventario;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import persistence.inventario.AlmacenPersistence;
import persistence.inventario.EgresoPersistence;
import persistence.inventario.IngresoPersistence;
import persistence.inventario.TraspasoPersistence;
import entidad.Almacen;
import entidad.Detalleegreso;
import entidad.Detalleingreso;
import entidad.Detalletransito;
import entidad.Egreso;
import entidad.Ingreso;
import entidad.Producto;
import entidad.Traspaso;
import entidad.Usuario;

import bean.bitacora.BitacoraAccion;
import bean.util.BaseBean;
import bean.util.Constantes;
import bean.util.Parametros;
import bean.util.SysMessage;

/**
 * @author penama
 * 
 */
@ManagedBean(name = "traspasobean")
@ViewScoped
public class TraspasoBean extends BaseBean<Producto> implements Serializable {

	private String usuario = "";
	private String fecha = "";
	private Integer codeAlmacenOrigen;
	private Integer codeAlmacenDestino;	
	private String comentario;
	private String motivo;
	
	private Almacen almacenOrigen;
	
	private Traspaso entidad;

	private BigDecimal montoTotal;

	@Inject
	private TraspasoPersistence traspasopersistence;
	
	@Inject
	private EgresoPersistence egresopersistence;
	
	@Inject
	private IngresoPersistence ingresopersistence;

	@Inject
	private AlmacenPersistence almacenpersistence;

	@Inject
	private BitacoraAccion bitacoraaccion;

	@PostConstruct
	public void init() {
		iniLogs(this.getClass());
		// baseproductopersistence.init();
		traspasopersistence.init();	
		egresopersistence.init();
		ingresopersistence.init();
		// prendaList = baseproductopersistence.obtenerPrendasAsociadas();
		iniNuevo();
	}

	public void iniEditar() {

	}

	/**
	 * M�todo para cuando se tenga que realizar un nuevo transito.
	 */
	public void iniNuevo() {
		montoTotal = new BigDecimal(0);
		fecha = jutil.getFecha(Parametros.FECHA_DDMMYYYY);
		usuario = jutil.getUsuarioN();
		// productopersistence.setup( this.getClass() );
	}

	/**
	 * Calcula el montoTotal del transito
	 */
	public void onUpdateMontoTotal() {
		montoTotal = new BigDecimal(0);
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			Producto producto = (Producto) iter.next();
			// luego validar realizar el calculo del porcentaje y precio promo.
			montoTotal = montoTotal.add(producto.getBaseproducto().getPrecio());
		}
	}

	/**
	 * Adiciona un item a la lista, adem�s de actualizar los precios.
	 * 
	 * @param detalleEgresoItem
	 */
	public void onAddDetalleEgresoProductoItem(
			DetalleEgresoItem detalleEgresoItem) {
		detalleEgresoItem.addDetalleEgresoProductoItem();
		onUpdateMontoTotal();
	}

	/**
	 * Al realizar el cambio del almacen, la lista debe limpiarse, el egreso solo se hace por almacen.
	 */
	public void onCambioAlmacenOrigen(){
		this.list.clear();
		this.montoTotal = new BigDecimal(0);
	}
	
	/**
	 * Valida que el m�todo no se encuentre duplicado en la lista.
	 * 
	 * @param baseProducto
	 * @return
	 */
	private boolean ExisteDuplicado(Producto producto) throws Exception {
		if (producto == null) {
			throw new Exception("Producto nulo");
		}

		for (Iterator iter = list.iterator(); iter.hasNext();) {
			Producto productoItem = (Producto) iter.next();
			if (productoItem.getCode().equals(producto.getCode())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Quita un registro del listado...
	 */
	public void onQuitarItem(Producto producto) {		
		list.remove(producto);
		montoTotal = montoTotal.subtract( producto.getBaseproducto().getPrecio() );
	}

	/**
	 * Valida los datos del transito.
	 * 
	 * @return
	 */
	private String onValidarTraspaso() {
		if (codeAlmacenOrigen == null || codeAlmacenOrigen == 0) {
			return "Debe seleccionar el almacen origen!!";
		}
		if (codeAlmacenDestino == null || codeAlmacenDestino == 0) {
			return "Debe seleccionar el almacen destino!!";
		}	
		if ( codeAlmacenDestino == codeAlmacenOrigen ){
			return "Debe seleccionar almacenes distintos!!";
		}
		if (list.isEmpty()) {
			return "Debe seleccionar al menos un producto...";
		}
		return Constantes.VACIO;
	}

	/**
	 * Guardar el transito.
	 */
	public String save() {
		String redirect = "";
		String textValidar = onValidarTraspaso();
		if (!textValidar.equals(Constantes.VACIO)) {
			SysMessage.warn(textValidar);
			return redirect;
		}

		List<Detalletransito> detalleList = null;
		try {
			// preparar el egreso y su detalle.
			Egreso egreso = prepararEgreso();
			List<Detalleegreso> detalleEgresoList = prepararDetalleEgreso();
			// preparar el ingreso y su detalle.
			Ingreso ingreso = prepararIngreso();
			List<Detalleingreso> detalleIngresoList = prepararDetalleIngreso();
			// preparar el traspaso.
			prepararTraspaso();
			// persistir transito y su detalle.
			traspasopersistence.save( entidad, egreso, detalleEgresoList, ingreso, detalleIngresoList );
			bitacoraaccion.accion(entidad.getClass().getName(),
					"Se guardo correctamente el traspaso: " + entidad.getCode()
							+ ".");
			SysMessage.info("traspaso guardado correctamente.");
			log.info("redireccionado");
			jutil.addObjectParam(entidad);
			redirect = "TraspasoView.xhtml" + Parametros.REDIRECT;
		} catch (Exception e) {
			log.error("[Fallo al guardar]", e);
			SysMessage.error("Fallo al guardar.");
		}
		return redirect;
	}
	
	/**
	 * prepara el traspaso
	 * 
	 * @throws Exception
	 */
	private void prepararTraspaso() throws Exception {		
		entidad = new Traspaso();		
		entidad.setActivo(true);
		entidad.setComentario( comentario );		
		entidad.setFecha(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		entidad.setMotivo(motivo);		
		entidad.setUsuario((Usuario) jutil.getUsuarioO());		
	}

	/**
	 * prepara el egreso.
	 * 
	 * @throws Exception
	 */
	private Egreso prepararEgreso() throws Exception {		
		Egreso egreso = new Egreso();
		egreso.setActivo(true);
		egreso.setTotaldescuento(new BigDecimal(0));
		egreso.setAlmacen(almacenpersistence.findAlmacenCode(codeAlmacenOrigen));
		egreso.setFecha(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		egreso.setTipoegreso( "TRASPASO" );
		egreso.setTotal(montoTotal);
		egreso.setUsuario((Usuario) jutil.getUsuarioO());
		return egreso;
	}
	
	/**
	 * Prepar� el detalle del egreso.
	 * 
	 * @return
	 * @throws Exception
	 */
	private List<Detalleegreso> prepararDetalleEgreso() throws Exception {
		List<Detalleegreso> detalleEgresoList = new ArrayList<Detalleegreso>();
		Detalleegreso detalleEgreso = null;
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			Producto producto = (Producto) iter.next();
			detalleEgreso = new Detalleegreso();
			detalleEgreso.setActivo(true);			
			detalleEgreso.setProducto(producto);
			detalleEgreso.setCantidad(1);			
			// validar luego el calculo del costo.
			detalleEgreso.setPrecio(producto.getBaseproducto().getPrecio());
			detalleEgreso.setDescuento( new BigDecimal(0) );
			detalleEgresoList.add(detalleEgreso);
		}
		return detalleEgresoList;
	}


	/**
	 * Prepara el ingreso 
	 * @return
	 * @throws Exception
	 */
	private Ingreso prepararIngreso() throws Exception {
		Ingreso ingreso = new Ingreso();
		ingreso.setActivo( true );
		ingreso.setAlmacen( almacenpersistence.findAlmacenCode( codeAlmacenDestino ) );
		ingreso.setFecha( new Timestamp(Calendar.getInstance().getTimeInMillis()) );
		ingreso.setTipoingreso( "TRASPASO" );
		ingreso.setTotal( montoTotal );
		ingreso.setUsuario( (Usuario)jutil.getUsuarioO() );
		return ingreso;
	}
	
	/**
	 * Prepar� el detalle del ingreso.
	 * 
	 * @return
	 * @throws Exception
	 */
	private List<Detalleingreso> prepararDetalleIngreso() throws Exception {
		List<Detalleingreso> detalleIngresoList = new ArrayList<Detalleingreso>();
		Detalleingreso detalleIngreso = null;
		Almacen almacen = almacenpersistence.findAlmacenCode( codeAlmacenDestino );
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			Producto producto = (Producto) iter.next();
			producto.setAlmacen( almacen );
			detalleIngreso = new Detalleingreso();
			detalleIngreso.setActivo(true);			
			detalleIngreso.setProducto(producto);
			detalleIngreso.setCantidad(1);
			// validar luego el calculo del costo.
			detalleIngreso.setPrecio(producto.getBaseproducto().getPrecio());
			detalleIngresoList.add(detalleIngreso);
		}
		return detalleIngresoList;
	}

	/**
	 * Adiciona un producto a la lista.
	 * 
	 * @param producto
	 */
	public void adicionarProducto(Producto producto) {
		try {
			if (ExisteDuplicado(producto)) {
				SysMessage.warn("Producto " + producto.getCode() + " duplicado!!!");
				return;
			}
		} catch (Exception e) {
			SysMessage.error("Debe seleccionar un producto");
			log.error(e);
		}
		montoTotal = montoTotal.add( producto.getBaseproducto().getPrecio() );
		list.add(producto);
	}

	/**
	 * Adiciona un listado de productos a la lista.
	 * 
	 * @param productoList
	 */
	public void adicionarProductos(List<Producto> productoList) {
		if (productoList.isEmpty()) {
			SysMessage.warn("Lista de productos esta vacia.");
			return;
		}
		for (Iterator iter = productoList.iterator(); iter.hasNext();) {
			Producto producto = (Producto) iter.next();
			try {
				if (ExisteDuplicado(producto)) {
					SysMessage.info("Producto " + producto.getCode() + " duplicado!!!");
					continue;
				}
			} catch (Exception e) {
				SysMessage.error(e.getMessage());
			}
			montoTotal = montoTotal.add( producto.getBaseproducto().getPrecio() );
			list.add(producto);
		}
	}

	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario
	 *            the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * @param fecha
	 *            the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the montoTotal
	 */
	public BigDecimal getMontoTotal() {
		return montoTotal;
	}

	/**
	 * @param montoTotal
	 *            the montoTotal to set
	 */
	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}

	/**
	 * @return the codeAlmacenOrigen
	 */
	public Integer getCodeAlmacenOrigen() {
		return codeAlmacenOrigen;
	}

	/**
	 * @param codeAlmacenOrigen the codeAlmacenOrigen to set
	 */
	public void setCodeAlmacenOrigen(Integer codeAlmacenOrigen) {
		this.codeAlmacenOrigen = codeAlmacenOrigen;
		this.almacenOrigen = almacenpersistence.findAlmacenCode( this.codeAlmacenOrigen );
	}

	/**
	 * @return the codeAlmacenDestino
	 */
	public Integer getCodeAlmacenDestino() {
		return codeAlmacenDestino;
	}

	/**
	 * @param codeAlmacenDestino the codeAlmacenDestino to set
	 */
	public void setCodeAlmacenDestino(Integer codeAlmacenDestino) {
		this.codeAlmacenDestino = codeAlmacenDestino;
	}

	/**
	 * @return the comentario
	 */
	public String getComentario() {
		return comentario;
	}

	/**
	 * @param comentario the comentario to set
	 */
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	/**
	 * @return the motivo
	 */
	public String getMotivo() {
		return motivo;
	}

	/**
	 * @param motivo the motivo to set
	 */
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	/**
	 * @return the almacenOrigen
	 */
	public Almacen getAlmacenOrigen() {
		return almacenOrigen;
	}

	/**
	 * @param almacenOrigen the almacenOrigen to set
	 */
	public void setAlmacenOrigen(Almacen almacenOrigen) {
		this.almacenOrigen = almacenOrigen;
	}

}
