/**
 * 
 */
package bean.inventario;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import persistence.inventario.TraspasoListPersistence;

import entidad.Traspaso;

import bean.bitacora.BitacoraAccion;
import bean.util.BaseBean;
import bean.util.Constantes;
import bean.util.IGeneral;
import bean.util.IPagination;
import bean.util.Jutil;
import bean.util.SysMessage;


/**
 * @author Marcus
 *
 */
@ManagedBean(name="traspasolistbean" )
@ViewScoped
public class TraspasoListBean extends BaseBean<Traspaso> implements IGeneral, IPagination {

	@Inject
	private TraspasoListPersistence traspasolistpersistence;
		
	@Inject
	private Jutil jutil;	
	
	private String filtroCode;
	private String filtroAlmacenIngreso;
	private String filtroAlmacenEgreso;
	private String filtroMotivo;
	private String filtroComentario;
	private String filtroUsuario;
	
	
	@Override
	@PostConstruct
	public void init() { 
		iniLogs(this.getClass());
		traspasolistpersistence.init();
		initFiltros();
		cargarLista();		
	}

	/**
	 * Inicializa en vacio los campos a utilizar como filtros.
	 */
	public void initFiltros(){
		filtroCode = filtroAlmacenEgreso = filtroAlmacenIngreso = filtroMotivo = filtroUsuario = filtroComentario = Constantes.VACIO;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void cargarLista(){
		try{
			this.list = traspasolistpersistence.getLista();			
		}catch (Exception e) {
			this.list = new ArrayList();
			log.error( "[TraspasoBean.cargarLista]", e );
		}
	}		
		
	public void editar() {
		if (selected != null) {
			entidad = selected;			
			nuevo = false;
			visibleNuevoEditar = true;
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
		log.info( "entidad:" + entidad + " nuevo: " + nuevo );
	}

	public void editar(Traspaso obj) {
		log.info("intentando editar" + obj.getCode() );
		selected = obj;
		editar();
	}

	public void onRowSelect(org.primefaces.event.SelectEvent event) {
		selected = (Traspaso)event.getObject();
		log.info( "Traspaso seleccionado:" + selected.getCode() );
		editar();
    }	
	
	public void buscar(){
		Map<String, String> mapaValor = new HashMap<String, String>();
		if ( !filtroCode.trim().isEmpty() ){
			mapaValor.put( "code", filtroCode );
		}
		if ( !filtroAlmacenEgreso.trim().isEmpty() ){
			mapaValor.put( "almacenEgreso", filtroAlmacenEgreso );
		}
		if ( !filtroAlmacenIngreso.trim().isEmpty() ){
			mapaValor.put( "almacenIngreso", filtroAlmacenIngreso );
		}
		if ( !filtroMotivo.trim().isEmpty() ){
			mapaValor.put( "motivo", filtroMotivo );
		}
		if ( !filtroComentario.trim().isEmpty() ){
			mapaValor.put( "comentario", filtroComentario );
		}
		if ( !filtroUsuario.trim().isEmpty() ){
			mapaValor.put( "usuario", filtroUsuario );
		}		
		list = traspasolistpersistence.getLista( mapaValor );		
	}
	
	/**
	 * Exporta el objeto Traspaso seleccionado.
	 */
	public void onVerTraspaso( Traspaso traspaso ){
		log.info( "Ejectando: traspasoView = " + traspaso + " [" + traspaso.getCode() + "]" );		
//		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put( "traspasoView", traspaso );
		jutil.addObjectParam( traspaso );
		log.info( "objeto Ejectado" );
	}
	
	@Override
	public void firstpage() {
		try{
			list = traspasolistpersistence.firstpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "firstpage", e );
		}
	}

	@Override
	public void previouspage() {
		try{
			list = traspasolistpersistence.previouspage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "previouspage", e );
		}
	}

	@Override
	public void nextpage() {
		try{
			list = traspasolistpersistence.nextpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "nextpage", e );
		}
	}

	@Override
	public void lastpage() {
		try{
			list = traspasolistpersistence.lastpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "lastpage", e );
		}
	}

	public int getCantidadRegistros() {
		return traspasolistpersistence.getCantidadRegistros();
	}
	
	public int getCantidadPaginas() {
		return traspasolistpersistence.getCantidadPaginas();
	}
	
	public int getPagina() {
		return traspasolistpersistence.getPagina();
	}

	
	/**
	 * @return the filtroCode
	 */
	public String getFiltroCode() {
		return filtroCode;
	}

	/**
	 * @param filtroCode the filtroCode to set
	 */
	public void setFiltroCode(String filtroCode) {
		this.filtroCode = filtroCode;
	}	

	public String getFiltroUsuario() {
		return filtroUsuario;
	}

	public void setFiltroUsuario(String filtroUsuario) {
		this.filtroUsuario = filtroUsuario;
	}

	/**
	 * @return the filtroAlmacenIngreso
	 */
	public String getFiltroAlmacenIngreso() {
		return filtroAlmacenIngreso;
	}

	/**
	 * @param filtroAlmacenIngreso the filtroAlmacenIngreso to set
	 */
	public void setFiltroAlmacenIngreso(String filtroAlmacenIngreso) {
		this.filtroAlmacenIngreso = filtroAlmacenIngreso;
	}

	/**
	 * @return the filtroAlmacenEgreso
	 */
	public String getFiltroAlmacenEgreso() {
		return filtroAlmacenEgreso;
	}

	/**
	 * @param filtroAlmacenEgreso the filtroAlmacenEgreso to set
	 */
	public void setFiltroAlmacenEgreso(String filtroAlmacenEgreso) {
		this.filtroAlmacenEgreso = filtroAlmacenEgreso;
	}

	/**
	 * @return the filtroMotivo
	 */
	public String getFiltroMotivo() {
		return filtroMotivo;
	}

	/**
	 * @param filtroMotivo the filtroMotivo to set
	 */
	public void setFiltroMotivo(String filtroMotivo) {
		this.filtroMotivo = filtroMotivo;
	}

	/**
	 * @return the filtroComentario
	 */
	public String getFiltroComentario() {
		return filtroComentario;
	}

	/**
	 * @param filtroComentario the filtroComentario to set
	 */
	public void setFiltroComentario(String filtroComentario) {
		this.filtroComentario = filtroComentario;
	}

	
	
}
