/**
 * 
 */
package bean.inventario;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import persistence.inventario.EgresoViewPersistence;
import persistence.inventario.IngresoViewPersistence;
import entidad.Detalleegreso;
import entidad.Detalleingreso;
import entidad.Traspaso;

import bean.util.Base;
import bean.util.Jutil;

/**
 * @author Marcus
 * 
 */
@ManagedBean(name = "traspasoviewbean")
@ViewScoped
public class TraspasoViewBean extends Base {

	@Inject
	private Jutil jutil;

	@Inject
	private IngresoViewPersistence ingresoviewpersistence;
	
	@Inject
	private EgresoViewPersistence egresoviewpersistence;

	private Traspaso traspasoView = null;

	private List<Detalleingreso> detalleIngresoList;
	
	private List<Detalleegreso> detalleEgresoList;
	
	@PostConstruct
	public void init() {
		setup(this.getClass());
		onVerTraspaso();
		egresoviewpersistence.init();
		ingresoviewpersistence.init();
	}

	/**
	 * Obtiene el traspaso a ver.
	 */
	public void onVerTraspaso() {
		log.info("Obteniendo traspaso a ver...");
		Object obj = jutil.getObjectParam();
		log.info("obteniendo objeto injectado: " + obj);
		if (obj == null) {
			try {
				jutil.redirect("TraspasoList.xhtml");
			} catch (Exception e) {
				log.error("Redirect fallido", e);
			}
		}
		traspasoView = (Traspaso) obj;
		log.info("Traspaso a ver: " + traspasoView.getCode() + " - Obteniendo detalle");
		try{
			detalleEgresoList = egresoviewpersistence.getDetalleEgreso( traspasoView.getEgreso().getCode() );
			detalleIngresoList = ingresoviewpersistence.getDetalleIngreso( traspasoView.getIngreso().getCode() );			
		}catch (Exception e) {
			log.error( "Error al obtener detalle traspaso", e );
			detalleEgresoList = new ArrayList<Detalleegreso>();
			detalleIngresoList = new ArrayList<Detalleingreso>();
		}
		
		log.info("DetalleEgreso.size: " + detalleEgresoList.size());
	}

	/**
	 * @return the traspasoView
	 */
	public Traspaso getTraspasoView() {
		return traspasoView;
	}

	/**
	 * @param traspasoView
	 *            the traspasoView to set
	 */
	public void setTraspasoView(Traspaso traspasoView) {
		this.traspasoView = traspasoView;
	}

	/**
	 * @return the detalleIngresoList
	 */
	public List<Detalleingreso> getDetalleIngresoList() {
		return detalleIngresoList;
	}

	/**
	 * @param detalleIngresoList the detalleIngresoList to set
	 */
	public void setDetalleIngresoList(List<Detalleingreso> detalleIngresoList) {
		this.detalleIngresoList = detalleIngresoList;
	}

	/**
	 * @return the detalleEgresoList
	 */
	public List<Detalleegreso> getDetalleEgresoList() {
		return detalleEgresoList;
	}

	/**
	 * @param detalleEgresoList the detalleEgresoList to set
	 */
	public void setDetalleEgresoList(List<Detalleegreso> detalleEgresoList) {
		this.detalleEgresoList = detalleEgresoList;
	}

}
