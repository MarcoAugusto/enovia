/**
 * 
 */
package bean.inventario;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import persistence.inventario.EgresoListPersistence;
import persistence.inventario.EgresoViewPersistence;

import entidad.Detalleegreso;
import entidad.Egreso;

import bean.bitacora.BitacoraAccion;
import bean.util.Base;
import bean.util.Constantes;
import bean.util.IGeneral;
import bean.util.IPagination;
import bean.util.Jutil;
import bean.util.SysMessage;

/**
 * @author Marcus
 * 
 */
@ManagedBean(name = "egresoviewbean")
@ViewScoped
public class EgresoViewBean extends Base {

	@Inject
	private BitacoraAccion bitacoraaccion;

	@Inject
	private Jutil jutil;

	@Inject
	private EgresoViewPersistence egresoviewpersistence;

	private Egreso egresoView = null;

	private List<Detalleegreso> detalleegresoList = null;

	@PostConstruct
	public void init() {
		setup(this.getClass());
		onVerEgreso();
	}

	/**
	 * Obtiene el egreso a ver.
	 */
	public void onVerEgreso() {
		log.info("Obteniendo egreso a ver...");
		// Object obj =
		// FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(
		// "egresoView" );
		Object obj = jutil.getObjectParam();
		log.info("obteniendo objeto injectado: " + obj);
		if (obj == null) {
			try {
				jutil.redirect("EgresoList.xhtml");
			} catch (Exception e) {
				log.error("Redirect fallido", e);
			}
		}
		egresoView = (Egreso) obj;
		log.info("Egreso a ver: " + egresoView.getCode() + " - Obteniendo detalle");
		try{
			detalleegresoList = egresoviewpersistence.getDetalleEgreso(egresoView.getCode());
		}catch (Exception e) {
			log.error( "Error al obtener detalle egreso", e );
			detalleegresoList = new ArrayList<Detalleegreso>();
		}
		
		log.info("Detalle.size: " + detalleegresoList.size());
	}

	/**
	 * @return the egresoView
	 */
	public Egreso getEgresoView() {
		return egresoView;
	}

	/**
	 * @param egresoView
	 *            the egresoView to set
	 */
	public void setEgresoView(Egreso egresoView) {
		this.egresoView = egresoView;
	}

	public List<Detalleegreso> getDetalleegresoList() {
		return detalleegresoList;
	}

	public void setDetalleegresoList(List<Detalleegreso> detalleegresoList) {
		this.detalleegresoList = detalleegresoList;
	}

}
