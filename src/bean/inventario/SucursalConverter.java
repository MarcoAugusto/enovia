/**
 * 
 */
package bean.inventario;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import entidad.Sucursal;

import persistence.inventario.SucursalPersistence;

/**
 * @author penama
 *
 */
@FacesConverter( "sucursalconverter" )
public class SucursalConverter implements Converter {
	
	private SucursalPersistence sucursalpersistence = new SucursalPersistence();
	
	public SucursalConverter(){
		sucursalpersistence.init();
	}
	
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		if ( arg2 == null || arg2.trim().isEmpty() ){
			return null;
		}
		Map<String, String> mapa = new HashMap<String, String>();
		mapa.put( "code" , arg2 );
		List<Sucursal> list = sucursalpersistence.getLista(mapa);		
		return list.get(0);
	}
	
	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		if ( arg2 == null ){
			return null;
		}
		return ((Sucursal)arg2).getCode();
	}

}
