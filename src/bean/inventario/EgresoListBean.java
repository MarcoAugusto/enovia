/**
 * 
 */
package bean.inventario;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import persistence.inventario.EgresoListPersistence;

import entidad.Egreso;

import bean.bitacora.BitacoraAccion;
import bean.util.BaseBean;
import bean.util.Constantes;
import bean.util.IGeneral;
import bean.util.IPagination;
import bean.util.Jutil;
import bean.util.SysMessage;


/**
 * @author Marcus
 *
 */
@ManagedBean(name="egresolistbean" )
@ViewScoped
public class EgresoListBean extends BaseBean<Egreso> implements IGeneral, IPagination {

	@Inject
	private EgresoListPersistence egresolistpersistence;
	
	@Inject
	private BitacoraAccion bitacoraaccion;
	
	@Inject
	private AlmacenBean almacenbean;
	
	@Inject
	private Jutil jutil;	
	
	private String filtroCode;
	private String filtroTipoegreso;
	private String filtroFechaIni;
	private String filtroFechaFin;
	private String filtroAlmacen;
	private String filtroUsuario;
	
	
	@Override
	@PostConstruct
	public void init() { 
		iniLogs(this.getClass());
		egresolistpersistence.init();
		initFiltros();
		cargarLista();		
		almacenbean.init();
	}

	/**
	 * Inicializa en vacio los campos a utilizar como filtros.
	 */
	public void initFiltros(){
		filtroCode = filtroTipoegreso = filtroFechaIni = filtroFechaFin = filtroUsuario = filtroAlmacen = Constantes.VACIO;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void cargarLista(){
		try{
			this.list = egresolistpersistence.getLista();			
		}catch (Exception e) {
			this.list = new ArrayList();
			log.error( "[EgresoBean.cargarLista]", e );
		}
	}		
		
	public void editar() {
		if (selected != null) {
			entidad = selected;			
			nuevo = false;
			visibleNuevoEditar = true;
		} else {
			log.warn("[usuario: " + jutil.getUsuarioN() + "] [No se encontro ningun registro seleccionado]");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
		log.info( "entidad:" + entidad + " nuevo: " + nuevo );
	}

	public void editar(Egreso obj) {
		log.info("intentando editar" + obj.getCode() );
		selected = obj;
		editar();
	}

	public void onRowSelect(org.primefaces.event.SelectEvent event) {
		selected = (Egreso)event.getObject();
		log.info( "Egreso seleccionado:" + selected.getCode() );
		editar();
    }	
	
	public void buscar(){
		Map<String, String> mapaValor = new HashMap<String, String>();
		if ( !filtroCode.trim().isEmpty() ){
			mapaValor.put( "code", filtroCode );
		}
		if ( !filtroTipoegreso.trim().isEmpty() ){
			mapaValor.put( "tipoegreso", filtroTipoegreso );
		}
		if ( !filtroFechaIni.trim().isEmpty() ){
			mapaValor.put( "fechaini", filtroFechaIni );
		}
		if ( !filtroFechaFin.trim().isEmpty() ){
			mapaValor.put( "fechafin", filtroFechaFin );
		}
		if ( !filtroAlmacen.trim().isEmpty() ){
			mapaValor.put( "almacen", filtroAlmacen );
		}
		if ( !filtroUsuario.trim().isEmpty() ){
			mapaValor.put( "usuario", filtroUsuario );
		}		
		list = egresolistpersistence.getLista( mapaValor );		
	}
	
	/**
	 * Exporta el objeto Egreso seleccionado.
	 */
	public void onVerEgreso( Egreso egreso ){
		log.info( "Ejectando: egresoView = " + egreso + " [" + egreso.getCode() + "]" );		
//		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put( "egresoView", egreso );
		jutil.addObjectParam( egreso );
		log.info( "objeto Ejectado" );
	}
	
	@Override
	public void firstpage() {
		try{
			list = egresolistpersistence.firstpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "firstpage", e );
		}
	}

	@Override
	public void previouspage() {
		try{
			list = egresolistpersistence.previouspage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "previouspage", e );
		}
	}

	@Override
	public void nextpage() {
		try{
			list = egresolistpersistence.nextpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "nextpage", e );
		}
	}

	@Override
	public void lastpage() {
		try{
			list = egresolistpersistence.lastpage();
		}catch (Exception e) {
			log.error( this.getClass().getName() + "lastpage", e );
		}
	}

	public int getCantidadRegistros() {
		return egresolistpersistence.getCantidadRegistros();
	}
	
	public int getCantidadPaginas() {
		return egresolistpersistence.getCantidadPaginas();
	}
	
	public int getPagina() {
		return egresolistpersistence.getPagina();
	}

	
	/**
	 * @return the filtroCode
	 */
	public String getFiltroCode() {
		return filtroCode;
	}

	/**
	 * @param filtroCode the filtroCode to set
	 */
	public void setFiltroCode(String filtroCode) {
		this.filtroCode = filtroCode;
	}	

	public String getFiltroUsuario() {
		return filtroUsuario;
	}

	public void setFiltroUsuario(String filtroUsuario) {
		this.filtroUsuario = filtroUsuario;
	}

	/**
	 * @return the filtroTipoegreso
	 */
	public String getFiltroTipoegreso() {
		return filtroTipoegreso;
	}

	/**
	 * @param filtroTipoegreso the filtroTipoegreso to set
	 */
	public void setFiltroTipoegreso(String filtroTipoegreso) {
		this.filtroTipoegreso = filtroTipoegreso;
	}

	/**
	 * @return the filtroFechaIni
	 */
	public String getFiltroFechaIni() {
		return filtroFechaIni;
	}

	/**
	 * @param filtroFechaIni the filtroFechaIni to set
	 */
	public void setFiltroFechaIni(String filtroFechaIni) {
		this.filtroFechaIni = filtroFechaIni;
	}

	/**
	 * @return the filtroFechaFin
	 */
	public String getFiltroFechaFin() {
		return filtroFechaFin;
	}

	/**
	 * @param filtroFechaFin the filtroFechaFin to set
	 */
	public void setFiltroFechaFin(String filtroFechaFin) {
		this.filtroFechaFin = filtroFechaFin;
	}

	/**
	 * @return the filtroAlmacen
	 */
	public String getFiltroAlmacen() {
		return filtroAlmacen;
	}

	/**
	 * @param filtroAlmacen the filtroAlmacen to set
	 */
	public void setFiltroAlmacen(String filtroAlmacen) {
		this.filtroAlmacen = filtroAlmacen;
	}

	
	
}
